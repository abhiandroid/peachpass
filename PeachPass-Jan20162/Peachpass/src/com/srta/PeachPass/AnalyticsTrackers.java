package com.srta.PeachPass;

/**
 * Created by Azularc on 13-10-2015.
 */

import android.app.Application;
import android.content.Context;


import java.util.HashMap;
import java.util.Map;



public class AnalyticsTrackers extends Application {
  public enum Target {
    APP,
    // Add more trackers here if you need, and update the code in #get(Target) below
  }

  private static AnalyticsTrackers sInstance;

  public static synchronized void initialize(Context context) {
    if (sInstance != null) {
      throw new IllegalStateException("Extra call to initialize analytics trackers");
    }

    sInstance = new AnalyticsTrackers(context);
  }

  public static synchronized AnalyticsTrackers getInstance() {
    if (sInstance == null) {
      throw new IllegalStateException("Call initialize() before getInstance()");
    }

    return sInstance;
  }

  private final Map<Target, com.google.android.gms.analytics.Tracker> mTrackers = new HashMap<Target, com.google.android.gms.analytics.Tracker>();
  private final Context mContext;

  /**
   * Don't instantiate directly - use {@link #getInstance()} instead.
   */
  private AnalyticsTrackers(Context context) {
    mContext = context.getApplicationContext();
  }

  public synchronized com.google.android.gms.analytics.Tracker get(Target target) {
    if (!mTrackers.containsKey(target)) {
      com.google.android.gms.analytics.Tracker tracker;
      switch (target) {
        case APP:
          tracker = com.google.android.gms.analytics.GoogleAnalytics.getInstance(mContext).newTracker(R.xml.app_tracker);
          break;
        default:
          throw new IllegalArgumentException("Unhandled analytics target " + target);
      }
      mTrackers.put(target, tracker);
    }

    return mTrackers.get(target);
  }
}
