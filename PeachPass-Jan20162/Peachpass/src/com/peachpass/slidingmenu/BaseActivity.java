package com.peachpass.slidingmenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.srta.PeachPass.R;

/** A helper class to bind the FragmentChangeActivity and all the Fragments */
public class BaseActivity extends SlidingFragmentActivity {

	protected ListFragment mFrag;
	public ImageView homeIcon;

	TextView txt_bubble;
	View view_bubble;
	View mCustomView;
	boolean isSecMenuOpened = false;

	public BaseActivity(int titleRes) {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// set the Behind View
		setBehindContentView(R.layout.menu_frame);

		// if (savedInstanceState == null) {
		// FragmentTransaction t = this.getSupportFragmentManager()
		// .beginTransaction();
		// mFrag = new SampleListFragment();
		// t.replace(R.id.menu_frame, mFrag);
		// t.commit();
		// } else {
		// mFrag = (ListFragment) this.getSupportFragmentManager()
		// .findFragmentById(R.id.menu_frame);
		// }

		/** Initialize the Sliding Menu */
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		// sm.setShadowDrawable(null);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		// sm.setFadeDegree(0.35f);
		sm.setFadeEnabled(false);
		sm.setBehindScrollScale(0);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

		sm.setOnOpenListener(new OnOpenListener() {

			@Override
			public void onOpen() {
				// TODO Auto-generated method stub
				Log.e("newlog", "setOnOpenListener");
			}
		});
		sm.setOnOpenedListener(new OnOpenedListener() {

			@Override
			public void onOpened() {
				// TODO Auto-generated method stub
				Log.e("newlog", "setOnOpenedListener");

			}
		});
		sm.setOnClosedListener(new OnClosedListener() {

			@Override
			public void onClosed() {
				// TODO Auto-generated method stub
				Log.e("peachpass", "on close");
				if (isSecMenuOpened) {
					Intent intent = new Intent("refreshNotificationScreen");
					intent.putExtra("message", "refreshlist");
					LocalBroadcastManager.getInstance(BaseActivity.this)
							.sendBroadcast(intent);
					isSecMenuOpened = false;
				}
			}
		});
		sm.setSecondaryOnOpenListner(new OnOpenListener() {

			@Override
			public void onOpen() {
				// TODO Auto-generated method stub
				isSecMenuOpened = true;
			}
		});

		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#F69B54")));

		/**
		 * ActionBar to set the title and the navigation icons for both the
		 * sliding menus
		 */
		ActionBar mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		mCustomView = mInflater.inflate(R.layout.action_bar, null);
		TextView mTitleTextView = (TextView) mCustomView
				.findViewById(R.id.title_text);
		mTitleTextView.setText("MY PEACH PASS");

		ImageView notificationIcon = (ImageView) mCustomView
				.findViewById(R.id.imageView2);
		notificationIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				showSecondaryMenu();

			}
		});

		homeIcon = (ImageView) mCustomView.findViewById(R.id.imageView1);
		homeIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				showMenu();
			}
		});

		txt_bubble = (TextView) mCustomView.findViewById(R.id.txt_bubble);
		view_bubble = mCustomView.findViewById(R.id.view_bubble);

		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// toggle();
			return true;
			// case R.id.github:
			// // Util.goToGitHub(this);
			// return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.sldingmenu, menu);
		return true;
	}

}
