package com.peachpass.utils;

import java.util.ArrayList;

import com.peachpass.data.FragmentTitle;
import com.peachpass.data.MyPeachPassData;

public class Cache {

	ArrayList<MyPeachPassData> myPeachPassdataList = null;
	ArrayList<FragmentTitle> fragmentTitle = null;

	private static Cache instance = null;
	public static boolean fromBack = false;

	protected Cache() {
		// Exists only to defeat instantiation.
		myPeachPassdataList = new ArrayList<MyPeachPassData>();
		fragmentTitle = new ArrayList<FragmentTitle>();
	}

	public ArrayList<FragmentTitle> getFragmentTitle() {
		return fragmentTitle;
	}

	public void setFragmentTitle(ArrayList<FragmentTitle> fragmentTitle) {
		this.fragmentTitle = fragmentTitle;
	}

	public static Cache getInstance() {
		
		if (instance == null) {
			instance = new Cache();
		}
		return instance;
	}

	public ArrayList<MyPeachPassData> getMyPeachPassdataList() {
		return myPeachPassdataList;
	}

	public void setMyPeachPassdataList(
			ArrayList<MyPeachPassData> myPeachPassdataList) {
		this.myPeachPassdataList = myPeachPassdataList;
	}

}
