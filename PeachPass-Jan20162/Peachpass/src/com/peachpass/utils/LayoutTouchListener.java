package com.peachpass.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.peachpass.activity.LoginScreenActivity;
import com.peachpass.activity.WalkthroughActivity;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

public class LayoutTouchListener implements OnTouchListener {

	static final String logTag = "LayoutTouchListener";
	private Activity activity;
	static final int MIN_DISTANCE = 100;// TODO change this runtime based on
										// screen resolution. for 1920x1080 is
										// to small the 100 distance
	private float downX, downY, upX, upY;

	private int from = 0;

	public LayoutTouchListener(Activity mainActivity, int from) {
		activity = mainActivity;
		this.from = from;
	}

	public void onRightToLeftSwipe() {
		// activity.doSomething();
		switch (from) {
		case 0:
			activity.startActivity(new Intent(activity, LoginScreenActivity.class));
			activity.overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			activity.finish();
			break;
		case 1:
			String session = SharedPreferenceHelper.getPreference(activity,
					SharedPreferenceHelper.SESSION);
			if (session.equals("")) {
				activity.startActivity(new Intent(activity,
						WalkthroughActivity.class));
				activity.overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
				//activity.finish();
				SharedPreferenceHelper.savePreferences(
						SharedPreferenceHelper.SESSION, "1", activity);
			} else {

				activity.startActivity(new Intent(activity, LoginScreenActivity.class));
				activity.overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
				activity.finish();
				
			}

			break;
			
		case 3:
			activity.startActivity(new Intent(activity,
					WalkthroughActivity.class));
			activity.overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
		//	activity.finish();
			break;

		default:
			break;
		}

	}

	public void onLeftToRightSwipe() {

		switch (from) {
		case 2:
			if (WalkthroughActivity.CURRENT_TAB == 0) {
				activity.startActivity(new Intent(activity, LoginScreenActivity.class));
				activity.overridePendingTransition(R.anim.slide_in_left,
						R.anim.animation_leave);
				activity.finish();
			}

			break;

		default:
			break;
		}
	}

	public void onTopToBottomSwipe() {
	}

	public void onBottomToTopSwipe() {
	}

	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: {
			downX = event.getX();
			downY = event.getY();
			return true;
		}
		case MotionEvent.ACTION_UP: {
			upX = event.getX();
			upY = event.getY();

			float deltaX = downX - upX;
			float deltaY = downY - upY;

			// swipe horizontal?
			if (Math.abs(deltaX) > MIN_DISTANCE) {
				// left or right
				if (deltaX < 0) {
					this.onLeftToRightSwipe();
					return true;
				}
				if (deltaX > 0) {
					this.onRightToLeftSwipe();
					return true;
				}
			} else {
				// Log.i(logTag, "Swipe was only " + Math.abs(deltaX)
				// + " long horizontally, need at least " + MIN_DISTANCE);
				// return false; // We don't consume the event
			}

			// swipe vertical?
			if (Math.abs(deltaY) > MIN_DISTANCE) {
				// top or down
				if (deltaY < 0) {
					this.onTopToBottomSwipe();
					return true;
				}
				if (deltaY > 0) {
					this.onBottomToTopSwipe();
					return true;
				}
			} else {
				// Log.i(logTag, "Swipe was only " + Math.abs(deltaX)
				// + " long vertically, need at least " + MIN_DISTANCE);
				// return false; // We don't consume the event
			}

			return false; // no swipe horizontally and no swipe vertically
		}// case MotionEvent.ACTION_UP:
		}
		return false;
	}

}
