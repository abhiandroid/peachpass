package com.peachpass.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ScreenUtils {

	/**
	 * fullScreen method hides the status and notification bar, making the
	 * application appear in full screen mode
	 */

	public static void fullScreen(Activity activity) {

		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		activity.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	/**
	 * snippet used all over the app(where ever necessary) to use external fonts
	 * - fontawesome
	 */
	public static Typeface returnTypeFace(Context ctx) {
		return Typeface.createFromAsset(ctx.getAssets(),
				"fontawesome-webfont.ttf");
	}

	public static Typeface returnTypeFaceHelv(Context ctx) {
		return Typeface.createFromAsset(ctx.getAssets(),
				"fonts/helveticaneue-webfont.ttf");
	}

	/**
	 * snippet to disable auto pop up of soft keypad in screens where there is
	 * an input field
	 */
	public static void disableAutoPopUp(EditText editTextField, Context ctx) {
		InputMethodManager imm = (InputMethodManager) ctx
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editTextField.getWindowToken(), 0);
	}

	/** programatically set padding values to views */
	public static int[] getPaddingVal(EditText value) {
		int val[] = new int[4];
		val[0] = value.getPaddingLeft();
		val[1] = value.getPaddingTop();
		val[2] = value.getPaddingRight();
		val[3] = value.getPaddingBottom();
		return val;
	}

	/** snippet to get the background color of the view */
	public static int getBgColor(View view) {
		int color = Color.TRANSPARENT;
		Drawable background = view.getBackground();
		if (background instanceof ColorDrawable)
			color = ((ColorDrawable) background).getColor();

		// Log.e("newlog", color+"");
		return color;
	}

	/**
	 * snippet used all over the app(where ever necessary) to use external fonts
	 * - helveticaneue
	 */
	public static Typeface returnHelvetica(Context context) {
		return Typeface.createFromAsset(context.getAssets(),
				"fonts/helveticaneue-webfont.ttf");
	}

	/**
	 * snippet used all over the app(where ever necessary) to use external fonts
	 * - helvetica-neue-bold
	 */
	public static Typeface returnHelveticaBold(Context context) {
		return Typeface.createFromAsset(context.getAssets(),
				"fonts/helvetica-neue-bold.ttf");
	}

	public static void showMultipleChoiceDlg(CharSequence[] array, final Context ctx,
			final TextView text, String title,final Activity ac,final TextView licState) {
		
		ScreenUtils.hideKeyboard(text, ctx);
		    
		final CharSequence[] items = array;
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("Choose a " + title);
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				text.setText(items[which]);
				if(licState!=null)
				licState.setText(items[which]);
				Utils.STATE_SELECTED = which;
			}
		});

		builder.show();
	}

	public static void hideKeyboard(View view,Context ctx) {
		// TODO Auto-generated method stub
		try {
			InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	public static ProgressDialog returnProgDialogObj(Context context,
			String message, boolean flag) {

		ProgressDialog pdiaolog = new ProgressDialog(context);
		pdiaolog.setMessage(message);
		pdiaolog.setCancelable(false);

		return pdiaolog;
	}

	public static void raiseToast(Context context, String message) {

		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	public static int[] getDimens(Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		int[] a = { width, height };
		return a;
	}

}
