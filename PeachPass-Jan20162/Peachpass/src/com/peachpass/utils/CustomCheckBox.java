package com.peachpass.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.srta.PeachPass.R;

public class CustomCheckBox extends CheckBox {

	public CustomCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setChecked(boolean t) {
		if (t) {
			this.setBackgroundResource(R.drawable.selector_checkbox);
		} else {
			this.setBackgroundResource(R.drawable.deselect_checkbox);
		}
		super.setChecked(t);
	}
}
