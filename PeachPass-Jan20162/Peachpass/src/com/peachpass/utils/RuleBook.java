package com.peachpass.utils;

import android.graphics.Color;
import android.util.Log;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**RuleBook class maintains all the validation conditions for Login and Create Account screens*/
public class RuleBook {

	public static final int USERNAME_LENGTH = 0;
	public static final int PIN_LENGTH = 4;
	public static final int PASSWORD_LENGTH = 8;
	public static final int EMAIL_LENGTH = 0;
	
	public static final int CVV_AMX = 4;
	public static final int CVV_OTHERS = 3;
	
	public static final int CC_AMX = 15;
	public static final int CC_OTHERS = 16;
	
	public static final int ANS_LENGTH = 4;
	

	/**check validation for username*/
	public static boolean isUserNameValid(String edtVal) {
		if (edtVal.trim().length() <= USERNAME_LENGTH)

			return false;
		return true;
	}

	/**check validation for Address*/
	public static boolean isAddressValid(String edtVal) {
		if (edtVal.trim().length() <= USERNAME_LENGTH)

			return false;
		return true;
	}
	
	/**check validation for PIN*/
	public static boolean isPinValid(String edtVal) {
		if (edtVal.trim().length() == PIN_LENGTH)

			return true;
		return false;
	}
	
	/**check validation for PIN*/
	public static boolean isSecQuestion(String edtVal) {
		if (edtVal.trim().length() >= ANS_LENGTH)

			return true;
		return false;
	}
	
	/**check validation for CardNo*/
	public static boolean isCardNoValid(String edtVal, boolean isAmx) {
		
		if(isAmx){
			if (edtVal.trim().length() == CC_AMX)
				return true;
			return false;
		}
		if (edtVal.trim().length() >= CC_OTHERS)
			return true;
		Log.e("peachpass","isCardNoValid- "+edtVal.trim().length());
		return false;
	}
	
	/**check validation for CVV*/
	public static boolean isCvvValid(String edtVal, boolean isAmx) {
		
		if(isAmx){
			if (edtVal.trim().length() == CVV_AMX)
				return true;
			return false;
		}
		if (edtVal.trim().length() == CVV_OTHERS)
			return true;
		return false;
	}
	
	/**check validation for password*/
	public static boolean isPasswordValid(String edtVal) {
		if (edtVal.trim().length() > PASSWORD_LENGTH && containsDigitAndOneLetter(edtVal))

			return true;
		return false;
	}

	/**check validation for email id*/
	public static boolean isEmailValid(String edtVal) {
		if (edtVal.trim().length() > EMAIL_LENGTH
				&& isEmailRegxValid(edtVal.trim()))

			return true;
		return false;
	}

	/**check validation for confirm password field*/
	public static boolean isConfirmPasswordValid(String edtVal1, String edtVal2) {
		if (edtVal1.trim().length() > PASSWORD_LENGTH
				&& edtVal2.trim().length() > PASSWORD_LENGTH
				&& edtVal1.trim().equals(edtVal2.trim()) && containsDigitAndOneLetter(edtVal1) && containsDigitAndOneLetter(edtVal2))

			return true;
		return false;
	}

	/**check validation for valid email id*/
	private static boolean isEmailRegxValid(String email) {
		Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static boolean allTabFieldsValid(View[] v) {

		for (View tempV : v) {
			if (ScreenUtils.getBgColor(tempV) != Color.parseColor("#75BE43"))
				return false;
		}
		return true;
	}

	public static final boolean containsDigitAndOneLetter(String s) {
	    boolean containsDigit = false;

	    if (s != null && !s.isEmpty()) {
	        for (char c : s.toCharArray()) {
	            if (containsDigit = Character.isDigit(c)) {
	                break;
	            }
	        }
	    }
	    boolean atleastOneAlpha = s.matches(".*[a-zA-Z]+.*");
	    if(atleastOneAlpha && containsDigit)
	    	return true;
	    return false;
	}
	
	
}
