package com.peachpass.utils;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebserviceUtils {

    public static final String EXTERNAL_IP_ADDRESS = "http://whatismyip.akamai.com/";
    // public static final String SOAP_ADDRESS = "https://uat.mobile.mypeachpass.com/MobileApp/MobileAppServiceSoapHttpPort?WSDL";
    public static final String SOAP_ADDRESS = "https://www.mypeachpass.com/MobileApp/MobileAppServiceSoapHttpPort?WSDL";

    public static final String ID = "1234";
    public static final String OS_TYPE = "Android";


    public static final String SOAP_ACTION_USER_LOGIN = "http://ws.mobileApp.csc.etcc.com//getAccountAlerts";

    public static final String SOAP_ACTION_ADDCREDITCARDBILLINGMETHOD = "http://ws.mobileApp.csc.etcc.com//addCreditCardBillingMethod";

    public static final String SOAP_ACTION_ADDVEHICLE = "http://ws.mobileApp.csc.etcc.com//addVehicle";

    public static final String SOAP_ACTION_CREATEFULFILLMENTBYTAGTYPECODE = "http://ws.mobileApp.csc.etcc.com//createFulfillmentByTagTypeCode";

    public static final String SOAP_ACTION_CREATEFULFILLMENTBYTAGVEHICLEINFO = "http://ws.mobileApp.csc.etcc.com//createFulfillmentByTagVehicleInfo";

    public static final String SOAP_ACTION_DELETECREDITCARDBILLINGMETHOD = "http://ws.mobileApp.csc.etcc.com//deleteCreditCardBillingMethod";

    public static final String SOAP_ACTION_DELETEVEHICLE = "http://ws.mobileApp.csc.etcc.com//deleteVehicle";

    public static final String SOAP_ACTION_EDITVEHICLE = "http://ws.mobileApp.csc.etcc.com//editVehicle";

    public static final String SOAP_ACTION_GETACCOUNTALERTS = "http://ws.mobileApp.csc.etcc.com//getAccountAlerts";

    public static final String SOAP_ACTION_GETACCOUNTINFOBYACCOUNTID = "http://ws.mobileApp.csc.etcc.com//getAccountInfoByAccountId";

    public static final String SOAP_ACTION_GETACCOUNTNOTIFICATIONPREFS = "http://ws.mobileApp.csc.etcc.com//getAccountNotificationPrefs";

    public static final String SOAP_ACTION_GETACCOUNTNOTIFICATIONS = "http://ws.mobileApp.csc.etcc.com//getAccountNotifications";

    public static final String SOAP_ACTION_GETINVOICEINFOBYNOTICENUMBER = "http://ws.mobileApp.csc.etcc.com//getInvoiceInfoByNoticeNumber";

    public static final String SOAP_ACTION_GETLOSTSTOLENTAGLIST = "http://ws.mobileApp.csc.etcc.com//getLostStolenTagList";

    public static final String SOAP_ACTION_GETPAYMENTSBYACCOUNTID = "http://ws.mobileApp.csc.etcc.com//getPaymentByAccountId";

    public static final String SOAP_ACTION_GETPRIMARYBILLINGMETHOD = "http://ws.mobileApp.csc.etcc.com//getPrimaryBillingMethod";

    public static final String SOAP_ACTION_GETSECONDARYBILLINGMETHOD = "http://ws.mobileApp.csc.etcc.com//getSecondaryBillingMethod";

    public static final String SOAP_ACTION_GETSTATEMENT = "http://ws.mobileApp.csc.etcc.com//getStatement";

    public static final String SOAP_ACTION_GETTOLLTRANSACTIONS = "http://ws.mobileApp.csc.etcc.com//getTollTransactions";

    public static final String SOAP_ACTION_GETTOP10TOLLTRANSACTIONS = "http://ws.mobileApp.csc.etcc.com//getTop10TollTransactions";

    public static final String SOAP_ACTION_GETUSERIDBYACCOUNTID = "http://ws.mobileApp.csc.etcc.com//getUserIdByAccountId";

    public static final String SOAP_ACTION_GETUSERIDBYTAGID = "http://ws.mobileApp.csc.etcc.com//getUserIdByTagId";

    public static final String SOAP_ACTION_GETVEHICLENONTOLLMODELIST = "http://ws.mobileApp.csc.etcc.com//getVehicleNonTollModeList";

    public static final String SOAP_ACTION_GETVEHICLETAGINFOLIST = "http://ws.mobileApp.csc.etcc.com//getVehicleTagInfoList";

    public static final String SOAP_ACTION_MAKEPAYMENT = "http://ws.mobileApp.csc.etcc.com//makePayment";

    public static final String SOAP_ACTION_MAKEVIOLATIONPAYMENT = "http://ws.mobileApp.csc.etcc.com//makeViolationPayment";

    public static final String SOAP_ACTION_MARKBILLINGMETHODASPRIMARY = "http://ws.mobileApp.csc.etcc.com//markBillingMethodAsPrimary";

    public static final String SOAP_ACTION_REMOVEVEHICLENONTOLLMODE = "http://ws.mobileApp.csc.etcc.com//removeVehicleNonTollMode";


    //public static final String SOAP_ACTION_SETUPACCOUNT = "http://ws.mobileApp.csc.etcc.com//setupAccount";
    public static final String SOAP_ACTION_SETUPACCOUNT = "https://www.mypeachpass.com/MobileApp/MobileAppServiceSoapHttpPort/setupAccount";

    public static final String SOAP_ACTION_SETUPONLINEACCESS = "http://ws.mobileApp.csc.etcc.com//setupOnlineAccess";

    public static final String SOAP_ACTION_UPDATEACCOUNTNOTIFICATIONPREFS = "http://ws.mobileApp.csc.etcc.com//updateAccountNotificationPrefs";

    public static final String SOAP_ACTION_UPDATEBILLINGPRIORITY = "http://ws.mobileApp.csc.etcc.com//updateBillingPriority";

    public static final String SOAP_ACTION_UPDATECREDITCARDBYBILLINGMETHOD = "http://ws.mobileApp.csc.etcc.com//updateCreditCardByBillingMethod";

    public static final String SOAP_ACTION_UPDATEPASSWORD = "http://ws.mobileApp.csc.etcc.com//updatePassword";

    public static final String SOAP_ACTION_USERLOGOUT = "http://ws.mobileApp.csc.etcc.com//userLogout";

    public static final String SOAP_ACTION_VERIFYSECURITYQUESTION = "http://ws.mobileApp.csc.etcc.com//verifySecurityQuestion";

    public static final String SOAP_ACTION_VIEWVIOLATIONSPERVEHICLE = "http://ws.mobileApp.csc.etcc.com//viewViolationsPerVehicle";

    public static final String SOAP_ACTION_VIEWPERSONALINFO = "http://ws.mobileApp.csc.etcc.com//viewPersonalInfo";
    public static final String SOAP_ACTION_SAVEPERSONALINFO = "http://ws.mobileApp.csc.etcc.com//savePersonalInfo";

    public static final String SOAP_ACTION_GET_VEHICLE_ID = "http://ws.mobileApp.csc.etcc.com//getVehicleId";

    public static final String SOAP_ACTION_SAVE_VEHICLE_NON_TOLL_MODE = "http://ws.mobileApp.csc.etcc.com//saveVehicleNonTollMode";

    public static final String SOAP_ACTION_GET_ACCOUNT_VEHICLE_TOLL_MODE = "http://ws.mobileApp.csc.etcc.com//getAccountVehicleTollMode";


    public static final String FORGET_PWD = "https://www.mypeachpass.com/olcsc/home/initPasswordRetrieval.do";

    public static final String CONTACT_US = "https://www.mypeachpass.com/olcsc/home/contactUsTab.do";

    public static final String CUST_AGRE = "https://www.peachpass.com/wp-content/uploads/2018/07/Customer-Agreement-R.-Effective-as-of-04.30.2018.pdf";

    public static final String CREATE_ACCOUNT_LINK = "https://www.mypeachpass.com/olcsc/home/noJavascript.do";

    public static final String WEBSITE_LINK = "http://mypeachpass.com";
    public static final String FAQ_LINK = "https://www.peachpass.com/how-do-i-get-a-peach-pass";
    public static final String FEED_BACK = "https://www.peachpass.com/mobileapp-support/";


    @SuppressWarnings("deprecation")
    public static String readFromAsset(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open("soap/" + fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

    public static String sendSoapRequest(Context context, String reqString,
                                         String soapAction) throws Exception {
        // System.out.println("Start sending " + action + " request");
        try {
           // SSLConnection.allowAllSSL();
            URL url = new URL(SOAP_ADDRESS);
            HttpURLConnection rc = (HttpURLConnection) url.openConnection();
            // System.out.println("Connection opened " + rc );
            rc.setRequestMethod("POST");
            rc.setConnectTimeout(50000); //set timeout to 5 seconds
            rc.setReadTimeout(50000);
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            String reqStr = reqString;
            int len = reqStr.length();
            rc.setRequestProperty("Content-Length", Integer.toString(len));
            rc.setRequestProperty("SOAPAction", soapAction);
            rc.connect();
            OutputStreamWriter out = new OutputStreamWriter(rc.getOutputStream());
            out.write(reqStr, 0, len);
            out.flush();
            // System.out.println("Request sent, reading response ");
            InputStreamReader read = new InputStreamReader(rc.getInputStream());
            // dumpHeaders( rc, System.out );
            // note that Content-Length is available at this point
            StringBuilder sb = new StringBuilder();
            int ch = read.read();
            while (ch != -1) {
                sb.append((char) ch);
                ch = read.read();
            }
            String response = sb.toString();
            Log.e("peachpass", "**********" + response + "**********");
            read.close();
            rc.disconnect();
            return response;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }

    }

    public static String getExternalIP() {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(EXTERNAL_IP_ADDRESS);
            // HttpGet httpget = new HttpGet("http://whatismyip.com.au/");
            // HttpGet httpget = new HttpGet("http://www.whatismyip.org/");
            HttpResponse response;

            response = httpclient.execute(httpget);

            // Log.i("externalip",response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                long len = entity.getContentLength();
                if (len != -1 && len < 1024) {
                    String str = EntityUtils.toString(entity);
                    // Log.i("externalip",str);
                    return str;
                } else {
                    return null;
                }
            } else {
                return null;
            }

        } catch (Exception e) {
            return null;
        }

    }

    public static final String SOMETHING_WENT_WRONG = "Something went wrong, please try again";
    public static final String NO_INTERNET_CONNECTION = "Check Internet Settings !";
    public static final String LOGIN_INCORRECT = "Your username or password is invalid.";
    public static final String PROGRESS_MESSAGE_USER_LOGIN = "Logging in...";
    public static final String PROGRESS_MESSAGE_FETCHING_DETAILS = "Fetching details...Please wait";
    public static final String PROGRESS_MESSAGE_FETCHING_TRANSACTIONS = "Fetching transaction details...Please wait";
    public static final String SESSION_EXPIRED = "Session expired...Try login again";
    public static final String LOGGING_OUT = "Signing out...";
    public static final String NO_DATA_DISPLAY = "No data to display";


    public static final String PROGRESS_MESSAGE_UPLOADING_DETAILS = "Uploading details...Please wait";
    public static final String VEHICLE_DETAILS_EDIT_SUCCESS = "Vehicle details uploaded successfully";
    public static final String UPDATE_CREDIT_CARD_SUCCESS = "Credit card details updated successfully";
}
