package com.peachpass.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.AppController;
import com.srta.PeachPass.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class Utils {
    //
    public static String selectValue ="";
    // public static final String startDate = "2015-02-02 08:00:00";
    // public static final String endDate = "2015-02-06 23:59:00";
    public static final String DATE_PATTERN_SOAP = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DATE_PATTERN_NOTIFICATION = "MMM dd, yyyy 'at' hh:mm aaa";
    public static final String DATE_PATTERN_MY_PEACH_PASS = "MMM,dd,hh:mm a";
    public static final String OTHER = "MM-dd-yyyy HH:mm:ss";

    public static final String WARNING_VIEW = "Warning";
    public static final String LOGIN_VIEW = "Login";
    public static final String CREATE_VIEW = "Create Account";
    public static final String WALKTHROUGH_VIEW = "Walkthrough";
    public static final String MYPEACHPASS_VIEW = "My Peach Pass";
    public static final String TOLLMODE_VIEW = "Toll Mode";
    public static final String TRANSACTIONS_VIEW = "Transactions";
    public static final String MYPROFILE_VIEW = "My Profile";
    public static final String ALERTS_VIEW = "Alerts";
    public static final String NOTIFICATIONS_VIEW = "Notifications";
    public static final String STATEMENTS_VIEW = "Statements";
    public static final String FAQ_VIEW = "FAQs";
    public static final String RESOURCES_VIEW = "Resources";
    public static final String EDITPROFILE_VIEW = "Edit Profile";
    public static final String ADDVEHICLE_VIEW = "Add Vehicle";
    public static final String EDITVEHICLE_VIEW = "Edit Vehicle";
    public static final String ADDCARD_VIEW = "Add Card";
    public static final String EDITCARD_VIEW = "Edit Card";
    public static final String EDITSETTINGS_VIEW = "Edit Settings";
    public static final String CONTACT_US = "contact_us";
    public static final String FORGOT_PASSWORD = "forgot_password";
    public static final String SETUP_ACCOUNT = "setup_account";
    public static final String VIOLATOIN_DETAILS = "violatoin_details";
    public static final String LEFT_MENU = "left_menu";
    public static final String WALKTHROUGH1 = "walkthrough1";
    public static final String WALKTHROUGH2="walkthrough2";
    public static final String WALKTHROUGH3="walkthrough3";
    public static final String WALKTHROUGH4="walkthrough4";
    public static final String WALKTHROUGH5="walkthrough5";


    public static int STATE_SELECTED = 0;
    public static String QUESTION_LIST[] = {
            "What is your mother's maiden name?",
            "What was the name of your first (elementary) school?",
            "What is your brother's middle name?",
            "What is your sister's middle name?",
            "In what city were you born?",
            "What was the make of your first car?",
            "What is the name of your first pet?",
            "What street did you grow up on?"};

    public static String SOURCE_LIST[] = {"Radio/TV Ad", "News Report",
            "Community Event", "EMAIL", "Website", "OTHER"};

    public static void callThisNumber(String phoneNumber, Context ctx) {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        ctx.startActivity(callIntent);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();

        return activeNetworkInfo != null;
    }

    public static String decodeString(String stingvalue)
            throws UnsupportedEncodingException {

        return URLDecoder.decode(stingvalue, "UTF-8");
    }

    public static void sendEmail(Context context, String subject, String body) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        // i.putExtra(Intent.EXTRA_EMAIL, recipients);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, body);
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean checkAddToFav(String key, Context ctx) {
        // TODO Auto-generated method stub
        try {
            String value = SharedPreferenceHelper.getPreference(ctx, key);
            if (value == null || value.equals("") || value.equals("0"))
                return false;
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    public static String getFavIds(Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Map<String, ?> keys = sharedPreferences.getAll();
        StringBuffer buff = new StringBuffer();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().toString();
            if (value != null && value.equals("1")) {
                buff.append(key).append(",");
            }
        }
        return buff.toString();
    }

    public static void saveInSdcard(String content) {
        try {
            File myFile = new File("/sdcard/mysdfile.txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(content);
            myOutWriter.close();
            fOut.close();

        } catch (Exception e) {

        }
    }

    public static Date getTodaysDateObj() throws Exception {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.ENGLISH);

        // Use Madrid's time zone to format the date in
        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        date = df.parse(df.format(date));

        return date;
    }

    public static String getEditTextValue(EditText editText) {

        return editText.getText().toString().trim();

    }

    public static String getOsVersion() {

        return Build.VERSION.RELEASE + "";
    }

    public static String getAccountId(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.ACCOUNT_ID);
    }

    public static String getAccountBalance(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.ACCOUNTBALANCE);
    }

    public static String getUsername(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.USERNAME);
    }

    public static String getPassword(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.PASSWORD);
    }

    public static String getRememberMe(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.REMEMBERME);
    }

    public static void saveUsername(Context context, String data) {

        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.USERNAME, data, context);
    }

    public static void savePassword(Context context, String data) {

        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.PASSWORD, data, context);
    }

    public static void saveRememberMe(Context context, String data) {

        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.REMEMBERME, data, context);
    }

    public static String getSessionId(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.SESSION_ID);
    }


    public static String getEffectiveDate(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.SESSION_ID);
    }

    public static void saveEffectiveDate(Context context, String data) {

        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.REMEMBERME, data, context);
    }

    public static String getTagId(Context context) {

        return SharedPreferenceHelper.getPreference(context,
                SharedPreferenceHelper.TAG_ID);
    }

    public static String parseDate(String time, String inputPattern,
                                   String outputPattern) {
        try {
            // String inputPattern = DATE_PATTERN_SOAP;
            // String outputPattern = "MMM dd, yyyy 'at' hh:mm aaa";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern,
                    Locale.ENGLISH);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern,
                    Locale.ENGLISH);

            Date date = null;
            String str = null;

            Log.e("Utils", "time--" + time);
            Log.e("Utils", "inputPattern--" + inputPattern);
            Log.e("Utils", "outputPattern--" + outputPattern);

            date = inputFormat.parse(time);
            str = outputFormat.format(date);
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("newlog", e.toString());
            return time;
        }

    }

    public static Calendar getDate(String time) {
        try {
            String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern,
                    Locale.ENGLISH);

            Date date = null;
            date = inputFormat.parse(time);

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("newlog", e.toString());
            return null;
        }

    }

    public static String getDateInThisFormat(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        Date date = new Date();
        return sdf.format(date);
        // return "05/2015";
    }

    public static String addMinutesToDate(int minutes, String format) {
        final long ONE_MINUTE_IN_MILLIS = 60000;// millisecs

        long curTimeInMs = new Date().getTime();
        Date afterAddingMins = new Date(curTimeInMs
                + (minutes * ONE_MINUTE_IN_MILLIS));
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        return sdf.format(afterAddingMins);
    }

    public static String getStateCode(int stateCode) {

        switch (stateCode) {
            case 0:
                return "AL";
            case 1:
                return "AK";
            case 2:
                return "AZ";
            case 3:
                return "AR";
            case 4:
                return "CA";
            case 5:
                return "CO";
            case 6:
                return "CT";
            case 7:
                return "DE";
            case 8:
                return "FL";
            case 9:
                return "GA";
            case 10:
                return "HI";
            case 11:
                return "ID";
            case 12:
                return "IL";
            case 13:
                return "IN";
            case 14:
                return "IA";
            case 15:
                return "KS";
            case 16:
                return "KY";
            case 17:
                return "LA";
            case 18:
                return "ME";
            case 19:
                return "MD";
            case 20:
                return "MA";
            case 21:
                return "MI";
            case 22:
                return "MN";
            case 23:
                return "MS";
            case 24:
                return "MO";
            case 25:
                return "MT";
            case 26:
                return "NE";
            case 27:
                return "NV";
            case 28:
                return "NH";
            case 29:
                return "NJ";
            case 30:
                return "NM";
            case 31:
                return "NY";
            case 32:
                return "NC";
            case 33:
                return "ND";
            case 34:
                return "OH";
            case 35:
                return "OK";
            case 36:
                return "OR";
            case 37:
                return "PA";
            case 38:
                return "RI";
            case 39:
                return "SC";
            case 40:
                return "SD";
            case 41:
                return "TN";
            case 42:
                return "TX";
            case 43:
                return "UT";
            case 44:
                return "VT";
            case 45:
                return "VA";
            case 46:
                return "WA";
            case 47:
                return "WV";
            case 48:
                return "WI";
            case 49:
                return "WY";

            default:
                return "";
        }
    }

    public static String getCardNamefromId(String id) {
        if (id.equals("1"))
            return "Master Card";

        if (id.equals("2"))
            return "Visa";

        if (id.equals("3"))
            return "American Express";

        if (id.equals("4"))
            return "Discover";

        return "";
    }

    public static String getCardIdfromName(String name) {
        if (name.equals("MasterCard"))
            return "1";

        if (name.equals("Visa"))
            return "2";

        if (name.equals("American Express"))
            return "3";

        if (name.equals("Discover"))
            return "4";

        return "";
    }

    public static String getMonthNamefromId(String id) {
        if (id.equals("01"))
            return "Jan";

        if (id.equals("02"))
            return "Feb";

        if (id.equals("03"))
            return "Mar";

        if (id.equals("04"))
            return "Apr";

        if (id.equals("05"))
            return "May";

        if (id.equals("06"))
            return "June";

        if (id.equals("07"))
            return "July";

        if (id.equals("08"))
            return "Aug";

        if (id.equals("09"))
            return "Sept";

        if (id.equals("10"))
            return "0ct";

        if (id.equals("11"))
            return "Nov";

        if (id.equals("12"))
            return "Dec";

        return "";
    }

    public static String getMonthIdfromName(String name) {
        if (name.equals("Jan"))
            return "01";

        if (name.equals("Feb"))
            return "02";

        if (name.equals("Mar"))
            return "03";

        if (name.equals("Apr"))
            return "04";

        if (name.equals("May"))
            return "05";

        if (name.equals("Jun"))
            return "06";

        if (name.equals("Jul"))
            return "07";

        if (name.equals("Aug"))
            return "08";

        if (name.equals("Sep"))
            return "09";

        if (name.equals("Oct"))
            return "10";

        if (name.equals("Nov"))
            return "11";

        if (name.equals("Dec"))
            return "12";

        return "";
    }

    public static String getStringResourceByName(String aString, Context context) {
        try {
            String packageName = context.getPackageName();
            int resId = context.getResources().getIdentifier(aString, "string",
                    packageName);
            return context.getString(resId);
        } catch (Exception e) {
            // TODO: handle exception
            return "Unknown Error";
        }

    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String firstCapital(String input) {

        return Character.toUpperCase(input.charAt(0)) + input.substring(1);

    }

    public static String firstCapitalName(String input) {

        try {
            return Character.toUpperCase(input.charAt(0))
                    + (input.substring(1)).toLowerCase();
        } catch (Exception e) {
            // TODO: handle exception
            return "";
        }

    }

    public static boolean ifDatePassed(String date) throws Exception {

        String current = getDateInThisFormat("yyyy/MM/dd");

        int state = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).parse(
                date).compareTo(
                new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
                        .parse(current));

        // Log.e("peachapss", date+"-"+state);
        if (state < 0) {
            return true;
        } else
            return false;
    }

    public static final LinkedHashMap<String, Integer> countyMap = new LinkedHashMap<String, Integer>();

    static {
        countyMap.put("Appling", 6);
        countyMap.put("Atkinson", 7);
        countyMap.put("Bacon", 8);
        countyMap.put("Baker", 9);
        countyMap.put("Baldwin", 10);
        countyMap.put("Banks", 395);
        countyMap.put("Barrow", 396);
        countyMap.put("Bartow", 397);
        countyMap.put("Ben Hill", 398);
        countyMap.put("Berrien", 399);
        countyMap.put("Bibb", 400);
        countyMap.put("Bleckley", 401);
        countyMap.put("Brantley", 402);
        countyMap.put("Brooks", 403);
        countyMap.put("Bryan", 404);
        countyMap.put("Bulloch", 405);
        countyMap.put("Burke", 406);
        countyMap.put("Butts", 407);
        countyMap.put("Calhoun", 408);
        countyMap.put("Camden", 409);
        countyMap.put("Candler", 410);
        countyMap.put("Carroll", 411);
        countyMap.put("Catoosa", 412);
        countyMap.put("Charlton", 413);
        countyMap.put("Chatham", 414);
        countyMap.put("Chattahoochee", 415);
        countyMap.put("Chattooga", 416);
        countyMap.put("Cherokee", 417);
        countyMap.put("Clarke", 418);
        countyMap.put("Clay", 419);
        countyMap.put("Clayton", 420);
        countyMap.put("Clinch", 421);
        countyMap.put("Cobb", 422);
        countyMap.put("Coffee", 423);
        countyMap.put("Colquitt", 424);
        countyMap.put("Columbia", 425);
        countyMap.put("Cook", 426);
        countyMap.put("Coweta", 427);
        countyMap.put("Crawford", 428);
        countyMap.put("Crisp", 429);
        countyMap.put("Dade", 430);
        countyMap.put("Dawson", 431);
        countyMap.put("Decatur", 432);
        countyMap.put("DeKalb", 433);
        countyMap.put("Dodge", 434);
        countyMap.put("Dooly", 435);
        countyMap.put("Dougherty", 436);
        countyMap.put("Douglas", 437);
        countyMap.put("Early", 438);
        countyMap.put("Echols", 439);
        countyMap.put("Effingham", 440);
        countyMap.put("Elbert", 441);
        countyMap.put("Emanuel", 442);
        countyMap.put("Evans", 443);
        countyMap.put("Fannin", 444);
        countyMap.put("Fayette", 445);
        countyMap.put("Floyd", 446);
        countyMap.put("Forsyth", 447);
        countyMap.put("Franklin", 448);
        countyMap.put("Fulton", 449);
        countyMap.put("Gilmer", 450);
        countyMap.put("Glascock", 451);
        countyMap.put("Glynn", 452);
        countyMap.put("Gordon", 453);
        countyMap.put("Grady", 454);
        countyMap.put("Greene", 455);
        countyMap.put("Gwinnett", 456);
        countyMap.put("Habersham", 457);
        countyMap.put("Hall", 458);
        countyMap.put("Hancock", 459);
        countyMap.put("Haralson", 460);
        countyMap.put("Harris", 461);
        countyMap.put("Hart", 462);
        countyMap.put("Heard", 463);
        countyMap.put("Henry", 464);
        countyMap.put("Houston", 465);
        countyMap.put("Irwin", 466);
        countyMap.put("Jackson", 467);
        countyMap.put("Jasper", 468);
        countyMap.put("Jeff Davis", 469);
        countyMap.put("Jefferson", 470);
        countyMap.put("Jenkins", 471);
        countyMap.put("Johnson", 472);
        countyMap.put("Jones", 473);
        countyMap.put("Lamar", 474);
        countyMap.put("Lanier", 475);
        countyMap.put("Laurens", 476);
        countyMap.put("Lee", 477);
        countyMap.put("Liberty", 478);
        countyMap.put("Lincoln", 479);
        countyMap.put("Long", 480);
        countyMap.put("Lowndes", 481);
        countyMap.put("Lumpkin", 482);
        countyMap.put("Macon", 483);
        countyMap.put("Madison", 484);
        countyMap.put("Marion", 485);
        countyMap.put("McDuffie", 486);
        countyMap.put("McIntosh", 487);
        countyMap.put("Meriwether", 488);
        countyMap.put("Miller", 489);
        countyMap.put("Mitchell", 490);
        countyMap.put("Monroe", 491);
        countyMap.put("Montgomery", 492);
        countyMap.put("Morgan", 493);
        countyMap.put("Murray", 494);
        countyMap.put("Muscogee", 495);
        countyMap.put("Newton", 496);
        countyMap.put("Oconee", 497);
        countyMap.put("Oglethorpe", 498);
        countyMap.put("Paulding", 499);
        countyMap.put("Peach", 500);
        countyMap.put("Pickens", 501);
        countyMap.put("Pierce", 502);
        countyMap.put("Pike", 503);
        countyMap.put("Polk", 504);
        countyMap.put("Pulaski", 505);
        countyMap.put("Putnam", 506);
        countyMap.put("Quitman", 507);
        countyMap.put("Rabun", 508);
        countyMap.put("Randolph", 509);
        countyMap.put("Richmond", 510);
        countyMap.put("Rockdale", 511);
        countyMap.put("Schley", 512);
        countyMap.put("Screven", 513);
        countyMap.put("Seminole", 514);
        countyMap.put("Spalding", 515);
        countyMap.put("Stephens", 516);
        countyMap.put("Stewart", 517);
        countyMap.put("Sumter", 518);
        countyMap.put("Talbot", 519);
        countyMap.put("Taliaferro", 520);
        countyMap.put("Tattnall", 521);
        countyMap.put("Taylor", 522);
        countyMap.put("Telfair", 523);
        countyMap.put("Terrell", 524);
        countyMap.put("Thomas", 525);
        countyMap.put("Tift", 526);
        countyMap.put("Toombs", 527);
        countyMap.put("Towns", 528);
        countyMap.put("Treutlen", 529);
        countyMap.put("Troup", 530);
        countyMap.put("Turner", 531);
        countyMap.put("Twiggs", 532);
        countyMap.put("Union", 533);
        countyMap.put("Upson", 534);
        countyMap.put("Walker", 535);
        countyMap.put("Walton", 536);
        countyMap.put("Ware", 537);
        countyMap.put("Warren", 538);
        countyMap.put("Washington", 539);
        countyMap.put("Wayne", 540);
        countyMap.put("Webster", 541);
        countyMap.put("Wheeler", 542);
        countyMap.put("White", 543);
        countyMap.put("Whitfield", 544);
        countyMap.put("Wilcox", 545);
        countyMap.put("Wilkes", 546);
        countyMap.put("Wilkinson", 547);
        countyMap.put("Worth", 548);
        countyMap.put("OTHER", 9999);
    }


    public static final LinkedHashMap<String, Integer> questionMap = new LinkedHashMap<String, Integer>();

    static {
        questionMap.put("What is your mother's maiden name?", 1);
        questionMap.put("What was the name of your first (elementary) school?",
                2);
        questionMap.put("What is your brother's middle name?", 3);
        questionMap.put("What is your sister's middle name?", 4);
        questionMap.put("In what city were you born?", 5);
        questionMap.put("What was the make of your first car?", 6);
        questionMap.put("What is the name of your first pet?", 7);
        questionMap.put("What street did you grow up on?", 8);
    }

    public static CharSequence[] returnYears() {
        CharSequence[] array_3 = {"2015", "2016", "2017", "2018", "2019",
                "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027",
                "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035",
                "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043",
                "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051",
                "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059",
                "2060", "2061", "2062", "2063", "2064", "2065"};

        return array_3;
    }

    public static void openLinkInBrowser(String URL, Activity activity) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
        activity.startActivity(browserIntent);
    }



    public static boolean isValidExpDate(String userDate) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
            Date date1 = sdf.parse(userDate);
            Date date2 = sdf.parse(Utils.getDateInThisFormat("MMM yyyy"));

            System.out.println(sdf.format(date1));
            System.out.println(sdf.format(date2));

            if (date1.compareTo(date2) > 0) {
                System.out.println("Date1 is after Date2");
                return true;
            } else if (date1.compareTo(date2) < 0) {
                System.out.println("Date1 is before Date2");
                return false;
            } else if (date1.compareTo(date2) == 0) {
                System.out.println("Date1 is equal to Date2");
                return true;
            }

            return true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return false;

    }


    public static void setTracker(String title) {
        AppController.getInstance().trackScreenView(title);
    }

    public static void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
