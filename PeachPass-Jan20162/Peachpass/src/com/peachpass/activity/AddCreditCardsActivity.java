package com.peachpass.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.CreditCardData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;

public class AddCreditCardsActivity extends Activity {

	List<String> stateList = null;
	List<String> countyList = null;

	Context context;

	EditText edt_tab4_cardno, edt_tab4_fln, edt_tab4_fln_last,
			edt_tab4_address, edt_tab4_aptSuite, edt_tab4_city, edt_tab4_zip;

	TextView txt_create_account_4_state, txt_tab4_county;
	TextView txt_changetoll_arrow_4_4, txt_changetoll_arrow_county_4,
			txt_changetoll_arrow_cardtype, txt_changetoll_arrow_4_month,
			txt_changetoll_arrow_4_year, txt_create_account_next4;
	TextView txt_create_account_4_card, txt_create_account_4_year,
			txt_create_account_4_month;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Set the screen to full screen mode before setting the layout */
		ScreenUtils.fullScreen(this);
		context = this;
		stateList = Arrays.asList(this.getResources().getStringArray(
				R.array.state_type));

		countyList = Arrays.asList(this.getResources().getStringArray(
				R.array.county_type));

		setContentView(R.layout.activity_add_credit_card);

		init();

		Typeface font = ScreenUtils.returnTypeFace(this);

		Typeface font1 = ScreenUtils.returnTypeFaceHelv(this);

		txt_create_account_4_state.setText(stateList.get(9));
		txt_tab4_county.setText(countyList.get(0));

		txt_changetoll_arrow_4_4.setTypeface(font);
		txt_changetoll_arrow_county_4.setTypeface(font);
		txt_changetoll_arrow_cardtype.setTypeface(font);
		txt_changetoll_arrow_4_month.setTypeface(font);
		txt_changetoll_arrow_4_year.setTypeface(font);
		txt_create_account_next4.setTypeface(font);

		edt_tab4_cardno.setTypeface(font1);
		edt_tab4_fln.setTypeface(font1);
		edt_tab4_fln_last.setTypeface(font1);
		edt_tab4_address.setTypeface(font1);
		edt_tab4_aptSuite.setTypeface(font1);
		edt_tab4_city.setTypeface(font1);
		edt_tab4_zip.setTypeface(font1);

		txt_create_account_4_card.setText("MasterCard");
		txt_create_account_4_year.setText("2020");
		txt_create_account_4_month.setText("Jan");

		Utils.setTracker(Utils.ADDCARD_VIEW);
	}

	private void init() {

		edt_tab4_cardno = (EditText) findViewById(R.id.edt_tab4_cardno);
		edt_tab4_fln = (EditText) findViewById(R.id.edt_tab4_fln);
		edt_tab4_fln_last = (EditText) findViewById(R.id.edt_tab4_fln_last);
		edt_tab4_address = (EditText) findViewById(R.id.edt_tab4_address);
		edt_tab4_aptSuite = (EditText) findViewById(R.id.edt_tab4_aptSuite);
		edt_tab4_city = (EditText) findViewById(R.id.edt_tab4_city);
		edt_tab4_zip = (EditText) findViewById(R.id.edt_tab4_zip);

		txt_create_account_4_state = (TextView) findViewById(R.id.txt_create_account_4_state);
		txt_tab4_county = (TextView) findViewById(R.id.txt_tab4_county);
		txt_changetoll_arrow_cardtype = (TextView) findViewById(R.id.txt_changetoll_arrow_cardtype);
		txt_changetoll_arrow_4_month = (TextView) findViewById(R.id.txt_changetoll_arrow_4_month);
		txt_changetoll_arrow_4_year = (TextView) findViewById(R.id.txt_changetoll_arrow_4_year);
		txt_create_account_next4 = (TextView) findViewById(R.id.txt_create_account_next4);

		txt_changetoll_arrow_4_4 = (TextView) findViewById(R.id.txt_changetoll_arrow_4_4);
		txt_changetoll_arrow_county_4 = (TextView) findViewById(R.id.txt_changetoll_arrow_county_4);

		txt_create_account_4_card = (TextView) findViewById(R.id.txt_create_account_4_card);
		txt_create_account_4_year = (TextView) findViewById(R.id.txt_create_account_4_year);
		txt_create_account_4_month = (TextView) findViewById(R.id.txt_create_account_4_month);
	}

	public void onclickNextButtons(View view) {
		if (Utils.isNetworkAvailable(context)) {
			
			String key = txt_tab4_county.getText().toString();
			Log.e("peachpass", Utils.countyMap.get(key).toString());
			
			final String cardTypeId = getSelectedCCType(txt_create_account_4_card.getText().toString()); 
			final String ccNumber=getEdtString(edt_tab4_cardno); 
			final String expiration=getSelectedMonth(txt_create_account_4_month.getText().toString())+"/"+getSelectedYear(txt_create_account_4_year.getText().toString());
			final String firstName=getEdtString(edt_tab4_fln);
			final String middleInitial=""; 
			final String lastName=getEdtString(edt_tab4_fln_last);
			final String address=getEdtString(edt_tab4_address);
			final String aptSuite=getEdtString(edt_tab4_aptSuite);
			final String city=getEdtString(edt_tab4_city); 
			final String state=txt_create_account_4_state.getText().toString();
			final String zip=getEdtString(edt_tab4_zip);
			final String county = Utils.countyMap.get(key).toString();
			
			new AsyncTask<CreditCardData, String, String>() {
				ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
						context,
						WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
						false);

				String status = "";

				@Override
				protected void onPreExecute() {
					try {
						pdialog.show();
					} catch (Exception e) {
						// TODO: handle exception
					}
				};

				@Override
				protected String doInBackground(CreditCardData... params) {
					// TODO Auto-generated method stub
					try {

						Log.e("peachpass", params[0].toString());

						String requestBody = WebserviceUtils.readFromAsset(
								"addCreditCardBillingMethod", context);
						requestBody = requestBody.replaceAll("_cardTypeId_",cardTypeId);
						requestBody = requestBody.replaceAll("_ccNumber_",ccNumber);
						requestBody = requestBody.replaceAll("_expiration_",expiration);
						requestBody = requestBody.replaceAll("_firstName_",firstName);
						requestBody = requestBody.replaceAll("_lastName_",lastName);
						requestBody = requestBody.replaceAll("_address_",address);
						requestBody = requestBody.replaceAll("_aptSuite_",aptSuite);
						requestBody = requestBody.replaceAll("_city_",city);
						requestBody = requestBody.replaceAll("_state_",state);
						requestBody = requestBody.replaceAll("_zip_",zip);
						requestBody = requestBody.replaceAll("_county_",county);
						
						

						requestBody = requestBody.replaceAll("_accountId_",
								Utils.getAccountId(context));
						requestBody = requestBody.replaceAll("_userName_",
								Utils.getUsername(context));
						requestBody = requestBody.replaceAll("_sessionId_",
								Utils.getSessionId(context));

						requestBody = requestBody.replaceAll("_osType_",
								WebserviceUtils.OS_TYPE);
						requestBody = requestBody.replaceAll("_osVersion_",
								Utils.getOsVersion());
						requestBody = requestBody.replaceAll("_ipAddress_",
								WebserviceUtils.getExternalIP());

						requestBody = requestBody.replaceAll("_id_",
								WebserviceUtils.ID);

						Log.e("peachpass", requestBody + "");
						String result = WebserviceUtils.sendSoapRequest(
								context, requestBody,
								WebserviceUtils.SOAP_ACTION_ADDCREDITCARDBILLINGMETHOD);
						XMLParser parser = new XMLParser();
						String xml = result;
						Document doc = parser.getDomElement(xml); // getting DOM
						// element
						NodeList nl = doc.getElementsByTagName("result");
						Element e = (Element) nl.item(0);
						status = parser.getValue(e, "ns1:status");
						// Log.e("peachpass", status + "****status");
						return status;

					} catch (Exception e) {
						// TODO: handle exception

					}
					return null;
				}

				@Override
				protected void onPostExecute(String result) {
					// TODO Auto-generated method stub
					super.onPostExecute(result);

					try {
						pdialog.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
					if (result != null) {

						if (result.equals("0")) {
							setResult(Activity.RESULT_OK);
							finish();
							ScreenUtils
									.raiseToast(
											context,
											WebserviceUtils.VEHICLE_DETAILS_EDIT_SUCCESS);
						} else {
							String errorStatus = Utils.getStringResourceByName(
									"status_" + result, context);
							
							ScreenUtils.raiseToast(context, errorStatus);
						}

					} else {
						ScreenUtils.raiseToast(context,
								WebserviceUtils.SOMETHING_WENT_WRONG);
					}
				}

			}.execute(new CreditCardData[] {new CreditCardData(cardTypeId, ccNumber, expiration, firstName, middleInitial, lastName, address, aptSuite, city, state, zip, county)});
		} else {
			ScreenUtils.raiseToast(context,
					WebserviceUtils.NO_INTERNET_CONNECTION);
		}
	}

	public void onClickBack(View view) {
		
	if(checkIfFieldsAreEmpty()){
			
			finish();
			
		}else{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Are you sure you want to cancel without saving changes?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								setResult(Activity.RESULT_CANCELED);
								finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
		}

	}

	public void onClickShowDropdown(View view) {

		switch (view.getId()) {

		case R.id.ll_create_account_4_state:

			final CharSequence[] array_1 = stateList
					.toArray(new CharSequence[stateList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array_1, this,
					txt_create_account_4_state, "state", this, null);
			break;

		case R.id.ll_create_account_4_county:
			final CharSequence[] array = countyList
					.toArray(new CharSequence[countyList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array, this, txt_tab4_county,
					"county", this, null);
			break;
		case R.id.ll_create_account_4_card_type:

			final CharSequence[] array_2 = { "MasterCard", "Visa",
					"American Express", "Discover" };
			ScreenUtils.showMultipleChoiceDlg(array_2, this,
					txt_create_account_4_card, "card type", this, null);
			break;

		case R.id.ll_create_account_4_year:

			ScreenUtils.showMultipleChoiceDlg(Utils.returnYears(), this,
					txt_create_account_4_year, "year", this, null);
			break;

		case R.id.ll_create_account_4_month:

			final CharSequence[] array_4 = { "Jan", "Feb", "Mar", "Apr", "May",
					"June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };
			ScreenUtils.showMultipleChoiceDlg(array_4, this,
					txt_create_account_4_month, "month", this, null);
			break;
		default:
			break;
		}

	}
	
	private String getEdtString(EditText edt){
		
		return edt.getText().toString().trim();
	}
	
	 private String getSelectedCCType(String selectedItem) {


	        if (selectedItem.equals("MasterCard")) {
	            return "1";
	        }

	        if (selectedItem.equals("Visa")) {
	            return "2";
	        }

	        if (selectedItem.equals("American Express")) {
	            return "3";
	        }

	        if (selectedItem.equals("Discover")) {
	            return "4";
	        }

	        return "";
	    }

	    private String getSelectedMonth(String selectedItem) {

	        if (selectedItem.equals("Jan")) {
	            return "01";
	        }

	        if (selectedItem.equals("Feb")) {
	            return "02";
	        }

	        if (selectedItem.equals("Mar")) {
	            return "03";
	        }

	        if (selectedItem.equals("Apr")) {
	            return "04";
	        }

	        if (selectedItem.equals("May")) {
	            return "05";
	        }

	        if (selectedItem.equals("June")) {
	            return "06";
	        }

	        if (selectedItem.equals("July")) {
	            return "07";
	        }

	        if (selectedItem.equals("Aug")) {
	            return "08";
	        }

	        if (selectedItem.equals("Sept")) {
	            return "09";
	        }

	        if (selectedItem.equals("Oct")) {
	            return "10";
	        }

	        if (selectedItem.equals("Nov")) {
	            return "11";
	        }

	        if (selectedItem.equals("Dec")) {
	            return "12";
	        }
	        return "";
	    }

	    private String getSelectedYear(String selectedItem) {


	        String sub = selectedItem.substring(selectedItem.length() - 2);

	        return sub;
	    }
	    
	    private boolean checkIfFieldsAreEmpty(){
			EditText[] edtValues = {edt_tab4_cardno, edt_tab4_fln, edt_tab4_fln_last,
					edt_tab4_address, edt_tab4_aptSuite, edt_tab4_city, edt_tab4_zip};
			
			for(EditText edt:edtValues){
				int length = Utils.getEditTextValue(edt).length();
				if(length>0)
					return false;
			}
			return true;
		}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(context).reportActivityStart(this);

		FlurryAgent.onStartSession(context);
		FlurryAgent.setLogEvents(true);

		FlurryAgent.logEvent(Utils.ADDCARD_VIEW);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

		FlurryAgent.onEndSession(context);
	}

}
