package com.peachpass.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.SetupAccountData;
import com.peachpass.utils.RuleBook;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetupAccountActivity extends Activity {

	RelativeLayout rel_tab1, rel_tab2, rel_tab3, rel_tab4, rel_tab5;
	RelativeLayout rel_tab1_1, rel_tab1_2;
	RelativeLayout rel_tab3_1, rel_tab3_2, rel_tab3_3, rel_tab3_4;

	View layoutTab1, layoutTab2, layoutTab3, layoutTab4, layoutTab5;

	View circletab1, circletab2, circletab3, circletab4, circletab5;

	TextView txt_next2, txt_next3, txt_next4, txt_next5;

	TextView txt_create_account_next2, txt_create_account_next3,
			txt_create_account_next4;

	Typeface font = null;

	EditText edt_tab2_accno, edt_tab2_4digit;
	EditText edt_tab3_username_sp, edt_tab3_email_sp, edt_tab3_pwd_sp,
			edt_tab3_confpwd_sp;

	TextView txt_create_accountno, txt_create_account_digit;
	TextView txt_create_account_username, txt_create_account_email_id,
			txt_create_account_pwd, txt_create_account_con_pwd;

	TextView txt_circle_2, txt_circle_3, txt_circle_4, txt_circle_5;

	Button btn_create_accnt_tab1_1, btn_create_accnt_tab1_2;

	EditText edt_tab3_flname, edt_tab3_flname_last, edt_tab3_address,
			edt_tab3_city, edt_tab3_zip, edt_tab3_phone, edt_tab3_drvrlic,
			edt_create_account_3_secret_question_1,
			edt_create_account_3_secret_question_2,
			edt_create_account_3_secret_question_3, edt_tab3_pin;

	TextView txt_cross;

	LinearLayout ll_create_account_3_secret_question_1,
			ll_create_account_3_secret_question_2,
			ll_create_account_3_secret_question_3, ll_create_account_3_source;

	LinearLayout ll_tab4_chkbox;
	TextView txt_tab3_drvrlic_append_state;

	/* Dropdowns */
	TextView txt_create_account_3_state, txt_changetoll_arrow,
			txt_changetoll_arrow_1, txt_changetoll_arrow_2,
			txt_changetoll_arrow_3, txt_create_account_3_secret_question_1,
			txt_create_account_3_secret_question_2,
			txt_create_account_3_secret_question_3,
			txt_create_account_3_source, txt_changetoll_arrow_source,
			txt_changetoll_arrow_county_3, txt_tab3_county;

	List<String> stateList = null;
	List<String> questionList = null;
	List<String> sourceList = null;
	List<String> countyList = null;
	
	SetupAccountData setupAccountData = new SetupAccountData();

	Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ScreenUtils.fullScreen(this);
		setContentView(R.layout.activity_setup_account);
		context = this;
		init();

		font = ScreenUtils.returnTypeFace(this);
		txt_cross.setTypeface(font);

		TextView txt = (TextView) findViewById(R.id.tv_create_accnt);
		txt.setTypeface(font);

		initFontAwosmeIcons();
		initArrows(layoutTab2, layoutTab3, layoutTab4, layoutTab5);
		initFontAwosmeIconsLayout();
		initEditFields();

		setFontToButton();

		initTab3Widgests();
		initTab4Widgests();
		initTab5Widgets();

		setListenerstoEditFields();

		rel_tab1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toggle1();
			}
		});

		rel_tab2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				toggle2();
			}
		});
		rel_tab3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// toggle3();
				goToTab3();
			}
		});
		rel_tab4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// toggle4();
				goToTab4();
			}
		});
		rel_tab5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// toggle5();
				// goToTab5();
			}
		});

		toggle1();

		// chk_tab4.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// if (chk_tab4.isChecked()) {
		// ll_tab4_chkbox.setVisibility(View.GONE);
		// } else
		// ll_tab4_chkbox.setVisibility(View.VISIBLE);
		// }
		// });

		stateList = Arrays.asList(this.getResources().getStringArray(
				R.array.state_type));
		countyList = Arrays.asList(this.getResources().getStringArray(
				R.array.county_type));
		questionList = Arrays.asList(Utils.QUESTION_LIST);
		sourceList = Arrays.asList(Utils.SOURCE_LIST);

		txt_create_account_3_state.setText(stateList.get(9));
		txt_tab3_county.setText(countyList.get(0));
		txt_create_account_3_secret_question_1.setText(questionList.get(0));
		txt_create_account_3_secret_question_2.setText(questionList.get(1));
		txt_create_account_3_secret_question_3.setText(questionList.get(2));
		txt_create_account_3_source.setText(sourceList.get(0));

		toggle2();
	}

	private void initTab5Widgets() {

	}

	private void initTab4Widgests() {
		// TODO Auto-generated method stub

	}

	private void initTab3Widgests() {
		// TODO Auto-generated method stub
		Typeface typeFace = ScreenUtils.returnHelvetica(this);
		txt_tab3_drvrlic_append_state = (TextView) findViewById(R.id.txt_tab3_drvrlic_append_state);
		edt_tab3_flname = (EditText) findViewById(R.id.edt_tab3_flname);
		edt_tab3_flname_last = (EditText) findViewById(R.id.edt_tab3_flname_last);
		edt_tab3_address = (EditText) findViewById(R.id.edt_tab3_address);
		edt_tab3_city = (EditText) findViewById(R.id.edt_tab3_city);
		edt_tab3_zip = (EditText) findViewById(R.id.edt_tab3_zip);
		txt_tab3_county = (TextView) findViewById(R.id.txt_tab3_county);
		edt_tab3_phone = (EditText) findViewById(R.id.edt_tab3_phone);
		// edt_tab3_country = (EditText) findViewById(R.id.edt_tab3_country);
		edt_tab3_drvrlic = (EditText) findViewById(R.id.edt_tab3_drvrlic);

		txt_changetoll_arrow_1 = (TextView) findViewById(R.id.txt_changetoll_arrow_1);
		txt_changetoll_arrow_2 = (TextView) findViewById(R.id.txt_changetoll_arrow_2);
		txt_changetoll_arrow_3 = (TextView) findViewById(R.id.txt_changetoll_arrow_3);
		txt_changetoll_arrow_county_3 = (TextView) findViewById(R.id.txt_changetoll_arrow_county_3);
		txt_changetoll_arrow_source = (TextView) findViewById(R.id.txt_changetoll_arrow_source);

		txt_create_account_3_secret_question_1 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_1);
		txt_create_account_3_secret_question_2 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_2);
		txt_create_account_3_secret_question_3 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_3);
		txt_create_account_3_source = (TextView) findViewById(R.id.txt_create_account_3_source);

		edt_create_account_3_secret_question_1 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_1);
		edt_create_account_3_secret_question_2 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_2);
		edt_create_account_3_secret_question_3 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_3);
		edt_tab3_pin = (EditText) findViewById(R.id.edt_tab3_pin);

		ll_create_account_3_secret_question_1 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_1);
		ll_create_account_3_secret_question_2 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_2);
		ll_create_account_3_secret_question_3 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_3);
		ll_create_account_3_source = (LinearLayout) findViewById(R.id.ll_create_account_3_source);

		edt_tab3_flname.setTypeface(typeFace);
		edt_tab3_flname_last.setTypeface(typeFace);
		edt_tab3_address.setTypeface(typeFace);
		edt_tab3_city.setTypeface(typeFace);
		edt_tab3_zip.setTypeface(typeFace);
		// edt_tab3_county.setTypeface(typeFace);
		edt_tab3_phone.setTypeface(typeFace);
		// edt_tab3_country.setTypeface(typeFace);
		edt_tab3_drvrlic.setTypeface(typeFace);
		edt_tab3_pin.setTypeface(typeFace);

		txt_create_account_3_secret_question_1.setTypeface(typeFace);
		edt_create_account_3_secret_question_1.setTypeface(typeFace);
		txt_create_account_3_secret_question_2.setTypeface(typeFace);
		edt_create_account_3_secret_question_2.setTypeface(typeFace);
		txt_create_account_3_secret_question_3.setTypeface(typeFace);
		edt_create_account_3_secret_question_3.setTypeface(typeFace);

		txt_create_account_3_source.setTypeface(typeFace);

		txt_changetoll_arrow_1.setTypeface(font);
		txt_changetoll_arrow_2.setTypeface(font);
		txt_changetoll_arrow_3.setTypeface(font);
		txt_changetoll_arrow_source.setTypeface(font);
		txt_changetoll_arrow_county_3.setTypeface(font);
	}

	private void setFontToButton() {
		// TODO Auto-generated method stub
		btn_create_accnt_tab1_1.setTypeface(ScreenUtils
				.returnHelveticaBold(this));
		btn_create_accnt_tab1_2.setTypeface(ScreenUtils
				.returnHelveticaBold(this));
	}

	private void initFontAwosmeIconsLayout() {
		// TODO Auto-generated method stubs
		rel_tab1_1 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab1_1);
		rel_tab1_2 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab1_2);

		rel_tab3_1 = (RelativeLayout) layoutTab3.findViewById(R.id.rel_tab3_1);
		rel_tab3_2 = (RelativeLayout) layoutTab3.findViewById(R.id.rel_tab3_2);
		rel_tab3_3 = (RelativeLayout) layoutTab3.findViewById(R.id.rel_tab3_3);
		rel_tab3_4 = (RelativeLayout) layoutTab3.findViewById(R.id.rel_tab3_4);
	}

	/** Initialize font awesome parameters */
	private void initFontAwosmeIcons() {
		// TODO Auto-generated method stub
		txt_create_account_username = (TextView) layoutTab3
				.findViewById(R.id.txt_create_account_username);
		txt_create_account_username.setTypeface(font);

		txt_create_account_email_id = (TextView) layoutTab3
				.findViewById(R.id.txt_create_account_email_id);
		txt_create_account_email_id.setTypeface(font);

		txt_create_account_pwd = (TextView) layoutTab3
				.findViewById(R.id.txt_create_account_pwd);
		txt_create_account_pwd.setTypeface(font);

		txt_create_account_con_pwd = (TextView) layoutTab3
				.findViewById(R.id.txt_create_account_con_pwd);
		txt_create_account_con_pwd.setTypeface(font);

		txt_create_account_next2 = (TextView) layoutTab3
				.findViewById(R.id.txt_create_account_next2);
		txt_create_account_next2.setTypeface(font);

		txt_create_accountno = (TextView) layoutTab2
				.findViewById(R.id.txt_create_accountno);
		txt_create_account_digit = (TextView) layoutTab2
				.findViewById(R.id.txt_create_account_digit);

		txt_circle_2.setTypeface(font);
		txt_circle_2.setTypeface(font);
		txt_circle_3.setTypeface(font);
		txt_circle_4.setTypeface(font);
		txt_circle_5.setTypeface(font);

		txt_changetoll_arrow.setTypeface(font);

		txt_create_accountno.setTypeface(font);
		txt_create_account_digit.setTypeface(font);

		// txt_changetoll_arrow_4_3.setTypeface(font);
		// txt_changetoll_arrow_4_4.setTypeface(font);
		// txt_changetoll_arrow_5.setTypeface(font);

	}

	private void setListenerstoEditFields() {
		// TODO Auto-generated method stub
		edt_tab2_accno.addTextChangedListener(new Tab2TextWatcher(
				edt_tab2_accno));
		edt_tab2_4digit.addTextChangedListener(new Tab2TextWatcher(
				edt_tab2_4digit));

		edt_tab3_username_sp.addTextChangedListener(new Tab3TextWatcher(
				edt_tab3_username_sp));
		edt_tab3_email_sp.addTextChangedListener(new Tab3TextWatcher(
				edt_tab3_email_sp));
		edt_tab3_pwd_sp.addTextChangedListener(new Tab3TextWatcher(
				edt_tab3_pwd_sp));
		edt_tab3_confpwd_sp.addTextChangedListener(new Tab3TextWatcher(
				edt_tab3_confpwd_sp));

		
		edt_create_account_3_secret_question_1
				.addTextChangedListener(new Tab4TextWatcher(
						edt_create_account_3_secret_question_1));
		edt_create_account_3_secret_question_2
				.addTextChangedListener(new Tab4TextWatcher(
						edt_create_account_3_secret_question_2));
		edt_create_account_3_secret_question_3
				.addTextChangedListener(new Tab4TextWatcher(
						edt_create_account_3_secret_question_3));

	}

	class Tab2TextWatcher implements TextWatcher {

		EditText edt;

		Tab2TextWatcher(EditText edt) {
			this.edt = edt;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

			switch (edt.getId()) {
			case R.id.edt_tab2_accno: {

				validations(edt, rel_tab1_1, s, false);
				break;
			}
			case R.id.edt_tab2_4digit: {
				validations4digit(edt, rel_tab1_2, s, false);
				break;
			}
			default:
				break;
			}

		}

	}

	/**
	 * When an object of a type is attached to an Editable, its methods will be
	 * called when the text is changed.
	 */
	class Tab3TextWatcher implements TextWatcher {

		EditText edt;

		Tab3TextWatcher(EditText edt) {
			this.edt = edt;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

			switch (edt.getId()) {
			case R.id.edt_tab3_username_sp: {

				validations(edt, rel_tab3_1, s, false);
				break;
			}
			case R.id.edt_tab3_email_sp: {
				validations(edt, rel_tab3_2, s, true);
				break;
			}
			case R.id.edt_tab3_pwd_sp: {

				validations(edt, rel_tab3_3, s, false);
				methodConfirmPwd();
				break;
			}

			case R.id.edt_tab3_confpwd_sp: {

				methodConfirmPwd();
				break;
			}

			default:
				break;
			}

		}

		void methodConfirmPwd() {
			int val[] = ScreenUtils.getPaddingVal(edt_tab3_pwd_sp);
			if (RuleBook.isConfirmPasswordValid(edt_tab3_pwd_sp.getText()
					.toString(), edt_tab3_confpwd_sp.getText().toString())) {
				changeBgTextField(rel_tab3_4, R.color.green);
				edt_tab3_confpwd_sp
						.setBackgroundResource(R.drawable.grey_border);
			} else {
				changeBgTextField(rel_tab3_4, R.color.grey);
				edt_tab3_confpwd_sp
						.setBackgroundResource(R.drawable.edit_warning_border);
			}

			edt_tab3_confpwd_sp.setPadding(val[0], val[1], val[2], val[3]);
		}

	}

	class Tab4TextWatcher implements TextWatcher {

		EditText edt;

		Tab4TextWatcher(EditText edt) {
			this.edt = edt;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			validations(edt, null, s, false);

		}

	}

	// class Tab3TextWatcher implements TextWatcher {
	//
	// EditText edt;
	//
	// Tab3TextWatcher(EditText edt) {
	// this.edt = edt;
	// }
	//
	// @Override
	// public void afterTextChanged(Editable s) {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence s, int start, int count,
	// int after) {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void onTextChanged(CharSequence s, int start, int before,
	// int count) {
	// // TODO Auto-generated method stub
	//
	// validations(edt, null, s, false);
	//
	// }
	//
	// }

	void changeBgTextField(View tv, int color) {
		tv.setBackgroundColor(getResources().getColor(color));
		tv.invalidate();
	}

	private void initEditFields() {
		// TODO Auto-generated method stub
		edt_tab2_accno = (EditText) layoutTab2
				.findViewById(R.id.edt_tab2_accno);
		edt_tab2_4digit = (EditText) layoutTab2
				.findViewById(R.id.edt_tab2_4digit);

		edt_tab3_username_sp = (EditText) layoutTab3
				.findViewById(R.id.edt_tab3_username_sp);
		edt_tab3_email_sp = (EditText) layoutTab3
				.findViewById(R.id.edt_tab3_email_sp);
		edt_tab3_pwd_sp = (EditText) layoutTab3
				.findViewById(R.id.edt_tab3_pwd_sp);
		edt_tab3_confpwd_sp = (EditText) layoutTab3
				.findViewById(R.id.edt_tab3_confpwd_sp);
	}


	public static boolean isValidPassword(final String password) {

		Pattern pattern;
		Matcher matcher;
		final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);

		return matcher.matches();

	}

	private boolean validations4digit(EditText edt, RelativeLayout rel,
			CharSequence s, boolean type) {
		// TODO Auto-generated method stub
		int val[] = ScreenUtils.getPaddingVal(edt);
		boolean result = false;
		if (type)
			result = RuleBook.isEmailValid(s.toString());
		else
			result = RuleBook.isPinValid(s.toString());
		if (result) {
			if (rel != null)
				changeBgTextField(rel, R.color.green);
			edt.setBackgroundResource(R.drawable.grey_border);
		} else {
			if (rel != null)
				changeBgTextField(rel, R.color.grey);
			edt.setBackgroundResource(R.drawable.edit_warning_border);
		}

		edt.setPadding(val[0], val[1], val[2], val[3]);
		return result;
	}

	private boolean validations(EditText edt, RelativeLayout rel,
			CharSequence s, boolean type) {
		// TODO Auto-generated method stub
		int val[] = ScreenUtils.getPaddingVal(edt);
		boolean result = false;
		if (type)
			result = RuleBook.isEmailValid(s.toString());
		else
			result = RuleBook.isUserNameValid(s.toString());
		if (result) {
			if (rel != null)
				changeBgTextField(rel, R.color.green);
			edt.setBackgroundResource(R.drawable.grey_border);
		} else {
			if (rel != null)
				changeBgTextField(rel, R.color.grey);
			edt.setBackgroundResource(R.drawable.edit_warning_border);
		}
		if(edt!=null&& edt.getId()==R.id.edt_tab3_pwd_sp)
			result = RuleBook.isPasswordValid(s.toString());
		
		if(edt!=null&& edt.getId()==R.id.edt_tab3_confpwd_sp)
			result  = RuleBook.isConfirmPasswordValid(getTextVal(edt_tab3_pwd_sp), getTextVal(edt_tab3_confpwd_sp));
		
		
		if(edt!=null&& edt.getId()==R.id.edt_create_account_3_secret_question_1)
			result = RuleBook.isSecQuestion(s.toString());
		if(edt!=null&& edt.getId()==R.id.edt_create_account_3_secret_question_2)
			result = RuleBook.isSecQuestion(s.toString());
		if(edt!=null&& edt.getId()==R.id.edt_create_account_3_secret_question_3)
			result = RuleBook.isSecQuestion(s.toString());
		edt.setPadding(val[0], val[1], val[2], val[3]);
		return result;
	}

	public void onclickNextButtons(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {

		case R.id.btn_create_accnt_tab1_2:
			startActivity(new Intent(SetupAccountActivity.this,
					CreateAccountActivity.class));

			break;
		case R.id.btn_create_accnt_tab1_1:
			// startActivity(new Intent(CreateAccountActivity.this,
			// LoginActivity.class));
			// finish();
			toggle2();
			break;
		case R.id.ll_tab2_nxt_btn:

			// goToTab3();
			goToTab3();
			break;
		case R.id.ll_tab3_nxt_btn:
			// toggle4();
			// goToTab4();
			goToTab4();
			break;
		case R.id.ll_tab4_nxt_btn:
			// toggle5();
			// goToTab5();
			boolean checkError = checkTab4();
			if (checkError) {

			} else {
				
				setupAccountData.setSecQuestion1(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_1)));
				setupAccountData.setSecAnswer1(getEdtVal(edt_create_account_3_secret_question_1));
				
				setupAccountData.setSecQuestion2(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_2)));
				setupAccountData.setSecAnswer2(getEdtVal(edt_create_account_3_secret_question_2));
				
				setupAccountData.setSecQuestion3(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_3)));
				setupAccountData.setSecAnswer3(getEdtVal(edt_create_account_3_secret_question_3));
				
				new AsyncTask<SetupAccountData, String, String>(){
					ProgressDialog pdialog = ScreenUtils
							.returnProgDialogObj(
									context,
									WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
									false);
					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						try {
							pdialog.show();	
						} catch (Exception e) {
							// TODO: handle exception
						}
						super.onPreExecute();
					}
					@Override
					protected String doInBackground(SetupAccountData... params) {
						// TODO Auto-generated method stub
						try {
							String requestBody = WebserviceUtils.readFromAsset("setupOnlineAccess", context);
							requestBody=requestBody.replaceAll("_accountId_", params[0].getAccountId());
				            requestBody=requestBody.replaceAll("_PIN_", params[0].getPIN());
				            requestBody=requestBody.replaceAll("_userId_", params[0].getUserId());
				            requestBody=requestBody.replaceAll("_pw_", params[0].getPw());
				            requestBody=requestBody.replaceAll("_secQuestion1_", params[0].getSecQuestion1()+"");
				            requestBody=requestBody.replaceAll("_secAnswer1_", params[0].getSecAnswer1());
				            requestBody=requestBody.replaceAll("_secQuestion2_", params[0].getSecQuestion2()+"");
				            requestBody=requestBody.replaceAll("_secAnswer2_", params[0].getSecAnswer2());
				            requestBody=requestBody.replaceAll("_secQuestion3_", params[0].getSecQuestion3()+"");
				            requestBody=requestBody.replaceAll("_secAnswer3_", params[0].getSecAnswer3());
							
							Log.e("peachpass", requestBody+"----requestBody");
							String result = WebserviceUtils.sendSoapRequest(context, requestBody, WebserviceUtils.SOAP_ACTION_SETUPONLINEACCESS);
							XMLParser parser = new XMLParser();
							String xml = result;
							Document doc = parser.getDomElement(xml); // getting DOM element
							NodeList nl = doc.getElementsByTagName("result");
							for (int i = 0; i < nl.getLength();) {

								Element e = (Element) nl.item(i);
								String status = parser.getValue(e, "ns1:status"); 
								Log.e("peachpass", "status--" + status);
								return status;
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						return null;
					}
					@Override
					protected void onPostExecute(String result) {
						try {
							pdialog.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
							if(result!=null){
								
								if(result.equals("0")){
									Intent i2 = new Intent(SetupAccountActivity.this,
											LoginActivity.class);
									i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(i2);
									ScreenUtils
											.raiseToast(
													SetupAccountActivity.this,
													"Your account has been created. You will receive an email confirmation shortly. You can now login using your username and password!");
								}else{
									String errorStatus = Utils
			                                .getStringResourceByName("status_"
			                                        + result, context);
			                        ScreenUtils.raiseToast(context, errorStatus);
								}
								
							}else{
								
								ScreenUtils.raiseToast(context, WebserviceUtils.SOMETHING_WENT_WRONG);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						super.onPostExecute(result);
					};
					
				}.execute(new SetupAccountData[]{setupAccountData});
			}

			break;
		case R.id.ll_tab5_nxt_btn:
			// finish();
			// goToTabLast();
			break;

		default:
			break;
		}

	}

	private void initArrows(View viewTab2, View viewTab3, View viewTab4,
			View viewTab5) {
		// TODO Auto-generated method stub
		txt_next2 = (TextView) viewTab2
				.findViewById(R.id.txt_create_account_next2);
		txt_next3 = (TextView) layoutTab4
				.findViewById(R.id.txt_create_account_next3);
		txt_next4 = (TextView) viewTab4
				.findViewById(R.id.txt_create_account_next4);
		txt_next5 = (TextView) viewTab5
				.findViewById(R.id.txt_create_account_next5);

		setArrowFonts(txt_next2, txt_next3, txt_next4, txt_next5);
	}

	private void setArrowFonts(TextView txt_next2, TextView txt_next3,
			TextView txt_next4, TextView txt_next5) {
		// TODO Auto-generated method stub
		txt_next2.setTypeface(font);
		txt_next3.setTypeface(font);
		// txt_next4.setTypeface(font);
		// txt_next5.setTypeface(font);
	}

	protected void toggle1() {
		// TODO Auto-generated method stub
		layoutTab1.setVisibility(View.VISIBLE);
		layoutTab2.setVisibility(View.GONE);
		layoutTab3.setVisibility(View.GONE);
		layoutTab4.setVisibility(View.GONE);
		layoutTab5.setVisibility(View.GONE);

		circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
	}

	protected void toggle2() {
		// TODO Auto-generated method stub
		layoutTab1.setVisibility(View.GONE);
		layoutTab2.setVisibility(View.VISIBLE);
		layoutTab3.setVisibility(View.GONE);
		layoutTab4.setVisibility(View.GONE);
		layoutTab5.setVisibility(View.GONE);

		if (RuleBook.allTabFieldsValid(new View[] { rel_tab1_1, rel_tab1_2 }))
			circletab2
					.setBackgroundResource(R.drawable.shape_circle_green_color);
		else
			circletab2
					.setBackgroundResource(R.drawable.shape_circle_blue_color);

		circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
	}

	protected void toggle3() {
		// TODO Auto-generated method stub
		layoutTab1.setVisibility(View.GONE);
		layoutTab2.setVisibility(View.GONE);
		layoutTab3.setVisibility(View.VISIBLE);
		layoutTab4.setVisibility(View.GONE);
		layoutTab5.setVisibility(View.GONE);

		// if (RuleBook.allTabFieldsValid(new View[] { rel_tab2_1, rel_tab2_2,
		// rel_tab2_3, rel_tab2_4 }))
		{
			circletab2
					.setBackgroundResource(R.drawable.shape_circle_green_color);
			txt_circle_2.setText(getResources().getString(R.string.fa_check));
		}
		// else {
		// circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
		// txt_circle_2.setText("2");
		// }
		circletab3.setBackgroundResource(R.drawable.shape_circle_blue_color);
		// circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
		// circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
	}

	protected void toggle4() {
		// TODO Auto-generated method stub
		layoutTab1.setVisibility(View.GONE);
		layoutTab2.setVisibility(View.GONE);
		layoutTab3.setVisibility(View.GONE);
		layoutTab4.setVisibility(View.VISIBLE);
		layoutTab5.setVisibility(View.GONE);

		// circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
		// circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab4.setBackgroundResource(R.drawable.shape_circle_blue_color);
		// circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
	}

	protected void toggle5() {
		// TODO Auto-generated method stub
		layoutTab1.setVisibility(View.GONE);
		layoutTab2.setVisibility(View.GONE);
		layoutTab3.setVisibility(View.GONE);
		layoutTab4.setVisibility(View.GONE);
		layoutTab5.setVisibility(View.VISIBLE);

		// circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
		// circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
		// circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
		circletab5.setBackgroundResource(R.drawable.shape_circle_blue_color);
	}

	private void init() {
		// TODO Auto-generated method stub

		txt_cross = (TextView) findViewById(R.id.txt_cross);

		circletab1 = findViewById(R.id.view_tab1);
		circletab2 = findViewById(R.id.view_tab2);
		circletab3 = findViewById(R.id.view_tab3);
		circletab4 = findViewById(R.id.view_tab4);
		circletab5 = findViewById(R.id.view_tab5);

		layoutTab1 = findViewById(R.id.layout_create_accnt_tab1);
		layoutTab2 = findViewById(R.id.layout_create_accnt_tab2);
		layoutTab3 = findViewById(R.id.layout_create_accnt_tab3);
		layoutTab4 = findViewById(R.id.layout_create_accnt_tab4);
		layoutTab5 = findViewById(R.id.layout_create_accnt_tab5);

		rel_tab1 = (RelativeLayout) findViewById(R.id.rel_tab1);
		rel_tab2 = (RelativeLayout) findViewById(R.id.rel_tab2);
		rel_tab3 = (RelativeLayout) findViewById(R.id.rel_tab3);
		rel_tab4 = (RelativeLayout) findViewById(R.id.rel_tab4);
		rel_tab5 = (RelativeLayout) findViewById(R.id.rel_tab5);

		txt_circle_2 = (TextView) findViewById(R.id.txt_circle_2);
		txt_circle_3 = (TextView) findViewById(R.id.txt_circle_3);
		txt_circle_4 = (TextView) findViewById(R.id.txt_circle_4);
		txt_circle_5 = (TextView) findViewById(R.id.txt_circle_5);

		btn_create_accnt_tab1_1 = (Button) findViewById(R.id.btn_create_accnt_tab1_1);
		btn_create_accnt_tab1_2 = (Button) findViewById(R.id.btn_create_accnt_tab1_2);

		// chk_tab4 = (CheckBox) findViewById(R.id.chk_tab4);
		ll_tab4_chkbox = (LinearLayout) findViewById(R.id.ll_tab4_chkbox);

		txt_create_account_3_state = (TextView) findViewById(R.id.txt_create_account_3_state);

		txt_changetoll_arrow = (TextView) findViewById(R.id.txt_changetoll_arrow);

	}

	/** click event to direct user to Login screen */
	public void onClickBack(View view) {

		// startActivity(new Intent(CreateAccountActivity.this,
		// LoginActivity.class));
		finish();
		// onBackPressed();
	}

	/** returns value of the edit field */
	CharSequence getEdtVale(EditText edt) {
		return edt.getText().toString();
	}

	public void onClickShowDropdown(View view) {
		switch (view.getId()) {
		case R.id.ll_create_account_3_state:

			final CharSequence[] array_1 = stateList
					.toArray(new CharSequence[stateList.size()]);
			ScreenUtils.showMultipleChoiceDlg(array_1, this,
					txt_create_account_3_state, "state", this,
					txt_tab3_drvrlic_append_state);

			break;
		case R.id.ll_create_account_3_county:

			final CharSequence[] array_county = countyList
					.toArray(new CharSequence[countyList.size()]);
			ScreenUtils.showMultipleChoiceDlg(array_county, this,
					txt_tab3_county, "county", this, null);

			break;

		case R.id.ll_create_account_3_secret_question_1:
			final CharSequence[] array_7 = questionList
					.toArray(new CharSequence[questionList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array_7, this,
					txt_create_account_3_secret_question_1, "Secret Question",
					this, null);
			break;
		case R.id.ll_create_account_3_secret_question_2:
			final CharSequence[] array_8 = questionList
					.toArray(new CharSequence[questionList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array_8, this,
					txt_create_account_3_secret_question_2, "Secret Question",
					this, null);
			break;
		case R.id.ll_create_account_3_secret_question_3:
			final CharSequence[] array_9 = questionList
					.toArray(new CharSequence[questionList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array_9, this,
					txt_create_account_3_secret_question_3, "Secret Question",
					this, null);
			break;
		case R.id.ll_create_account_3_source:
			final CharSequence[] array_10 = sourceList
					.toArray(new CharSequence[sourceList.size()]);

			ScreenUtils.showMultipleChoiceDlg(array_10, this,
					txt_create_account_3_source, "Source", this, null);
			break;

		default:
			break;
		}
	}

	private void goToTab3() {
		if (RuleBook.allTabFieldsValid(new View[] { rel_tab1_1, rel_tab1_2 })){
			setupAccountData.setAccountId(getEdtVal(edt_tab2_accno));
			setupAccountData.setPIN(getEdtVal(edt_tab2_4digit));
			toggle3();
		}
		else {
			// edt_tab2_accno,edt_tab2_4digit
			validations(edt_tab2_accno, rel_tab1_1, getEdtVale(edt_tab2_accno),
					false);
			validations4digit(edt_tab2_4digit, rel_tab1_2,
					getEdtVale(edt_tab2_4digit), false);

			if (RuleBook
					.allTabFieldsValid(new View[] { rel_tab1_1, rel_tab1_2 })) {
				circletab2
						.setBackgroundResource(R.drawable.shape_circle_green_color);
				txt_circle_2.setText(getResources()
						.getString(R.string.fa_check));
			} else {
				circletab2
						.setBackgroundResource(R.drawable.shape_circle_blue_color);
				txt_circle_2.setText("2");
			}
		}
	}

	private void goToTab4() {

		boolean checkError = checkTab3();

		if (!checkError) {
			toggle4();

			circletab3
					.setBackgroundResource(R.drawable.shape_circle_green_color);
			txt_circle_3.setText(getResources().getString(R.string.fa_check));
		}

		if (RuleBook.allTabFieldsValid(new View[] { rel_tab3_1, rel_tab3_2,
				rel_tab3_3, rel_tab3_4 })){
			
			setupAccountData.setUserId(getEdtVal(edt_tab3_username_sp));
			setupAccountData.setPw(getEdtVal(edt_tab3_pwd_sp));
			toggle4();
		}
		else {
			validations(edt_tab3_username_sp, rel_tab3_1,
					getEdtVale(edt_tab3_username_sp), false);
			validations(edt_tab3_email_sp, rel_tab3_2,
					getEdtVale(edt_tab3_email_sp), true);
			validations(edt_tab3_pwd_sp, rel_tab3_3,
					getEdtVale(edt_tab3_pwd_sp), false);
			validations(edt_tab3_confpwd_sp, rel_tab3_4,
					getEdtVale(edt_tab3_confpwd_sp), false);

			if (RuleBook.allTabFieldsValid(new View[] { rel_tab3_1, rel_tab3_2,
					rel_tab3_3, rel_tab3_4 })) {
				circletab3
						.setBackgroundResource(R.drawable.shape_circle_green_color);
				txt_circle_3.setText(getResources()
						.getString(R.string.fa_check));
			} else {
				circletab3
						.setBackgroundResource(R.drawable.shape_circle_blue_color);
				txt_circle_3.setText("3");
			}
		}
	}

	private boolean checkTab3() {
		EditText editFields[] = new EditText[] { edt_tab3_username_sp,
				edt_tab3_email_sp, edt_tab3_pwd_sp, edt_tab3_confpwd_sp };
		boolean checkError = false;

		for (EditText edt : editFields) {
			validations(edt, null, getEdtVale(edt), false);
		}

		for (EditText edt : editFields) {
			if (!validations(edt, null, getEdtVale(edt), false)) {
				checkError = true;
				break;
			}
		}
		return checkError;
	}

	private boolean checkTab4() {
		EditText editFields[] = new EditText[] {
				edt_create_account_3_secret_question_1,
				edt_create_account_3_secret_question_2,
				edt_create_account_3_secret_question_3 };
		boolean checkError = false;

		for (EditText edt : editFields) {
			validations(edt, null, getEdtVale(edt), false);
		}

		for (EditText edt : editFields) {
			if (!validations(edt, null, getEdtVale(edt), false)) {
				checkError = true;
				break;
			}
		}
		return checkError;
	}
	
	String getEdtVal(EditText edt){
		return edt.getText().toString().trim();
	}
	String getTextVal(TextView textView){
		return textView.getText().toString();
	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(context).reportActivityStart(this);

		FlurryAgent.onStartSession(context);
		FlurryAgent.setLogEvents(true);

		FlurryAgent.logEvent(Utils.SETUP_ACCOUNT);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

		FlurryAgent.onEndSession(context);
	}

}
