package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.srta.PeachPass.R;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;


public class AccountTypeDiscription extends Activity {


    TextView tv_selectaccounttype;
    ImageView iv_close;
    Context context;
    TextView pdfView;
    Button cancelbutton, nextbutton;
    int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accounttypediscription);
        context = this;
        tv_selectaccounttype = (TextView) findViewById(R.id.tv_selectaccounttype);
        cancelbutton = (Button) findViewById(R.id.cancelbutton);
        nextbutton = (Button) findViewById(R.id.nextbtn);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        WebView webView1 = (WebView) findViewById(R.id.webview);
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                value = getIntent().getExtras().getInt("id");
            }
        }


        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                if ("personal".equalsIgnoreCase(getIntent().getExtras().getString("personaltoll"))) {
                    tv_selectaccounttype.setText("Personal Toll Account");

                    webView1.getSettings().setJavaScriptEnabled(true);
                    webView1.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView1.setWebViewClient(new Callback());

                    String pdfURL = "https://www.mypeachpass.com/olcsc/preprinted/CustomerAgreementEnglish.pdf";
                    webView1.loadUrl(
                            "http://docs.google.com/gview?embedded=true&url=" + pdfURL);


                } else if ("commercial".equalsIgnoreCase(getIntent().getExtras().getString("commecialtoll"))) {
                    tv_selectaccounttype.setText("Commercial Toll Account");
                    webView1.getSettings().setJavaScriptEnabled(true);
                    webView1.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView1.setWebViewClient(new Callback());

                    String pdfURL = "https://www.mypeachpass.com/olcsc/preprinted/CustomerAgreementEnglish.pdf";
                    webView1.loadUrl(
                            "http://docs.google.com/gview?embedded=true&url=" + pdfURL);

                } else if ("exempt".equalsIgnoreCase(getIntent().getExtras().getString("exempttoll"))) {
                    tv_selectaccounttype.setText("Exempt Toll Account");
                    webView1.getSettings().setJavaScriptEnabled(true);
                    webView1.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView1.setWebViewClient(new Callback());

                    String pdfURL = "https://www.mypeachpass.com/olcsc/preprinted/CustomerAgreementEnglish.pdf";
                    webView1.loadUrl(
                            "http://docs.google.com/gview?embedded=true&url=" + pdfURL);


                }
            }


        }

        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, CreateAccountActivity.class);
                in.putExtra("id", value);
                startActivity(in);

            }
        });

        cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }
}


