package com.peachpass.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnCloseListener;
import com.peachpass.fragments.LeftMenuFragment;
import com.peachpass.fragments.MyPeachPassFragment;
import com.peachpass.fragments.NotificationMenuFragment;
import com.peachpass.slidingmenu.BaseActivity;
import com.peachpass.utils.Cache;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

/*FragmentChangeActivity acts like a base for all the fragments viz. MyPeachPass, Transaction, Change Toll Mode, Sliding Menu*/
public class FragmentChangeActivity extends BaseActivity implements
        OnCloseListener {

    private Fragment mContent;
    FragmentManager frgmanager;

    TextView txt_bubble;
    View view_bubble;
    View mCustomView;

    ActionBar mActionBar;
    Context context;
    Activity activity;


    public FragmentChangeActivity() {
        super( R.string.changing_fragments);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        // set the Above View

        // if (savedInstanceState != null)
        // mContent = getSupportFragmentManager().getFragment(
        // savedInstanceState, "mContent");
        if (mContent == null)
            mContent = new MyPeachPassFragment(R.color.red); // MyPeachPassFragment

        frgmanager = getSupportFragmentManager();
        // set the Above View
        setContentView(R.layout.content_frame);
        frgmanager.beginTransaction().replace(R.id.content_frame, mContent)
                .commit();

        // if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // }

        // set the Behind View
        setBehindContentView(R.layout.menu_frame);
        frgmanager.beginTransaction()
                .replace(R.id.menu_frame, new LeftMenuFragment()).commit(); // LeftMenuFragment

        // set right side setSecondaryMenu
        getSlidingMenu().setSecondaryMenu(R.layout.menu_frame_two);
        getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
        frgmanager.beginTransaction()
                .replace(R.id.menu_frame_two, new NotificationMenuFragment()) // NotificationMenuFragment
                .commit();

        // customize the SlidingMenu
        getSlidingMenu().setMode(SlidingMenu.LEFT_RIGHT);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        frgmanager
                .addOnBackStackChangedListener(new OnBackStackChangedListener() {

                    @Override
                    public void onBackStackChanged() {
                        // TODO Auto-generated method stub
                        Log.e("peachpass",
                                "addOnBackStackChangedListener called");

                    }
                });

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        mCustomView = mInflater.inflate(R.layout.action_bar, null);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("fragchangectivity"));

        changeTitle("MY PEACH PASS", null, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        frgmanager.putFragment(outState, "mContent", mContent);
    }

    /*
     * switchContent changes the content of this base activity everytime user
     * selects a new fragment
     */
    public void switchContent(Fragment fragment, String title,
                              ScrollView scroll_view, boolean flag, boolean fromSlide) {
        // Log.e("newlog", "fragment--" + fragment + "title--" + title);
        if (flag) {

            if (title != null && title.contains("MY PROFILE")
                    && scroll_view == null) {
                mContent = fragment;
                FragmentTransaction ft = frgmanager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left);
                ft.replace(R.id.content_frame, mContent);
                // ft.addToBackStack(null);
                ft.commit();
                getSlidingMenu().showContent();

                changeTitle(title, scroll_view, fromSlide);
            } else {
                try {
                    mContent = fragment;
                    FragmentTransaction ft = frgmanager.beginTransaction();

                    ft.replace(R.id.content_frame, mContent);
                    // ft.addToBackStack(null);
                    ft.commit();
                    getSlidingMenu().showContent();

                    changeTitle(title, scroll_view, fromSlide);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } else {

            showSecondaryMenu();
        }

    }

    // public void switchContentForEditVechile(Fragment fragment, String title,
    // ScrollView scroll_view, boolean flag) {
    // // Log.e("newlog", "fragment--" + fragment + "title--" + title);
    // if (flag) {
    //
    // if (title != null && title.contains("My Profile")
    // && scroll_view == null) {
    // mContent = fragment;
    // FragmentTransaction ft = getSupportFragmentManager()
    // .beginTransaction();
    // ft.setCustomAnimations(R.anim.slide_in_right,
    // R.anim.slide_out_left);
    // ft.replace(R.id.content_frame, mContent).commit();
    // getSlidingMenu().showContent();
    //
    // changeTitle(title, scroll_view);
    // } else {
    //
    // mContent = fragment;
    // getSupportFragmentManager().beginTransaction()
    // .replace(R.id.content_frame, mContent).addToBackStack("tag").commit();
    // getSlidingMenu().showContent();
    //
    // changeTitle(title, scroll_view);
    // }
    // } else
    // showSecondaryMenu();
    //
    // }

    /*
     * after changing the content ,changeTitle() sets the title on the action
     * bar for the selected fragment
     */
    private void changeTitle(String title, final ScrollView scroll_view,
                             final boolean fromSlide) {
        // TODO Auto-generated method stub

        TextView mTitleTextView = (TextView) mCustomView
                .findViewById(R.id.title_text);
        mTitleTextView.setText(title);

        ImageView notificationIcon = (ImageView) mCustomView
                .findViewById(R.id.imageView2);
        notificationIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                showSecondaryMenu() ;
            }
        });

        ImageView feedback=(ImageView) mCustomView.findViewById(R.id.feedback);

        feedback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL="https://www.peachpass.com/mobileapp-support/";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                startActivity(browserIntent);
            }
        });

        ImageView homeIcon = (ImageView) mCustomView
                .findViewById(R.id.imageView1);
        if (!fromSlide) {
            homeIcon.setImageResource(R.drawable.slide_menu);// slide_back
        } else {
            homeIcon.setImageResource(R.drawable.slide_menu);
        }
        homeIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (scroll_view != null)
                    scroll_view.scrollTo(0, 0);

                if (fromSlide) {
                    showMenu();
                } else {
                    Log.e("peachpass", "onBackPressed called");
                    Cache.fromBack = true;
                    // changeTitle(title, scroll_view, fromSlide);

                    // onBackPressed();
                    showMenu();
                }
            }
        });

        txt_bubble = (TextView) mCustomView
                .findViewById(R.id.txt_bubble);
        view_bubble = mCustomView
                .findViewById(R.id.view_bubble);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // getSupportFragmentManager().ge
        // try {
        // int sizeOfList = Cache.getInstance().getFragmentTitle().size();
        // if(sizeOfList>0){
        // FragmentTitle fragmentTitle =
        // Cache.getInstance().getFragmentTitle().get(sizeOfList-1);
        // changeTitle(fragmentTitle.getLabel(), null,
        // fragmentTitle.isFromSlide());
        // Cache.getInstance().getFragmentTitle().remove(sizeOfList-1);
        // }
        // } catch (Exception e) {
        // // TODO: handle exception
        // }

    }

    // @Override
    // public void onBackPressed() {
    // // TODO Auto-generated method stub
    // super.onBackPressed();
    // // Intent intent = new Intent("custom-event-name");
    // // // You can also include some extra data.
    // // intent.putExtra("message", "This is my message!");
    // // LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    // }
    // @Override
    // public void onBackPressed() {
    // if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
    // getSupportFragmentManager().popBackStack();
    // return;
    // }
    //
    // super.onBackPressed();
    // }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        // TODO Auto-generated method stub
        super.onActivityResult(arg0, arg1, arg2);
    }

    @Override
    public void onClose() {
        // TODO Auto-generated method stub

    }

    protected void setNumbersToNitifications() {
        // TODO Auto-generated method stub
        try {
            txt_bubble = (TextView) mCustomView
                    .findViewById(R.id.txt_bubble);
            view_bubble = mCustomView
                    .findViewById(R.id.view_bubble);
            String getUnreadCount = SharedPreferenceHelper.getPreference(this,
                    SharedPreferenceHelper.UNREADNOTIFICATIONS);
            Log.e("peachpass", getUnreadCount + "------getUnreadCount");
            int count = 0;
            try {
                count = Integer.parseInt(getUnreadCount);
            } catch (Exception e) {
                // TODO: handle exception
                count = 0;
            }
            try {
                if (count == 0) {
                    Log.e("peachpass", count + "------count1f");
                    view_bubble.setVisibility(View.GONE);
                    txt_bubble.setVisibility(View.GONE);
                } else {
                    Log.e("peachpass", count + "------count2f");
                    view_bubble.setVisibility(View.VISIBLE);
                }

                if (count > 9) {
                    Log.e("peachpass", count + "------count3f");
                    txt_bubble.setText("9");
                } else {
                    Log.e("peachpass", count + "------count4f");
                    txt_bubble.setText(count + "");
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            if (message != null && message.equals("updatebubble")) {
                try {
                    setNumbersToNitifications();
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }

}
