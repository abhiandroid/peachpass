package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

public class ContactUsActivity extends Activity {

    TextView txt_create_account_next5;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);

        setContentView(R.layout.contact_us_fragment);

        init();

        Typeface font = ScreenUtils.returnTypeFace(this);
        txt_create_account_next5.setTypeface(font);

    }

    private void init() {
        // TODO Auto-generated method stub
        txt_create_account_next5 = (TextView) findViewById(R.id.txt_create_account_next5);
    }

    public void onclickNextButtons(View view) {
        finish();
    }

    public void onClickBack(View view) {
        onBackPressed();

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.CONTACT_US);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }
}
