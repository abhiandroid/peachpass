package com.peachpass.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.srta.PeachPass.R;

/**
 * Created by NabeelRangari on 4/20/17.
 */

public class CreateAccountInfoActivity extends Activity {

    TextView tv_points_1, tv_points_2, tv_points_3, tv_points_4,txt_cross;
    Button bt_start_now, bt_coonect_existing;
    Context context;
    Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtils.fullScreen(this);
        activity = this;
        setContentView(R.layout.activity_create_account_info);
        context = this;

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        tv_points_1 = (TextView) findViewById(R.id.tv_point_1);
        tv_points_2 = (TextView) findViewById(R.id.tv_point_2);
        tv_points_3 = (TextView) findViewById(R.id.tv_point_3);
        tv_points_4 = (TextView) findViewById(R.id.tv_point_4);

        bt_coonect_existing = (Button) findViewById(R.id.bt_coonect_existing);
        bt_start_now = (Button) findViewById(R.id.bt_start_now);

        String txt1_1 = "Create account at ";

        String txt1_2 = "mypeachpass.com";

        String txt1_3 = " on mobile or desktop.";

        String txt2_1 = "\"Connect”";

        String txt2_2 = " your account on the login screen of this app.";

        SpannableStringBuilder builder1 = new SpannableStringBuilder();

        SpannableString str1 = new SpannableString(txt1_1);
        str1.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, str1.length(), 0);
        builder1.append(str1);

        SpannableString str2 = new SpannableString(txt1_2);
        str2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 0, str2.length(), 0);
        str2.setSpan(new StyleSpan(Typeface.BOLD), 0, txt1_2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder1.append(str2);

        SpannableString str3 = new SpannableString(txt1_3);
        str3.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, str3.length(), 0);
        builder1.append(str3);

        tv_points_1.setText(builder1, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder builder2 = new SpannableStringBuilder();

        SpannableString str4 = new SpannableString(txt2_1);
        str4.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 0, str4.length(), 0);
        str4.setSpan(new StyleSpan(Typeface.BOLD), 0, txt2_1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder2.append(str4);

        SpannableString str5 = new SpannableString(txt2_2);
        str5.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, str5.length(), 0);
        builder2.append(str5);

        tv_points_4.setText(builder2, TextView.BufferType.SPANNABLE);


    }

    public void onClickBack(View view) {

//		startActivity(new Intent(CreateAccountActivity.this,
//				LoginActivity.class));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                "Are you sure you want to cancel?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                finish();
                                Intent intent = new Intent(context,
                                        LoginScreenActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void onClickOpenBrowser(View view) {
//       Intent intent = new Intent(context, CreateAccountActivity.class);
//        startActivity(intent);
//
        Utils.openLinkInBrowser(WebserviceUtils.CREATE_ACCOUNT_LINK,
                this);
    }

    public void onClickConnect(View view){
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_point_1:
                Utils.openLinkInBrowser(WebserviceUtils.WEBSITE_LINK,
                        this);
                break;
            case R.id.bt_coonect_existing:
                intent = new Intent(context, SetupAccountNewActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(context, LoginScreenActivity.class);
        startActivity(intent);
        finish();
    }
}