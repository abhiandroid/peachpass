package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.ViolationDetailData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

import java.text.DecimalFormat;

public class ViolationDetailActivity extends Activity {

    TextView txt_alert_icon;
    ViolationDetailData data;
    Context context;
    TextView txt_mpp_month, txt_mpp_date, txt_mpp_line1, txt_mpp_line2,
            txt_licplate, txt_datetime, txt_lane, txt_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);

        setContentView(R.layout.violation_detail_item_transaction);
        data = (ViolationDetailData) getIntent().getSerializableExtra("value");
        init();

        Typeface font = ScreenUtils.returnTypeFace(this);
        txt_alert_icon.setTypeface(font);

        setText(data);

    }

    private void setText(ViolationDetailData data) {
        // TODO Auto-generated method stub
        txt_mpp_month.setText(data.getMonth());
        txt_mpp_date.setText(data.getDay());
        txt_mpp_line1.setText(data.getTxnLane());
        txt_mpp_line2.setText(data.getLaneType() + " | " + data.getTime());
        txt_licplate.setText(data.getLicPlate());
        txt_datetime.setText(data.getDateTime());
        txt_lane.setText(data.getTxnLane());
        txt_amount.setText(data.getAmount());

        try {
            boolean isPositive = false;
            String currency = data.getAmount();
            if (currency.contains("-"))
                isPositive = false;
            else isPositive = true;
            currency = currency.replaceAll("\\-", "");
            currency = currency.replaceAll("\\$", "");
            if (currency.contains(".")) {

            } else {
                currency = currency + ".";
            }
            currency = currency + "0000";
            Log.e("peachpass", currency + "----");
            DecimalFormat df = new DecimalFormat("#0.00");
            currency = "$" + df.format(Double.parseDouble(currency));
            //currency = "$"+Double.valueOf(df.format(Double.parseDouble(currency)));

            //Double newVal = new BigDecimal(currency).setScale(2,RoundingMode.HALF_UP).doubleValue();
            //currency = "$"+newVal;
            if (!isPositive)
                currency = " - " + currency;
            // - $2.00
            Log.e("peachpass", currency + "!!!!!");
            txt_amount.setText(currency);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void init() {
        // TODO Auto-generated method stub
        txt_alert_icon = (TextView) findViewById(R.id.txt_alert_icon);

        txt_mpp_month = (TextView) findViewById(R.id.txt_mpp_month);
        txt_mpp_date = (TextView) findViewById(R.id.txt_mpp_date);
        txt_mpp_line1 = (TextView) findViewById(R.id.txt_mpp_line1);
        txt_mpp_line2 = (TextView) findViewById(R.id.txt_mpp_line2);
        txt_licplate = (TextView) findViewById(R.id.txt_licplate);
        txt_datetime = (TextView) findViewById(R.id.txt_datetime);
        txt_lane = (TextView) findViewById(R.id.txt_lane);
        txt_amount = (TextView) findViewById(R.id.txt_amount);
    }

    public void onClickBack(View view) {
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.VIOLATOIN_DETAILS);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }

}
