package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srta.PeachPass.R;


public class AccountTypeSelectActivity extends Activity {


    LinearLayout commercialtoll, exempttoll, personaltoll;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accounttypeselectactivity);
        context = this;
        personaltoll = (LinearLayout) findViewById(R.id.ll_perosnaltoll);
        exempttoll = (LinearLayout) findViewById(R.id.ll_Exempttoll);
        commercialtoll = (LinearLayout) findViewById(R.id.ll_commercialtoll);
       ;


        personaltoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, AccountTypeDiscription.class);
                in.putExtra("personaltoll", "personal");
                in.putExtra("id", 1);
                startActivity(in);
            }
        });


        exempttoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, AccountTypeDiscription.class);
                in.putExtra("exempttoll", "exempt");
                in.putExtra("id", 13);
                startActivity(in);
            }
        });


        commercialtoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, AccountTypeDiscription.class);
                in.putExtra("commecialtoll", "commercial");
                in.putExtra("id", 5);
                startActivity(in);
            }
        });

    }
}
