package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.utils.ScreenUtils;
import com.srta.PeachPass.R;

public class MainActivity extends Activity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);

        setContentView(R.layout.date_time_dialog);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }


}
