package com.peachpass.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.CreditCardData;
import com.peachpass.data.PrimaryBillingData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;

public class UpdateCreditCardsActivity extends Activity {

    List<String> stateList = null;
    List<String> countyList = null;
    String ccType, ccMonth, ccYear, ccExpDate, cardNo;
    String billingMethodId, accountId, isPrimary;
    Context context;

    EditText edt_tab4_cardno;

    //   TextView txt_update_account_4_state;
    TextView txt_changetoll_arrow_4_4, txt_changetoll_arrow_county_4,
            txt_changetoll_arrow_cardtype, txt_changetoll_arrow_4_month,
            txt_changetoll_arrow_4_year, txt_update_account_next4;
    TextView txt_update_account_4_card_type, txt_update_account_4_year,
            txt_update_account_4_month;

    public static String month = "", year = "", state = "";
    
    CreditCardData orgdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);
        context = this;
        stateList = Arrays.asList(this.getResources().getStringArray(
                R.array.state_type));

        countyList = Arrays.asList(this.getResources().getStringArray(
                R.array.county_type));

        setContentView(R.layout.activity_update_credit_card);

        init();

        Typeface font = ScreenUtils.returnTypeFace(this);

        Typeface font1 = ScreenUtils.returnTypeFaceHelv(this);


        PrimaryBillingData pbd = (PrimaryBillingData) getIntent().getExtras().getSerializable("billdata");

        billingMethodId = pbd.getBillingMethodId();
        cardNo = pbd.getCcLastFour();
        ccExpDate = pbd.getExpDate();
        ccType = pbd.getCcType();

        String expArray[] = ccExpDate.split("/");
        month = Utils.getMonthNamefromId(expArray[0]);
        year = expArray[1];

      /*  txt_changetoll_arrow_4_4.setTypeface(font);
        txt_changetoll_arrow_county_4.setTypeface(font);*/
        txt_changetoll_arrow_cardtype.setTypeface(font);
        txt_changetoll_arrow_4_month.setTypeface(font);
        txt_changetoll_arrow_4_year.setTypeface(font);
        txt_update_account_next4.setTypeface(font);

        edt_tab4_cardno.setTypeface(font1);


        edt_tab4_cardno.setText("**** **** **** " + cardNo);


        txt_update_account_4_card_type.setText(ccType);
        txt_update_account_4_year.setText(year);
        txt_update_account_4_month.setText(month);

        Log.e("UpdateCard before", "ccType :" + ccType + "month :" + month + "year :" + year + "cardNo :" + cardNo);

        orgdata = new CreditCardData(ccType,cardNo,ccExpDate);
        orgdata.setCcNumber(cardNo);
        orgdata.setCardTypeId(getSelectedCCType(orgdata.getCardTypeId()));
        
        
        /*
         * CreditCardData [cardTypeId=Visa, ccNumber=1111, expiration=null/null] fresh
           CreditCardData [cardTypeId=Visa, ccNumber=**** **** **** 1111, expiration=12/18] org
*/
        Utils.setTracker(Utils.EDITCARD_VIEW);

    }

  

	private void init() {

        edt_tab4_cardno = (EditText) findViewById(R.id.edt_tab4_cardno);

        txt_changetoll_arrow_cardtype = (TextView) findViewById(R.id.txt_changetoll_arrow_cardtype);
        txt_changetoll_arrow_4_month = (TextView) findViewById(R.id.txt_changetoll_arrow_4_month);
        txt_changetoll_arrow_4_year = (TextView) findViewById(R.id.txt_changetoll_arrow_4_year);
        txt_update_account_next4 = (TextView) findViewById(R.id.txt_update_account_next4);


        txt_update_account_4_year = (TextView) findViewById(R.id.txt_update_account_4_year);
        txt_update_account_4_card_type = (TextView) findViewById(R.id.txt_update_card_type);
        txt_update_account_4_month = (TextView) findViewById(R.id.txt_update_account_4_month);
    }

    public void onclickNextButtons(View view) {

        if (Utils.isNetworkAvailable(context)) {


            ccType = getSelectedCCType(txt_update_account_4_card_type.getText().toString());
            ccMonth = getSelectedMonth(txt_update_account_4_month.getText().toString());
            ccYear = getSelectedYear(txt_update_account_4_year.getText().toString());

            cardNo = edt_tab4_cardno.getText().toString();

            final String expiration = ccMonth + "/" + ccYear;


            Log.e("UpdateCard after", "ccType :" + ccType + "ccMonth :" + ccMonth + "ccYear :" + ccYear);


            new AsyncTask<CreditCardData, String, String>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        context,
                        WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
                        false);

                String status = "";

                @Override
                protected void onPreExecute() {
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }

                ;

                @Override
                protected String doInBackground(CreditCardData... params) {
                    // TODO Auto-generated method stub
                    try {

                        Log.e("peachpass", params[0].toString());

                        String requestBody = WebserviceUtils.readFromAsset(
                                "updateCreditCardByBillingMethod", context);


                        requestBody = requestBody.replaceAll("_creditCardType_",
                                ccType);
                        requestBody = requestBody.replaceAll("_billingMethodId_",
                                billingMethodId);
                        requestBody = requestBody.replaceAll("_creditCardNumber_",
                                cardNo);
                        requestBody = requestBody.replaceAll("_expiredDate_",
                                expiration);


                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(context));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(context));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(context));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody + "--------requestBody");
                        String result = WebserviceUtils.sendSoapRequest(
                                context, requestBody,
                                WebserviceUtils.SOAP_ACTION_UPDATECREDITCARDBYBILLINGMETHOD);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        Log.e("peachpass", status + "****status");
                        Log.e("peachpass", result + "------------result");

                        return status;

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);

                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    if (result != null) {

                        if (result.equals("0")) {
                            setResult(Activity.RESULT_OK);
                            finish();
                            ScreenUtils
                                    .raiseToast(
                                            context,
                                            WebserviceUtils.UPDATE_CREDIT_CARD_SUCCESS);
                        } else {
                            String errorStatus = Utils.getStringResourceByName(
                                    "status_" + result, context);

                            ScreenUtils.raiseToast(context, errorStatus);
                        }

                    } else {
                        ScreenUtils.raiseToast(context,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }
                }

            }.execute(new CreditCardData[]{new CreditCardData(ccType, cardNo, expiration, "", "", "", "", "", "", "", "", "")});
        } else {
            ScreenUtils.raiseToast(context,
                    WebserviceUtils.NO_INTERNET_CONNECTION);
        }
    }

    public void onClickBack(View view) {
    	
    	/*
09-22 17:41:23.139: E/peachpass(24325): CreditCardData [cardTypeId=Visa, ccNumber=**** **** **** 1111, expiration=12/18]
CreditCardData [cardTypeId=Visa, ccNumber=1111, expiration=null/null]
*/
    
        
        
        ccType = getSelectedCCType(txt_update_account_4_card_type.getText().toString());
        ccMonth = getSelectedMonth(txt_update_account_4_month.getText().toString());
        ccYear = getSelectedYear(txt_update_account_4_year.getText().toString());
        
        final String expiration = ccMonth + "/" + ccYear;
    	CreditCardData freshData = new CreditCardData(ccType,cardNo,expiration);

    	Log.e("peachpass", freshData.toString());
    	Log.e("peachpass", orgdata.toString());
		if (freshData.equals(orgdata)) {

			finish();

		} else {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                "Are you sure you want to cancel without saving changes?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                setResult(Activity.RESULT_CANCELED);
                                finish();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
		}
    }

    public void onClickShowDropdown(View view) {

        switch (view.getId()) {

           /* case R.id.ll_update_account_4_state:

                final CharSequence[] array_1 = stateList
                        .toArray(new CharSequence[stateList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_1, this,
                        txt_update_account_4_state, "state", this, null);
                break;

            case R.id.ll_update_account_4_county:
                final CharSequence[] array = countyList
                        .toArray(new CharSequence[countyList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array, this, txt_tab4_county,
                        "county", this, null);
                break;*/
            case R.id.ll_update_account_4_card_type:

                final CharSequence[] array_2 = {"MasterCard", "Visa",
                        "American Express", "Discover"};
                ScreenUtils.showMultipleChoiceDlg(array_2, this,
                        txt_update_account_4_card_type, "card type", this, null);
                break;

            case R.id.ll_update_account_4_year:

                ScreenUtils.showMultipleChoiceDlg(Utils.returnYears(), this,
                        txt_update_account_4_year, "year", this, null);
                break;

            case R.id.ll_update_account_4_month:

                final CharSequence[] array_4 = {"Jan", "Feb", "Mar", "Apr", "May",
                        "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};
                ScreenUtils.showMultipleChoiceDlg(array_4, this,
                        txt_update_account_4_month, "month", this, null);
                break;
            default:
                break;
        }

    }

    private String getSelectedCCType(String selectedItem) {


        if (selectedItem.equals("MasterCard")) {
            return "1";
        }

        if (selectedItem.equals("Visa")) {
            return "2";
        }

        if (selectedItem.equals("American Express")) {
            return "3";
        }

        if (selectedItem.equals("Discover")) {
            return "4";
        }

        return "";
    }

    private String getSelectedMonth(String selectedItem) {

        if (selectedItem.equals("Jan")) {
            return "01";
        }

        if (selectedItem.equals("Feb")) {
            return "02";
        }

        if (selectedItem.equals("Mar")) {
            return "03";
        }

        if (selectedItem.equals("Apr")) {
            return "04";
        }

        if (selectedItem.equals("May")) {
            return "05";
        }

        if (selectedItem.equals("June")) {
            return "06";
        }

        if (selectedItem.equals("July")) {
            return "07";
        }

        if (selectedItem.equals("Aug")) {
            return "08";
        }

        if (selectedItem.equals("Sept")) {
            return "09";
        }

        if (selectedItem.equals("Oct")) {
            return "10";
        }

        if (selectedItem.equals("Nov")) {
            return "11";
        }

        if (selectedItem.equals("Dec")) {
            return "12";
        }
        return "";
    }

    private String getSelectedYear(String selectedItem) {


        String sub = selectedItem.substring(selectedItem.length() - 2);

        return sub;
    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.EDITCARD_VIEW);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }


  /*  private String getSelectedState(String selectedItem) {

        if (selectedItem.equals("MasterCard")) {
            return "1";
        }

        return "";
    }*/

  /*  private String getSelectedCounty(String selectedItem) {

        if (selectedItem.equals("MasterCard")) {
            return "1";
        }

        return "";
    }*/

}
