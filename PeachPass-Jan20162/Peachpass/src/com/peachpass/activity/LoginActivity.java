package com.peachpass.activity;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mobsandgeeks.ui.Shimmer;
import com.mobsandgeeks.ui.ShimmerTextView;
import com.peachpass.data.LoginData;
import com.peachpass.utils.Cache;
import com.peachpass.utils.LayoutTouchListener;
import com.peachpass.utils.RuleBook;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class LoginActivity extends Activity {

    LinearLayout ll_login_swipe;

    CheckBox chk_remberme;
    TextView txt_log_user, txt_log_pwd, txt_frgt_pwd, tv_show_pass;
    WebView txt_login_text;
    EditText edt_log_user, edt_log_pwd;
    Dialog connectionDialog;

    Button btn_login;

    boolean validUsername = false;
    boolean validPwd = false;
    boolean visible;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Set the screen to full screen mode before setting the layout */

        context = this;
        ScreenUtils.fullScreen(this);

        // Crittercism.initialize(getApplicationContext(), "5637d03b8d4d8c0a00d0808a");

        setContentView(R.layout.activity_login);

        init();

        txt_log_user = (TextView) findViewById(R.id.txt_log_user);
        txt_log_pwd = (TextView) findViewById(R.id.txt_log_pwd);
        TextView txt3 = (TextView) findViewById(R.id.textView3);
        tv_show_pass = (TextView) findViewById(R.id.tv_show_pass);

        Typeface font = ScreenUtils.returnTypeFace(this);

        txt_log_user.setTypeface(font);
        txt_log_pwd.setTypeface(font);

        txt3.setText(getResources().getString(R.string.fa_angle_double_left) + getResources().getString(R.string.fa_angle_double_left));
        txt3.setTypeface(font);

        edt_log_user = (EditText) findViewById(R.id.edt_log_user);
        edt_log_pwd = (EditText) findViewById(R.id.edt_log_pwd);

        tv_show_pass.setTypeface(font);

        Typeface typeFace = ScreenUtils.returnHelvetica(this);
        edt_log_user.setTypeface(typeFace);
        edt_log_pwd.setTypeface(typeFace);
        btn_login.setTypeface(ScreenUtils.returnHelveticaBold(this));

        //OnClick For Show Password
        tv_show_pass.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!visible) {
                    tv_show_pass.setText("Hide");
                    edt_log_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    visible = true;
                } else if (visible) {
                    tv_show_pass.setText("Show");
                    edt_log_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    visible = false;
                }
                edt_log_pwd.setSelection(edt_log_pwd.getText().length());
            }
        });

        /** set click event to ForgotPasswordActivity */
        txt_frgt_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				startActivity(new Intent(LoginActivity.this,
//						ForgotPasswordActivity.class));
                Utils.openLinkInBrowser(WebserviceUtils.FORGET_PWD, LoginActivity.this);
            }
        });

        btn_login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                loginMethod();

            }
        });

        edt_log_user.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                checkUserName();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        edt_log_pwd.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                checkPassword();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        ShimmerTextView txt_shimmer_swipe = (ShimmerTextView) findViewById(R.id.txt_shimmer_login_swipe);
        txt_shimmer_swipe.setTypeface(font);
        getShimmerConfig(txt_shimmer_swipe);


        Utils.setTracker(Utils.LOGIN_VIEW);


        checkLoginStatus();
    }

    private void loginMethod() {

        checkUserName();
        checkPassword();
        if (validUsername && validPwd) {

            if (Utils.isNetworkAvailable(context))

                new AsyncTask<String, String, LoginData>() {
                    ProgressDialog pdialog = ScreenUtils
                            .returnProgDialogObj(
                                    context,
                                    WebserviceUtils.PROGRESS_MESSAGE_USER_LOGIN,
                                    false);

                    @Override
                    protected void onPreExecute() {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.show();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        super.onPreExecute();
                    }

                    @Override
                    protected LoginData doInBackground(String... params) {
                        // TODO Auto-generated method stub
                        LoginData loginData = new LoginData();
                        try {

                            String requestBody = WebserviceUtils.readFromAsset("userLogin", context);
                            requestBody = requestBody.replaceAll("_userName_", params[0]);
                            requestBody = requestBody.replaceAll("_password_", params[1]);
                            requestBody = requestBody.replaceAll("_osType_", WebserviceUtils.OS_TYPE);
                            requestBody = requestBody.replaceAll("_osVersion_", Utils.getOsVersion());
                            requestBody = requestBody.replaceAll("_ipAddress_", WebserviceUtils.getExternalIP());
                            requestBody = requestBody.replaceAll("_id_", WebserviceUtils.ID);

                            Log.e("peachpass", requestBody + "");
                            String result = WebserviceUtils.sendSoapRequest(context, requestBody, WebserviceUtils.SOAP_ACTION_USER_LOGIN);
                            XMLParser parser = new XMLParser();
                            Log.e("peachpass", result + "");
                            String xml = result;
                            Document doc = parser.getDomElement(xml); // getting DOM element

                            NodeList nl = doc.getElementsByTagName("result");

                            for (int i = 0; i < nl.getLength(); ) {

                                Element e = (Element) nl.item(i);

                                String status = parser.getValue(e, "ns1:status");
                                String sessionId = parser.getValue(e, "ns1:sessionId");
                                String orinalId = parser.getValue(e, "ns1:originaId");
                                String activationDateTime = parser.getValue(e, "ns1:activationDateTime");
                                String accountId = parser.getValue(e, "ns1:accountId");
                                String userName = parser.getValue(e, "ns1:userName");
                                String tagIds = parser.getValue(e, "ns1:tagIds");

                                Log.e("peachpass", "status--" + status);
                                Log.e("peachpass", "sessionId--" + sessionId);
                                Log.e("peachpass", "orinalId--" + orinalId);
                                Log.e("peachpass", "activationDateTime--" + activationDateTime);
                                Log.e("peachpass", "accountId--" + accountId);
                                Log.e("peachpass", "userName--" + userName);

                                loginData.setStatus(status);
                                loginData.setSessionId(sessionId);
                                loginData.setOrinalId(orinalId);
                                loginData.setActivationDateTime(activationDateTime);
                                loginData.setAccountId(accountId);
                                loginData.setUserName(userName);
                                loginData.setTagIds(tagIds);

                                return loginData;
                            }

                        } catch (Exception e) {
                            // TODO: handle exception

                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(LoginData result) {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.dismiss();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {
                            if (result != null) {

                                if (result.getStatus().equals("0")) {

                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.ACCOUNT_ID, result.getAccountId(), context);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.ACTIVATION_DATE_TIME, result.getActivationDateTime(), context);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.USERNAME, result.getUserName(), context);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.PASSWORD, edt_log_pwd.getText().toString(), context);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.SESSION_ID, result.getSessionId(), context);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.TAG_ID, result.getTagIds(), context);

                                    Crashlytics.setString("username", result.getUserName());
                                    Crashlytics.setString("password", edt_log_pwd.getText().toString());

                                    startActivity(new Intent(LoginActivity.this,
                                            FragmentChangeActivity.class));
                                    Cache.fromBack = false;
                                } else {

                                    ScreenUtils.raiseToast(context, WebserviceUtils.LOGIN_INCORRECT);
                                }

                            } else {

                                ScreenUtils.raiseToast(context, WebserviceUtils.SOMETHING_WENT_WRONG);
                                Log.e("peachpass", "result--* " + result);
                            }


                        } catch (Exception e) {
                            // TODO: handle exception
                            ScreenUtils.raiseToast(context, WebserviceUtils.SOMETHING_WENT_WRONG);
                            e.printStackTrace();
                        }

                        super.onPostExecute(result);
                    }
                }.execute(new String[]{
                        Utils.getEditTextValue(edt_log_user),
                        Utils.getEditTextValue(edt_log_pwd)});

            else {
                ScreenUtils.raiseToast(context, WebserviceUtils.NO_INTERNET_CONNECTION);
            }

        }
    }

    private void checkLoginStatus() {

        String checkBoxVal = Utils.getRememberMe(context);

        if (checkBoxVal != null && checkBoxVal.equals("true")) {

            chk_remberme.setChecked(true);
            setTextDetails();

            loginMethod();
        } else chk_remberme.setChecked(false);
    }

    private void setTextDetails() {
        edt_log_user.setText(Utils.getUsername(context));
        edt_log_pwd.setText(Utils.getPassword(context));
    }

    /* check validation for Password */
    private void checkPassword() {
        // TODO Auto-generated method stub
        if (RuleBook.isUserNameValid(edt_log_pwd.getText().toString().trim())) {
            validPwd = true;
            txt_log_pwd.setTextColor(getResources().getColor(
                    R.color.login_grey_usrpwd));
        } else {
            validPwd = false;
            txt_log_pwd.setTextColor(getResources().getColor(R.color.red));
        }
    }

    /* check validation for UserName */
    private void checkUserName() {
        // TODO Auto-generated method stub
        String userName = edt_log_user.getText().toString().trim();
        Log.e("checkUserName", "userName : " + userName + "--");
        if (RuleBook.isUserNameValid(userName)) {
            validUsername = true;
            txt_log_user.setTextColor(getResources().getColor(
                    R.color.login_grey_usrpwd));
        } else {
            validUsername = false;
            txt_log_user.setTextColor(getResources().getColor(R.color.red));
        }
    }

    private void init() {
        // TODO Auto-generated method stub
        ll_login_swipe = (LinearLayout) findViewById(R.id.ll_login_swipe);
        ll_login_swipe.setOnTouchListener(new LayoutTouchListener(this, 3));

        txt_frgt_pwd = (TextView) findViewById(R.id.txt_frgt_pwd);
        txt_login_text = (WebView) findViewById(R.id.txt_login_text);

        btn_login = (Button) findViewById(R.id.btn_login);
        txt_login_text.getSettings().setJavaScriptEnabled(true);
        txt_login_text.loadDataWithBaseURL(null, "<html><font color=\"#AEADAD\">If you are experiencing login problems, please call<br/>Peach Pass Customer Service at </font> <font color=\"#78BD1D\">1-855-724-7277</font></html>", "text/html", "utf-8", null);
        WebSettings webSettings = txt_login_text.getSettings();
        webSettings.setDefaultFontSize(14);
        txt_login_text.setBackgroundColor(Color.TRANSPARENT);
        txt_login_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Do what you want
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:1-855-724-7277"));
                    startActivity(intent);
                }

                return false;
            }
        });

        chk_remberme = (CheckBox) findViewById(R.id.chk_remberme);
        chk_remberme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                                        chk_remberme.setChecked(isChecked);
                                                        Utils.saveRememberMe(context, isChecked + "");
                                                    }
                                                }
        );

    }

    public void onClickGoToCreateAccount(View view) {

        startActivity(new Intent(this, CreateAccountActivity.class));
    }

    public void onClickWalkthrough(View view) {
        startActivity(new Intent(this, WalkthroughActivity.class));
        //finish();
    }

    // void showDialog() {
    //
    // connectionDialog = new Dialog(this, R.style.CustomAlertDialog);
    // connectionDialog.setContentView(R.layout.alert_dialog_forgot_pwd);
    //
    // connectionDialog.getWindow().getAttributes().windowAnimations =
    // R.style.dialog_animation;
    //
    // Button btnCont = (Button) connectionDialog
    // .findViewById(R.id.btn_alert_cont);
    // Button btnCancl = (Button) connectionDialog
    // .findViewById(R.id.btn_alert_cancel);
    //
    // btnCont.setOnClickListener(new OnClickListener() {
    //
    // @Override
    // public void onClick(View v) {
    // // TODO Auto-generated method stub
    // if (connectionDialog != null) {
    // if (connectionDialog.isShowing()) {
    // connectionDialog.dismiss();
    // }
    // }
    // }
    // });
    // btnCancl.setOnClickListener(new OnClickListener() {
    //
    // @Override
    // public void onClick(View v) {
    // // TODO Auto-generated method stub
    // if (connectionDialog != null) {
    // if (connectionDialog.isShowing()) {
    // connectionDialog.dismiss();
    // }
    // }
    // }
    // });
    //
    // connectionDialog.show();
    // }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        // if (connectionDialog != null) {
        // if (connectionDialog.isShowing()) {
        // connectionDialog.dismiss();
        // }
        // }
    }

    void getShimmerConfig(ShimmerTextView txt_shimmer_swipe) {

        Shimmer shimmer = new Shimmer();
        shimmer.start(txt_shimmer_swipe);
        shimmer.setDuration(5000);
        shimmer.setStartDelay(300);

        shimmer.setDirection(Shimmer.ANIMATION_DIRECTION_RTL);
        shimmer.setAnimatorListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.LOGIN_VIEW);
    }


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }

}
