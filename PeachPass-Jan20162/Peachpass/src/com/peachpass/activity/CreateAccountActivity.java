package com.peachpass.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.CreateAccountData;
import com.peachpass.utils.RuleBook;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;

public class CreateAccountActivity extends Activity {

    RelativeLayout rel_tab2, rel_tab3, rel_tab4, rel_tab5;
    RelativeLayout rel_tab2_1, rel_tab2_2, rel_tab2_3, rel_tab2_4, txt_create_account_next1;

    View layoutTab2, layoutTab3, layoutTab4, layoutTab5;

    View circletab2, circletab3, circletab4, circletab5;

    TextView txt_next2, txt_next3, txt_next4, txt_next5;

    TextView txt_create_account_next2, txt_create_account_next3,
            txt_create_account_next4;

    Typeface font = null;

    EditText edt_tab2_username, edt_tab2_email, edt_tab2_pwd, edt_tab2_confpwd;

    TextView txt_create_account_username, txt_create_account_email_id,
            txt_create_account_pwd, txt_create_account_con_pwd;

    TextView txt_circle_2, txt_circle_3, txt_circle_4, txt_circle_5;

    // Button btn_create_accnt_tab1_1, btn_create_accnt_tab1_2;

    EditText edt_tab3_flname, edt_tab3_flname_last, edt_tab3_address,
            edt_tab3_city, edt_tab3_zip, edt_tab3_phone, edt_tab3_drvrlic,
            edt_create_account_3_secret_question_1,
            edt_create_account_3_secret_question_2,
            edt_create_account_3_secret_question_3, edt_tab3_pin;

    EditText edt_tab4_cardno, edt_tab4_fln, edt_tab4_fln_last,
            edt_tab4_address, edt_tab4_city, edt_tab4_zip, edt_tab4_phn, edt_tab4_card_cvv;

    EditText edt_tab5_vehy, edt_tab5_vehmake, edt_tab5_vehmodel,
            edt_tab5_vehcolor, edt_tab5_vehpltn, edt_tab5_vehTagId, edt_tab5_vehactivationcode;

    TextView txt_cross;

    LinearLayout ll_create_account_3_secret_question_1,
            ll_create_account_3_secret_question_2,
            ll_create_account_3_secret_question_3, ll_create_account_3_source, ll_tab2_nxt_btn,
            tabscontent, tabsview;

    CheckBox chk_tab4;
    LinearLayout ll_tab4_chkbox;
    TextView txt_tab3_drvrlic_append_state;

    /* Dropdowns */
    TextView txt_create_account_3_state, txt_changetoll_arrow,
            txt_changetoll_arrow_1, txt_changetoll_arrow_2,
            txt_changetoll_arrow_3, txt_create_account_3_secret_question_1,
            txt_create_account_3_secret_question_2,
            txt_create_account_3_secret_question_3,
            txt_create_account_3_source, txt_changetoll_arrow_source,
            txt_changetoll_arrow_county_3, txt_tab3_county, txt_tab4_county,
            txt_changetoll_arrow_county_4;

    TextView txt_create_account_4_card, txt_changetoll_arrow_4,
            txt_create_account_4_year, txt_changetoll_arrow_4_2,
            txt_create_account_4_month, txt_changetoll_arrow_4_3,
            txt_create_account_4_state, txt_changetoll_arrow_4_4,
            txt_create_account_5_state, txt_changetoll_arrow_5;

    List<String> stateList = null;
    List<String> questionList = null;
    List<String> sourceList = null;
    List<String> countyList = null;

    CreateAccountData createAccountData = new CreateAccountData();
    LinearLayout ll_create_account_4_card_type, ll_create_account_4_month, ll_create_account_4_year;
    Context context;

    TextView txt_cust_agrmnt;
    int value;

    private static final String TAG = "CreateAccountTabs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScreenUtils.fullScreen(this);
        setContentView(R.layout.activity_create_account);

        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        init();

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                value = getIntent().getExtras().getInt("id");
                createAccountData.setAccountTypeId(value);
            }
        }

        font = ScreenUtils.returnTypeFace(this);
        txt_cross.setTypeface(font);
        initFontAwosmeIcons();
        initArrows(layoutTab2, layoutTab3, layoutTab4, layoutTab5);
        initFontAwosmeIconsLayout();
        initEditFields();

        txt_next2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    hideKeyboard();

                }
            }
        });

        txt_next3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    hideKeyboard1();
                }
            }
        });

        txt_next4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    hideKeyboard2();
                }
            }
        });

        txt_next5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    hideKeyboard3();
                }
            }
        });

        //setFontToButton();

        initTab3Widgests();
        initTab4Widgests();
        initTab5Widgets();

        setListenerstoEditFields();


        txt_create_account_next1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_tab2_pwd.length() < 8) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(
                            "Password must have a minimum of 8 characters and at least 1 uppercase letter, 1 number, and 1 special character other than $.")
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    goToTab3();
                }
            }
        });
        txt_next2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_tab2_pwd.length() < 8) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(
                            "Password must have a minimum of 8 characters and at least 1 uppercase  letter, 1 number, and 1 special character other than $.")
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    goToTab3();
                }
            }
        });

        txt_next3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goToTab4();
            }
        });

        txt_next4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goToTab5();
            }
        });

        txt_next5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goToTabLast();
            }
        });


//        rel_tab1.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                toggle1();
//            }
//        });

        rel_tab2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                toggle2();
            }
        });
        rel_tab3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // toggle3();
                goToTab3();
            }
        });
        rel_tab4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // toggle4();
                goToTab4();
            }
        });
        rel_tab5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // toggle5();
                goToTab5();
            }
        });

        toggle2();

        chk_tab4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (chk_tab4.isChecked()) {
                    ll_tab4_chkbox.setVisibility(View.GONE);
                } else
                    ll_tab4_chkbox.setVisibility(View.VISIBLE);
            }
        });

        stateList = Arrays.asList(this.getResources().getStringArray(
                R.array.state_type));
        countyList = Arrays.asList(this.getResources().getStringArray(
                R.array.county_type));
        questionList = Arrays.asList(Utils.QUESTION_LIST);
        sourceList = Arrays.asList(Utils.SOURCE_LIST);

        txt_create_account_3_state.setText(stateList.get(9));
        txt_tab3_county.setText(countyList.get(0));
        txt_tab4_county.setText(countyList.get(0));
        txt_create_account_4_state.setText(stateList.get(9));
        txt_create_account_5_state.setText(stateList.get(9));
        txt_create_account_3_secret_question_1.setText(questionList.get(0));
        txt_create_account_3_secret_question_2.setText(questionList.get(1));
        txt_create_account_3_secret_question_3.setText(questionList.get(2));
        txt_create_account_3_source.setText(sourceList.get(0));

        txt_cust_agrmnt = (TextView) findViewById(R.id.txt_cust_agrmnt);

        txt_cust_agrmnt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				startActivity(new Intent(LoginActivity.this,
//						ForgotPasswordActivity.class));
                Utils.openLinkInBrowser(WebserviceUtils.CUST_AGRE, CreateAccountActivity.this);
            }
        });


        Utils.setTracker(Utils.CREATE_VIEW);
    }

    private void initTab5Widgets() {
        // TODO Auto-generated method stub
        Typeface typeFace = ScreenUtils.returnHelvetica(this);

        edt_tab5_vehy = (EditText) findViewById(R.id.edt_tab5_vehy);
        edt_tab5_vehmake = (EditText) findViewById(R.id.edt_tab5_vehmake);
        edt_tab5_vehmodel = (EditText) findViewById(R.id.edt_tab5_vehmodel);
        edt_tab5_vehcolor = (EditText) findViewById(R.id.edt_tab5_vehcolor);
        edt_tab5_vehpltn = (EditText) findViewById(R.id.edt_tab5_vehpltn);
        //edt_tab5_vehprmcode = (EditText) findViewById(R.id.edt_tab5_vehprmcode);
        edt_tab5_vehTagId = (EditText) findViewById(R.id.edt_tab5_vehTagId);
        edt_tab5_vehactivationcode = (EditText) findViewById(R.id.edt_tab5_vehactivationcode);

        edt_tab5_vehy.setTypeface(typeFace);
        edt_tab5_vehmake.setTypeface(typeFace);
        edt_tab5_vehmodel.setTypeface(typeFace);
        edt_tab5_vehcolor.setTypeface(typeFace);
        edt_tab5_vehpltn.setTypeface(typeFace);
        //edt_tab5_vehprmcode.setTypeface(typeFace);
        edt_tab5_vehTagId.setTypeface(typeFace);
        edt_tab5_vehactivationcode.setTypeface(typeFace);

    }

    private void initTab4Widgests() {
        // TODO Auto-generated method stub
        Typeface typeFace = ScreenUtils.returnHelvetica(this);
        edt_tab4_cardno = (EditText) findViewById(R.id.edt_tab4_cardno);
        edt_tab4_fln = (EditText) findViewById(R.id.edt_tab4_fln);
        edt_tab4_fln_last = (EditText) findViewById(R.id.edt_tab4_fln_last);
        edt_tab4_address = (EditText) findViewById(R.id.edt_tab4_address);
        edt_tab4_city = (EditText) findViewById(R.id.edt_tab4_city);
        edt_tab4_zip = (EditText) findViewById(R.id.edt_tab4_zip);
        txt_tab4_county = (TextView) findViewById(R.id.txt_tab4_county);
        edt_tab4_phn = (EditText) findViewById(R.id.edt_tab4_phn);
        edt_tab4_card_cvv = (EditText) findViewById(R.id.edt_tab4_card_cvv);
        // edt_tab4_country = (EditText) findViewById(R.id.edt_tab4_country);

        edt_tab4_cardno.setTypeface(typeFace);
        edt_tab4_fln.setTypeface(typeFace);
        edt_tab4_fln_last.setTypeface(typeFace);
        edt_tab4_address.setTypeface(typeFace);
        edt_tab4_city.setTypeface(typeFace);
        edt_tab4_zip.setTypeface(typeFace);
        // edt_tab4_county.setTypeface(typeFace);
        edt_tab4_phn.setTypeface(typeFace);
        // edt_tab4_country.setTypeface(typeFace);
        // txt_tab4_county.setTypeface(font);
        edt_tab4_card_cvv.setTypeface(typeFace);
        txt_changetoll_arrow_county_4 = (TextView) findViewById(R.id.txt_changetoll_arrow_county_4);
        txt_changetoll_arrow_county_4.setTypeface(font);

        ll_create_account_4_card_type = (LinearLayout) findViewById(R.id.ll_create_account_4_card_type);
        ll_create_account_4_month = (LinearLayout) findViewById(R.id.ll_create_account_4_month);
        ll_create_account_4_year = (LinearLayout) findViewById(R.id.ll_create_account_4_year);
    }

    private void initTab3Widgests() {
        // TODO Auto-generated method stub
        Typeface typeFace = ScreenUtils.returnHelvetica(this);
        txt_tab3_drvrlic_append_state = (TextView) findViewById(R.id.txt_tab3_drvrlic_append_state);
        edt_tab3_flname = (EditText) findViewById(R.id.edt_tab3_flname);
        edt_tab3_flname_last = (EditText) findViewById(R.id.edt_tab3_flname_last);
        edt_tab3_address = (EditText) findViewById(R.id.edt_tab3_address);
        edt_tab3_city = (EditText) findViewById(R.id.edt_tab3_city);
        edt_tab3_zip = (EditText) findViewById(R.id.edt_tab3_zip);
        txt_tab3_county = (TextView) findViewById(R.id.txt_tab3_county);
        edt_tab3_phone = (EditText) findViewById(R.id.edt_tab3_phone);
        // edt_tab3_country = (EditText) findViewById(R.id.edt_tab3_country);
        edt_tab3_drvrlic = (EditText) findViewById(R.id.edt_tab3_drvrlic);

        txt_changetoll_arrow_1 = (TextView) findViewById(R.id.txt_changetoll_arrow_1);
        txt_changetoll_arrow_2 = (TextView) findViewById(R.id.txt_changetoll_arrow_2);
        txt_changetoll_arrow_3 = (TextView) findViewById(R.id.txt_changetoll_arrow_3);
        txt_changetoll_arrow_county_3 = (TextView) findViewById(R.id.txt_changetoll_arrow_county_3);
        txt_changetoll_arrow_source = (TextView) findViewById(R.id.txt_changetoll_arrow_source);

        txt_create_account_3_secret_question_1 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_1);
        txt_create_account_3_secret_question_2 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_2);
        txt_create_account_3_secret_question_3 = (TextView) findViewById(R.id.txt_create_account_3_secret_question_3);
        txt_create_account_3_source = (TextView) findViewById(R.id.txt_create_account_3_source);

        edt_create_account_3_secret_question_1 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_1);
        edt_create_account_3_secret_question_2 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_2);
        edt_create_account_3_secret_question_3 = (EditText) findViewById(R.id.edt_create_account_3_secret_question_3);
        edt_tab3_pin = (EditText) findViewById(R.id.edt_tab3_pin);

        ll_create_account_3_secret_question_1 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_1);
        ll_create_account_3_secret_question_2 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_2);
        ll_create_account_3_secret_question_3 = (LinearLayout) findViewById(R.id.ll_create_account_3_secret_question_3);
        ll_create_account_3_source = (LinearLayout) findViewById(R.id.ll_create_account_3_source);

        edt_tab3_flname.setTypeface(typeFace);
        edt_tab3_flname_last.setTypeface(typeFace);
        edt_tab3_address.setTypeface(typeFace);
        edt_tab3_city.setTypeface(typeFace);
        edt_tab3_zip.setTypeface(typeFace);
        // edt_tab3_county.setTypeface(typeFace);
        edt_tab3_phone.setTypeface(typeFace);
        // edt_tab3_country.setTypeface(typeFace);
        edt_tab3_drvrlic.setTypeface(typeFace);
        edt_tab3_pin.setTypeface(typeFace);

        txt_create_account_3_secret_question_1.setTypeface(typeFace);
        edt_create_account_3_secret_question_1.setTypeface(typeFace);
        txt_create_account_3_secret_question_2.setTypeface(typeFace);
        edt_create_account_3_secret_question_2.setTypeface(typeFace);
        txt_create_account_3_secret_question_3.setTypeface(typeFace);
        edt_create_account_3_secret_question_3.setTypeface(typeFace);

        txt_create_account_3_source.setTypeface(typeFace);

        txt_changetoll_arrow_1.setTypeface(font);
        txt_changetoll_arrow_2.setTypeface(font);
        txt_changetoll_arrow_3.setTypeface(font);
        txt_changetoll_arrow_source.setTypeface(font);
        txt_changetoll_arrow_county_3.setTypeface(font);
    }

//    private void setFontToButton() {
//        // TODO Auto-generated method stub
//        btn_create_accnt_tab1_1.setTypeface(ScreenUtils
//                .returnHelveticaBold(this));
//        btn_create_accnt_tab1_2.setTypeface(ScreenUtils
//                .returnHelveticaBold(this));
//    }

    private void initFontAwosmeIconsLayout() {
        // TODO Auto-generated method stub
        rel_tab2_1 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab2_1);
        rel_tab2_2 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab2_2);
        rel_tab2_3 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab2_3);
        rel_tab2_4 = (RelativeLayout) layoutTab2.findViewById(R.id.rel_tab2_4);
    }

    /**
     * Initialize font awesome parameters
     */
    private void initFontAwosmeIcons() {
        // TODO Auto-generated method stub
        txt_create_account_username = (TextView) layoutTab2
                .findViewById(R.id.txt_create_account_username);
        txt_create_account_username.setTypeface(font);

        txt_create_account_email_id = (TextView) layoutTab2
                .findViewById(R.id.txt_create_account_email_id);
        txt_create_account_email_id.setTypeface(font);

        txt_create_account_pwd = (TextView) layoutTab2
                .findViewById(R.id.txt_create_account_pwd);
        txt_create_account_pwd.setTypeface(font);

        txt_create_account_con_pwd = (TextView) layoutTab2
                .findViewById(R.id.txt_create_account_con_pwd);
        txt_create_account_con_pwd.setTypeface(font);

        txt_circle_2.setTypeface(font);
        txt_circle_3.setTypeface(font);
        txt_circle_4.setTypeface(font);
        txt_circle_5.setTypeface(font);

        txt_changetoll_arrow.setTypeface(font);
        txt_changetoll_arrow_4.setTypeface(font);
        txt_changetoll_arrow_4_2.setTypeface(font);
        txt_changetoll_arrow_4_3.setTypeface(font);
        txt_changetoll_arrow_4_4.setTypeface(font);
        txt_changetoll_arrow_5.setTypeface(font);

    }

    private void setListenerstoEditFields() {
        // TODO Auto-generated method stub
        edt_tab2_username.addTextChangedListener(new Tab2TextWatcher(
                edt_tab2_username));
        edt_tab2_email.addTextChangedListener(new Tab2TextWatcher(
                edt_tab2_email));
        edt_tab2_pwd.addTextChangedListener(new Tab2TextWatcher(edt_tab2_pwd));
        edt_tab2_confpwd.addTextChangedListener(new Tab2TextWatcher(
                edt_tab2_confpwd));


        EditText editFields[] = new EditText[]{edt_tab3_flname,
                edt_tab3_flname_last, edt_tab3_address, edt_tab3_city,
                edt_tab3_zip, edt_tab3_phone, edt_tab3_drvrlic,
                edt_create_account_3_secret_question_1,
                edt_create_account_3_secret_question_2,
                edt_create_account_3_secret_question_3, edt_tab3_pin};

        for (EditText edtTemp : editFields) {
            edtTemp.addTextChangedListener(new Tab3TextWatcher(edtTemp));
        }

        EditText editFields1[] = new EditText[]{edt_tab4_cardno,
                edt_tab4_fln, edt_tab4_fln_last, edt_tab4_address,
                edt_tab4_city, edt_tab4_zip, edt_tab4_phn, edt_tab4_card_cvv};

        for (EditText edtTemp : editFields1) {
            edtTemp.addTextChangedListener(new Tab3TextWatcher(edtTemp));
        }
        EditText editFields2[] = new EditText[]{edt_tab5_vehy,
                edt_tab5_vehmake, edt_tab5_vehmodel, edt_tab5_vehcolor,
                edt_tab5_vehpltn, edt_tab5_vehTagId, edt_tab5_vehactivationcode};
        for (EditText edtTemp : editFields2) {
            edtTemp.addTextChangedListener(new Tab3TextWatcher(edtTemp));
        }

    }

    /**
     * When an object of a type is attached to an Editable, its methods will be
     * called when the text is changed.
     */
    class Tab2TextWatcher implements TextWatcher {

        EditText edt;

        Tab2TextWatcher(EditText edt) {
            this.edt = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub

            switch (edt.getId()) {
                case R.id.edt_tab2_username: {
                    // int val[] = ScreenUtils.getPaddingVal(edt);
                    // if (RuleBook.isUserNameValid(s.toString())) {
                    // changeBgTextField(rel_tab2_1, R.color.green);
                    // edt.setBackgroundResource(R.drawable.grey_border);
                    // } else {
                    // changeBgTextField(rel_tab2_1, R.color.grey);
                    // edt.setBackgroundResource(R.drawable.edit_warning_border);
                    // }
                    //
                    // edt.setPadding(val[0], val[1], val[2], val[3]);

                    validations(edt, rel_tab2_1, s, false);
                    break;
                }
                case R.id.edt_tab2_email: {
                    // int val[] = ScreenUtils.getPaddingVal(edt);
                    // if (RuleBook.isEmailValid(s.toString())) {
                    // changeBgTextField(rel_tab2_2, R.color.green);
                    // edt.setBackgroundResource(R.drawable.grey_border);
                    // } else {
                    // changeBgTextField(rel_tab2_2, R.color.grey);
                    // edt.setBackgroundResource(R.drawable.edit_warning_border);
                    // }
                    // edt.setPadding(val[0], val[1], val[2], val[3]);

                    validations(edt, rel_tab2_2, s, true);
                    break;
                }
                case R.id.edt_tab2_pwd: {
                    // int val[] = ScreenUtils.getPaddingVal(edt);
                    // if (RuleBook.isPasswordValid(s.toString())) {
                    // changeBgTextField(rel_tab2_3, R.color.green);
                    // edt.setBackgroundResource(R.drawable.grey_border);
                    // } else {
                    // changeBgTextField(rel_tab2_3, R.color.grey);
                    // edt.setBackgroundResource(R.drawable.edit_warning_border);
                    // }
                    //
                    // edt.setPadding(val[0], val[1], val[2], val[3]);

                    validations(edt, rel_tab2_3, s, false);
                    methodConfirmPwd();
                    break;
                }

                case R.id.edt_tab2_confpwd: {

                    methodConfirmPwd();
                    break;
                }

                default:
                    break;
            }

            /*
             * if (RuleBook.allTabFieldsValid(new View[] { rel_tab2_1,
             * rel_tab2_2, rel_tab2_3, rel_tab2_4 })) { // circletab2 //
             * .setBackgroundResource(R.drawable.shape_circle_green_color); //
             * txt_circle_2.setText(getResources() //
             * .getString(R.string.fa_check)); } else { circletab2
             * .setBackgroundResource(R.drawable.shape_circle_blue_color);
             * txt_circle_2.setText("2"); }
             */
        }

        void methodConfirmPwd() {
            int val[] = ScreenUtils.getPaddingVal(edt_tab2_pwd);
            if (RuleBook.isConfirmPasswordValid(edt_tab2_pwd.getText()
                    .toString(), edt_tab2_confpwd.getText().toString())) {
                changeBgTextField(rel_tab2_4, R.color.green);
                edt_tab2_confpwd.setBackgroundResource(R.drawable.grey_border);
            } else {
                changeBgTextField(rel_tab2_4, R.color.grey);
                edt_tab2_confpwd
                        .setBackgroundResource(R.drawable.edit_warning_border);
            }

            edt_tab2_confpwd.setPadding(val[0], val[1], val[2], val[3]);
        }

    }

    class Tab3TextWatcher implements TextWatcher {

        EditText edt;

        Tab3TextWatcher(EditText edt) {
            this.edt = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub

            validations(edt, null, s, false);

        }

    }

    void changeBgTextField(View tv, int color) {
        tv.setBackgroundColor(getResources().getColor(color));
        tv.invalidate();
    }

    private void initEditFields() {
        // TODO Auto-generated method stub
        Typeface typeFace = ScreenUtils.returnHelvetica(this);
        edt_tab2_username = (EditText) layoutTab2
                .findViewById(R.id.edt_tab2_username);
        edt_tab2_email = (EditText) layoutTab2
                .findViewById(R.id.edt_tab2_email);
        edt_tab2_pwd = (EditText) layoutTab2.findViewById(R.id.edt_tab2_pwd);
        edt_tab2_confpwd = (EditText) layoutTab2
                .findViewById(R.id.edt_tab2_confpwd);
        ll_tab2_nxt_btn = (LinearLayout) layoutTab2.findViewById(R.id.ll_tab2_nxt_btn);


        edt_tab2_username.setTypeface(typeFace);
        edt_tab2_email.setTypeface(typeFace);
        edt_tab2_pwd.setTypeface(typeFace);
        edt_tab2_confpwd.setTypeface(typeFace);
    }

    private boolean validations(EditText edt, RelativeLayout rel,
                                CharSequence s, boolean type) {
        // TODO Auto-generated method stub
        int val[] = ScreenUtils.getPaddingVal(edt);
        boolean result = false;
        if (type)
            result = RuleBook.isEmailValid(s.toString());
        else {
            if (edt.getId() == R.id.edt_tab3_address) {
                result = RuleBook.isAddressValid(s.toString());
            } else {
                result = RuleBook.isUserNameValid(s.toString());
            }
        }


        if (edt != null && edt.getId() == R.id.edt_tab3_pin)
            result = RuleBook.isPinValid(s.toString());


        if (edt != null && edt.getId() == R.id.edt_tab5_vehy)
            result = RuleBook.isPinValid(s.toString());

        if (edt != null && edt.getId() == R.id.edt_tab2_pwd) {
            result = RuleBook.isPasswordValid(s.toString());
        }

        if (edt != null && edt.getId() == R.id.edt_tab2_confpwd) {

            result = RuleBook.isConfirmPasswordValid(getTextVal(edt_tab2_pwd), getTextVal(edt_tab2_confpwd));
        }

        if (edt != null && edt.getId() == R.id.edt_create_account_3_secret_question_1)
            result = RuleBook.isSecQuestion(s.toString());
        if (edt != null && edt.getId() == R.id.edt_create_account_3_secret_question_2)
            result = RuleBook.isSecQuestion(s.toString());
        if (edt != null && edt.getId() == R.id.edt_create_account_3_secret_question_3)
            result = RuleBook.isSecQuestion(s.toString());


        String cardType = getTextVal(txt_create_account_4_card);
        boolean isCardAmex = false;
        if (cardType.startsWith("America"))
            isCardAmex = true;

        if (edt != null && edt.getId() == R.id.edt_tab4_cardno)
            result = RuleBook.isCardNoValid(s.toString(), isCardAmex);

        if (edt != null && edt.getId() == R.id.edt_tab4_card_cvv)
            result = RuleBook.isCvvValid(s.toString(), isCardAmex);

        if (result) {
            if (rel != null)
                changeBgTextField(rel, R.color.green);
            edt.setBackgroundResource(R.drawable.grey_border);
        } else {
            if (rel != null)
                changeBgTextField(rel, R.color.grey);
            edt.setBackgroundResource(R.drawable.edit_warning_border);
        }

        edt.setPadding(val[0], val[1], val[2], val[3]);
        return result;
    }

    public void onclickNextButtons(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {

//            case R.id.btn_create_accnt_tab1_2:
//                toggle2();
//                break;
//            case R.id.btn_create_accnt_tab1_1:
//                startActivity(new Intent(CreateAccountActivity.this,
//                        SetupAccountActivity.class));
//                //finish();
//                break;
            case R.id.ll_tab2_nxt_btn:

                if (edt_tab2_pwd.length() < 8) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(
                            "Password must have a minimum of 8 characters and at least 1 uppercase letter, 1 number, and 1 special character other than $.")
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    goToTab3();
                }
                break;
            case R.id.ll_tab3_nxt_btn:
                // toggle4();
                goToTab4();
                break;
            case R.id.ll_tab4_nxt_btn:
                // toggle5();
                goToTab5();
                break;
            case R.id.ll_tab5_nxt_btn:
                // finish();
                goToTabLast();
                break;

            default:
                break;
        }

    }

    private void initArrows(View viewTab2, View viewTab3, View viewTab4,
                            View viewTab5) {

        // TODO Auto-generated method stub
        txt_next2 = (EditText) viewTab2
                .findViewById(R.id.txt_create_account_next2);
        txt_next3 = (EditText) viewTab3
                .findViewById(R.id.txt_create_account_next3);
        txt_next4 = (EditText) viewTab4
                .findViewById(R.id.txt_create_account_next4);
        txt_next5 = (EditText) viewTab5
                .findViewById(R.id.txt_create_account_next5);

        setArrowFonts(txt_next2, txt_next3, txt_next4, txt_next5);
    }

    private void setArrowFonts(TextView txt_next2, TextView txt_next3,
                               TextView txt_next4, TextView txt_next5) {
        // TODO Auto-generated method stub
        txt_next2.setTypeface(font);
        txt_next3.setTypeface(font);
        txt_next4.setTypeface(font);
        txt_next5.setTypeface(font);
    }

//    protected void toggle1() {
//        // TODO Auto-generated method stub
//        layoutTab1.setVisibility(View.VISIBLE);
//        layoutTab2.setVisibility(View.GONE);
//        layoutTab3.setVisibility(View.GONE);
//        layoutTab4.setVisibility(View.GONE);
//        layoutTab5.setVisibility(View.GONE);
//
//        circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
//        circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
//        circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
//        circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
//    }

    protected void toggle2() {
        // TODO Auto-generated method stub
        //layoutTab1.setVisibility(View.GONE);
        layoutTab2.setVisibility(View.VISIBLE);
        layoutTab3.setVisibility(View.GONE);
        layoutTab4.setVisibility(View.GONE);
        layoutTab5.setVisibility(View.GONE);

        if (RuleBook.allTabFieldsValid(new View[]{rel_tab2_1, rel_tab2_2,
                rel_tab2_3, rel_tab2_4}))
            circletab2
                    .setBackgroundResource(R.drawable.shape_circle_green_color);
        else
            circletab2
                    .setBackgroundResource(R.drawable.shape_circle_blue_color);

        circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_3.setText("2");

        circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_4.setText("3");

        circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_5.setText("4");
    }


    protected void toggle3() {
        // TODO Auto-generated method stub
        //  layoutTab1.setVisibility(View.GONE);
        layoutTab2.setVisibility(View.GONE);
        layoutTab3.setVisibility(View.VISIBLE);
        layoutTab4.setVisibility(View.GONE);
        layoutTab5.setVisibility(View.GONE);

        // if (RuleBook.allTabFieldsValid(new View[] { rel_tab2_1, rel_tab2_2,
        // rel_tab2_3, rel_tab2_4 }))
        {
            circletab2
                    .setBackgroundResource(R.drawable.shape_circle_green_color);
            txt_circle_2.setText(getResources().getString(R.string.fa_check));
        }
        // else {
        // circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
        // txt_circle_2.setText("2");
        // }
        circletab3.setBackgroundResource(R.drawable.shape_circle_blue_color);
        txt_circle_3.setText("2");

        circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_4.setText("3");

        circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_5.setText("4");
        // circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
        // circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
    }

    protected void toggle4() {
        // TODO Auto-generated method stub
        //  layoutTab1.setVisibility(View.GONE);
        layoutTab2.setVisibility(View.GONE);
        layoutTab3.setVisibility(View.GONE);
        layoutTab4.setVisibility(View.VISIBLE);
        layoutTab5.setVisibility(View.GONE);

        // circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
        // circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
        circletab4.setBackgroundResource(R.drawable.shape_circle_blue_color);
        txt_circle_4.setText("3");

        circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
        txt_circle_5.setText("4");

        // circletab5.setBackgroundResource(R.drawable.shape_circle_grey);
    }

    protected void toggle5() {
        // TODO Auto-generated method stub
        //  layoutTab1.setVisibility(View.GONE);
        layoutTab2.setVisibility(View.GONE);
        layoutTab3.setVisibility(View.GONE);
        layoutTab4.setVisibility(View.GONE);
        layoutTab5.setVisibility(View.VISIBLE);

        // circletab2.setBackgroundResource(R.drawable.shape_circle_grey);
        // circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
        // circletab4.setBackgroundResource(R.drawable.shape_circle_grey);
        circletab2
                .setBackgroundResource(R.drawable.shape_circle_green_color);
        txt_circle_2.setText(getResources().getString(R.string.fa_check));

        circletab3
                .setBackgroundResource(R.drawable.shape_circle_green_color);
        txt_circle_3.setText(getResources().getString(R.string.fa_check));

        circletab4
                .setBackgroundResource(R.drawable.shape_circle_green_color);
        txt_circle_4.setText(getResources().getString(R.string.fa_check));

        circletab5.setBackgroundResource(R.drawable.shape_circle_blue_color);
    }

    private void init() {
        // TODO Auto-generated method stub

        txt_cross = (TextView) findViewById(R.id.txt_cross);

        //circletab1 = findViewById(R.id.view_tab1);
        circletab2 = findViewById(R.id.view_tab2);
        circletab3 = findViewById(R.id.view_tab3);
        circletab4 = findViewById(R.id.view_tab4);
        circletab5 = findViewById(R.id.view_tab5);

        // layoutTab1 = findViewById(R.id.layout_create_accnt_tab1);
        layoutTab2 = findViewById(R.id.layout_create_accnt_tab2);
        layoutTab3 = findViewById(R.id.layout_create_accnt_tab3);
        layoutTab4 = findViewById(R.id.layout_create_accnt_tab4);
        layoutTab5 = findViewById(R.id.layout_create_accnt_tab5);

        //  rel_tab1 = (RelativeLayout) findViewById(R.id.rel_tab1);
        rel_tab2 = (RelativeLayout) findViewById(R.id.rel_tab2);
        rel_tab3 = (RelativeLayout) findViewById(R.id.rel_tab3);
        rel_tab4 = (RelativeLayout) findViewById(R.id.rel_tab4);
        rel_tab5 = (RelativeLayout) findViewById(R.id.rel_tab5);
        txt_create_account_next1 = (RelativeLayout) findViewById(R.id.txt_create_account_next1);

        txt_circle_2 = (TextView) findViewById(R.id.txt_circle_2);
        txt_circle_3 = (TextView) findViewById(R.id.txt_circle_3);
        txt_circle_4 = (TextView) findViewById(R.id.txt_circle_4);
        txt_circle_5 = (TextView) findViewById(R.id.txt_circle_5);

//        btn_create_accnt_tab1_1 = (Button) findViewById(R.id.btn_create_accnt_tab1_1);
//        btn_create_accnt_tab1_2 = (Button) findViewById(R.id.btn_create_accnt_tab1_2);

        chk_tab4 = (CheckBox) findViewById(R.id.chk_tab4);
        ll_tab4_chkbox = (LinearLayout) findViewById(R.id.ll_tab4_chkbox);

        txt_create_account_3_state = (TextView) findViewById(R.id.txt_create_account_3_state);
        txt_create_account_4_card = (TextView) findViewById(R.id.txt_create_account_4_card);
        txt_create_account_4_month = (TextView) findViewById(R.id.txt_create_account_4_month);
        txt_create_account_4_year = (TextView) findViewById(R.id.txt_create_account_4_year);
        txt_create_account_4_state = (TextView) findViewById(R.id.txt_create_account_4_state);
        txt_create_account_5_state = (TextView) findViewById(R.id.txt_create_account_5_state);

        txt_changetoll_arrow = (TextView) findViewById(R.id.txt_changetoll_arrow);
        txt_changetoll_arrow_4 = (TextView) findViewById(R.id.txt_changetoll_arrow_4);
        txt_changetoll_arrow_4_2 = (TextView) findViewById(R.id.txt_changetoll_arrow_4_2);
        txt_changetoll_arrow_4_3 = (TextView) findViewById(R.id.txt_changetoll_arrow_4_3);
        txt_changetoll_arrow_4_4 = (TextView) findViewById(R.id.txt_changetoll_arrow_4_4);
        txt_changetoll_arrow_5 = (TextView) findViewById(R.id.txt_changetoll_arrow_5);
    }

    /**
     * click event to direct user to Login screen
     */
    public void onClickBack(View view) {

//		startActivity(new Intent(CreateAccountActivity.this,
//				LoginActivity.class));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                "Are you sure you want to cancel without saving changes?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                finish();
                                Intent intent = new Intent(context,
                                        LoginScreenActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    /**
     * returns value of the edit field
     */
    CharSequence getEdtVale(EditText edt) {
        return edt.getText().toString().trim();
    }

    /**
     * returns value of the edit field
     */
    String getEdtValue(EditText edt) {
        return edt.getText().toString().trim();
    }

    public void onClickShowDropdown(View view) {
        switch (view.getId()) {
            case R.id.ll_create_account_3_state:

                final CharSequence[] array_1 = stateList
                        .toArray(new CharSequence[stateList.size()]);
                ScreenUtils.showMultipleChoiceDlg(array_1, this,
                        txt_create_account_3_state, "state", this, txt_tab3_drvrlic_append_state);

                break;
            case R.id.ll_create_account_3_county:

                final CharSequence[] array_county = countyList
                        .toArray(new CharSequence[countyList.size()]);
                ScreenUtils.showMultipleChoiceDlg(array_county, this,
                        txt_tab3_county, "county", this, null);

                break;
            case R.id.ll_create_account_4_county:

                final CharSequence[] array_county4 = countyList
                        .toArray(new CharSequence[countyList.size()]);
                ScreenUtils.showMultipleChoiceDlg(array_county4, this,
                        txt_tab4_county, "county", this, null);

                break;

            case R.id.ll_create_account_4_card_type:

                final CharSequence[] array_2 = {"MasterCard", "Visa",
                        "American Express", "Discover"};
                showMultipleChoiceDlg(array_2, this,
                        txt_create_account_4_card, "card type", this, null);
                break;

            case R.id.ll_create_account_4_year:

                showMultipleChoiceDlg(Utils.returnYears(), this,
                        txt_create_account_4_year, "year", this, null);
                break;

            case R.id.ll_create_account_4_month:

                final CharSequence[] array_4 = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
                showMultipleChoiceDlg(array_4, this,
                        txt_create_account_4_month, "month", this, null);
                break;

            case R.id.ll_create_account_4_state:

                final CharSequence[] array_5 = stateList
                        .toArray(new CharSequence[stateList.size()]);
                ScreenUtils.showMultipleChoiceDlg(array_5, this,
                        txt_create_account_4_state, "state", this, null);

                break;
            case R.id.ll_create_account_5_state:
                final CharSequence[] array_6 = stateList
                        .toArray(new CharSequence[stateList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_6, this,
                        txt_create_account_5_state, "state", this, null);
                break;
            case R.id.ll_create_account_3_secret_question_1:
                final CharSequence[] array_7 = questionList
                        .toArray(new CharSequence[questionList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_7, this,
                        txt_create_account_3_secret_question_1, "Secret Question", this, null);
                break;
            case R.id.ll_create_account_3_secret_question_2:
                final CharSequence[] array_8 = questionList
                        .toArray(new CharSequence[questionList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_8, this,
                        txt_create_account_3_secret_question_2, "Secret Question", this, null);
                break;
            case R.id.ll_create_account_3_secret_question_3:
                final CharSequence[] array_9 = questionList
                        .toArray(new CharSequence[questionList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_9, this,
                        txt_create_account_3_secret_question_3, "Secret Question", this, null);
                break;
            case R.id.ll_create_account_3_source:
                final CharSequence[] array_10 = sourceList
                        .toArray(new CharSequence[sourceList.size()]);

                ScreenUtils.showMultipleChoiceDlg(array_10, this,
                        txt_create_account_3_source, "Source", this, null);
                break;

            default:
                break;
        }
    }

    private void goToTab3() {
        if (RuleBook.allTabFieldsValid(new View[]{rel_tab2_1, rel_tab2_2,
                rel_tab2_3, rel_tab2_4})) {
            createAccountData.setUserId(getEdtValue(edt_tab2_username));
            createAccountData.setEmailAddr(getEdtValue(edt_tab2_email));
            createAccountData.setPw(getEdtValue(edt_tab2_pwd));
            Log.e("CreateAccount", "createAccountData-- " + createAccountData.toString());

            toggle3();
        } else {
            validations(edt_tab2_username, rel_tab2_1,
                    getEdtVale(edt_tab2_username), false);
            validations(edt_tab2_email, rel_tab2_2, getEdtVale(edt_tab2_email),
                    true);
            validations(edt_tab2_pwd, rel_tab2_3, getEdtVale(edt_tab2_pwd),
                    false);
            validations(edt_tab2_confpwd, rel_tab2_4,
                    getEdtVale(edt_tab2_confpwd), false);

            if (RuleBook.allTabFieldsValid(new View[]{rel_tab2_1, rel_tab2_2,
                    rel_tab2_3, rel_tab2_4})) {
                circletab2
                        .setBackgroundResource(R.drawable.shape_circle_green_color);
                txt_circle_2.setText(getResources()
                        .getString(R.string.fa_check));
            } else {
                circletab2
                        .setBackgroundResource(R.drawable.shape_circle_blue_color);
                txt_circle_2.setText("1");
            }
        }
    }

    private void goToTab4() {

        // goToTab3();
        boolean checkError = checkTab3();

        if (!checkError) {

            createAccountData.setFirstName(getEdtValue(edt_tab3_flname));
            createAccountData.setLastName(getEdtValue(edt_tab3_flname_last));
            createAccountData.setAddress(getEdtValue(edt_tab3_address));
            createAccountData.setCity(getEdtValue(edt_tab3_city));
            createAccountData.setState(txt_create_account_3_state.getText().toString());
            createAccountData.setDlState(txt_create_account_3_state.getText().toString());
            createAccountData.setZip(getEdtValue(edt_tab3_zip));
            createAccountData.setCounty(Utils.countyMap.get(txt_tab3_county.getText().toString()) + "");
            createAccountData.setHomePhone(getEdtValue(edt_tab3_phone));
            createAccountData.setDlNumbr(getEdtValue(edt_tab3_drvrlic));

            createAccountData.setSecQuestion1(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_1)));
            createAccountData.setSecAnswer1(getEdtValue(edt_create_account_3_secret_question_1));

            createAccountData.setSecQuestion2(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_2)));
            createAccountData.setSecAnswer2(getEdtValue(edt_create_account_3_secret_question_2));

            createAccountData.setSecQuestion3(Utils.questionMap.get(getTextVal(txt_create_account_3_secret_question_3)));
            createAccountData.setSecAnswer3(getEdtValue(edt_create_account_3_secret_question_3));
            createAccountData.setPIN(getEdtValue(edt_tab3_pin));

            toggle4();

            circletab3
                    .setBackgroundResource(R.drawable.shape_circle_green_color);
            txt_circle_3.setText(getResources().getString(R.string.fa_check));

            Log.e("CreateAccount", "createAccountData-- " + createAccountData.toString());
        } else {

            if (RuleBook.allTabFieldsValid(new View[]{rel_tab2_1, rel_tab2_2,
                    rel_tab2_3, rel_tab2_4})) {

                circletab3
                        .setBackgroundResource(R.drawable.shape_circle_blue_color);
                txt_circle_3.setText("2");
            } else {
                circletab3.setBackgroundResource(R.drawable.shape_circle_grey);
                txt_circle_3.setText("2");
            }
        }
    }

    private boolean checkTab3() {
        EditText editFields[] = new EditText[]{edt_tab3_flname,
                edt_tab3_flname_last, edt_tab3_address, edt_tab3_city,
                edt_tab3_zip, edt_tab3_phone, edt_tab3_drvrlic,
                edt_create_account_3_secret_question_1,
                edt_create_account_3_secret_question_2,
                edt_create_account_3_secret_question_3, edt_tab3_pin};
        boolean checkError = false;

        for (EditText edt : editFields) {
            validations(edt, null, getEdtVale(edt), false);
        }

        for (EditText edt : editFields) {
            if (!validations(edt, null, getEdtVale(edt), false)) {
                checkError = true;
                break;
            }
        }
        return checkError;
    }

    private void goToTab5() {

        // goToTab4();
        // goToTab3();
        EditText editFields1[] = new EditText[]{edt_tab4_cardno, edt_tab4_card_cvv};
        EditText editFields2[] = new EditText[]{edt_tab4_fln,
                edt_tab4_fln_last, edt_tab4_address, edt_tab4_city,
                edt_tab4_zip, edt_tab4_phn};
        boolean checkError1 = false;

        for (EditText edt : editFields1) {
            validations(edt, null, getEdtVale(edt), false);
        }
        for (EditText edt : editFields1) {
            if (!validations(edt, null, getEdtVale(edt), false)) {
                checkError1 = true;
                break;
            }
        }
        boolean checkError2 = false;

        for (EditText edt : editFields2) {
            validations(edt, null, getEdtVale(edt), false);
        }

        for (EditText edt : editFields2) {
            if (!validations(edt, null, getEdtVale(edt), false)) {
                checkError2 = true;
                break;
            }
        }

        String cardType = getTextVal(txt_create_account_4_card);
        String cardMonth = getTextVal(txt_create_account_4_month);
        String cardYear = getTextVal(txt_create_account_4_year);
        if (cardType.startsWith("card type")) {
            checkError1 = true;
            checkError2 = true;
            ll_create_account_4_card_type.setBackgroundResource(R.drawable.edit_warning_border);
        } else {
            ll_create_account_4_card_type.setBackgroundResource(R.drawable.grey_border);
        }
        if (cardMonth.startsWith("month")) {
            checkError1 = true;
            checkError2 = true;
            ll_create_account_4_month.setBackgroundResource(R.drawable.edit_warning_border);
        } else {
            ll_create_account_4_month.setBackgroundResource(R.drawable.grey_border);
        }
        if (cardYear.startsWith("year")) {
            checkError1 = true;
            checkError2 = true;
            ll_create_account_4_year.setBackgroundResource(R.drawable.edit_warning_border);
        } else {
            ll_create_account_4_year.setBackgroundResource(R.drawable.grey_border);
        }

        boolean checked = chk_tab4.isChecked();

        if (checked) {

            if (!checkError1) {

                if (Utils.isValidExpDate(getTextVal(txt_create_account_4_month) + " " + getTextVal(txt_create_account_4_year)))
                    toggle5();
                else {
                    ScreenUtils.raiseToast(context, "Enter a valid date !");
                }

                if (checked) {
                    createAccountData.setCardType(Utils.getCardIdfromName(cardType));
                    createAccountData.setCardNumber(getEdtValue(edt_tab4_cardno));
                    createAccountData.setExpDate(Utils.getMonthIdfromName(cardMonth) + "/" + cardYear.charAt(2) + "" + cardYear.charAt(3));
                    createAccountData.setCVV(getEdtValue(edt_tab4_card_cvv));
                    createAccountData.setCCAddrSame(false);

                    createAccountData.setBilling_firstName(getEdtValue(edt_tab3_flname));
                    createAccountData.setBilling_lastName(getEdtValue(edt_tab3_flname_last));
                    createAccountData.setBilling_address(getEdtValue(edt_tab3_address));
                    createAccountData.setBilling_city(getEdtValue(edt_tab3_city));
                    createAccountData.setBilling_state(txt_create_account_3_state.getText().toString());
                    createAccountData.setBilling_zip(getEdtValue(edt_tab3_zip));
                    createAccountData.setBilling_county(Utils.countyMap.get(txt_tab3_county.getText().toString()) + "");
                    createAccountData.setHomePhone(getEdtValue(edt_tab3_phone));
                } else {

                    createAccountData.setCardType(Utils.getCardIdfromName(cardType));
                    createAccountData.setCardNumber(getEdtValue(edt_tab4_cardno));
                    createAccountData.setExpDate(Utils.getMonthIdfromName(cardMonth) + "/" + cardYear.charAt(2) + "" + cardYear.charAt(3));
                    createAccountData.setCVV(getEdtValue(edt_tab4_card_cvv));
                    createAccountData.setCCAddrSame(true);

                    createAccountData.setBilling_firstName("");
                    createAccountData.setBilling_lastName("");
                    createAccountData.setBilling_address("");
                    createAccountData.setBilling_city("");
                    createAccountData.setBilling_state("");
                    createAccountData.setBilling_zip("");
                    createAccountData.setBilling_county("");
                    createAccountData.setHomePhone("");

                }
                circletab4
                        .setBackgroundResource(R.drawable.shape_circle_green_color);
                txt_circle_4.setText(getResources()
                        .getString(R.string.fa_check));
                Log.e("CreateAccount", "createAccountData-- " + createAccountData.toString());

            } else {
                if (!checkTab3()) {
                    circletab4
                            .setBackgroundResource(R.drawable.shape_circle_blue_color);
                    txt_circle_4.setText("3");
                } else {
                    circletab4
                            .setBackgroundResource(R.drawable.shape_circle_grey);
                    txt_circle_4.setText("3");
                }
            }

        } else {

            if (!checkError1 && !checkError2) {

                createAccountData.setCardType(Utils.getCardIdfromName(cardType));
                createAccountData.setCardNumber(getEdtValue(edt_tab4_cardno));
                createAccountData.setExpDate(Utils.getMonthIdfromName(cardMonth) + "/" + cardYear.charAt(2) + "" + cardYear.charAt(3));
                createAccountData.setCVV(getEdtValue(edt_tab4_card_cvv));
                createAccountData.setCCAddrSame(false);

                createAccountData.setBilling_firstName(getEdtValue(edt_tab4_fln));
                createAccountData.setBilling_lastName(getEdtValue(edt_tab4_fln_last));
                createAccountData.setBilling_address(getEdtValue(edt_tab4_address));
                createAccountData.setBilling_city(getEdtValue(edt_tab4_city));
                createAccountData.setBilling_state(txt_create_account_4_state.getText().toString());
                createAccountData.setBilling_zip(getEdtValue(edt_tab4_zip));
                createAccountData.setBilling_county(Utils.countyMap.get(txt_tab4_county.getText().toString()) + "");
                createAccountData.setHomePhone(getEdtValue(edt_tab4_phn));

                Log.e("CreateAccount", "createAccountData else-- " + createAccountData.toString());


                if (Utils.isValidExpDate(getTextVal(txt_create_account_4_month) + " " + getTextVal(txt_create_account_4_year)))
                    toggle5();
                else {
                    ScreenUtils.raiseToast(context, "Enter a valid date !");
                }

                circletab4
                        .setBackgroundResource(R.drawable.shape_circle_green_color);
                txt_circle_4.setText(getResources()
                        .getString(R.string.fa_check));
            } else {

                if (!checkTab3()) {
                    circletab4
                            .setBackgroundResource(R.drawable.shape_circle_blue_color);
                    txt_circle_4.setText("4");
                } else {
                    circletab4
                            .setBackgroundResource(R.drawable.shape_circle_grey);
                    txt_circle_4.setText("4");
                }
            }

        }

    }


    @SuppressLint("StaticFieldLeak")
    private void goToTabLast() {

        // goToTab5();
        // goToTab4();
        // goToTab3();

        EditText editFields[] = new EditText[]{edt_tab5_vehy,
                edt_tab5_vehmake, edt_tab5_vehmodel, edt_tab5_vehcolor,
                edt_tab5_vehpltn};
        boolean checkError = false;

        for (EditText edt : editFields) {
            validations(edt, null, getEdtVale(edt), false);
        }

        for (EditText edt : editFields) {
            if (!validations(edt, null, getEdtVale(edt), false)) {
                checkError = true;
                break;
            }
        }
        if (!checkError) {

            circletab5
                    .setBackgroundResource(R.drawable.shape_circle_green_color);
            txt_circle_5.setText(getResources().getString(R.string.fa_check));
            //finish();
            createAccountData.setVehicle_year(getEdtValue(edt_tab5_vehy));
            createAccountData.setVehicle_make(getEdtValue(edt_tab5_vehmake));
            createAccountData.setVehicle_model(getEdtValue(edt_tab5_vehmodel));
            createAccountData.setVehicle_color(getEdtValue(edt_tab5_vehcolor));
            createAccountData.setVehicle_licPlate(getEdtValue(edt_tab5_vehpltn));
            createAccountData.setVehicle_state(txt_create_account_5_state.getText().toString());
            createAccountData.setVehicle_activationCode(getEdtValue(edt_tab5_vehactivationcode));
            createAccountData.setVehicle_tagId(getEdtValue(edt_tab5_vehTagId));

            Log.e("newlog", createAccountData.toString());

            new AsyncTask<CreateAccountData, String, String>() {
                ProgressDialog pdialog = ScreenUtils
                        .returnProgDialogObj(
                                context,
                                WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
                                false);

                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(CreateAccountData... params) {
                    // TODO Auto-generated method stub
                    try {

                        String requestBody = WebserviceUtils.readFromAsset("setupAccount", context);
                        requestBody = requestBody.replaceAll("acctTypeId", params[0].getAccountTypeId() + "");
                        requestBody = requestBody.replaceAll("_firstName_", params[0].getFirstName());
                        requestBody = requestBody.replaceAll("_lastName_", params[0].getLastName());
                        requestBody = requestBody.replaceAll("_emailAddr_", params[0].getEmailAddr());
                        requestBody = requestBody.replaceAll("_userId_", params[0].getUserId());
                        requestBody = requestBody.replaceAll("_pw_", params[0].getPw());
                        requestBody = requestBody.replaceAll("_secQuestion1_", params[0].getSecQuestion1() + "");
                        requestBody = requestBody.replaceAll("_secAnswer1_", params[0].getSecAnswer1());
                        requestBody = requestBody.replaceAll("_secQuestion2_", params[0].getSecQuestion2() + "");
                        requestBody = requestBody.replaceAll("_secAnswer2_", params[0].getSecAnswer2());
                        requestBody = requestBody.replaceAll("_secQuestion3_", params[0].getSecQuestion3() + "");
                        requestBody = requestBody.replaceAll("_secAnswer3_", params[0].getSecAnswer3());
                        requestBody = requestBody.replaceAll("_address_", params[0].getAddress());
                        requestBody = requestBody.replaceAll("_city_", params[0].getCity());
                        requestBody = requestBody.replaceAll("_state_", params[0].getState());
                        requestBody = requestBody.replaceAll("_zip_", params[0].getZip());
                        requestBody = requestBody.replaceAll("_county_", params[0].getCounty());
                        requestBody = requestBody.replaceAll("_homePhone_", params[0].getHomePhone());
                        requestBody = requestBody.replaceAll("_dlState_", params[0].getDlState());
                        requestBody = requestBody.replaceAll("_dlNumbr_", params[0].getDlNumbr());
                        requestBody = requestBody.replaceAll("_isCCAddrSame_", params[0].isCCAddrSame() + "");
                        requestBody = requestBody.replaceAll("_cardType_", params[0].getCardType());
                        requestBody = requestBody.replaceAll("_cardNumber_", params[0].getCardNumber());
                        requestBody = requestBody.replaceAll("_expDate_", params[0].getExpDate());
                        requestBody = requestBody.replaceAll("_bfirstName_", params[0].getBilling_firstName());
                        requestBody = requestBody.replaceAll("_blastName_", params[0].getBilling_lastName());
                        requestBody = requestBody.replaceAll("_baddress_", params[0].getBilling_address());
                        requestBody = requestBody.replaceAll("_bcity_", params[0].getBilling_city());
                        requestBody = requestBody.replaceAll("_bstate_", params[0].getBilling_state());
                        requestBody = requestBody.replaceAll("_bzip_", params[0].getBilling_zip());
                        requestBody = requestBody.replaceAll("_bcounty_", params[0].getBilling_county());
                        requestBody = requestBody.replaceAll("_year_", params[0].getVehicle_year());
                        requestBody = requestBody.replaceAll("_color_", params[0].getVehicle_color());
                        requestBody = requestBody.replaceAll("_make_", params[0].getVehicle_make());
                        requestBody = requestBody.replaceAll("_model_", params[0].getVehicle_model());
                        //requestBody=requestBody.replaceAll("_tagId_", params[0].getVehicle_tagId());
                        //requestBody=requestBody.replaceAll("_activationCode_", params[0].getVehicle_activationCode());
                        requestBody = requestBody.replaceAll("_licPlate_", params[0].getVehicle_licPlate());
                        requestBody = requestBody.replaceAll("_vstate_", params[0].getVehicle_state());
                        requestBody = requestBody.replaceAll("_CVV_", params[0].getCVV());
                       // requestBody = requestBody.replaceAll("acctTypeId", params[0].getAccountTypeId() + "");


                        Log.e("peachpass", requestBody + "");
                        String result = WebserviceUtils.sendSoapRequest(context, requestBody, WebserviceUtils.SOAP_ACTION_SETUPACCOUNT);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM element

                        NodeList nl = doc.getElementsByTagName("result");

                        for (int i = 0; i < nl.getLength(); ) {

                            Element e = (Element) nl.item(i);

                            String status = parser.getValue(e, "ns1:status");

                            return status;
                        }

                        //return "0";
                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String result) {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        if (result != null) {

                            if (result.equals("0")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage(
                                        "Account created successfully. You will receive an email shortly with instructions on logging in and configuring your Peach Pass account.")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id) {
                                                        finish();
                                                    }
                                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            } else {
                                String errorStatus = Utils.getStringResourceByName(
                                        "status_" + result, context);
                                // Log.e("peachpass", errorStatus +
                                // "****errorStatus");
                                ScreenUtils.raiseToast(context, errorStatus);
                                Log.e("peachpass", "zipcode error");
                            }

                        } else {

                            ScreenUtils.raiseToast(context, WebserviceUtils.SOMETHING_WENT_WRONG);
                        }


                    } catch (Exception e) {
                        // TODO: handle exception
                        ScreenUtils.raiseToast(context, WebserviceUtils.SOMETHING_WENT_WRONG);
                    }
                    super.onPostExecute(result);
                }
            }.execute(new CreateAccountData[]{createAccountData});


        } else {
            circletab5
                    .setBackgroundResource(R.drawable.shape_circle_blue_color);
            txt_circle_5.setText("4");
        }
    }

    String getTextVal(TextView textView) {
        return textView.getText().toString();
    }


    void showMultipleChoiceDlg(CharSequence[] array, final Context ctx,
                               final TextView text, final String title, final Activity ac, final TextView licState) {

        ScreenUtils.hideKeyboard(text, ctx);

        final CharSequence[] items = array;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Choose a " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                CharSequence item = items[which];
                String itemString = item + "";
                text.setText(itemString);
                if (title.equals("card type")) {
                    if (itemString.startsWith("card"))
                        ll_create_account_4_card_type.setBackgroundResource(R.drawable.edit_warning_border);
                    else
                        ll_create_account_4_card_type.setBackgroundResource(R.drawable.grey_border);
                }
                if (title.equals("month")) {
                    if (itemString.startsWith("month"))
                        ll_create_account_4_month.setBackgroundResource(R.drawable.edit_warning_border);
                    else ll_create_account_4_month.setBackgroundResource(R.drawable.grey_border);
                }
                if (title.equals("year")) {
                    if (itemString.startsWith("year"))
                        ll_create_account_4_year.setBackgroundResource(R.drawable.edit_warning_border);
                    else ll_create_account_4_year.setBackgroundResource(R.drawable.grey_border);
                }

            }
        });

        builder.show();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.CREATE_VIEW);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txt_next2.getWindowToken(), 0);
    }

    private void hideKeyboard1() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txt_next3.getWindowToken(), 0);
    }

    private void hideKeyboard2() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txt_next4.getWindowToken(), 0);
    }

    private void hideKeyboard3() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txt_next4.getWindowToken(), 0);
    }


}
