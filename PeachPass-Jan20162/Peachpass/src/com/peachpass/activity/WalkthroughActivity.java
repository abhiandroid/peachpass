package com.peachpass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.adapter.TestFragmentAdapter;
import com.peachpass.utils.LayoutTouchListener;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class WalkthroughActivity extends FragmentActivity {

    TestFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    Context context;
    public static int CURRENT_TAB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);
        setContentView(R.layout.activity_walkthrough);

        init();

        mAdapter = new TestFragmentAdapter(getSupportFragmentManager());

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOnTouchListener(new LayoutTouchListener(this, 2));
        mPager.setAdapter(mAdapter);

        final CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator = indicator;
        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;
        // indicator.setBackgroundColor(0xFFCCCCCC);
        indicator.setRadius(7 * density);
        indicator.setPageColor(0xFFCDCCCB);
        indicator.setFillColor(0xFF75BE43); // green
        indicator.setStrokeColor(0xFFCDCCCB);
        indicator.setStrokeWidth(0 * density);

        /**
         * set listener to indicators to capture the event of the tabs that are
         * in focus
         */
        indicator.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                //ll_main_tab.setWeightSum(1);

                CURRENT_TAB = arg0;
                switch (arg0) {
                    case 4:
                        method1();
                        break;

                    default:
                        method2();
                        break;

                }
            }

            /** temporarily make visible the footer buttons for the first 4 tabs */
            private void method2() {
                indicator.setVisibility(View.VISIBLE);

            }

            /** temporarily hide the footer buttons for the last tab */
            private void method1() {
                indicator.setVisibility(View.GONE);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        /**
         * detect right-left gesture for the first page. if swipe detected ,
         * direct user towards the Login screen
         */
        mPager.setOnTouchListener(new OnSwipeTouchListener() {
            public void onSwipeRight() {

                if (CURRENT_TAB == 0) {
                    startActivity(new Intent(WalkthroughActivity.this,
                            LoginScreenActivity.class));
                    overridePendingTransition(R.anim.slide_in_left,
                            R.anim.animation_leave);
                    finish();
                }

            }

        });

        Utils.setTracker(Utils.WALKTHROUGH_VIEW);
    }

    /**
     * initialize all the widgets
     */
    private void init() {
        // TODO Auto-generated method stub
//		ll_main_tab = (LinearLayout) findViewById(R.id.ll_main_tab);
//		ll_top_tab = (LinearLayout) findViewById(R.id.ll_top_tab);
//		ll_bottom_tab = (LinearLayout) findViewById(R.id.ll_bottom_tab);
//		ll_bototm_tab_1 = (LinearLayout) findViewById(R.id.ll_bototm_tab_1);
//		ll_bototm_tab_2 = (LinearLayout) findViewById(R.id.ll_bototm_tab_2);
    }

    /**
     * click events to footer buttons
     */
    public void onClickChangeSlide(View view) {
        if (view.getId() == R.id.ll_bototm_tab_1) {
            Intent i2 = new Intent(this, LoginScreenActivity.class);
            i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i2);
            finish();
            overridePendingTransition(R.anim.slide_in_left,
                    R.anim.animation_leave);
        } else if (view.getId() == R.id.ll_bototm_tab_2) {

            Intent i2 = new Intent(this, CreateAccountInfoActivity.class);
            i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i2);
            finish();
            overridePendingTransition(R.anim.slide_in_left,
                    R.anim.animation_leave);

        }

    }

    /**
     * Swipe gesture detection method
     */
    class OnSwipeTouchListener implements OnTouchListener {

        @SuppressWarnings("deprecation")
        private final GestureDetector gestureDetector = new GestureDetector(
                new GestureListener());

        public boolean onTouch(final View v, final MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                onTouch(e);
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2,
                                   float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD
                                && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                        }
                    } else {
                        // onTouch(e);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onTouch(MotionEvent e) {
        }

        public void onSwipeRight() {

        }

        public void onSwipeLeft() {
            /** nothing, this means,swipes to left will be ignored*/
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.WALKTHROUGH_VIEW);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }


}
