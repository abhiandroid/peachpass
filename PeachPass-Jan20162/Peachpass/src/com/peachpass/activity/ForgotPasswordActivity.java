package com.peachpass.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

public class ForgotPasswordActivity extends Activity {

    TextView txt_frgt_pwd, txt_frgt_email_id;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /**Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);

        setContentView(R.layout.forget_password);

        init();
        Typeface font = ScreenUtils.returnTypeFace(this);

        txt_frgt_pwd = (TextView) findViewById(R.id.txt_frgt_pwd);
        txt_frgt_pwd.setTypeface(font);

        txt_frgt_email_id = (TextView) findViewById(R.id.txt_frgt_email_id);
        txt_frgt_email_id.setTypeface(font);

    }

    private void init() {
        // TODO Auto-generated method stub

    }

    public void onclickNextButtons(View view) {
        finish();
    }

    public void onClickBack(View view) {
        onBackPressed();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(this);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

        FlurryAgent.logEvent(Utils.FORGOT_PASSWORD);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(this);

        FlurryAgent.onEndSession(context);
    }
}
