package com.peachpass.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.NotifPrefData;
import com.peachpass.data.VehicleData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class EditSettingsActivity extends Activity {

	Context context;

	TextView txt_update_account_next4;

	View[] view = new View[17];

	int ids[] = { R.id.ll_1, R.id.ll_2, R.id.ll_3, R.id.ll_4, R.id.ll_5,
			R.id.ll_6, R.id.ll_7, R.id.ll_8, R.id.ll_9, R.id.ll_10, R.id.ll_11,
			R.id.ll_12, R.id.ll_13, R.id.ll_14, R.id.ll_15, R.id.ll_16, R.id.ll_17 };
	
	CheckBox[] checkBoxEmail = new CheckBox[17];
	CheckBox[] checkBoxPush = new CheckBox[17];
	TextView[] textView = new TextView[17];
	
	String emailChecked_org ="";
	String pushChecked_org ="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Set the screen to full screen mode before setting the layout */
		ScreenUtils.fullScreen(this);
		context = this;
		setContentView(R.layout.activity_editnotification);

		txt_update_account_next4 = (TextView) findViewById(R.id.txt_update_account_next4);
		txt_update_account_next4.setTypeface(ScreenUtils.returnTypeFace(this));
		
		
		for(int i=0;i<ids.length;i++){
		 View view = findViewById(ids[i]);
		 checkBoxEmail[i] = (CheckBox) view.findViewById(R.id.chk_email);
		 checkBoxPush[i] = (CheckBox) view.findViewById(R.id.chk_push);
		 textView[i] = (TextView) view.findViewById(R.id.txt_email);
		}
		
		try {
			setCheckStatus();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		emailChecked_org = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_EMAIL);
		pushChecked_org = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_PUSH);
		
		//String splitEmail[] = emailChecked.split("\\,");
		//String splitPush[] = pushChecked.split("\\,");
		
		try {
			loadAccountPreferences();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(CheckBox chk: checkBoxEmail){
			chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
					        @Override
					        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					            //do stuff
					        	saveChanges();
				
					        }
					    });
			
		}

		Utils.setTracker(Utils.EDITSETTINGS_VIEW);
	}

	private void loadAccountPreferences() throws Exception {
		// TODO Auto-generated method stub
			new AsyncTask<String, String, ArrayList<NotifPrefData>>() {
				ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
						context,
						WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
						false);

				@Override
				protected void onPreExecute() {
					try {
						pdialog.show();
					} catch (Exception e) {
						// TODO: handle exception
					}

				};

				@Override
				protected ArrayList<NotifPrefData> doInBackground(
						String... params) {
					// TODO Auto-generated method stub
					try {
						VehicleData vechileTaginfo = getTagInfoList();
						if(vechileTaginfo!=null){
						ArrayList<NotifPrefData> vechileArrayTaginfo = getNotifPrefData(vechileTaginfo.getVehicleId_new());
						
						return vechileArrayTaginfo;
						}

					} catch (Exception e) {
						// TODO: handle exception

					}
					return null;
				}

				@Override
				protected void onPostExecute(ArrayList<NotifPrefData> result) {
					// TODO Auto-generated method stub
					super.onPostExecute(result);

					try {
						pdialog.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {

						if (result != null && result.size() >0) {
							int count = result.size();
							for(int i=0;i<count;i++){
								
								String val = result.get(i).getComValue();
								if(val!=null && val.trim().length()>0){
									textView[i].setText(val);
									checkBoxEmail[i].setChecked(true);
									Log.e("peachpass", "val--"+val);
								}else{
									Log.e("peachpass", "val--"+val);
									checkBoxEmail[i].setChecked(false);
								}
							}
							
						} else {
							
							//handleVisibility(new int[]{View.GONE,View.GONE,View.VISIBLE});
							for(int i=0;i<ids.length;i++){
								checkBoxEmail[i].setChecked(false);
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						try {
							//handleVisibility(new int[]{View.GONE,View.VISIBLE,View.GONE});
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				}

				private VehicleData getTagInfoList()
						throws Exception {

					ArrayList<VehicleData> vechileArray = new ArrayList<VehicleData>();
					String requestBody = WebserviceUtils.readFromAsset(
							"getAccountVehicleTollMode", context);
					requestBody = requestBody.replaceAll("_accountId_",
							Utils.getAccountId(context));
					requestBody = requestBody.replaceAll("_userName_",
							Utils.getUsername(context));
					requestBody = requestBody.replaceAll("_sessionId_",
							Utils.getSessionId(context));

					requestBody = requestBody.replaceAll("_osType_",
							WebserviceUtils.OS_TYPE);
					requestBody = requestBody.replaceAll("_osVersion_",
							Utils.getOsVersion());
					requestBody = requestBody.replaceAll("_ipAddress_",
							WebserviceUtils.getExternalIP());

					requestBody = requestBody.replaceAll("_id_",
							WebserviceUtils.ID);

					Log.e("peachpass", requestBody + "$$$$$$$$");
					String result = WebserviceUtils
							.sendSoapRequest(
									context,
									requestBody,
									WebserviceUtils.SOAP_ACTION_GET_ACCOUNT_VEHICLE_TOLL_MODE);
					XMLParser parser = new XMLParser();
					String xml = result;
					Document doc = parser.getDomElement(xml); // getting DOM
																// element
					NodeList nl = doc.getElementsByTagName("result");
					Element e = (Element) nl.item(0);
					String status = parser.getValue(e, "ns1:status");
					nl = doc.getElementsByTagName("ns1:vehicleTollModeList");

					for (int i = 0; i < nl.getLength(); i++) {

						e = (Element) nl.item(i);

						String vehicleId_ = parser.getValue(e, "ns1:vehicleId");
						String licPlate = parser.getValue(e, "ns1:licPlate");
						String make = parser.getValue(e, "ns1:make");
						String model = parser.getValue(e, "ns1:model");
						String color = parser.getValue(e, "ns1:color");
						String year = parser.getValue(e, "ns1:year");
						String tollMode = parser.getValue(e, "ns1:tollMode");
						String travelEndDate = parser.getValue(e,
								"ns1:travelEndDate");
						
						VehicleData vehicleData = new VehicleData(vehicleId_,
								licPlate, make, model, color, year, tollMode,
								travelEndDate);
						vechileArray.add(vehicleData);
						
					}
					return vechileArray.get(0);
				}
				
				private ArrayList<NotifPrefData> getNotifPrefData(String vehicleId)
						throws Exception {

					ArrayList<NotifPrefData> notifArray = new ArrayList<NotifPrefData>();
					String requestBody = WebserviceUtils.readFromAsset(
							"getAccountNotificationPrefs", context);
					requestBody = requestBody.replaceAll("_accountId_",
							Utils.getAccountId(context));
					requestBody = requestBody.replaceAll("_userName_",
							Utils.getUsername(context));
					requestBody = requestBody.replaceAll("_sessionId_",
							Utils.getSessionId(context));

					requestBody = requestBody.replaceAll("_osType_",
							WebserviceUtils.OS_TYPE);
					requestBody = requestBody.replaceAll("_osVersion_",
							Utils.getOsVersion());
					requestBody = requestBody.replaceAll("_ipAddress_",
							WebserviceUtils.getExternalIP());
					requestBody = requestBody.replaceAll("_vehicleId_",vehicleId);

					requestBody = requestBody.replaceAll("_id_",
							WebserviceUtils.ID);

					Log.e("peachpass", requestBody + "$$$$$$$$");
					String result = WebserviceUtils
							.sendSoapRequest(
									context,
									requestBody,
									WebserviceUtils.SOAP_ACTION_GETACCOUNTNOTIFICATIONPREFS);
					XMLParser parser = new XMLParser();
					String xml = result;
					Document doc = parser.getDomElement(xml); // getting DOM element
					NodeList nl = doc.getElementsByTagName("result");
					Element e = (Element) nl.item(0);
					String status = parser.getValue(e, "ns1:status");
					nl = doc.getElementsByTagName("ns1:notificationPrefs");
					for (int i = 0; i < nl.getLength(); i++) {

						e = (Element) nl.item(i);

						String notifGroupName = parser.getValue(e, "ns1:notifGroupName");
						String notifTypeName = parser.getValue(e, "ns1:notifTypeName");
						String notificationMethod = parser.getValue(e, "ns1:notificationMethod");
						String comValue = parser.getValue(e, "ns1:comValue");
						
						NotifPrefData data = new NotifPrefData(notifGroupName, notifTypeName, notificationMethod, comValue);
						notifArray.add(data);
						
					}
					return notifArray;
				}


			}.execute();
	}

	public void onClickBack(View view) {
		
		saveChanges();
		
		String emailChecked_fresh = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_EMAIL);
		String pushChecked_fresh = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_PUSH);
		
		finish();
//		if(emailChecked_fresh.equals(emailChecked_org) && pushChecked_fresh.equals(pushChecked_org)){
//			
//			finish();
//		}else{
//			
//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
//			builder.setMessage(
//					"Are you sure you want to cancel without saving changes?")
//					.setCancelable(false)
//					.setPositiveButton("Yes",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int id) {
//									onBackPressed();
//								}
//							})
//					.setNegativeButton("No", new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int id) {
//							dialog.cancel();
//						}
//					});
//			AlertDialog alert = builder.create();
//			alert.show();
//		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		
	}

	public void onclickNextButtons(View view) {

		saveChanges();
		ScreenUtils.raiseToast(EditSettingsActivity.this,
				"Notification preferences saved sucessfully !");
		finish();
	}
	
	private void setCheckStatus() throws Exception{
		
		String emailChecked = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_EMAIL);
		String pushChecked = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.EDIT_NOTIFICATION_PUSH);
		
		String splitEmail[] = emailChecked.split("\\,");
		String splitPush[] = pushChecked.split("\\,");
		
		for(int i=0;i<splitEmail.length;i++){
			
			if(splitEmail[i]!=null && splitEmail[i].equals("1")){
				checkBoxEmail[i].setChecked(true);
			}else{
				checkBoxEmail[i].setChecked(false);
			}
		}
		
		for(int i=0;i<splitPush.length;i++){
			
			if(splitPush[i]!=null && splitPush[i].equals("1")){
				checkBoxPush[i].setChecked(true);
			}else{
				checkBoxPush[i].setChecked(false);
			}
		}
		
	}
	
	private void saveChanges(){
		int j=0;
		StringBuilder sbEmail = new StringBuilder();
		for(CheckBox chk: checkBoxEmail){
			Log.e("peachpass", chk.isChecked()+"--"+j);
			
			int status =0;
			if(chk.isChecked())
				status =1;
			else 
				status =0;
			if(j==checkBoxEmail.length-1)
			sbEmail.append(status+"");
			else sbEmail.append(status+",");
			j++;
		}
		SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.EDIT_NOTIFICATION_EMAIL, sbEmail.toString(),context);
		Log.e("peachpass", sbEmail.toString());
		j=0;
		sbEmail = new StringBuilder();
		for(CheckBox chk: checkBoxPush){
			Log.e("peachpass", chk.isChecked()+"--"+j);
			int status =0;
			if(chk.isChecked())
				status =1;
			else 
				status =0;
			if(j==checkBoxEmail.length-1)
			sbEmail.append(status+"");
			else sbEmail.append(status+",");
			j++;
		}
		SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.EDIT_NOTIFICATION_PUSH, sbEmail.toString(),context);
		Log.e("peachpass", sbEmail.toString());
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(context).reportActivityStart(this);

		FlurryAgent.onStartSession(context);
		FlurryAgent.setLogEvents(true);

		FlurryAgent.logEvent(Utils.EDITSETTINGS_VIEW);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

		FlurryAgent.onEndSession(context);
	}

}
