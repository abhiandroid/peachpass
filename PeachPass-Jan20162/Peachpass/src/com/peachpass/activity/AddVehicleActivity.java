package com.peachpass.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.AddVehicleData;
import com.peachpass.utils.RuleBook;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;

public class AddVehicleActivity extends Activity {

	TextView txt_create_account_next5, txt_edt_vehicle_state,
			txt_changetoll_arrow;
	EditText edt_tab5_vehy, edt_tab5_vehmake, edt_tab5_vehmodel,
			edt_tab5_vehcolor, edt_tab5_vehpltn;

	List<String> stateList = null;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Set the screen to full screen mode before setting the layout */
		ScreenUtils.fullScreen(this);
		context = this;
		stateList = Arrays.asList(this.getResources().getStringArray(
				R.array.state_type));

		setContentView(R.layout.add_vehicle);

		init();

		Typeface font = ScreenUtils.returnTypeFace(this);
		txt_create_account_next5.setTypeface(font);
		txt_changetoll_arrow.setTypeface(font);

		Typeface font1 = ScreenUtils.returnTypeFaceHelv(this);
		edt_tab5_vehy.setTypeface(font1);
		edt_tab5_vehmake.setTypeface(font1);
		edt_tab5_vehmodel.setTypeface(font1);
		edt_tab5_vehcolor.setTypeface(font1);
		edt_tab5_vehpltn.setTypeface(font1);

		txt_edt_vehicle_state.setText(stateList.get(9));

		edt_tab5_vehy.addTextChangedListener(new TabTextWatcher(edt_tab5_vehy));
		edt_tab5_vehmake.addTextChangedListener(new TabTextWatcher(
				edt_tab5_vehmake));
		edt_tab5_vehmodel.addTextChangedListener(new TabTextWatcher(
				edt_tab5_vehmodel));
		edt_tab5_vehcolor.addTextChangedListener(new TabTextWatcher(
				edt_tab5_vehcolor));
		edt_tab5_vehpltn.addTextChangedListener(new TabTextWatcher(
				edt_tab5_vehpltn));

		Utils.setTracker(Utils.ADDVEHICLE_VIEW);
	}

	private void init() {
		// TODO Auto-generated method stub
		txt_create_account_next5 = (TextView) findViewById(R.id.txt_create_account_next5);
		txt_edt_vehicle_state = (TextView) findViewById(R.id.txt_edt_vehicle_state);
		txt_changetoll_arrow = (TextView) findViewById(R.id.txt_changetoll_arrow);

		edt_tab5_vehy = (EditText) findViewById(R.id.edt_tab5_vehy);
		edt_tab5_vehmake = (EditText) findViewById(R.id.edt_tab5_vehmake);
		edt_tab5_vehmodel = (EditText) findViewById(R.id.edt_tab5_vehmodel);
		edt_tab5_vehcolor = (EditText) findViewById(R.id.edt_tab5_vehcolor);
		edt_tab5_vehpltn = (EditText) findViewById(R.id.edt_tab5_vehpltn);

	}

	public void onclickNextButtons(View view) {

//		String year = Utils.getEditTextValue(edt_tab5_vehy);
//		String make = Utils.getEditTextValue(edt_tab5_vehmake);
//		String model = Utils.getEditTextValue(edt_tab5_vehmodel);
//		String color = Utils.getEditTextValue(edt_tab5_vehcolor);
//		String pltn = Utils.getEditTextValue(edt_tab5_vehpltn);
//
//		if (year.trim().length() != 4) {
//			ScreenUtils.raiseToast(context, "Add a valid year !");
//			return;
//		}
//		if (make.trim().length() == 0) {
//			ScreenUtils.raiseToast(context, "Add a valid make !");
//			return;
//		}
//		if (model.trim().length() == 0) {
//			ScreenUtils.raiseToast(context, "Add a valid model !");
//			return;
//		}
//		if (color.trim().length() == 0) {
//			ScreenUtils.raiseToast(context, "Add a valid color !");
//			return;
//		}
//		if (pltn.trim().length() == 0) {
//			ScreenUtils.raiseToast(context, "Add a valid plate # !");
//			return;
//		}
		EditText[] edtValues = { edt_tab5_vehy, edt_tab5_vehmake,
				edt_tab5_vehmodel, edt_tab5_vehcolor, edt_tab5_vehpltn };
		
		int i=0;
		boolean valid= false;
		for(EditText edt: edtValues){
			if(i==0)
				valid= validations(edt, getEdtVale(edt), 0);
			else
				valid= validations(edt, getEdtVale(edt), 1);
			if(valid==false)
				break;
			i++;
		}
		

		if(valid)
		if (Utils.isNetworkAvailable(context)) {
			new AsyncTask<AddVehicleData, String, String>() {
				ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
						context,
						WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
						false);

				String status = "";

				@Override
				protected void onPreExecute() {
					try {
						pdialog.show();
					} catch (Exception e) {
						// TODO: handle exception
					}
				};

				@Override
				protected String doInBackground(AddVehicleData... params) {
					// TODO Auto-generated method stub
					try {

						Log.e("peachpass", params[0].toString());

						String requestBody = WebserviceUtils.readFromAsset(
								"addVehicle", context);

						requestBody = requestBody.replaceAll("_year_",
								params[0].getYear());
						requestBody = requestBody.replaceAll("_color_",
								params[0].getColor());
						requestBody = requestBody.replaceAll("_make_",
								params[0].getMake());
						requestBody = requestBody.replaceAll("_model_",
								params[0].getModel());
						requestBody = requestBody.replaceAll("_licensePlate_",
								params[0].getLicensePlate());
						requestBody = requestBody.replaceAll("_state_",
								params[0].getState());

						requestBody = requestBody.replaceAll("_accountId_",
								Utils.getAccountId(context));
						requestBody = requestBody.replaceAll("_userName_",
								Utils.getUsername(context));
						requestBody = requestBody.replaceAll("_sessionId_",
								Utils.getSessionId(context));

						requestBody = requestBody.replaceAll("_osType_",
								WebserviceUtils.OS_TYPE);
						requestBody = requestBody.replaceAll("_osVersion_",
								Utils.getOsVersion());
						requestBody = requestBody.replaceAll("_ipAddress_",
								WebserviceUtils.getExternalIP());

						requestBody = requestBody.replaceAll("_id_",
								WebserviceUtils.ID);

						Log.e("peachpass", requestBody + "");
						String result = WebserviceUtils.sendSoapRequest(
								context, requestBody,
								WebserviceUtils.SOAP_ACTION_ADDVEHICLE);
						XMLParser parser = new XMLParser();
						String xml = result;
						Log.e("peachpass", xml + "");
						Document doc = parser.getDomElement(xml); // getting DOM
																	// element
						NodeList nl = doc.getElementsByTagName("result");
						Element e = (Element) nl.item(0);
						status = parser.getValue(e, "ns1:status");

						return status;

					} catch (Exception e) {
						// TODO: handle exception

					}
					return null;
				}

				@Override
				protected void onPostExecute(String result) {
					// TODO Auto-generated method stub
					super.onPostExecute(result);

					try {
						pdialog.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
					if (result != null) {

						// if (result.equals("0")||result.equals("701")) temp.
						// commented
						if (result.equals("0")) {
							setResult(Activity.RESULT_OK);
							finish();
							ScreenUtils
									.raiseToast(
											context,
											WebserviceUtils.VEHICLE_DETAILS_EDIT_SUCCESS);
							sendBroadcast();
						} else {
							String errorStatus = Utils.getStringResourceByName(
									"status_" + result, context);
							ScreenUtils.raiseToast(context, errorStatus);
						}

					} else {
						ScreenUtils.raiseToast(context,
								WebserviceUtils.SOMETHING_WENT_WRONG);
					}
				}

			}.execute(new AddVehicleData[] { new AddVehicleData(Utils
					.getEditTextValue(edt_tab5_vehy), Utils
					.getEditTextValue(edt_tab5_vehcolor), Utils
					.getEditTextValue(edt_tab5_vehmake), Utils
					.getEditTextValue(edt_tab5_vehmodel), "5", Utils
					.getEditTextValue(edt_tab5_vehpltn), txt_edt_vehicle_state
					.getText().toString()) });

		} else {
			ScreenUtils.raiseToast(context,
					WebserviceUtils.NO_INTERNET_CONNECTION);
		}
	}

	public void onClickBack(View view) {
		if (checkIfFieldsAreEmpty()) {

			finish();

		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"Are you sure you want to cancel without saving changes?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									setResult(Activity.RESULT_CANCELED);
									finish();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}

	}

	public void onClickShowDropdown(View view) {
		// final CharSequence[] array_1 = { "state 1", "state 2", "state 3",
		// "state 4", "state 5" };
		List<String> stateList = Arrays.asList(this.getResources()
				.getStringArray(R.array.state_type));
		final CharSequence[] array_1 = stateList
				.toArray(new CharSequence[stateList.size()]);

		ScreenUtils.showMultipleChoiceDlg(array_1, this, txt_edt_vehicle_state,
				"state", this, null);
	}

	private boolean checkIfFieldsAreEmpty() {
		EditText[] edtValues = { edt_tab5_vehy, edt_tab5_vehmake,
				edt_tab5_vehmodel, edt_tab5_vehcolor, edt_tab5_vehpltn };

		for (EditText edt : edtValues) {
			int length = Utils.getEditTextValue(edt).length();
			if (length > 0)
				return false;
		}
		return true;
	}

	private void sendBroadcast() {
		Intent intent = new Intent("addVehicleNotification");
		intent.putExtra("message", "This is my message!");
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	private boolean validations(EditText edt, CharSequence s, int type) {
		// TODO Auto-generated method stub
		int val[] = ScreenUtils.getPaddingVal(edt);
		boolean result = false;

		switch (type) {
		case 0:
			result = RuleBook.isPinValid(s.toString());
			break;
		case 1:
			result = RuleBook.isUserNameValid(s.toString());
			break;
		case 2:
			result = RuleBook.isUserNameValid(s.toString());
			break;
		case 3:
			result = RuleBook.isUserNameValid(s.toString());
			break;
		case 4:
			result = RuleBook.isUserNameValid(s.toString());
			break;

		default:
			break;
		}

		if (result) {
			edt.setBackgroundResource(R.drawable.grey_border);
		} else {
			edt.setBackgroundResource(R.drawable.edit_warning_border);
		}

		edt.setPadding(val[0], val[1], val[2], val[3]);
		return result;
	}

	/** returns value of the edit field */
	CharSequence getEdtVale(EditText edt) {
		return edt.getText().toString();
	}

	class TabTextWatcher implements TextWatcher {

		EditText edt;

		TabTextWatcher(EditText edt) {
			this.edt = edt;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

			switch (edt.getId()) {
			case R.id.edt_tab5_vehy: {

				validations(edt, s, 0);
				break;
			}
			case R.id.edt_tab5_vehmake: {

				validations(edt, s, 1);
				break;
			}
			case R.id.edt_tab5_vehmodel: {

				validations(edt, s, 2);
				break;
			}
			case R.id.edt_tab5_vehcolor: {

				validations(edt, s, 3);
				break;
			}
			case R.id.edt_tab5_vehpltn: {

				validations(edt, s, 4);
				break;
			}

			default:
				break;
			}
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(context).reportActivityStart(this);

		FlurryAgent.onStartSession(context);
		FlurryAgent.setLogEvents(true);

		FlurryAgent.logEvent(Utils.ADDVEHICLE_VIEW);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

		FlurryAgent.onEndSession(context);
	}
}
