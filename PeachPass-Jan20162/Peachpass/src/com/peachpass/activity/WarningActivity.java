package com.peachpass.activity;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobsandgeeks.ui.Shimmer;
import com.mobsandgeeks.ui.ShimmerTextView;
import com.peachpass.utils.ConnectionDetector;
import com.peachpass.utils.Utils;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;
import com.peachpass.utils.LayoutTouchListener;
import com.peachpass.utils.ScreenUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.R.attr.button;


public class WarningActivity extends Activity {

//    LinearLayout ll_warning_swipe;
//    TextView txt_warning_next;
//    Typeface font = null;
//    ShimmerTextView txt_shimmer_swipe;
//    Context context;
//
//    Boolean isInternetPresent = false;
//    ConnectionDetector connectionDetector;

//    public static final int SM_REQUEST_CODE = 0;
//    public static final String SM_RESPONDENT = "smRespondent";
//    public static final String SM_ERROR = "smError";
//    public static final String RESPONSES = "responses";
//    public static final String QUESTION_ID = "question_id";
//    public static final String FEEDBACK_QUESTION_ID = "813797519";
//    public static final String ANSWERS = "answers";
//    public static final String ROW_ID = "row_id";
//    public static final String FEEDBACK_FIVE_STARS_ROW_ID = "9082377273";
//    public static final String FEEDBACK_POSITIVE_ROW_ID_2 = "9082377274";
//    public static final String SAMPLE_APP = "PeachPass";
//    public static final String SURVEY_HASH = "KGLHMQJ";  //LBQK27G Demo
//    private SurveyMonkey sdkInstance = new SurveyMonkey();

    ImageView splash_image;
    TextView close_page;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Set the screen to full screen mode before setting the layout */
        ScreenUtils.fullScreen(this);
        setContentView(R.layout.activity_warning);
        context=this;
        splash_image=(ImageView)findViewById(R.id.splash_peachpass);
        close_page=(TextView)findViewById(R.id.close_page);

        close_page.setTypeface(ScreenUtils.returnTypeFace(context));

        close_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(WarningActivity.this,LoginScreenActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });


//        font = ScreenUtils.returnTypeFace(this);
//        init();
//        context = this;
//        connectionDetector = new ConnectionDetector(context);
//
//        //Here we're setting up the SurveyMonkey Intercept Dialog -- the user will be prompted to take the survey 3 days after app install.
//        // Once prompted, the user will be reminded to take the survey again in 3 weeks if they decline or 3 months if they consent to take the survey.
//        // The onStart method can be overloaded so that you can supply your own prompts and intervals -- for more information, see our documentation on Github.
//        sdkInstance.onStart(this, SAMPLE_APP, SM_REQUEST_CODE, SURVEY_HASH);
//        //sdkInstance.startSMFeedbackActivityForResult(this, SM_REQUEST_CODE, SURVEY_HASH);
//        //Log.e("peachpass", parseDate(""));
//
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        alertDialogBuilder.setTitle(R.string.prompt_title_text);
//        alertDialogBuilder.setMessage(R.string.prompt_message_text);
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.setPositiveButton("Feedback",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface arg0, int arg1) {
//                        //Toast.makeText(WarningActivity.this, "You clicked yes button", Toast.LENGTH_LONG).show();
//                        sdkInstance.startSMFeedbackActivityForResult(WarningActivity.this, SM_REQUEST_CODE, SURVEY_HASH);
//                        SharedPreferenceHelper.saveIntPreferences(SharedPreferenceHelper.MONKEYSURVEYPOPUP, 0, context);
//                        Log.e("WarningActivity", "Yes");
//                    }
//                });
//
//        alertDialogBuilder.setNegativeButton("No thanks", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //  finish();
//                Log.e("WarningActivity", "No");
//                SharedPreferenceHelper.saveIntPreferences(SharedPreferenceHelper.MONKEYSURVEYPOPUP, SharedPreferenceHelper.MONKEYSURVEYVALUE++, context);
//            }
//        });
//
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        String monkeySurveyFill = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.MONKEYSURVEYFILL);
//        int monkeySurveyValue = SharedPreferenceHelper.getIntPreference(context, SharedPreferenceHelper.MONKEYSURVEYPOPUP);
//        String appOpeningStatus = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.APPOPENINGSTATUS);
//        Log.e("WarningActivity", "APPOPENINGSTATUS- " + appOpeningStatus);
//        if (connectionDetector.isConnectingToInternet()) {
//            if (!monkeySurveyFill.contains("true")) {
//                if (SharedPreferenceHelper.MONKEYSURVEY) {
//                    if (appOpeningStatus.contains("true")) {
//                        if (monkeySurveyValue <= 1) {
//                            Log.e("WarningActivity", "monkeySurveyValue- " + monkeySurveyValue);
//                            alertDialog.show();
//                        }
//                    }
//
//                }
//            }
//
//        } else {
//            // Internet connection is not present
//            Utils.showAlertDialog(context, "No Internet Connection",
//                    "Please check your internet connection.", false);
//        }
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.APPOPENINGSTATUS, "true", context);
//    }
//
//    /**
//     * initialize all the widgets
//     */
//    private void init() {
//        // TODO Auto-generated method stub
//        ll_warning_swipe = (LinearLayout) findViewById(R.id.ll_warning_swipe);
//        ll_warning_swipe.setOnTouchListener(new LayoutTouchListener(this, 1));
//
//        txt_warning_next = (TextView) findViewById(R.id.txt_wrng_angle_left);
//
//        txt_warning_next.setText(getResources().getString(
//                R.string.fa_angle_double_left)
//                + getResources().getString(R.string.fa_angle_double_left));
//        txt_warning_next.setTypeface(font);
//
//        txt_shimmer_swipe = (ShimmerTextView) findViewById(R.id.txt_shimmer_swipe);
//        txt_shimmer_swipe.setTypeface(font);
//        getShimmerConfig();
//
//        Utils.setTracker(Utils.WARNING_VIEW);
//    }
//
//    void getShimmerConfig() {
//
//        Shimmer shimmer = new Shimmer();
//        shimmer.start(txt_shimmer_swipe);
//        shimmer.setDuration(5000);
//        shimmer.setStartDelay(300);
//        shimmer.setDirection(Shimmer.ANIMATION_DIRECTION_RTL);
//        shimmer.setAnimatorListener(new AnimatorListener() {
//
//            @Override
//            public void onAnimationStart(Animator animation) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        //This is where you consume the respondent data returned by the SurveyMonkey Mobile Feedback SDK
//        //In this example, we deserialize the user's response, check to see if they gave our app 4 or 5 stars, and then provide visual prompts to the user based on their response
//        super.onActivityResult(requestCode, resultCode, intent);
//        if (resultCode == RESULT_OK) {
//            boolean isPromoter = false;
//            try {
//                String respondent = intent.getStringExtra(SM_RESPONDENT);
//                Log.d("SM", respondent);
//                JSONObject surveyResponse = new JSONObject(respondent);
//                JSONArray responsesList = surveyResponse.getJSONArray(RESPONSES);
//                JSONObject response;
//                JSONArray answers;
//                JSONObject currentAnswer;
//                for (int i = 0; i < responsesList.length(); i++) {
//                    response = responsesList.getJSONObject(i);
//                    if (response.getString(QUESTION_ID).equals(FEEDBACK_QUESTION_ID)) {
//                        answers = response.getJSONArray(ANSWERS);
//                        for (int j = 0; j < answers.length(); j++) {
//                            currentAnswer = answers.getJSONObject(j);
//                            if (currentAnswer.getString(ROW_ID).equals(FEEDBACK_FIVE_STARS_ROW_ID) || currentAnswer.getString(ROW_ID).equals(FEEDBACK_POSITIVE_ROW_ID_2)) {
//                                isPromoter = true;
//                                break;
//                            }
//                        }
//                        if (isPromoter) {
//                            break;
//                        }
//                    }
//                }
//            } catch (JSONException e) {
//                Log.getStackTraceString(e);
//            }
//            if (isPromoter) {
//                Toast t = Toast.makeText(this, getString(R.string.thanks_prompt), Toast.LENGTH_LONG);
//                t.show();
//                SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.MONKEYSURVEYFILL, "true", context);
//            } else {
//                Toast t = Toast.makeText(this, getString(R.string.detractor_prompt), Toast.LENGTH_LONG);
//                t.show();
//                SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.MONKEYSURVEYFILL, "false", context);
//            }
//        } else {
////            Toast t = Toast.makeText(this, getString(R.string.error_prompt), Toast.LENGTH_LONG);
////            t.show();
//            SharedPreferenceHelper.saveIntPreferences(SharedPreferenceHelper.MONKEYSURVEYPOPUP, 0, context);
//            SMError e = (SMError) intent.getSerializableExtra(SM_ERROR);
//            Log.d("SM-ERROR", e.getDescription());
//        }
//    }
//
//
//    @Override
//    protected void onStart() {
//        // TODO Auto-generated method stub
//        super.onStart();
//        GoogleAnalytics.getInstance(context).reportActivityStart(this);
//
//        FlurryAgent.onStartSession(context);
//        FlurryAgent.setLogEvents(true);
//
//        FlurryAgent.logEvent(Utils.WARNING_VIEW);
//    }
//
//
//    @Override
//    protected void onStop() {
//        // TODO Auto-generated method stub
//        super.onStop();
//        GoogleAnalytics.getInstance(context).reportActivityStop(this);
//
//        FlurryAgent.onEndSession(context);
//    }
//
//
////	private String parseDate(String time) {
////		try {
////			time = "2015-05-16T10:20:17.000-05:00";
////			String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
////			//String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSxxx";
////			//May 16, 2015 at 08:50 pm
////
////			String outputPattern = "MMM dd, yyyy 'at' hh:mm aaa";
////			SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
////			SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
////
////			Date date = null;
////			String str = null;
////
////			date = inputFormat.parse(time);
////			str = outputFormat.format(date);
////			return str;
////		} catch (Exception e) {
////			e.printStackTrace();
////			Log.e("newlog", e.toString());
////			return time;
////		}
////
////	}

    }
}
