package com.peachpass.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.ViewPersonalInfoData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class EditProfileActivity extends Activity {

	TextView txt_create_account_next3, txt_edt_profile_state,
			txt_edt_profile_county, txt_changetoll_arrow,
			txt_changetoll_arrow_county;

	EditText edt_tab3_flname, edt_tab3_flname_last, edt_tab3_address,
			edt_tab3_city, edt_tab3_zip, edt_tab3_phone, edt_tab3_drvrlic,
			edt_tab3_email;

	List<String> stateList = null;
	List<String> countyList = null;
	Context context;
	LinearLayout ll_save;

	ViewPersonalInfoData orgViewPersonalInfoData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Set the screen to full screen mode before setting the layout */
		ScreenUtils.fullScreen(this);
		context = this;

		stateList = Arrays.asList(this.getResources().getStringArray(
				R.array.state_type));
		countyList = Arrays.asList(this.getResources().getStringArray(
				R.array.county_type));
		setContentView(R.layout.edit_profile);

		init();

		Typeface font = ScreenUtils.returnTypeFace(this);
		txt_create_account_next3.setTypeface(font);
		txt_changetoll_arrow.setTypeface(font);
		txt_changetoll_arrow_county.setTypeface(font);

		Typeface font1 = ScreenUtils.returnTypeFaceHelv(this);
		edt_tab3_flname.setTypeface(font1);
		edt_tab3_address.setTypeface(font1);
		edt_tab3_city.setTypeface(font1);
		edt_tab3_zip.setTypeface(font1);
		// edt_tab3_county.setTypeface(font1);
		edt_tab3_phone.setTypeface(font1);
		edt_tab3_drvrlic.setTypeface(font1);

		setTextDetails();

		String county="";
		try {
			 county = String.valueOf(Utils.countyMap.get(txt_edt_profile_county.getText().toString()));
		} catch (Exception e) {
			// TODO: handle exception
			county="";
		}

		orgViewPersonalInfoData = new ViewPersonalInfoData(
				getEditVal(edt_tab3_flname), getEditVal(edt_tab3_flname_last),
				getEditVal(edt_tab3_email), getEditVal(edt_tab3_address),
				getEditVal(edt_tab3_city), txt_edt_profile_state.getText()
						.toString(), getEditVal(edt_tab3_zip), county, getEditVal(edt_tab3_phone),
				getEditVal(edt_tab3_drvrlic), txt_edt_profile_state.getText()
						.toString(), null);


		Utils.setTracker(Utils.EDITPROFILE_VIEW);

        ll_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveApi();
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
	public void saveApi(){
        try{

        	Log.e("SaveProfile-- ","True");
            new AsyncTask<ViewPersonalInfoData, String, ViewPersonalInfoData>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(context,
                        WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS, false);

                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    super.onPreExecute();
                }

                @Override
                protected ViewPersonalInfoData doInBackground(
                        ViewPersonalInfoData... params) {
                    // TODO Auto-generated method stub
                    ViewPersonalInfoData data = params[0];
                    Log.e("peachpass", data.toString());
                    try {

                        String requestBody = WebserviceUtils.readFromAsset(
                                "savePersonalInfo", context);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(context));
                        requestBody = requestBody.replaceAll("_firstName_",
                                data.getFirstName());
                        requestBody = requestBody.replaceAll("_lastName_",
                                data.getLastName());
                        requestBody = requestBody.replaceAll("_email_",
                                data.getEmail());
                        requestBody = requestBody.replaceAll("_address_",
                                data.getAddress());
                        requestBody = requestBody.replaceAll("_city_",
                                data.getCity());
                        requestBody = requestBody.replaceAll("_state_",
                                data.getState());
                        requestBody = requestBody
                                .replaceAll("_zip_", data.getZip());
                        requestBody = requestBody.replaceAll("_county_",
                                data.getCounty());
                        requestBody = requestBody.replaceAll("_phone_",
                                data.getPhone());
                        requestBody = requestBody.replaceAll("_driverLisence_",
                                data.getDriverLisence());
                        requestBody = requestBody.replaceAll(
                                "_driverLisenceState_",
                                data.getDriverLisenceState());

                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(context));
                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        // Log.e("peachpass", requestBody+"");
                        String result = WebserviceUtils.sendSoapRequest(context,
                                requestBody,
                                WebserviceUtils.SOAP_ACTION_SAVEPERSONALINFO);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element

                        NodeList nl = doc.getElementsByTagName("result");

                        for (int i = 0; i < nl.getLength();) {

                            Element e = (Element) nl.item(i);

                            String status = parser.getValue(e, "ns1:status");

							return new ViewPersonalInfoData(
                                    data.getFirstName(), data.getLastName(),
                                    data.getEmail(), data.getAddress(),
                                    data.getCity(), data.getState(), data.getZip(),
                                    data.getCounty(), data.getPhone(),
                                    data.getDriverLisence(),
                                    data.getDriverLisenceState(), status);
                        }

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(ViewPersonalInfoData result)                                {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        if (result != null) {

                            if (result.getStatus().equals("0")) {
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.FIRST_NAME,
                                        result.getFirstName(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.LAST_NAME,
                                        result.getLastName(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.EMAIL,
                                        result.getEmail(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.ADDRESS,
                                        result.getAddress(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.CITY,
                                        result.getCity(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.STATE,
                                        result.getState(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.ZIP,
                                        result.getZip(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.CountyValue,
                                        result.getCounty(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.PHONE,
                                        result.getPhone(), context);
                                SharedPreferenceHelper.savePreferences(
                                        SharedPreferenceHelper.DRIVER_LISENCE,
                                        result.getDriverLisence(), context);
                                SharedPreferenceHelper
                                        .savePreferences(
                                                SharedPreferenceHelper.DRIVER_LISENCE_STATE,
                                                result.getDriverLisenceState(),
                                                context);


								Log.e("Result-- "+result.toString(),"Value");
                                ScreenUtils.raiseToast(context,
                                        "Profile edited successfully !");
                                finish();

                            } else if (result.getStatus() != null
                                    && !result.getStatus().equals("0")) {
                                String errorStatus = Utils.getStringResourceByName(
                                        "status_" + result.getStatus(), context);
                                ScreenUtils.raiseToast(context, errorStatus);
                            } else {

                                ScreenUtils.raiseToast(context,
                                        WebserviceUtils.SESSION_EXPIRED);
                            }

                        } else {

                            ScreenUtils.raiseToast(context,
                                    WebserviceUtils.SOMETHING_WENT_WRONG);
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                        ScreenUtils.raiseToast(context,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }

                    super.onPostExecute(result);
                }
            }.execute(new ViewPersonalInfoData(
                    getEditVal(edt_tab3_flname), getEditVal(edt_tab3_flname_last),
                    getEditVal(edt_tab3_email), getEditVal(edt_tab3_address),
                    getEditVal(edt_tab3_city), txt_edt_profile_state.getText()
                    .toString(), getEditVal(edt_tab3_zip),String.valueOf(Utils.countyMap.get(txt_edt_profile_county.getText().toString())), getEditVal(edt_tab3_phone),
                    getEditVal(edt_tab3_drvrlic), txt_edt_profile_state.getText()
                    .toString(), null));

        }catch(Exception e){
            e.printStackTrace();
        }
    }

	@SuppressLint("SetTextI18n")
	private void setTextDetails() {
		// TODO Auto-generated method stub
		edt_tab3_flname.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.FIRST_NAME));
		edt_tab3_flname_last.setText(SharedPreferenceHelper.getPreference(
				context, SharedPreferenceHelper.LAST_NAME));
		edt_tab3_email.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.EMAIL));
		edt_tab3_address.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.ADDRESS));
		edt_tab3_city.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.CITY));
		edt_tab3_zip.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.ZIP));
		txt_edt_profile_county.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.CountyValue));
		edt_tab3_phone.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.PHONE));
		edt_tab3_drvrlic.setText(SharedPreferenceHelper.getPreference(context,
				SharedPreferenceHelper.DRIVER_LISENCE));

		try {
			int statePosn = stateList.indexOf(SharedPreferenceHelper
					.getPreference(context, SharedPreferenceHelper.STATE));
			// Log.e("peachpass", statePosn + "---statePosn");
			txt_edt_profile_state.setText(stateList.get(statePosn));
		} catch (Exception e) {
			// TODO: handle exception
			txt_edt_profile_state.setText(stateList.get(9));
		}

		try {
			String actualCounty = SharedPreferenceHelper.getPreference(context,
					SharedPreferenceHelper.CountyValue);
			 Log.e("peachpass", actualCounty + "---actualCounty");
			actualCounty = actualCounty.toLowerCase(Locale.US);
			// Log.e("peachpass", actualCounty + "---actualCounty");
			actualCounty = Utils.firstCapital(actualCounty);
			// Log.e("peachpass", actualCounty + "---actualCounty");
			int countyPosn = countyList.indexOf(actualCounty);
			Log.e("peachpass", countyPosn + "---countyPosn");
			txt_edt_profile_county.setText(countyPosn);
		} catch (Exception e) {
			// TODO: handle exception
			// txt_edt_profile_county.setText(stateList.get(9));
		}
	}

	private void init() {
		// TODO Auto-generated method stub
		txt_create_account_next3 = (TextView) findViewById(R.id.txt_create_account_next3);
		txt_edt_profile_state = (TextView) findViewById(R.id.txt_edt_profile_state);
		txt_edt_profile_county = (TextView) findViewById(R.id.txt_edt_profile_county);
		txt_changetoll_arrow = (TextView) findViewById(R.id.txt_changetoll_arrow);
		txt_changetoll_arrow_county = (TextView) findViewById(R.id.txt_changetoll_arrow_county);

		edt_tab3_flname = (EditText) findViewById(R.id.edt_tab3_flname);
		edt_tab3_flname_last = (EditText) findViewById(R.id.edt_tab3_flname_last);
		edt_tab3_email = (EditText) findViewById(R.id.edt_tab3_email);
		edt_tab3_address = (EditText) findViewById(R.id.edt_tab3_address);
		edt_tab3_city = (EditText) findViewById(R.id.edt_tab3_city);
		edt_tab3_zip = (EditText) findViewById(R.id.edt_tab3_zip);
		// edt_tab3_county = (EditText) findViewById(R.id.edt_tab3_county);
		edt_tab3_phone = (EditText) findViewById(R.id.edt_tab3_phone);
		edt_tab3_drvrlic = (EditText) findViewById(R.id.edt_tab3_drvrlic);
		ll_save=(LinearLayout)findViewById(R.id.ll_save);

	}

		// finish();

	public void onClickBack(View view) {

		try{
			ViewPersonalInfoData freshData = new ViewPersonalInfoData(
					getEditVal(edt_tab3_flname), getEditVal(edt_tab3_flname_last),
					getEditVal(edt_tab3_email), getEditVal(edt_tab3_address),
					getEditVal(edt_tab3_city), txt_edt_profile_state.getText()
					.toString(), getEditVal(edt_tab3_zip), Utils.countyMap
					.get(txt_edt_profile_county.getText().toString())
					.toString(), getEditVal(edt_tab3_phone),
					getEditVal(edt_tab3_drvrlic), txt_edt_profile_state.getText()
					.toString(), null);

			if (freshData.equals(orgViewPersonalInfoData)) {

				finish();

			} else {

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"Are you sure you want to cancel without saving changes?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int id) {
										onBackPressed();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int id) {
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}

		}catch(Exception e){
		   //e.printStackTrace();
			onBackPressed();
		}


	}

	public void onClickShowDropdown(View view) {

		final CharSequence[] array_1 = stateList
				.toArray(new CharSequence[stateList.size()]);

		ScreenUtils.showMultipleChoiceDlg(array_1, this, txt_edt_profile_state,
				"state", this, null);
	}

	public void onClickShowDropdownCounty(View view) {

		final CharSequence[] array_1 = countyList
				.toArray(new CharSequence[countyList.size()]);

		ScreenUtils.showMultipleChoiceDlg(array_1, this,
				txt_edt_profile_county, "county", this, null);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		// setResult(11);
	}

	private String getEditVal(EditText edt) {
		return edt.getText().toString();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(context).reportActivityStart(this);

		FlurryAgent.onStartSession(context);
		FlurryAgent.setLogEvents(true);

		FlurryAgent.logEvent(Utils.EDITPROFILE_VIEW);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(context).reportActivityStop(this);

		FlurryAgent.onEndSession(context);
	}

}
