package com.peachpass.data;

import java.util.ArrayList;

public class StatementMasterData {

	ArrayList<AccountSummaryData> accountSummaryArray = new ArrayList<AccountSummaryData>();
	ArrayList<VehicleSummaryStatement> vehicleSummaryArray = new ArrayList<VehicleSummaryStatement>();
	ArrayList<AccountDetailData> accountDetailArray = new ArrayList<AccountDetailData>();
	ArrayList<VehicleDetailStatementData> vehicleDetailArray = new ArrayList<VehicleDetailStatementData>();

	public ArrayList<AccountSummaryData> getAccountSummaryArray() {
		return accountSummaryArray;
	}

	public void setAccountSummaryArray(
			ArrayList<AccountSummaryData> accountSummaryArray) {
		this.accountSummaryArray = accountSummaryArray;
	}

	public ArrayList<VehicleSummaryStatement> getVehicleSummaryArray() {
		return vehicleSummaryArray;
	}

	public void setVehicleSummaryArray(
			ArrayList<VehicleSummaryStatement> vehicleSummaryArray) {
		this.vehicleSummaryArray = vehicleSummaryArray;
	}

	public ArrayList<AccountDetailData> getAccountDetailArray() {
		return accountDetailArray;
	}

	public void setAccountDetailArray(
			ArrayList<AccountDetailData> accountDetailArray) {
		this.accountDetailArray = accountDetailArray;
	}

	public ArrayList<VehicleDetailStatementData> getVehicleDetailArray() {
		return vehicleDetailArray;
	}

	public void setVehicleDetailArray(
			ArrayList<VehicleDetailStatementData> vehicleDetailArray) {
		this.vehicleDetailArray = vehicleDetailArray;
	}

	public StatementMasterData(
			ArrayList<AccountSummaryData> accountSummaryArray,
			ArrayList<VehicleSummaryStatement> vehicleSummaryArray,
			ArrayList<AccountDetailData> accountDetailArray,
			ArrayList<VehicleDetailStatementData> vehicleDetailArray) {
		super();
		this.accountSummaryArray = accountSummaryArray;
		this.vehicleSummaryArray = vehicleSummaryArray;
		this.accountDetailArray = accountDetailArray;
		this.vehicleDetailArray = vehicleDetailArray;
	}

}
