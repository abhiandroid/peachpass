package com.peachpass.data;

public class VehicleDetailStatementData {

	String tagIdLicPlate;
	String dateTime;
	String plazaLane;
	String direction;
	String location;
	String transTypeDescr;
	String amount;

	public String getTagIdLicPlate() {
		return tagIdLicPlate;
	}

	public void setTagIdLicPlate(String tagIdLicPlate) {
		this.tagIdLicPlate = tagIdLicPlate;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getPlazaLane() {
		return plazaLane;
	}

	public void setPlazaLane(String plazaLane) {
		this.plazaLane = plazaLane;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransTypeDescr() {
		return transTypeDescr;
	}

	public void setTransTypeDescr(String transTypeDescr) {
		this.transTypeDescr = transTypeDescr;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public VehicleDetailStatementData(String tagIdLicPlate, String dateTime,
			String plazaLane, String direction, String location,
			String transTypeDescr, String amount) {
		super();
		this.tagIdLicPlate = tagIdLicPlate;
		this.dateTime = dateTime;
		this.plazaLane = plazaLane;
		this.direction = direction;
		this.location = location;
		this.transTypeDescr = transTypeDescr;
		this.amount = amount;
	}

}
