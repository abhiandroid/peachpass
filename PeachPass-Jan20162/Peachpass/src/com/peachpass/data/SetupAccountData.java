package com.peachpass.data;

public class SetupAccountData {

	String accountId;
	String PIN;
	String userId;
	String pw;
	int secQuestion1;
	String secAnswer1;
	int secQuestion2;
	String secAnswer2;
	int secQuestion3;
	String secAnswer3;

	public SetupAccountData(String accountId, String pIN, String userId,
			String pw, int secQuestion1, String secAnswer1, int secQuestion2,
			String secAnswer2, int secQuestion3, String secAnswer3) {
		super();
		this.accountId = accountId;
		PIN = pIN;
		this.userId = userId;
		this.pw = pw;
		this.secQuestion1 = secQuestion1;
		this.secAnswer1 = secAnswer1;
		this.secQuestion2 = secQuestion2;
		this.secAnswer2 = secAnswer2;
		this.secQuestion3 = secQuestion3;
		this.secAnswer3 = secAnswer3;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String pIN) {
		PIN = pIN;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public int getSecQuestion1() {
		return secQuestion1;
	}

	public void setSecQuestion1(int secQuestion1) {
		this.secQuestion1 = secQuestion1;
	}

	public String getSecAnswer1() {
		return secAnswer1;
	}

	public void setSecAnswer1(String secAnswer1) {
		this.secAnswer1 = secAnswer1;
	}

	public int getSecQuestion2() {
		return secQuestion2;
	}

	public void setSecQuestion2(int secQuestion2) {
		this.secQuestion2 = secQuestion2;
	}

	public String getSecAnswer2() {
		return secAnswer2;
	}

	public void setSecAnswer2(String secAnswer2) {
		this.secAnswer2 = secAnswer2;
	}

	public int getSecQuestion3() {
		return secQuestion3;
	}

	public void setSecQuestion3(int secQuestion3) {
		this.secQuestion3 = secQuestion3;
	}

	public String getSecAnswer3() {
		return secAnswer3;
	}

	public void setSecAnswer3(String secAnswer3) {
		this.secAnswer3 = secAnswer3;
	}

	public SetupAccountData() {
		super();
	}
	
	

}
