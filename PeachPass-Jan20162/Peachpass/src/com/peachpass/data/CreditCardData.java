package com.peachpass.data;

public class CreditCardData {

	String cardTypeId;//
	String ccNumber;//
	String expiration;//
	String firstName;
	String middleInitial;
	String lastName;
	String address;
	String aptSuite;
	String city;
	String state;
	String zip;
	String county;

	public String getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(String cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}

	public String getExpiration() {
		return expiration;
	}

	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAptSuite() {
		return aptSuite;
	}

	public void setAptSuite(String aptSuite) {
		this.aptSuite = aptSuite;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public CreditCardData(String cardTypeId, String ccNumber,
			String expiration, String firstName, String middleInitial,
			String lastName, String address, String aptSuite, String city,
			String state, String zip, String county) {
		super();
		this.cardTypeId = cardTypeId;
		this.ccNumber = ccNumber;
		this.expiration = expiration;
		this.firstName = firstName;
		this.middleInitial = middleInitial;
		this.lastName = lastName;
		this.address = address;
		this.aptSuite = aptSuite;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.county = county;
	}
	
	public CreditCardData(String cardTypeId, String ccNumber,String expiration){
		super();
		this.cardTypeId = cardTypeId;
		this.ccNumber = ccNumber;
		this.expiration = expiration;
	}
	
	
	public boolean equals(Object object) {
		if (object instanceof CreditCardData) {

			return ((cardTypeId
					.equals(((CreditCardData) object).cardTypeId))
					&& (ccNumber
							.equals(((CreditCardData) object).ccNumber))
					&& (expiration
							.equals(((CreditCardData) object).expiration)));
		}
		return false;
	}

	@Override
	public String toString() {
		return "CreditCardData [cardTypeId=" + cardTypeId + ", ccNumber="
				+ ccNumber + ", expiration=" + expiration + "]";
	}
	
	

}
