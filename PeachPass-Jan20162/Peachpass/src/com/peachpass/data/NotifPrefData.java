package com.peachpass.data;

public class NotifPrefData {

	String notifGroupName;
	String notifTypeName;
	String notificationMethod;
	String comValue;

	public String getNotifGroupName() {
		return notifGroupName;
	}

	public void setNotifGroupName(String notifGroupName) {
		this.notifGroupName = notifGroupName;
	}

	public String getNotifTypeName() {
		return notifTypeName;
	}

	public void setNotifTypeName(String notifTypeName) {
		this.notifTypeName = notifTypeName;
	}

	public String getNotificationMethod() {
		return notificationMethod;
	}

	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	public String getComValue() {
		return comValue;
	}

	public void setComValue(String comValue) {
		this.comValue = comValue;
	}

	public NotifPrefData(String notifGroupName, String notifTypeName,
			String notificationMethod, String comValue) {
		super();
		this.notifGroupName = notifGroupName;
		this.notifTypeName = notifTypeName;
		this.notificationMethod = notificationMethod;
		this.comValue = comValue;
	}

	@Override
	public String toString() {
		return "NotifPrefData [notifGroupName=" + notifGroupName
				+ ", notifTypeName=" + notifTypeName + ", notificationMethod="
				+ notificationMethod + ", comValue=" + comValue + "]";
	}

}
