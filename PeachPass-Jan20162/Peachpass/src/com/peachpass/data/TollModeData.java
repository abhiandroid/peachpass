package com.peachpass.data;

public class TollModeData {

	String vehicleId;
	String durationId;
	String effectiveTime;

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getDurationId() {
		return durationId;
	}

	public void setDurationId(String durationId) {
		this.durationId = durationId;
	}

	public String getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public TollModeData(String vehicleId, String durationId,
			String effectiveTime) {
		super();
		this.vehicleId = vehicleId;
		this.durationId = durationId;
		this.effectiveTime = effectiveTime;
	}

	@Override
	public String toString() {
		return "TollModeData [vehicleId=" + vehicleId + ", durationId="
				+ durationId + ", effectiveTime=" + effectiveTime + "]";
	}

}
