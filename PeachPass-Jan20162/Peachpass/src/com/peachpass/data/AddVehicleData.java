package com.peachpass.data;

public class AddVehicleData {

    String year;
    String color;
    String make;
    String model;
    String tagTypeId;
    String licensePlate;
    String state;
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getTagTypeId() {
		return tagTypeId;
	}
	public void setTagTypeId(String tagTypeId) {
		this.tagTypeId = tagTypeId;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public AddVehicleData(String year, String color, String make, String model,
			String tagTypeId, String licensePlate, String state) {
		super();
		this.year = year;
		this.color = color;
		this.make = make;
		this.model = model;
		this.tagTypeId = tagTypeId;
		this.licensePlate = licensePlate;
		this.state = state;
	}
	@Override
	public String toString() {
		return "AddVehicleData [year=" + year + ", color=" + color + ", make="
				+ make + ", model=" + model + ", tagTypeId=" + tagTypeId
				+ ", licensePlate=" + licensePlate + ", state=" + state + "]";
	}
    
    
    
}
