package com.peachpass.data;

public class AlertData {

	String alert;

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public AlertData(String alert) {
		super();
		this.alert = alert;
	}

}
