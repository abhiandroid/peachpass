package com.peachpass.data;

import java.io.Serializable;

/**
 * Created by Azularc on 10-09-2015.
 * <ns1:status>0</ns1:status>
 * <ns1:accountId>15574104</ns1:accountId>
 * <ns1:billingMethodId>108193171</ns1:billingMethodId>
 * <ns1:ccLastFour>1111</ns1:ccLastFour>
 * <ns1:expDate>12/18</ns1:expDate>
 * <ns1:ccType>2</ns1:ccType>
 * <ns1:isPrimary>Y</ns1:isPrimary>
 * <ns1:isAchType>N</ns1:isAchType>
 */
public class PrimaryBillingData implements Serializable {


    String accountId;
    String billingMethodId;
    String ccLastFour;
    String expDate;
    String ccType;
    String isPrimary;

    public PrimaryBillingData(String accountId, String billingMethodId, String ccLastFour, String expDate, String ccType, String isPrimary) {
        this.accountId = accountId;
        this.billingMethodId = billingMethodId;
        this.ccLastFour = ccLastFour;
        this.expDate = expDate;
        this.ccType = ccType;
        this.isPrimary = isPrimary;
    }

    @Override
    public String toString() {
        return "PrimaryBillingData{" +
                "accountId='" + accountId + '\'' +
                ", billingMethodId='" + billingMethodId + '\'' +
                ", ccLastFour='" + ccLastFour + '\'' +
                ", expDate='" + expDate + '\'' +
                ", ccType='" + ccType + '\'' +
                ", isPrimary='" + isPrimary + '\'' +
                '}';
    }

    public String getBillingMethodId() {
        return billingMethodId;
    }

    public void setBillingMethodId(String billingMethodId) {
        this.billingMethodId = billingMethodId;
    }

    public String getCcLastFour() {
        return ccLastFour;
    }

    public void setCcLastFour(String ccLastFour) {
        this.ccLastFour = ccLastFour;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
