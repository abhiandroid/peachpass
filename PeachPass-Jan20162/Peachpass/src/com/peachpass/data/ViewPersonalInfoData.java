package com.peachpass.data;

import java.util.Comparator;

public class ViewPersonalInfoData implements Comparable<ViewPersonalInfoData> {

	@Override
	public String toString() {
		return "ViewPersonalInfoData [firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", address=" + address
				+ ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", county=" + county + ", phone=" + phone
				+ ", driverLisence=" + driverLisence + ", driverLisenceState="
				+ driverLisenceState + ", status=" + status + "]";
	}

	String firstName;
	String lastName;
	String email;
	String address;
	String city;
	String state;
	String zip;
	String county;
	String phone;
	String driverLisence;
	String driverLisenceState;
	String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDriverLisence() {
		return driverLisence;
	}

	public void setDriverLisence(String driverLisence) {
		this.driverLisence = driverLisence;
	}

	public String getDriverLisenceState() {
		return driverLisenceState;
	}

	public void setDriverLisenceState(String driverLisenceState) {
		this.driverLisenceState = driverLisenceState;
	}

	public ViewPersonalInfoData(String firstName, String lastName,
								String email, String address, String city, String state,
								String zip, String county, String phone, String driverLisence,
								String driverLisenceState, String status) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.county = county;
		this.phone = phone;
		this.driverLisence = driverLisence;
		this.driverLisenceState = driverLisenceState;
		this.status = status;
	}

	public boolean equals(Object object) {
		if (object instanceof ViewPersonalInfoData) {

			return ((firstName
					.equals(((ViewPersonalInfoData) object).firstName))
					&& (lastName
					.equals(((ViewPersonalInfoData) object).lastName))
					&& (email
					.equals(((ViewPersonalInfoData) object).email))
					&& (address
					.equals(((ViewPersonalInfoData) object).address))
					&& (city
					.equals(((ViewPersonalInfoData) object).city))
					&& (state
					.equals(((ViewPersonalInfoData) object).state))
					&& (zip
					.equals(((ViewPersonalInfoData) object).zip))
					&& (county
					.equals(((ViewPersonalInfoData) object).county))
					&& (phone
					.equals(((ViewPersonalInfoData) object).phone))
					&& (driverLisence
					.equals(((ViewPersonalInfoData) object).driverLisence))
					&& (driverLisenceState
					.equals(((ViewPersonalInfoData) object).driverLisenceState)));
		}
		return false;
	}

	@Override
	public int compareTo(ViewPersonalInfoData another) {
		// TODO Auto-generated method stub
		return 0;
	}

	// class SizeComparator implements Comparator<ViewPersonalInfoData> {
	// @Override
	// public int compare(ViewPersonalInfoData orgData, ViewPersonalInfoData
	// newData) {
	//
	// String org_firstName = orgData.getFirstName();
	// String org_lastName= orgData.getLastName();
	// String org_email= orgData.getEmail();
	// String org_address= orgData.getAddress();
	// String org_city= orgData.getCity();
	// String org_state= orgData.getState();
	// String org_zip= orgData.getZip();
	// String org_county= orgData.getCounty();
	// String org_phone= orgData.getPhone();
	// String org_driverLisence= orgData.getDriverLisence();
	// String org_driverLisenceState= orgData.getDriverLisenceState();
	//
	// String new_firstName = newData.getFirstName();
	// String new_lastName= newData.getLastName();
	// String new_email= newData.getEmail();
	// String new_address= newData.getAddress();
	// String new_city= newData.getCity();
	// String new_state= newData.getState();
	// String new_zip= newData.getZip();
	// String new_county= newData.getCounty();
	// String new_phone= newData.getPhone();
	// String new_driverLisence= newData.getDriverLisence();
	// String new_driverLisenceState= newData.getDriverLisenceState();
	//
	//
	// if (org_firstName!=null && new_firstName !=null &&
	// org_firstName.equals(new_firstName))
	// if (org_lastName!=null && new_lastName !=null &&
	// org_lastName.equals(new_lastName))
	// if (org_email!=null && new_email !=null && org_email.equals(new_email))
	// if (org_address!=null && new_address !=null &&
	// org_address.equals(new_address))
	// if (org_city!=null && new_city !=null && org_city.equals(new_city))
	// if (org_state!=null && new_state !=null && org_state.equals(new_state))
	// if (org_zip!=null && new_zip !=null && org_zip.equals(new_zip))
	// if (org_county!=null && new_county !=null &&
	// org_county.equals(new_county))
	// if (org_phone!=null && new_phone !=null && org_phone.equals(new_phone))
	// if (org_driverLisence!=null && new_driverLisence !=null &&
	// org_driverLisence.equals(new_driverLisence))
	// if (org_driverLisenceState!=null && new_driverLisenceState !=null &&
	// org_driverLisenceState.equals(new_driverLisenceState)){
	//
	// return 1;
	// }
	//
	//
	// return 0;
	//
	// }
	// }
}
