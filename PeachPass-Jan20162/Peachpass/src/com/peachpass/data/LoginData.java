package com.peachpass.data;

public class LoginData {

	String status;
	String sessionId;
	String orinalId;
	String activationDateTime;
	String accountId;
	String userName;
	String tagIds;

	public String getTagIds() {
		return tagIds;
	}

	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getOrinalId() {
		return orinalId;
	}

	public void setOrinalId(String orinalId) {
		this.orinalId = orinalId;
	}

	public String getActivationDateTime() {
		return activationDateTime;
	}

	public void setActivationDateTime(String activationDateTime) {
		this.activationDateTime = activationDateTime;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "LoginData [status=" + status + ", sessionId=" + sessionId
				+ ", orinalId=" + orinalId + ", activationDateTime="
				+ activationDateTime + ", accountId=" + accountId
				+ ", userName=" + userName + "]";
	}

}
