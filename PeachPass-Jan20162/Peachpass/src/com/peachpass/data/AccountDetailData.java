package com.peachpass.data;

public class AccountDetailData {

	String transactionDate;
	String location;
	String transTypeDescr;
	String amount;

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransTypeDescr() {
		return transTypeDescr;
	}

	public void setTransTypeDescr(String transTypeDescr) {
		this.transTypeDescr = transTypeDescr;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public AccountDetailData(String transactionDate, String location,
			String transTypeDescr, String amount) {
		super();
		this.transactionDate = transactionDate;
		this.location = location;
		this.transTypeDescr = transTypeDescr;
		this.amount = amount;
	}

}
