package com.peachpass.data;

public class AccountVehicleToll {

	String vehicleId;
	String licPlate;
	String make;
	String model;
	String color;
	String year;
	String tollMode;
	String travelEndDate;

	public AccountVehicleToll(String vehicleId, String licPlate, String make,
			String model, String color, String year, String tollMode,
			String travelEndDate) {
		super();
		this.vehicleId = vehicleId;
		this.licPlate = licPlate;
		this.make = make;
		this.model = model;
		this.color = color;
		this.year = year;
		this.tollMode = tollMode;
		this.travelEndDate = travelEndDate;
	}

	@Override
	public String toString() {
		return "AccountVehicleToll [vehicleId=" + vehicleId + ", licPlate="
				+ licPlate + ", make=" + make + ", model=" + model + ", color="
				+ color + ", year=" + year + ", tollMode=" + tollMode
				+ ", travelEndDate=" + travelEndDate + "]";
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getLicPlate() {
		return licPlate;
	}

	public void setLicPlate(String licPlate) {
		this.licPlate = licPlate;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getTollMode() {
		return tollMode;
	}

	public void setTollMode(String tollMode) {
		this.tollMode = tollMode;
	}

	public String getTravelEndDate() {
		return travelEndDate;
	}

	public void setTravelEndDate(String travelEndDate) {
		this.travelEndDate = travelEndDate;
	}

}
