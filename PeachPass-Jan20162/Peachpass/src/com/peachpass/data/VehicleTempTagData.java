package com.peachpass.data;

public class VehicleTempTagData {

	String licPlate;
	String tagId;

	public String getLicPlate() {
		return licPlate;
	}

	public void setLicPlate(String licPlate) {
		this.licPlate = licPlate;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public VehicleTempTagData(String licPlate, String tagId) {
		super();
		this.licPlate = licPlate;
		this.tagId = tagId;
	}

}
