package com.peachpass.data;

public class FragmentTitle {

	String label;
	boolean fromSlide;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isFromSlide() {
		return fromSlide;
	}

	public void setFromSlide(boolean fromSlide) {
		this.fromSlide = fromSlide;
	}

	public FragmentTitle(String label, boolean fromSlide) {
		super();
		this.label = label;
		this.fromSlide = fromSlide;
	}

}
