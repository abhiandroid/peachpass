package com.peachpass.data;

public class AccountSummaryData {

	String description;
	String quantity;
	String amount;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public AccountSummaryData(String description, String quantity, String amount) {
		super();
		this.description = description;
		this.quantity = quantity;
		this.amount = amount;
	}

}
