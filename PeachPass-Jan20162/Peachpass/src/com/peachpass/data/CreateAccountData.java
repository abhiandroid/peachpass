package com.peachpass.data;

public class CreateAccountData {

    String firstName;//_firstName_
    String lastName;//_lastName_
    String emailAddr;//_emailAddr_
    String userId;//_userId_
    String pw;//_pw_
    int secQuestion1;//_secQuestion1_
    String secAnswer1;//_secAnswer1_
    int secQuestion2;//_secQuestion2_
    String secAnswer2;//_secAnswer2_
    int secQuestion3;//_secQuestion3_
    String secAnswer3;//_secAnswer3_
    String PIN;//_PIN_
    String address;//_address_
    String city;//_city_
    String state;//_state_
    String zip;//_zip_
    String county;//_county_
    String homePhone;//_homePhone_
    String dlState;//_dlState_
    String dlNumbr;//_dlNumbr_
    boolean isCCAddrSame;//_isCCAddrSame_
    String cardType;//_cardType_
    String cardNumber;//_cardNumber_
    String expDate;//_expDate_
    String billing_firstName;//_bfirstName_
    String billing_lastName;//_blastName_
    String billing_address;//_baddress_
    String billing_city;//_bcity_
    String billing_state;//_bstate_
    String billing_zip;//_bzip_
    String billing_county;//_bcounty_
    String vehicle_year;//_year_
    String vehicle_color;//_color_
    String vehicle_make;//_make_
    String vehicle_model;//_model_
    String vehicle_tagId;//_tagId_
    String vehicle_activationCode;//_activationCode_
    String vehicle_licPlate;//_licPlate_
    String vehicle_state;//_vstate_
    String CVV;//_CVV_
    int accountTypeId;//acctTypeId


    public CreateAccountData() {
        super();
    }

    public CreateAccountData(String firstName, String lastName,
                             String emailAddr, String userId, String pw, int secQuestion1,
                             String secAnswer1, int secQuestion2, String secAnswer2,
                             int secQuestion3, String secAnswer3, String pIN, String address,
                             String city, String state, String zip, String county,
                             String homePhone, String dlState, String dlNumbr,
                             boolean isCCAddrSame, String cardType, String cardNumber,
                             String expDate, String billing_firstName, String billing_lastName,
                             String billing_address, String billing_city, String billing_state,
                             String billing_zip, String billing_county, String vehicle_year,
                             String vehicle_color, String vehicle_make, String vehicle_model,
                             String vehicle_tagId, String vehicle_activationCode,
                             String vehicle_licPlate, String vehicle_state, String cVV, int accountTypeId) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddr = emailAddr;
        this.userId = userId;
        this.pw = pw;
        this.secQuestion1 = secQuestion1;
        this.secAnswer1 = secAnswer1;
        this.secQuestion2 = secQuestion2;
        this.secAnswer2 = secAnswer2;
        this.secQuestion3 = secQuestion3;
        this.secAnswer3 = secAnswer3;
        PIN = pIN;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.county = county;
        this.homePhone = homePhone;
        this.dlState = dlState;
        this.dlNumbr = dlNumbr;
        this.isCCAddrSame = isCCAddrSame;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.expDate = expDate;
        this.billing_firstName = billing_firstName;
        this.billing_lastName = billing_lastName;
        this.billing_address = billing_address;
        this.billing_city = billing_city;
        this.billing_state = billing_state;
        this.billing_zip = billing_zip;
        this.billing_county = billing_county;
        this.vehicle_year = vehicle_year;
        this.vehicle_color = vehicle_color;
        this.vehicle_make = vehicle_make;
        this.vehicle_model = vehicle_model;
        this.vehicle_tagId = vehicle_tagId;
        this.vehicle_activationCode = vehicle_activationCode;
        this.vehicle_licPlate = vehicle_licPlate;
        this.vehicle_state = vehicle_state;
        CVV = cVV;
        this.accountTypeId = accountTypeId;
    }


//    public int getAccountTypeID() {
//        return accountTypeID;
//    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public int getSecQuestion1() {
        return secQuestion1;
    }

    public void setSecQuestion1(int secQuestion1) {
        this.secQuestion1 = secQuestion1;
    }

    public String getSecAnswer1() {
        return secAnswer1;
    }

    public void setSecAnswer1(String secAnswer1) {
        this.secAnswer1 = secAnswer1;
    }

    public int getSecQuestion2() {
        return secQuestion2;
    }

    public void setSecQuestion2(int secQuestion2) {
        this.secQuestion2 = secQuestion2;
    }

    public String getSecAnswer2() {
        return secAnswer2;
    }

    public void setSecAnswer2(String secAnswer2) {
        this.secAnswer2 = secAnswer2;
    }

    public int getSecQuestion3() {
        return secQuestion3;
    }

    public void setSecQuestion3(int secQuestion3) {
        this.secQuestion3 = secQuestion3;
    }

    public String getSecAnswer3() {
        return secAnswer3;
    }

    public void setSecAnswer3(String secAnswer3) {
        this.secAnswer3 = secAnswer3;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String pIN) {
        PIN = pIN;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getDlState() {
        return dlState;
    }

    public void setDlState(String dlState) {
        this.dlState = dlState;
    }

    public String getDlNumbr() {
        return dlNumbr;
    }

    public void setDlNumbr(String dlNumbr) {
        this.dlNumbr = dlNumbr;
    }

    public boolean isCCAddrSame() {
        return isCCAddrSame;
    }

    public void setCCAddrSame(boolean isCCAddrSame) {
        this.isCCAddrSame = isCCAddrSame;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getBilling_firstName() {
        return billing_firstName;
    }

    public void setBilling_firstName(String billing_firstName) {
        this.billing_firstName = billing_firstName;
    }

    public String getBilling_lastName() {
        return billing_lastName;
    }

    public void setBilling_lastName(String billing_lastName) {
        this.billing_lastName = billing_lastName;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getBilling_state() {
        return billing_state;
    }

    public void setBilling_state(String billing_state) {
        this.billing_state = billing_state;
    }

    public String getBilling_zip() {
        return billing_zip;
    }

    public void setBilling_zip(String billing_zip) {
        this.billing_zip = billing_zip;
    }

    public String getBilling_county() {
        return billing_county;
    }

    public void setBilling_county(String billing_county) {
        this.billing_county = billing_county;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

//    public void setAccountTypeID(int accountTypeID) {
//        this.accountTypeID = accountTypeID;
//    }

    public String getVehicle_color() {
        return vehicle_color;
    }

    public void setVehicle_color(String vehicle_color) {
        this.vehicle_color = vehicle_color;
    }

    public String getVehicle_make() {
        return vehicle_make;
    }

    public void setVehicle_make(String vehicle_make) {
        this.vehicle_make = vehicle_make;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_tagId() {
        return vehicle_tagId;
    }

    public void setVehicle_tagId(String vehicle_tagId) {
        this.vehicle_tagId = vehicle_tagId;
    }

    public String getVehicle_activationCode() {
        return vehicle_activationCode;
    }

    public void setVehicle_activationCode(String vehicle_activationCode) {
        this.vehicle_activationCode = vehicle_activationCode;
    }

    public String getVehicle_licPlate() {
        return vehicle_licPlate;
    }

    public void setVehicle_licPlate(String vehicle_licPlate) {
        this.vehicle_licPlate = vehicle_licPlate;
    }

    public String getVehicle_state() {
        return vehicle_state;
    }

    public void setVehicle_state(String vehicle_state) {
        this.vehicle_state = vehicle_state;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String cVV) {
        CVV = cVV;
    }

    @Override
    public String toString() {
        return "CreateAccountData [firstName=" + firstName + ", lastName="
                + lastName + ", emailAddr=" + emailAddr + ", userId=" + userId
                + ", pw=" + pw + ", secQuestion1=" + secQuestion1
                + ", secAnswer1=" + secAnswer1 + ", secQuestion2="
                + secQuestion2 + ", secAnswer2=" + secAnswer2
                + ", secQuestion3=" + secQuestion3 + ", secAnswer3="
                + secAnswer3 + ", PIN=" + PIN + ", address=" + address
                + ", city=" + city + ", state=" + state + ", zip=" + zip
                + ", county=" + county + ", homePhone=" + homePhone
                + ", dlState=" + dlState + ", dlNumbr=" + dlNumbr
                + ", isCCAddrSame=" + isCCAddrSame + ", cardType=" + cardType
                + ", cardNumber=" + cardNumber + ", expDate=" + expDate
                + ", billing_firstName=" + billing_firstName
                + ", billing_lastName=" + billing_lastName
                + ", billing_address=" + billing_address + ", billing_city="
                + billing_city + ", billing_state=" + billing_state
                + ", billing_zip=" + billing_zip + ", billing_county="
                + billing_county + ", vehicle_year=" + vehicle_year
                + ", vehicle_color=" + vehicle_color + ", vehicle_make="
                + vehicle_make + ", vehicle_model=" + vehicle_model
                + ", vehicle_tagId=" + vehicle_tagId
                + ", vehicle_activationCode=" + vehicle_activationCode
                + ", vehicle_licPlate=" + vehicle_licPlate + ", vehicle_state="
                + vehicle_state + ", CVV=" + CVV + ", acctTypeId=" + accountTypeId + "]";
    }


    public void setAccountTypeId(int accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public int getAccountTypeId() {
        return accountTypeId;
    }
}
