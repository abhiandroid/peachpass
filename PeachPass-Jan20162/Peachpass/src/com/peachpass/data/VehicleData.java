package com.peachpass.data;

import java.io.Serializable;

public class VehicleData implements Serializable {

//	String tagId;
//	String tagStatus;
//	String tagType;
//	String licPlate;//
//	String year;//
//	String model;//
//	String travelStartDate;
//	String travelEndDate;
	
	/***/
	String vehicleId_new;
    String licPlate_new;
    String make_new;
    String model_new;
    String color_new;
    String year_new;
    String tollMode_new;
    String travelEndDate_new;

	public VehicleData(String vehicleId_new, String licPlate_new,
			String make_new, String model_new, String color_new,
			String year_new, String tollMode_new, String travelEndDate_new) {
		super();
		this.vehicleId_new = vehicleId_new;
		this.licPlate_new = licPlate_new;
		this.make_new = make_new;
		this.model_new = model_new;
		this.color_new = color_new;
		this.year_new = year_new;
		this.tollMode_new = tollMode_new;
		this.travelEndDate_new = travelEndDate_new;
	}

//	public String getTollState() {
//		return tollState;
//	}
//
//	public void setTollState(String tollState) {
//		this.tollState = tollState;
//	}

//	String color;//
//	String make;//
//	String state;//
//	String tollState;
	
	
	public boolean equals(Object object) {
		if (object instanceof VehicleData) {

			return ((licPlate_new
					.equals(((VehicleData) object).licPlate_new))
					&& (year_new
							.equals(((VehicleData) object).year_new))
					&& (model_new
							.equals(((VehicleData) object).model_new))
					&& (color_new
							.equals(((VehicleData) object).color_new))
					&& (make_new
							.equals(((VehicleData) object).make_new))
//					&& (state
//							.equals(((VehicleData) object).state))
							);
		}
		return false;
	}

//	public String getColor() {
//		return color;
//	}
//
//	public void setColor(String color) {
//		this.color = color;
//	}
//
//	public String getMake() {
//		return make;
//	}
//
//	public void setMake(String make) {
//		this.make = make;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}

//	public String getTagId() {
//		return tagId;
//	}
//
//	public void setTagId(String tagId) {
//		this.tagId = tagId;
//	}
//
//	public String getTagStatus() {
//		return tagStatus;
//	}
//
//	public void setTagStatus(String tagStatus) {
//		this.tagStatus = tagStatus;
//	}
//
//	public String getTagType() {
//		return tagType;
//	}
//
//	public void setTagType(String tagType) {
//		this.tagType = tagType;
//	}
//
//	public String getLicPlate() {
//		return licPlate;
//	}
//
//	public void setLicPlate(String licPlate) {
//		this.licPlate = licPlate;
//	}
//
//	public String getYear() {
//		return year;
//	}
//
//	public void setYear(String year) {
//		this.year = year;
//	}
//
//	public String getModel() {
//		return model;
//	}

	public String getVehicleId_new() {
		return vehicleId_new;
	}

	public void setVehicleId_new(String vehicleId_new) {
		this.vehicleId_new = vehicleId_new;
	}

	public String getLicPlate_new() {
		return licPlate_new;
	}

	public void setLicPlate_new(String licPlate_new) {
		this.licPlate_new = licPlate_new;
	}

	public String getMake_new() {
		return make_new;
	}

	public void setMake_new(String make_new) {
		this.make_new = make_new;
	}

	public String getModel_new() {
		return model_new;
	}

	public void setModel_new(String model_new) {
		this.model_new = model_new;
	}

	public String getColor_new() {
		return color_new;
	}

	public void setColor_new(String color_new) {
		this.color_new = color_new;
	}

	public String getYear_new() {
		return year_new;
	}

	public void setYear_new(String year_new) {
		this.year_new = year_new;
	}

	public String getTollMode_new() {
		return tollMode_new;
	}

	public void setTollMode_new(String tollMode_new) {
		this.tollMode_new = tollMode_new;
	}

	public String getTravelEndDate_new() {
		return travelEndDate_new;
	}

	public void setTravelEndDate_new(String travelEndDate_new) {
		this.travelEndDate_new = travelEndDate_new;
	}

//	public void setModel(String model) {
//		this.model = model;
//	}
//
//	public String getTravelStartDate() {
//		return travelStartDate;
//	}
//
//	public void setTravelStartDate(String travelStartDate) {
//		this.travelStartDate = travelStartDate;
//	}
//
//	public String getTravelEndDate() {
//		return travelEndDate;
//	}
//
//	public void setTravelEndDate(String travelEndDate) {
//		this.travelEndDate = travelEndDate;
//	}
	
//	public VehicleData(String tagId, String tagStatus, String tagType,
//			String licPlate, String year, String model, String travelStartDate,
//			String travelEndDate) {
//		super();
//		this.tagId = tagId;
//		this.tagStatus = tagStatus;
//		this.tagType = tagType;
//		this.licPlate = licPlate;
//		this.year = year;
//		this.model = model;
//		this.travelStartDate = travelStartDate;
//		this.travelEndDate = travelEndDate;
//	}

//	public VehicleData(String tagId, String tagStatus, String tagType,
//			String licPlate, String year, String model, String travelStartDate,
//			String travelEndDate,String tollState) {
//		super();
//		this.tagId = tagId;
//		this.tagStatus = tagStatus;
//		this.tagType = tagType;
//		this.licPlate = licPlate;
//		this.year = year;
//		this.model = model;
//		this.travelStartDate = travelStartDate;
//		this.travelEndDate = travelEndDate;
//		this.tollState=tollState;
//	}

//	public VehicleData(String tagId, String tagStatus, String tagType,
//			String licPlate, String year, String model, String travelStartDate,
//			String travelEndDate, String color, String make, String state) {
//		super();
//		this.tagId = tagId;
//		this.tagStatus = tagStatus;
//		this.tagType = tagType;
//		this.licPlate = licPlate;
//		this.year = year;
//		this.model = model;
//		this.travelStartDate = travelStartDate;
//		this.travelEndDate = travelEndDate;
//		this.color = color;
//		this.make = make;
//		this.state = state;
//	}
//	public VehicleData(String licPlate,String year,String model,String color,String make,String state){
//		super();
//		this.licPlate = licPlate;
//		this.year = year;
//		this.model = model;
//		this.color = color;
//		this.make = make;
//		this.state = state;
//	}

//	@Override
//	public String toString() {
//		return "VehicleData [tagId=" + tagId + ", tagStatus=" + tagStatus
//				+ ", tagType=" + tagType + ", licPlate=" + licPlate + ", year="
//				+ year + ", model=" + model + ", travelStartDate="
//				+ travelStartDate + ", travelEndDate=" + travelEndDate
//				+ ", color=" + color + ", make=" + make + ", state=" + state
//				+ "]";
//	}
	

}
