package com.peachpass.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ViolationDetailData implements Serializable{

	String month;
	String day;
	String txnLane;
	String laneType;
	String time;
	String licPlate;
	String dateTime;
	String direction = "-";
	String description = "-";
	String amount;

	public ViolationDetailData(String month, String day, String txnLane,
			String laneType, String time, String licPlate, String dateTime,
			String direction, String description, String amount) {
		super();
		this.month = month;
		this.day = day;
		this.txnLane = txnLane;
		this.laneType = laneType;
		this.time = time;
		this.licPlate = licPlate;
		this.dateTime = dateTime;
		this.direction = direction;
		this.description = description;
		this.amount = amount;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTxnLane() {
		return txnLane;
	}

	public void setTxnLane(String txnLane) {
		this.txnLane = txnLane;
	}

	public String getLaneType() {
		return laneType;
	}

	public void setLaneType(String laneType) {
		this.laneType = laneType;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLicPlate() {
		return licPlate;
	}

	public void setLicPlate(String licPlate) {
		this.licPlate = licPlate;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
