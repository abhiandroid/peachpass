package com.peachpass.data;

public class GetAccountInfoByAccountIdData {

	String criticalNotificationsFlag;
	String lowAccountBalanceLevel;
	String isNonTollMode;
	String pin;
	String originalId;
	String isShortTermAccount;
	String primaryEmailAddress;
	String minimumReBillAmount;
	String lastPaymentDate;
	String accountBalance;
	String defaultLowBalance;
	String lastPaymentType;
	String accountStatusId;
	String tagIds;
	String autoReplenishFlag;
	String accountTypeId;
	String rebillAmountPerVehicle;
	String lastPaymentAmount;
	String rebillAmount;
	String status;

	public String getCriticalNotificationsFlag() {
		return criticalNotificationsFlag;
	}

	public void setCriticalNotificationsFlag(String criticalNotificationsFlag) {
		this.criticalNotificationsFlag = criticalNotificationsFlag;
	}

	public String getLowAccountBalanceLevel() {
		return lowAccountBalanceLevel;
	}

	public void setLowAccountBalanceLevel(String lowAccountBalanceLevel) {
		this.lowAccountBalanceLevel = lowAccountBalanceLevel;
	}

	public String getIsNonTollMode() {
		return isNonTollMode;
	}

	public void setIsNonTollMode(String isNonTollMode) {
		this.isNonTollMode = isNonTollMode;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public String getIsShortTermAccount() {
		return isShortTermAccount;
	}

	public void setIsShortTermAccount(String isShortTermAccount) {
		this.isShortTermAccount = isShortTermAccount;
	}

	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}

	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}

	public String getMinimumReBillAmount() {
		return minimumReBillAmount;
	}

	public void setMinimumReBillAmount(String minimumReBillAmount) {
		this.minimumReBillAmount = minimumReBillAmount;
	}

	public String getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getDefaultLowBalance() {
		return defaultLowBalance;
	}

	public void setDefaultLowBalance(String defaultLowBalance) {
		this.defaultLowBalance = defaultLowBalance;
	}

	public String getLastPaymentType() {
		return lastPaymentType;
	}

	public void setLastPaymentType(String lastPaymentType) {
		this.lastPaymentType = lastPaymentType;
	}

	public String getAccountStatusId() {
		return accountStatusId;
	}

	public void setAccountStatusId(String accountStatusId) {
		this.accountStatusId = accountStatusId;
	}

	public String getTagIds() {
		return tagIds;
	}

	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	public String getAutoReplenishFlag() {
		return autoReplenishFlag;
	}

	public void setAutoReplenishFlag(String autoReplenishFlag) {
		this.autoReplenishFlag = autoReplenishFlag;
	}

	public String getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(String accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getRebillAmountPerVehicle() {
		return rebillAmountPerVehicle;
	}

	public void setRebillAmountPerVehicle(String rebillAmountPerVehicle) {
		this.rebillAmountPerVehicle = rebillAmountPerVehicle;
	}

	public String getLastPaymentAmount() {
		return lastPaymentAmount;
	}

	public void setLastPaymentAmount(String lastPaymentAmount) {
		this.lastPaymentAmount = lastPaymentAmount;
	}

	public String getRebillAmount() {
		return rebillAmount;
	}

	public void setRebillAmount(String rebillAmount) {
		this.rebillAmount = rebillAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GetAccountInfoByAccountIdData(String criticalNotificationsFlag,
			String lowAccountBalanceLevel, String isNonTollMode, String pin,
			String originalId, String isShortTermAccount,
			String primaryEmailAddress, String minimumReBillAmount,
			String lastPaymentDate, String accountBalance,
			String defaultLowBalance, String lastPaymentType,
			String accountStatusId, String tagIds, String autoReplenishFlag,
			String accountTypeId, String rebillAmountPerVehicle,
			String lastPaymentAmount, String rebillAmount, String status) {
		super();
		this.criticalNotificationsFlag = criticalNotificationsFlag;
		this.lowAccountBalanceLevel = lowAccountBalanceLevel;
		this.isNonTollMode = isNonTollMode;
		this.pin = pin;
		this.originalId = originalId;
		this.isShortTermAccount = isShortTermAccount;
		this.primaryEmailAddress = primaryEmailAddress;
		this.minimumReBillAmount = minimumReBillAmount;
		this.lastPaymentDate = lastPaymentDate;
		this.accountBalance = accountBalance;
		this.defaultLowBalance = defaultLowBalance;
		this.lastPaymentType = lastPaymentType;
		this.accountStatusId = accountStatusId;
		this.tagIds = tagIds;
		this.autoReplenishFlag = autoReplenishFlag;
		this.accountTypeId = accountTypeId;
		this.rebillAmountPerVehicle = rebillAmountPerVehicle;
		this.lastPaymentAmount = lastPaymentAmount;
		this.rebillAmount = rebillAmount;
		this.status = status;
	}

	

}
