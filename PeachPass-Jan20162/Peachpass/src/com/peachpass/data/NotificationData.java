package com.peachpass.data;

/** PoJo classes to hold Notification Data*/
public class NotificationData {

	String description;
	String notificationDate;
	boolean read;
	String fontAwsome;
	String compType;
	String status;
	
	

	public String getCompType() {
		return compType;
	}

	public void setCompType(String compType) {
		this.compType = compType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getText() {
		return description;
	}

	public String getFontAwsome() {
		return fontAwsome;
	}

	public void setFontAwsome(String fontAwsome) {
		this.fontAwsome = fontAwsome;
	}

	public void setText(String text) {
		this.description = text;
	}

	public String getDate() {
		return notificationDate;
	}

	public void setDate(String date) {
		this.notificationDate = date;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public NotificationData(String text, String date, boolean read,String fontAwsome) {
		super();
		this.description = text;
		this.notificationDate = date;
		this.read = read;
		this.fontAwsome = fontAwsome;
	}
	public NotificationData(String text, String date, boolean read,String compType, String status) {
		super();
		this.description = text;
		this.notificationDate = date;
		this.read = read;
		this.compType = compType;
		this.status = status;
	}

	@Override
	public String toString() {
		return "NotificationData [text=" + description + ", date=" + notificationDate + ", read="
				+ read + "]";
	}

}
