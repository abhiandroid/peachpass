package com.peachpass.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.peachpass.utils.Utils;

/** PoJo classes to hold Peach Pass Data*/
public class MyPeachPassData  implements Comparable<MyPeachPassData>{

	 String month;
	 String day;
	 String i85;
	 String direction;
	 String licPlateNo;
	 String lane;
	 String time;
	 String currency;
	 String transacDate;
	 String transactionId;
	 boolean read;
	 
	 
	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransacDate() {
		return transacDate;
	}
	public void setTransacDate(String transacDate) {
		this.transacDate = transacDate;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getI85() {
		return i85;
	}
	public void setI85(String i85) {
		this.i85 = i85;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getLicPlateNo() {
		return licPlateNo;
	}
	public void setLicPlateNo(String licPlateNo) {
		this.licPlateNo = licPlateNo;
	}
	public String getLane() {
		return lane;
	}
	public void setLane(String lane) {
		this.lane = lane;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Override
	public String toString() {
		return "MyPeachPassData [month=" + month + ", day=" + day + ", i85="
				+ i85 + ", direction=" + direction + ", licPlateNo="
				+ licPlateNo + ", lane=" + lane + ", time=" + time
				+ ", currency=" + currency + "]";
	}
	public MyPeachPassData(String month, String day, String i85,
			String direction, String licPlateNo, String lane, String time,
			String currency) {
		super();
		this.month = month;
		this.day = day;
		this.i85 = i85;
		this.direction = direction;
		this.licPlateNo = licPlateNo;
		this.lane = lane;
		this.time = time;
		this.currency = currency;
	}
	
	public MyPeachPassData(String month, String day, String i85,
			String direction, String licPlateNo, String lane, String time,
			String currency, String transacDate) {
		super();
		this.month = month;
		this.day = day;
		this.i85 = i85;
		this.direction = direction;
		this.licPlateNo = licPlateNo;
		this.lane = lane;
		this.time = time;
		this.currency = currency;
		this.transacDate = transacDate;
	}
	
	public MyPeachPassData(String month, String day, String i85,
			String direction, String licPlateNo, String lane, String time,
			String currency,String transacDate, String transactionId, boolean read) {
		super();
		this.month = month;
		this.day = day;
		this.i85 = i85;
		this.direction = direction;
		this.licPlateNo = licPlateNo;
		this.lane = lane;
		this.time = time;
		this.currency = currency;
		this.transacDate = transacDate;
		this.transactionId = transactionId;
		this.read= read;
	}
	@Override
	public int compareTo(MyPeachPassData another) {
		// TODO Auto-generated method stub
		Log.e("peachpass", "compareto--"+another.getTransacDate());
		try {
			if (transacDate == null || another.getTransacDate() == null)
			      return 0;
			 SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_PATTERN_SOAP,Locale.ENGLISH);
			 Date date1 = formatter.parse(transacDate);
			 Date date2 = formatter.parse(another.getTransacDate());
			    return date1.compareTo(date2);	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
		 
	}
	
	
	
	/* 
	 * <ns1:transactionDate>2014-08-31T15:00:00.000-04:00</ns1:transactionDate>
               <ns1:postedDate>2015-07-20T21:14:33.000-04:00</ns1:postedDate>
               <ns1:tagId xsi:nil="1"/>
               <ns1:licensePlate>NBA2027</ns1:licensePlate>
               <ns1:lane>85A-PH03-01</ns1:lane>
               <ns1:dir>North</ns1:dir>
               <ns1:location xsi:nil="1"/>
               <ns1:amount>-2.0</ns1:amount>*/

//	public MyPeachPassData(String month, String day, String line1,
//			String line2, String currency) {
//		super();
//		this.month = month;
//		this.day = day;
//		this.line1 = line1;
//		this.line2 = line2;
//		this.currency = currency;
//	}

}
