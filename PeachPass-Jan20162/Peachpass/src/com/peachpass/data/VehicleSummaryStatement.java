package com.peachpass.data;

public class VehicleSummaryStatement {

	String tagId;
	String licPlate;
	String state;
	String description;
	String txns;
	String amount;

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getLicPlate() {
		return licPlate;
	}

	public void setLicPlate(String licPlate) {
		this.licPlate = licPlate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTxns() {
		return txns;
	}

	public void setTxns(String txns) {
		this.txns = txns;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public VehicleSummaryStatement(String tagId, String licPlate, String state,
			String description, String txns, String amount) {
		super();
		this.tagId = tagId;
		this.licPlate = licPlate;
		this.state = state;
		this.description = description;
		this.txns = txns;
		this.amount = amount;
	}

}
