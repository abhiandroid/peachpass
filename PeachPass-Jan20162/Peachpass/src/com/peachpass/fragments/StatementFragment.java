package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.data.AccountDetailData;
import com.peachpass.data.AccountSummaryData;
import com.peachpass.data.StatementMasterData;
import com.peachpass.data.VehicleDetailStatementData;
import com.peachpass.data.VehicleSummaryStatement;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class StatementFragment extends Fragment {

    Activity mActivity;
    // StatementListAdapter adapter;
    String newDateString;
    private int mColorRes = -1;

    LinearLayout ll_acc_sum_parrent, ll_veh_sum_parrent, ll_acc_detail_parrent,
            ll_veh_detail_parrent;
    TextView txt_veh_amnt, txt_veh_detail_amnt;
    Spinner spinner_past_months;
    LinearLayout ll_accountsummaryheader, ll_accountsummaryheader_nodata;
    TextView txt_accountsummaryheader_nodata;

    LinearLayout ll_accountdetailheader, ll_accountdetailheader_nodata;
    TextView txt_accountdetailheader_nodata;
    Context context;

    public StatementFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public StatementFragment(int colorRes) {
        mColorRes = colorRes;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");
        // int color = getResources().getColor(mColorRes);
        // adapter = new StatementListAdapter(mActivity);

        View view = inflater.inflate(R.layout.statements, container, false);
        Typeface font = ScreenUtils.returnTypeFace(mActivity);

        TextView txt_fa_tran_crd_card = (TextView) view
                .findViewById(R.id.txt_fa_tran_crd_card);
        txt_fa_tran_crd_card.setTypeface(font);

        TextView txt_fa_tran_statements = (TextView) view
                .findViewById(R.id.txt_fa_tran_statements);
        txt_fa_tran_statements.setTypeface(font);

        // ListView lv = (ListView)
        // view.findViewById(R.id.listView_mypeachpass);
        // lv.setAdapter(adapter);

        LinearLayout ll_trn_alert = (LinearLayout) view
                .findViewById(R.id.ll_trn_alert);
        LinearLayout ll_trn_stmnt = (LinearLayout) view
                .findViewById(R.id.ll_trn_stmnt);

        ll_acc_sum_parrent = (LinearLayout) view
                .findViewById(R.id.ll_acc_sum_parrent);
        ll_veh_sum_parrent = (LinearLayout) view
                .findViewById(R.id.ll_veh_sum_parrent);
        ll_acc_detail_parrent = (LinearLayout) view
                .findViewById(R.id.ll_acc_detail_parrent);
        ll_veh_detail_parrent = (LinearLayout) view
                .findViewById(R.id.ll_veh_detail_parrent);

        txt_veh_amnt = (TextView) view.findViewById(R.id.txt_veh_amnt);
        txt_veh_detail_amnt = (TextView) view
                .findViewById(R.id.txt_veh_detail_amnt);

		
		/*No data available widgets*/
        ll_accountsummaryheader = (LinearLayout) view
                .findViewById(R.id.ll_accountsummaryheader);
        ll_accountsummaryheader_nodata = (LinearLayout) view
                .findViewById(R.id.ll_accountsummaryheader_nodata);
        txt_accountsummaryheader_nodata = (TextView) view.findViewById(R.id.txt_accountsummaryheader_nodata);

        ll_accountdetailheader = (LinearLayout) view
                .findViewById(R.id.ll_accountdetailheader);
        ll_accountdetailheader_nodata = (LinearLayout) view
                .findViewById(R.id.ll_accountdetailheader_nodata);
        txt_accountdetailheader_nodata = (TextView) view.findViewById(R.id.txt_accountdetailheader_nodata);


        ll_trn_alert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(
                                new AlertsFragment(R.color.red),
                                "ALERTS", null, true, false);
                    }
            }
        });

        ll_trn_stmnt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(
                                new TransactionsFragment(R.color.red),
                                "TRANSACTIONS", null, true, false);
                    }
            }
        });

        spinner_past_months = (Spinner) view
                .findViewById(R.id.spinner_past_months);

        final ArrayList<String> pastMonths = new ArrayList<String>();
        for (int i = 0; i < 12; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MONTH, -i);
            SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy",
                    Locale.ENGLISH);
            pastMonths.add(format.format(calendar.getTime()));
        }
        ArrayAdapter<String> pastMonthsAdapter = new ArrayAdapter<String>(
                mActivity, android.R.layout.simple_spinner_item, pastMonths);
        spinner_past_months.setAdapter(pastMonthsAdapter);

        spinner_past_months
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView,
                                               View selectedItemView, int position, long id) {
                        // your code here
                        String selected = pastMonths.get(position);

                        final String OLD_FORMAT = "MMMM yyyy";
                        final String NEW_FORMAT = "MM/yyyy";

                        // August 12, 2010
                        String oldDateString = selected;

                        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT,
                                Locale.ENGLISH);
                        Date d = null;
                        try {
                            d = sdf.parse(oldDateString);
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        sdf.applyPattern(NEW_FORMAT);
                        newDateString = sdf.format(d);

                        if (Utils.isNetworkAvailable(mActivity))
                            new AsyncTask<String, String, StatementMasterData>() {
                                ProgressDialog pdialog = ScreenUtils
                                        .returnProgDialogObj(
                                                mActivity,
                                                WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                                                false);

                                @Override
                                protected void onPreExecute() {
                                    try {
                                        pdialog.show();
                                    } catch (Exception e) {
                                        // TODO: handle exception
                                    }

                                }

                                ;

                                @Override
                                protected StatementMasterData doInBackground(
                                        String... params) {
                                    // TODO Auto-generated method stub
                                    try {

                                        ArrayList<AccountSummaryData> accountSummaryArray = new ArrayList<AccountSummaryData>();
                                        ArrayList<VehicleSummaryStatement> vehicleSummaryArray = new ArrayList<VehicleSummaryStatement>();
                                        ArrayList<AccountDetailData> accountDetailArray = new ArrayList<AccountDetailData>();
                                        ArrayList<VehicleDetailStatementData> vehicleDetailArray = new ArrayList<VehicleDetailStatementData>();

                                        String requestBody = WebserviceUtils
                                                .readFromAsset("getStatement",
                                                        mActivity);
                                        requestBody = requestBody.replaceAll(
                                                "_accountId_",
                                                Utils.getAccountId(mActivity));
                                        requestBody = requestBody.replaceAll(
                                                "_userName_",
                                                Utils.getUsername(mActivity));
                                        requestBody = requestBody.replaceAll(
                                                "_sessionId_",
                                                Utils.getSessionId(mActivity));
                                        requestBody = requestBody.replaceAll(
                                                "_period_", newDateString);

                                        requestBody = requestBody.replaceAll(
                                                "_osType_",
                                                WebserviceUtils.OS_TYPE);
                                        requestBody = requestBody.replaceAll(
                                                "_osVersion_",
                                                Utils.getOsVersion());
                                        requestBody = requestBody.replaceAll(
                                                "_ipAddress_",
                                                WebserviceUtils.getExternalIP());

                                        requestBody = requestBody.replaceAll(
                                                "_id_", WebserviceUtils.ID);

                                        Log.e("peachpass", requestBody + "");
                                        String result = WebserviceUtils
                                                .sendSoapRequest(
                                                        mActivity,
                                                        requestBody,
                                                        WebserviceUtils.SOAP_ACTION_GETSTATEMENT);
                                        XMLParser parser = new XMLParser();
                                        String xml = result;
                                        Document doc = parser
                                                .getDomElement(xml); // getting
                                        // DOM
                                        // element

                                        NodeList nl = doc
                                                .getElementsByTagName("ns1:accountSummary");

                                        for (int i = 0; i < nl.getLength(); i++) {

                                            Element e = (Element) nl.item(i);

                                            String description = parser
                                                    .getValue(e,
                                                            "ns1:description");
                                            String quantity = parser.getValue(
                                                    e, "ns1:quantity");
                                            String amount = parser.getValue(e,
                                                    "ns1:amount");

                                            AccountSummaryData notData = new AccountSummaryData(
                                                    description, quantity,
                                                    amount);
                                            accountSummaryArray.add(notData);
                                        }

                                        nl = doc.getElementsByTagName("ns1:vehicleSummary");

                                        for (int i = 0; i < nl.getLength(); i++) {

                                            Element e = (Element) nl.item(i);

                                            String tagId = parser.getValue(e,
                                                    "ns1:tagId");
                                            String licPlate = parser.getValue(
                                                    e, "ns1:licPlate");
                                            String state = parser.getValue(e,
                                                    "ns1:state");
                                            String description = parser
                                                    .getValue(e,
                                                            "ns1:description");
                                            String txns = parser.getValue(e,
                                                    "ns1:txns");
                                            String amount = parser.getValue(e,
                                                    "ns1:amount");

                                            VehicleSummaryStatement notData = new VehicleSummaryStatement(
                                                    tagId, licPlate, state,
                                                    description, txns, amount);
                                            vehicleSummaryArray.add(notData);
                                        }

                                        nl = doc.getElementsByTagName("ns1:accountDetail");

                                        for (int i = 0; i < nl.getLength(); i++) {

                                            Element e = (Element) nl.item(i);

                                            String transactionDate = parser
                                                    .getValue(e,
                                                            "ns1:transactionDate");
                                            String location = parser.getValue(
                                                    e, "ns1:location");
                                            String transTypeDescr = parser
                                                    .getValue(e,
                                                            "ns1:transTypeDescr");
                                            String amount = parser.getValue(e,
                                                    "ns1:amount");

                                            AccountDetailData notData = new AccountDetailData(
                                                    transactionDate, location,
                                                    transTypeDescr, amount);
                                            accountDetailArray.add(notData);
                                        }

                                        nl = doc.getElementsByTagName("ns1:vehicleDetail");

                                        for (int i = 0; i < nl.getLength(); i++) {

                                            Element e = (Element) nl.item(i);

                                            String tagIdLicPlate = parser
                                                    .getValue(e,
                                                            "ns1:tagIdLicPlate");
                                            String dateTime = parser.getValue(
                                                    e, "ns1:dateTime");
                                            String plazaLane = parser.getValue(
                                                    e, "ns1:plazaLane");
                                            String direction = parser.getValue(
                                                    e, "ns1:direction");
                                            String location = parser.getValue(
                                                    e, "ns1:location");
                                            String transTypeDescr = parser
                                                    .getValue(e,
                                                            "ns1:transTypeDescr");
                                            String amount = parser.getValue(e,
                                                    "ns1:amount");

                                            VehicleDetailStatementData notData = new VehicleDetailStatementData(
                                                    tagIdLicPlate, dateTime,
                                                    plazaLane, direction,
                                                    location, transTypeDescr,
                                                    amount);
                                            vehicleDetailArray.add(notData);
                                        }

                                        StatementMasterData masterArray = new StatementMasterData(
                                                accountSummaryArray,
                                                vehicleSummaryArray,
                                                accountDetailArray,
                                                vehicleDetailArray);

                                        return masterArray;

                                    } catch (Exception e) {
                                        // TODO: handle exception

                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(
                                        StatementMasterData result) {
                                    // TODO Auto-generated method stub
                                    super.onPostExecute(result);

                                    try {
                                        pdialog.dismiss();
                                    } catch (Exception e) {
                                        // TODO: handle exception
                                    }
                                    clearAlldata();
                                    if (result != null) {

                                        // for (AccountDetailData temp : result)
                                        // adapter.addItem(temp);

                                        // adapter.notifyDataSetChanged();
                                        // if (result.size() == 0) {
                                        // adapter.clearItems();
                                        // adapter.notifyDataSetChanged();
                                        // ScreenUtils.raiseToast(mActivity,
                                        // WebserviceUtils.NO_DATA_DISPLAY);
                                        // }

                                        fillWithdata(result);
                                    } else {
                                        ScreenUtils
                                                .raiseToast(
                                                        mActivity,
                                                        WebserviceUtils.SOMETHING_WENT_WRONG);
                                    }
                                }

                                private void fillWithdata(
                                        StatementMasterData result) {
                                    // TODO Auto-generated method stub
                                    ArrayList<AccountSummaryData> accountSummaryArray = result
                                            .getAccountSummaryArray();
                                    if (accountSummaryArray == null
                                            || accountSummaryArray.size() == 0) {
                                        setNoDataDetails(ll_accountsummaryheader, ll_accountsummaryheader_nodata, txt_accountsummaryheader_nodata, "No data found for " + spinner_past_months.getSelectedItem());
                                    } else {
                                        int count = accountSummaryArray.size();
                                        Log.e("Statement", "count-- " + count);
                                        int loop = 0;
                                        ll_acc_sum_parrent.removeAllViews();
                                        for (AccountSummaryData data : accountSummaryArray) {
                                            View acc_child = getInflater()
                                                    .inflate(R.layout.statement_acc_summary_row, null);
                                            TextView txt_acc_desc = (TextView) acc_child
                                                    .findViewById(R.id.txt_acc_desc);
                                            TextView txt_acc_qty = (TextView) acc_child
                                                    .findViewById(R.id.txt_acc_qty);
                                            TextView txt_acc_amnt = (TextView) acc_child
                                                    .findViewById(R.id.txt_acc_amnt);
                                            txt_acc_desc
                                                    .setText(returnData(data
                                                            .getDescription()));
                                            txt_acc_qty.setText(returnData(data
                                                    .getQuantity()));
                                            txt_acc_amnt
                                                    .setText(returnData(data
                                                            .getAmount()));
                                            ll_acc_sum_parrent
                                                    .addView(acc_child);
                                            if (loop == 0)
                                                txt_acc_desc.setText("Beginning Balance");

                                            if (loop == 1 && count == 2)
                                                txt_acc_desc.setText("Ending Balance");

                                            if (loop == 1 && count == 3)
                                                txt_acc_desc.setText("Payments/Credits");

                                            if (loop == 2 && count == 3)
                                                txt_acc_desc.setText("Ending Balance");

                                            if (count == 4) {
                                                if (loop == 1) {
                                                    txt_acc_desc.setText("Beginning Balance");
                                                }
                                                if (loop == 2) {
                                                    txt_acc_desc.setText("Payments/Credits");
                                                }
                                                if (loop == 3) {
                                                    txt_acc_desc.setText("Ending Balance");
                                                }
                                            }

                                            Log.e("Statement", "loop-- " + loop);
                                            loop++;
                                        }
                                    }

                                    ArrayList<VehicleSummaryStatement> vehicleSummaryArray = result
                                            .getVehicleSummaryArray();
                                    if (vehicleSummaryArray == null
                                            || vehicleSummaryArray.size() == 0) {
                                        // ScreenUtils.raiseToast(mActivity,
                                        // WebserviceUtils.NO_DATA_DISPLAY);
                                        txt_veh_amnt.setText("No data found for " + spinner_past_months.getSelectedItem());
                                    } else {
                                        double total = 0.0;
                                        ll_veh_sum_parrent.removeAllViews();
                                        for (VehicleSummaryStatement data : vehicleSummaryArray) {
                                            View acc_child = getInflater()
                                                    .inflate(
                                                            R.layout.statement_vehicle_summary_row_alt,
                                                            null);
//											TextView txt_tagid = (TextView) acc_child
//													.findViewById(R.id.txt_tagid);
                                            TextView txt_lic_plate = (TextView) acc_child
                                                    .findViewById(R.id.txt_lic_plate);
//											TextView txt_lic_plate_state = (TextView) acc_child
//													.findViewById(R.id.txt_lic_plate_state);
                                            TextView txt_desc = (TextView) acc_child
                                                    .findViewById(R.id.txt_desc);
//											TextView txt_trans = (TextView) acc_child
//													.findViewById(R.id.txt_trans);
                                            TextView txt_amnt = (TextView) acc_child
                                                    .findViewById(R.id.txt_amnt);

//											txt_tagid.setText(returnData(data
//													.getTagId()));
                                            txt_lic_plate
                                                    .setText(returnData(data
                                                            .getLicPlate()));
//											txt_lic_plate_state
//													.setText(returnData(data
//															.getState()));
                                            txt_desc.setText(returnData(data
                                                    .getDescription()));
//											txt_trans.setText(returnData(data
//													.getTxns()));
                                            txt_amnt.setText(returnData(data
                                                    .getAmount()));

                                            ll_veh_sum_parrent
                                                    .addView(acc_child);

                                            if (data.getAmount() != null
                                                    && data.getAmount().trim()
                                                    .length() > 0) {
                                                try {
                                                    total = total
                                                            + Double.parseDouble(data
                                                            .getAmount());
                                                } catch (Exception e) {
                                                    // TODO: handle exception
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                        txt_veh_amnt.setText("TOTAL AMOUNT : " + total + "");
                                    }

                                    ArrayList<AccountDetailData> accountDetailArray = result
                                            .getAccountDetailArray();
                                    if (accountDetailArray == null
                                            || accountDetailArray.size() == 0) {

                                        setNoDataDetails(ll_accountdetailheader, ll_accountdetailheader_nodata, txt_accountdetailheader_nodata, "No data found for " + spinner_past_months.getSelectedItem());
                                    } else {
                                        ll_acc_detail_parrent.removeAllViews();
                                        for (AccountDetailData data : accountDetailArray) {
                                            View acc_child = getInflater()
                                                    .inflate(
                                                            R.layout.statement_acc_detail_row,
                                                            null);
                                            TextView txt_datetime = (TextView) acc_child
                                                    .findViewById(R.id.txt_datetime);
                                            TextView txt_loc = (TextView) acc_child
                                                    .findViewById(R.id.txt_loc);
                                            TextView txt_desc = (TextView) acc_child
                                                    .findViewById(R.id.txt_desc);
                                            TextView txt_amnt = (TextView) acc_child
                                                    .findViewById(R.id.txt_amnt);
                                            try {
                                                txt_datetime.setText(Utils.parseDate(
                                                        data.getTransactionDate(),
                                                        Utils.DATE_PATTERN_SOAP,
                                                        Utils.OTHER));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            /*txt_datetime.setText(Utils.parseDate(
                                                    data.getTransactionDate(),
                                                    Utils.DATE_PATTERN_SOAP,
                                                    Utils.OTHER));*/
                                            txt_loc.setText(returnData(data
                                                    .getLocation()));
                                            txt_desc.setText(returnData(data
                                                    .getTransTypeDescr()));
                                            txt_amnt.setText(returnData(data
                                                    .getAmount()));
                                            ll_acc_detail_parrent
                                                    .addView(acc_child);
                                        }
                                    }

                                    ArrayList<VehicleDetailStatementData> vehicleDetailArray = result
                                            .getVehicleDetailArray();
                                    if (vehicleDetailArray == null
                                            || vehicleDetailArray.size() == 0) {
                                        // ScreenUtils.raiseToast(mActivity,
                                        // WebserviceUtils.NO_DATA_DISPLAY);
                                        txt_veh_detail_amnt.setText("No data found for " + spinner_past_months.getSelectedItem());
                                    } else {
                                        double total = 0.0;
                                        ll_veh_detail_parrent.removeAllViews();
                                        for (VehicleDetailStatementData data : vehicleDetailArray) {
                                            View acc_child = getInflater()
                                                    .inflate(
                                                            R.layout.statement_veh_detail_row,
                                                            null);
                                            TextView txt_lic_plate = (TextView) acc_child
                                                    .findViewById(R.id.txt_lic_plate);
                                            TextView txt_datetime = (TextView) acc_child
                                                    .findViewById(R.id.txt_datetime);
                                            TextView txt_plazalane = (TextView) acc_child
                                                    .findViewById(R.id.txt_plazalane);
                                            TextView txt_dir = (TextView) acc_child
                                                    .findViewById(R.id.txt_dir);
                                            TextView txt_desc = (TextView) acc_child
                                                    .findViewById(R.id.txt_desc);
                                            TextView txt_amnt = (TextView) acc_child
                                                    .findViewById(R.id.txt_amnt);

                                            txt_lic_plate.setText(returnData(data
                                                    .getTagIdLicPlate()));
                                            txt_datetime.setText(Utils.parseDate(
                                                    data.getDateTime(),
                                                    Utils.DATE_PATTERN_SOAP,
                                                    Utils.OTHER));
                                            txt_plazalane
                                                    .setText(returnData(data
                                                            .getPlazaLane()));
                                            txt_dir.setText(returnData(data
                                                    .getDirection()));
                                            txt_desc.setText(returnData(data
                                                    .getTransTypeDescr()));
                                            txt_amnt.setText(returnData(data
                                                    .getAmount()));

                                            ll_veh_detail_parrent
                                                    .addView(acc_child);

                                            if (data.getAmount() != null
                                                    && data.getAmount().trim()
                                                    .length() > 0) {
                                                try {

                                                    Log.e("peachpass", "Double.parseDouble(data.getAmount()---" + Double.parseDouble(data.getAmount()));
                                                    total = total
                                                            + Double.parseDouble(data
                                                            .getAmount());
                                                    Log.e("peachpass", "Double.parseDouble(data.getAmount()---" + total);
                                                } catch (Exception e) {
                                                    // TODO: handle exception
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                        try {
                                            total = Math.round(total * 100);
                                            total = total / 100;
                                            txt_veh_detail_amnt.setText("SUBTOTAL AMOUNT : " + total + "");
                                        } catch (Exception e) {
                                            // TODO: handle exception
                                        }

                                    }

                                }

                            }.execute();
                        else {
                            ScreenUtils.raiseToast(mActivity,
                                    WebserviceUtils.NO_INTERNET_CONNECTION);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }

                });

        Utils.setTracker(Utils.STATEMENTS_VIEW);
        return view;
    }

    public void addItem(AccountDetailData item) {
        // if (adapter != null)
        // adapter.addItem(item);
    }

    public void clearList() {
        // if (adapter != null)
        // adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    LayoutInflater getInflater() {

        return mActivity.getLayoutInflater();
    }

    String returnData(String data) {
        if (data != null && data.length() > 0)
            return data;
        return "-";
    }

    void clearAlldata() {
        ll_acc_sum_parrent.removeAllViews();
        ll_veh_sum_parrent.removeAllViews();
        ll_acc_detail_parrent.removeAllViews();
        ll_veh_detail_parrent.removeAllViews();
        txt_veh_amnt.setText("");
        txt_veh_detail_amnt.setText("");

        ll_accountsummaryheader.setVisibility(View.VISIBLE);
        ll_accountsummaryheader_nodata.setVisibility(View.GONE);

        ll_accountdetailheader.setVisibility(View.VISIBLE);
        ll_accountdetailheader_nodata.setVisibility(View.GONE);
    }

    void setNoDataDetails(LinearLayout header, LinearLayout headerNoData, TextView textNodata, String details) {
        header.setVisibility(View.GONE);
        headerNoData.setVisibility(View.VISIBLE);
        textNodata.setText(details);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.STATEMENTS_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }
}
