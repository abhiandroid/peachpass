package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.activity.ViolationDetailActivity;
import com.peachpass.adapter.AlertListAdapter;
import com.peachpass.data.MyPeachPassData;
import com.peachpass.data.VehicleData;
import com.peachpass.data.ViolationDetailData;
import com.peachpass.utils.Cache;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@SuppressLint("ValidFragment")
public class AlertsFragment extends Fragment {

    AlertListAdapter adapter;
    private int mColorRes = -1;
    Activity mActivity;
    LinearLayout ll_no_alert_screen, ll_no_internet;
    ListView lv;
    ArrayList<MyPeachPassData> myPeachPassdataList;
    Context context;

    public AlertsFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public AlertsFragment(int colorRes) {
        mColorRes = colorRes;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");
        // int color = getResources().getColor(mColorRes);
        adapter = new AlertListAdapter(mActivity);

        // Log.e("peachpass", "onCreateView transaction");

        View view = inflater
                .inflate(R.layout.alerts_fragment, container, false);
        Typeface font = ScreenUtils.returnTypeFace(mActivity);

        TextView txt_fa_tran_crd_card = (TextView) view
                .findViewById(R.id.txt_fa_tran_crd_card);
        txt_fa_tran_crd_card.setTypeface(font);

        TextView txt_fa_tran_statements = (TextView) view
                .findViewById(R.id.txt_fa_tran_statements);
        txt_fa_tran_statements.setTypeface(font);

        lv = (ListView) view.findViewById(R.id.listView_mypeachpass);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(mActivity, ViolationDetailActivity.class);
                MyPeachPassData mppdata = myPeachPassdataList.get(position);
                ViolationDetailData data = new ViolationDetailData(mppdata.getMonth(), mppdata.getDay(), mppdata.getDirection(), mppdata.getLane(), mppdata.getTime(), mppdata.getLicPlateNo(), Utils.parseDate(mppdata.getTransacDate(), Utils.DATE_PATTERN_SOAP, Utils.OTHER), "-", "-", mppdata.getCurrency());
                intent.putExtra("value", data);
                startActivity(intent);

                Set<String> notificationSet = SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.ALERTS, mActivity);
                notificationSet.add(mppdata.getTransactionId());
                SharedPreferenceHelper.savePreferencesSet(SharedPreferenceHelper.ALERTS, notificationSet, mActivity);
                String count = SharedPreferenceHelper.getPreference(mActivity, SharedPreferenceHelper.UNREADALERTS);
                try {
                    int unreadCount = Integer.parseInt(count);
                    unreadCount = unreadCount - 1;
                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADALERTS, unreadCount + "", mActivity);
                    if (unreadCount < 0)
                        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADALERTS, 0 + "", mActivity);

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADALERTS, 0 + "", mActivity);
                }
                sendBroadcastToLeftMenu();

            }
        });

        LinearLayout ll_alert_trns = (LinearLayout) view
                .findViewById(R.id.ll_alert_trns);
        LinearLayout ll_alert_stmnts = (LinearLayout) view
                .findViewById(R.id.ll_alert_stmnts);

        ll_no_alert_screen = (LinearLayout) view
                .findViewById(R.id.ll_no_alert_screen);
        ll_no_internet = (LinearLayout) view
                .findViewById(R.id.ll_no_internet);
        View view_no_internet = (View) view
                .findViewById(R.id.view_no_internet);
        Button refresh = (Button) view_no_internet.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadData();
            }
        });

        ll_alert_trns.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(
                                new TransactionsFragment(R.color.red),
                                "TRANSACTIONS", null, true, false);
                        // FragmentTitle fragTitle = new FragmentTitle(
                        // "TRANSACTIONS", false);
                        // Cache.getInstance().getFragmentTitle().add(fragTitle);
                    }
            }
        });

        ll_alert_stmnts.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new StatementFragment(R.color.red),
                                "STATEMENTS", null, true, false);
                    }
            }
        });

        // if (1 == 2)
        loadData();

        Utils.setTracker(Utils.ALERTS_VIEW);

        return view;
    }

    private void loadData() {
        // TODO Auto-generated method stub
        new AsyncTask<String, String, ArrayList<MyPeachPassData>>() {
            ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(mActivity,
                    WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS, false);

            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                try {
                    pdialog.show();
                } catch (Exception e) {
                    // TODO: handle exception
                }
                super.onPreExecute();
            }

            @Override
            protected ArrayList<MyPeachPassData> doInBackground(
                    String... params) {
                // TODO Auto-generated method stub
                try {
                    ArrayList<VehicleData> vechileArrayTaginfo = getTagInfoList();
                    myPeachPassdataList = new ArrayList<MyPeachPassData>();

                    for (VehicleData vehData : vechileArrayTaginfo) {
                        String requestBody = WebserviceUtils.readFromAsset(
                                "viewViolationsPerVehicle", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));
                        requestBody = requestBody.replaceAll("_licPlate_",
                                "(GA)" + vehData.getLicPlate_new());

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", "requestBody---" + requestBody);
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_VIEWVIOLATIONSPERVEHICLE);
                        if (result == null)
                            return null;
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element

                        NodeList nl = doc.getElementsByTagName("ns1:txn");
                        /*
						 * <ns1:location>Pleasant Hill
						 * 02/85A-PH02-21</ns1:location>
						 */
                        for (int i = 0; i < nl.getLength(); i++) {

                            Element e = (Element) nl.item(i);
                            String transactionDate = parser.getValue(e,
                                    "ns1:violDateTime");//
                            String licensePlate = "(GA)" + vehData.getLicPlate_new();
                            String lane = parser.getValue(e, "ns1:laneType");// laneType
                            String dir = parser.getValue(e, "ns1:txnLane");// txnLane
                            String amount = parser.getValue(e, "ns1:txnTotal");//
                            String transactionId = parser.getValue(e, "ns1:transactionId");//
                            String month = "", day = "", time = "";

                            try {
                                String timeDetails[] = Utils.parseDate(
                                        transactionDate,
                                        Utils.DATE_PATTERN_SOAP,
                                        Utils.DATE_PATTERN_MY_PEACH_PASS)
                                        .split("\\,");
                                month = timeDetails[0];
                                day = timeDetails[1];
                                time = timeDetails[2];

                            } catch (Exception e2) {
                                // TODO: handle exception
                            }
                            MyPeachPassData data = new MyPeachPassData(month,
                                    day, "", dir, licensePlate, lane, time,
                                    amount, transactionDate, transactionId, false);
                            Log.e("peachpass", data.toString());
                            myPeachPassdataList.add(data);
                            // adapter.addItem(new MyPeachPassData(month, day,
                            // "I-85", dir, licensePlate, lane, time, amount));
                        }

                    }

                    // Cache.getInstance().setMyPeachPassdataList(
                    // myPeachPassdataList);
                    Collections.reverse(myPeachPassdataList);
                    return myPeachPassdataList;

                } catch (Exception e) {
                    // TODO: handle exception

                }
                return null;
            }

            private ArrayList<VehicleData> getTagInfoList() throws Exception {

                ArrayList<VehicleData> vechileArray = new ArrayList<VehicleData>();
                String requestBody = WebserviceUtils.readFromAsset(
                        "getAccountVehicleTollMode", mActivity);
                requestBody = requestBody.replaceAll("_accountId_",
                        Utils.getAccountId(mActivity));
                requestBody = requestBody.replaceAll("_userName_",
                        Utils.getUsername(mActivity));
                requestBody = requestBody.replaceAll("_sessionId_",
                        Utils.getSessionId(mActivity));

                requestBody = requestBody.replaceAll("_osType_",
                        WebserviceUtils.OS_TYPE);
                requestBody = requestBody.replaceAll("_osVersion_",
                        Utils.getOsVersion());
                requestBody = requestBody.replaceAll("_ipAddress_",
                        WebserviceUtils.getExternalIP());

                requestBody = requestBody
                        .replaceAll("_id_", WebserviceUtils.ID);

                Log.e("peachpass", requestBody + "$$$$$$$$");
                String result = WebserviceUtils
                        .sendSoapRequest(
                                mActivity,
                                requestBody,
                                WebserviceUtils.SOAP_ACTION_GET_ACCOUNT_VEHICLE_TOLL_MODE);
                XMLParser parser = new XMLParser();
                String xml = result;
                Document doc = parser.getDomElement(xml); // getting DOM
                // element
                NodeList nl = doc.getElementsByTagName("result");
                Element e = (Element) nl.item(0);
                nl = doc.getElementsByTagName("ns1:vehicleTollModeList");

                for (int i = 0; i < nl.getLength(); i++) {

                    e = (Element) nl.item(i);

                    String vehicleId_ = parser.getValue(e, "ns1:vehicleId");
                    String licPlate = parser.getValue(e, "ns1:licPlate");
                    String make = parser.getValue(e, "ns1:make");
                    String model = parser.getValue(e, "ns1:model");
                    String color = parser.getValue(e, "ns1:color");
                    String year = parser.getValue(e, "ns1:year");
                    String tollMode = parser.getValue(e, "ns1:tollMode");
                    String travelEndDate = parser.getValue(e,
                            "ns1:travelEndDate");

                    VehicleData vehicleData = new VehicleData(vehicleId_,
                            licPlate, make, model, color, year, tollMode,
                            travelEndDate);
                    vechileArray.add(vehicleData);

                }
                return vechileArray;
            }

            @Override
            protected void onPostExecute(ArrayList<MyPeachPassData> result) {
                // TODO Auto-generated method stub
                try {
                    pdialog.dismiss();
                } catch (Exception e) {
                    // TODO: handle exception
                }
                try {
                    Cache.fromBack = false;
                    if (result != null) {

                        if (result.size() > 0) {

                            ll_no_alert_screen.setVisibility(View.GONE);
                            lv.setVisibility(View.VISIBLE);
                            ll_no_internet.setVisibility(View.GONE);

                            adapter.insertList(result);
                            adapter.notifyDataSetChanged();

                            processThisDataForAlertCount(result);


                        } else {
                            ll_no_alert_screen.setVisibility(View.VISIBLE);
                            lv.setVisibility(View.GONE);
                            ll_no_internet.setVisibility(View.GONE);
//							ScreenUtils.raiseToast(mActivity,
//									WebserviceUtils.NO_DATA_DISPLAY);
                        }

                    } else {
                        ll_no_alert_screen.setVisibility(View.GONE);
                        lv.setVisibility(View.GONE);
                        ll_no_internet.setVisibility(View.VISIBLE);
//						ScreenUtils.raiseToast(mActivity,
//								WebserviceUtils.SOMETHING_WENT_WRONG);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    ll_no_alert_screen.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                    ll_no_internet.setVisibility(View.VISIBLE);
//					ScreenUtils.raiseToast(mActivity,
//							WebserviceUtils.SOMETHING_WENT_WRONG);
                }

                super.onPostExecute(result);
            }

            private void processThisDataForAlertCount(
                    ArrayList<MyPeachPassData> result) {
                // TODO Auto-generated method stub

                Set<String> dummySet = new HashSet<String>();
                Set<String> notificationSet = SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.ALERTS, mActivity);
                int count = 0;
                for (MyPeachPassData obj : result) {
                    if (isPresentInSet(notificationSet, obj)) {
                        //ignore
                        obj.setRead(true);
                    } else {
                        dummySet.add(obj.getTransactionId());
                        obj.setRead(false);
                        count++;
                    }
                }
                notificationSet.addAll(dummySet);
                //SharedPreferenceHelper.savePreferencesSet(SharedPreferenceHelper.ALERTS, notificationSet, mActivity);
                Log.e("peachpass", count + "--alertcount");
                SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADALERTS, count + "", mActivity);

                sendBroadcastToLeftMenu();

            }

            boolean isPresentInSet(Set<String> notificationSet, MyPeachPassData obj) {

                if (notificationSet.contains(obj.getTransactionId()))
                    return true;
                return false;
//				for(String prefSet:notificationSet){
//					String sliptArray[] = prefSet.split("\\@");
//					String date = sliptArray[0];
//					if(obj.getTransactionId().equals(date)){
//						return true;
//					}
//				}return false;
            }
        }.execute();
    }

    public void addItem(MyPeachPassData item) {
        if (adapter != null)
            adapter.addItem(item);
    }

    public void clearList() {
        if (adapter != null)
            adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

        setRetainInstance(true);
    }

    private void sendBroadcastToLeftMenu() {
        // TODO Auto-generated method stub

        Intent intent = new Intent("custom-event-name");
        intent.putExtra("message", "updatealert");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(getActivity());

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.ALERTS_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(getActivity());

        FlurryAgent.onEndSession(context);
    }


}
