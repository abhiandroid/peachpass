package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.utils.ScreenUtils;
import com.srta.PeachPass.R;

public class EditVechileFragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";
    Context context;
    int imageSource;

    @SuppressLint("ValidFragment")
    public EditVechileFragment(int imageSource) {
        this.imageSource = imageSource;

    }

    public EditVechileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if ((savedInstanceState != null)
                && savedInstanceState.containsKey(KEY_CONTENT)) {
            imageSource = savedInstanceState.getInt(KEY_CONTENT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.edit_vehicle,
                null);

        RelativeLayout image = (RelativeLayout) root
                .findViewById(R.id.rel_edit_vehicle);
        image.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /** Cross button click event */
                getFragmentManager().popBackStack();
//				if (getActivity() != null)
//					if (getActivity() instanceof FragmentChangeActivity) {
//						FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
//						fca.switchContent(new MyProfileFragment(R.color.red),
//								"My Profile", null, true);
//					}
            }
        });

        TextView txt_create_account_next5 = (TextView) root
                .findViewById(R.id.txt_create_account_next5);
        txt_create_account_next5.setTypeface(ScreenUtils
                .returnTypeFace(getActivity()));
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CONTENT, imageSource);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(getActivity());

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(getActivity());

        FlurryAgent.onEndSession(context);
    }

}
