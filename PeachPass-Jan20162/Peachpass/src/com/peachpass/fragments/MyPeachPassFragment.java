package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.adapter.ItemListAdapter;
import com.peachpass.data.FragmentTitle;
import com.peachpass.data.GetAccountInfoByAccountIdData;
import com.peachpass.data.MyPeachPassData;
import com.peachpass.data.ViewPersonalInfoData;
import com.peachpass.utils.Cache;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class MyPeachPassFragment extends Fragment {

    ItemListAdapter adapter;

    TextView txt_fa_crd_card, txt_fa_my_profile, txt_account_balance,
            txt_account_id, txt_full_name;
    ImageView img_mpp_tollmode;

    Activity mActivity;
    ListView listView_mypeachpass;

    LinearLayout ll_no_internet, ll_no_data;
    View view_no_internet;
    Context context;
    String county="";

    public MyPeachPassFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public MyPeachPassFragment(int colorRes) {
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        // int color = getResources().getColor(mColorRes);
        adapter = new ItemListAdapter(mActivity);
        Log.e("peachpass", "onCreateView MyPeachPassFragment");

        /** Dummy values added here */
//		MyPeachPassData items[] = {
//				new MyPeachPassData("JULY", "30", "I-85 North - BEF0922",
//						"85A - JC02 - 21 | 8:23 am", " - $2.00"),
//				new MyPeachPassData("JULY", "28", "I-85 North - PLATE#",
//						"85A - CP08 - 21 | 9:33 am", " - $2.00"),
//				new MyPeachPassData("JULY", "24", "I-85 North - BEF0922",
//						"85A - JC02 - 21 | 10:43 am", " - $2.00"),
//				new MyPeachPassData("JULY", "23", "I-85 North - BEF0922",
//						"85A - CP08 - 21 | 11:53 am", " - $2.00"),
//				new MyPeachPassData("JULY", "22", "I-85 North - BEF0922",
//						"85A - JC02 - 21 | 12:23 pm", " - $2.00"),
//				new MyPeachPassData("JULY", "21", "I-85 North - BEF0922",
//						"85A - CP08 - 21 | 13:23 90", " - $2.00"),
//				new MyPeachPassData("JULY", "24", "I-85 North - BEF0922",
//						"85A - JC02 - 21 | 10:43 am", " - $2.00"),
//				new MyPeachPassData("JULY", "23", "I-85 North - BEF0922",
//						"85A - CP08 - 21 | 11:53 am", " - $2.00"),
//				new MyPeachPassData("JULY", "22", "I-85 North - BEF0922",
//						"85A - JC02 - 21 | 12:23 pm", " - $2.00"),
//				new MyPeachPassData("JULY", "21", "I-85 North - BEF0922",
//						"85A - CP08 - 21 | 13:23 90", " - $2.00") };
//		for (MyPeachPassData temp : items)
//			adapter.addItem(temp);

        View view = inflater
                .inflate(R.layout.mypeachpass_alt, container, false);
        Typeface font = ScreenUtils.returnTypeFace(getActivity());

        txt_fa_crd_card = (TextView) view.findViewById(R.id.txt_fa_crd_card);
        txt_fa_crd_card.setTypeface(font);

        txt_fa_my_profile = (TextView) view
                .findViewById(R.id.txt_fa_my_profile);
        txt_fa_my_profile.setTypeface(font);

        txt_account_balance = (TextView) view
                .findViewById(R.id.txt_account_balance);
        txt_account_id = (TextView) view.findViewById(R.id.txt_account_id);
        txt_full_name = (TextView) view.findViewById(R.id.txt_full_name);

        TextView txt_fa_car = (TextView) view.findViewById(R.id.txt_fa_car);
        txt_fa_car.setTypeface(font);

        TextView txt_mpp_bell1 = (TextView) view
                .findViewById(R.id.txt_mpp_bell1);
        txt_mpp_bell1.setTypeface(font);

        TextView txt_mpp_bell2 = (TextView) view
                .findViewById(R.id.txt_mpp_bell2);
        txt_mpp_bell2.setTypeface(font);

        img_mpp_tollmode = (ImageView) view.findViewById(R.id.img_mpp_tollmode);
        listView_mypeachpass = (ListView) view.findViewById(R.id.listView_mypeachpass);
        // lv.setEnabled(false);
        listView_mypeachpass.setAdapter(adapter);

        // fillComments(items, ll_list_items, pixels);
        ll_no_internet = (LinearLayout) view.findViewById(R.id.ll_no_internet);
        ll_no_data = (LinearLayout) view.findViewById(R.id.ll_no_data);
        view_no_internet = (View) view
                .findViewById(R.id.view_no_internet);
        Button refresh = (Button) view_no_internet.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getAccountInfoByAccountId();
            }
        });

        /** click event to visit the Transaction screen */
        txt_fa_crd_card.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (getActivity() != null)
                    if (getActivity() instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
                        fca.switchContent(
                                new TransactionsFragment(R.color.red),
                                "TRANSACTIONS", null, true, false);
                        FragmentTitle fragTitle = new FragmentTitle("MY PEACH PASS", true);
                        Cache.getInstance().getFragmentTitle().add(fragTitle);
                    }
            }
        });

        /** click event to visit the My Profile screen */
        txt_fa_my_profile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (getActivity() != null)
                    if (getActivity() instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
                        fca.switchContent(new MyProfileFragment(R.color.red),
                                "MY PROFILE", null, true, false);
                        FragmentTitle fragTitle = new FragmentTitle("MY PEACH PASS", true);
                        Cache.getInstance().getFragmentTitle().add(fragTitle);
                    }
            }
        });

        /** click event to visit the Change Toll Mode screen */
        img_mpp_tollmode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (getActivity() != null)
                    try {
                        if (getActivity() instanceof FragmentChangeActivity) {
                            FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
                            fca.switchContent(new ChangeTollModeFragment(
                                    R.color.red), "CHANGE TOLL MODE", null, true, false);
                            FragmentTitle fragTitle = new FragmentTitle("MY PEACH PASS", true);
                            Cache.getInstance().getFragmentTitle().add(fragTitle);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

            }
        });

        if (savedInstanceState == null)
            getAccountInfoByAccountId();


        Utils.setTracker(Utils.MYPEACHPASS_VIEW);
        return view;
    }

    @SuppressLint("StaticFieldLeak")
    private void getAccountInfoByAccountId() {
        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(getActivity())) {
            if (!Cache.fromBack)
                new AsyncTask<String, String, GetAccountInfoByAccountIdData>() {

                    String status = "";
                    ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                            mActivity,
                            WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                            false);

                    @Override
                    protected void onPreExecute() {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.show();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        super.onPreExecute();
                    }

                    @Override
                    protected GetAccountInfoByAccountIdData doInBackground(
                            String... params) {
                        // TODO Auto-generated method stub

                        try {

                            String requestBody = WebserviceUtils.readFromAsset(
                                    "getAccountInfoByAccountId", mActivity);
                            requestBody = requestBody.replaceAll("_accountId_",
                                    Utils.getAccountId(mActivity));
                            requestBody = requestBody.replaceAll("_userName_",
                                    Utils.getUsername(mActivity));
                            requestBody = requestBody.replaceAll("_sessionId_",
                                    Utils.getSessionId(mActivity));

                            requestBody = requestBody.replaceAll("_osType_",
                                    WebserviceUtils.OS_TYPE);
                            requestBody = requestBody.replaceAll("_osVersion_",
                                    Utils.getOsVersion());
                            requestBody = requestBody.replaceAll("_ipAddress_",
                                    WebserviceUtils.getExternalIP());

                            requestBody = requestBody.replaceAll("_id_",
                                    WebserviceUtils.ID);

                            Log.e("peachpass", requestBody + "");
                            String result = WebserviceUtils
                                    .sendSoapRequest(
                                            mActivity,
                                            requestBody,
                                            WebserviceUtils.SOAP_ACTION_GETACCOUNTINFOBYACCOUNTID);
                            XMLParser parser = new XMLParser();
                            String xml = result;
                            Document doc = parser.getDomElement(xml); // getting DOM
                            // element

                            NodeList nl = doc.getElementsByTagName("result");

                            for (int i = 0; i < nl.getLength(); ) {

                                Element e = (Element) nl.item(i);

                                status = parser.getValue(e, "ns1:status");
                                String criticalNotificationsFlag = parser.getValue(
                                        e, "ns1:criticalNotificationsFlag");
                                String lowAccountBalanceLevel = parser.getValue(e,
                                        "ns1:lowAccountBalanceLevel");
                                String isNonTollMode = parser.getValue(e,
                                        "ns1:isNonTollMode");
                                String pin = parser.getValue(e, "ns1:pin");
                                String originalId = parser.getValue(e,
                                        "ns1:originalId");
                                String isShortTermAccount = parser.getValue(e,
                                        "ns1:isShortTermAccount");
                                String primaryEmailAddress = parser.getValue(e,
                                        "ns1:primaryEmailAddress");
                                String minimumReBillAmount = parser.getValue(e,
                                        "ns1:minimumReBillAmount");
                                String lastPaymentDate = parser.getValue(e,
                                        "ns1:lastPaymentDate");
                                String accountBalance = parser.getValue(e,
                                        "ns1:accountBalance");
                                String defaultLowBalance = parser.getValue(e,
                                        "ns1:defaultLowBalance");
                                String lastPaymentType = parser.getValue(e,
                                        "ns1:lastPaymentType");
                                String accountStatusId = parser.getValue(e,
                                        "ns1:accountStatusId");
                                String tagIds = parser.getValue(e, "ns1:tagIds");
                                String autoReplenishFlag = parser.getValue(e,
                                        "ns1:autoReplenishFlag");
                                String accountTypeId = parser.getValue(e,
                                        "ns1:accountTypeId");
                                String rebillAmountPerVehicle = parser.getValue(e,
                                        "ns1:rebillAmountPerVehicle");
                                String lastPaymentAmount = parser.getValue(e,
                                        "ns1:lastPaymentAmount");
                                String rebillAmount = parser.getValue(e,
                                        "<ns1:rebillAmount");
                                String status = parser.getValue(e, "ns1:status");

                                GetAccountInfoByAccountIdData getAcntInfAccntIdData = new GetAccountInfoByAccountIdData(
                                        criticalNotificationsFlag,
                                        lowAccountBalanceLevel, isNonTollMode, pin,
                                        originalId, isShortTermAccount,
                                        primaryEmailAddress, minimumReBillAmount,
                                        lastPaymentDate, accountBalance,
                                        defaultLowBalance, lastPaymentType,
                                        accountStatusId, tagIds, autoReplenishFlag,
                                        accountTypeId, rebillAmountPerVehicle,
                                        lastPaymentAmount, rebillAmount, status);

                                return getAcntInfAccntIdData;
                            }

                        } catch (Exception e) {
                            // TODO: handle exception

                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(
                            GetAccountInfoByAccountIdData result) {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.dismiss();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {
                            if (result != null) {

                                if (result.getStatus().equals("0")) {

                                    SharedPreferenceHelper.savePreferences(
                                            SharedPreferenceHelper.ACCOUNTBALANCE,
                                            result.getAccountBalance(), mActivity);
                                    SharedPreferenceHelper
                                            .savePreferences(
                                                    SharedPreferenceHelper.PRIMARY_EMAIL_ID,
                                                    result.getPrimaryEmailAddress(),
                                                    mActivity);

                                    txt_account_balance.setText("$"
                                            + result.getAccountBalance());
                                    txt_account_id.setText("Account ID: "
                                            + Utils.getAccountId(mActivity));

                                } else if (status != null && !status.equals("0")) {
                                    String errorStatus = Utils
                                            .getStringResourceByName("status_"
                                                    + status, mActivity);
                                    ScreenUtils.raiseToast(mActivity, errorStatus);
                                } else {

                                    ScreenUtils.raiseToast(mActivity,
                                            WebserviceUtils.SESSION_EXPIRED);
                                }

                            } else {

                                ScreenUtils.raiseToast(mActivity,
                                        WebserviceUtils.SOMETHING_WENT_WRONG);
                            }

                        } catch (Exception e) {
                            // TODO: handle exception
                            ScreenUtils.raiseToast(mActivity,
                                    WebserviceUtils.SOMETHING_WENT_WRONG);
                        }

                        super.onPostExecute(result);
                    }
                }.execute();


            if (!Cache.fromBack)
                new AsyncTask<String, String, ViewPersonalInfoData>() {
                    ProgressDialog pdialog = ScreenUtils
                            .returnProgDialogObj(
                                    mActivity,
                                    WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                                    false);
                    String status = "";

                    @Override
                    protected void onPreExecute() {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.show();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        super.onPreExecute();
                    }

                    @Override
                    protected ViewPersonalInfoData doInBackground(String... params) {
                        // TODO Auto-generated method stub

                        try {

                            String requestBody = WebserviceUtils.readFromAsset("viewPersonalInfo", mActivity);
                            requestBody = requestBody.replaceAll("_accountId_", Utils.getAccountId(mActivity));
                            requestBody = requestBody.replaceAll("_userName_", Utils.getUsername(mActivity));
                            requestBody = requestBody.replaceAll("_sessionId_", Utils.getSessionId(mActivity));


                            requestBody = requestBody.replaceAll("_osType_", WebserviceUtils.OS_TYPE);
                            requestBody = requestBody.replaceAll("_osVersion_", Utils.getOsVersion());
                            requestBody = requestBody.replaceAll("_ipAddress_", WebserviceUtils.getExternalIP());

                            requestBody = requestBody.replaceAll("_id_", WebserviceUtils.ID);

                            Log.e("peachpass", requestBody + "");
                            String result = WebserviceUtils.sendSoapRequest(mActivity, requestBody, WebserviceUtils.SOAP_ACTION_VIEWPERSONALINFO);
                            XMLParser parser = new XMLParser();
                            String xml = result;
                            Document doc = parser.getDomElement(xml); // getting DOM element

                            NodeList nl = doc.getElementsByTagName("result");

                            for (int i = 0; i < nl.getLength(); ) {

                                Element e = (Element) nl.item(i);

                                status = parser.getValue(e, "ns1:status");
                                String firstName = parser.getValue(e, "ns1:firstName");
                                String lastName = parser.getValue(e, "ns1:lastName");
                                String email = parser.getValue(e, "ns1:email");
                                String address = parser.getValue(e, "ns1:address");
                                String city = parser.getValue(e, "ns1:city");
                                String state = parser.getValue(e, "ns1:state");
                                String zip = parser.getValue(e, "ns1:zip");
                                county = parser.getValue(e, "ns1:county");
                                Log.e("MyProfile","County-->"+county);
                                String phone = parser.getValue(e, "ns1:phone");
                                String driverLisence = parser.getValue(e, "ns1:driverLisence");
                                String driverLisenceState = parser.getValue(e, "ns1:driverLisenceState");

                                ViewPersonalInfoData viewPersonalInfoData = new ViewPersonalInfoData(firstName, lastName, email, address, city, state, zip, county, phone, driverLisence, driverLisenceState, status);

                                Log.e("ProfileInfo-- "+viewPersonalInfoData,"Data");
                                return viewPersonalInfoData;
                            }

                        } catch (Exception e) {
                            // TODO: handle exception

                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(ViewPersonalInfoData result) {
                        // TODO Auto-generated method stub
                        try {
                            pdialog.dismiss();
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {
                            if (result != null) {

                                if (result.getStatus().equals("0")) {
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.FIRST_NAME, result.getFirstName(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.LAST_NAME, result.getLastName(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.EMAIL, result.getEmail(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.ADDRESS, result.getAddress(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.CITY, result.getCity(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.STATE, result.getState(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.ZIP, result.getZip(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.CountyValue,county , mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.PHONE, result.getPhone(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DRIVER_LISENCE, result.getDriverLisence(), mActivity);
                                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DRIVER_LISENCE_STATE, result.getDriverLisenceState(), mActivity);

                                    Log.e("MyProfile","Result.GETCounty-->"+result.getCounty());
                                    txt_full_name.setText(Utils.firstCapitalName(result.getFirstName()) + " " + Utils.firstCapitalName(result.getLastName()));
                                    //txt_account_id.setText("Account ID: "+Utils.getAccountId(mActivity));
                                    Intent intent = new Intent("custom-event-name");
////							  // You can also include some extra data.
                                    intent.putExtra("message", "name");
                                    LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);

                                } else if (status != null && !status.equals("0")) {
                                    String errorStatus = Utils
                                            .getStringResourceByName("status_"
                                                    + status, mActivity);
                                    ScreenUtils.raiseToast(mActivity, errorStatus);
                                } else {

                                    ScreenUtils.raiseToast(mActivity, WebserviceUtils.SESSION_EXPIRED);
                                }

                            } else {

                                ScreenUtils.raiseToast(mActivity, WebserviceUtils.SOMETHING_WENT_WRONG);
                            }


                        } catch (Exception e) {
                            // TODO: handle exception
                            ScreenUtils.raiseToast(mActivity, WebserviceUtils.SOMETHING_WENT_WRONG);
                        }

                        super.onPostExecute(result);
                    }
                }.execute();


            new AsyncTask<String, String, ArrayList<MyPeachPassData>>() {
                ProgressDialog pdialog = ScreenUtils
                        .returnProgDialogObj(
                                mActivity,
                                WebserviceUtils.PROGRESS_MESSAGE_FETCHING_TRANSACTIONS,
                                false);

                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    super.onPreExecute();
                }

                @Override
                protected ArrayList<MyPeachPassData> doInBackground(String... params) {
                    // TODO Auto-generated method stub
                    //Log.e("peachpass", "Cache.fromBack-"+Cache.fromBack);
                    //Log.e("peachpass", "Cache.getInstance().getMyPeachPassdataList()-"+Cache.getInstance().getMyPeachPassdataList());
                    try {
                        ArrayList<MyPeachPassData> myPeachPassdataList = new ArrayList<MyPeachPassData>();
                        if (Cache.fromBack) {
                            if (Cache.getInstance().getMyPeachPassdataList() != null && Cache.getInstance().getMyPeachPassdataList().size() > 0)
                                myPeachPassdataList = Cache.getInstance().getMyPeachPassdataList();
                            //Log.e("peachpass", "return data here");
                            return myPeachPassdataList;
                        } else {
                            myPeachPassdataList = new ArrayList<MyPeachPassData>();
                            //Log.e("peachpass", "inside else");
                        }


                        String requestBody = WebserviceUtils.readFromAsset("getTop10TollTransactions", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_", Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_", Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_", Utils.getSessionId(mActivity));


                        requestBody = requestBody.replaceAll("_osType_", WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_", Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_", WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_", WebserviceUtils.ID);

                        String result = WebserviceUtils.sendSoapRequest(mActivity, requestBody, WebserviceUtils.SOAP_ACTION_GETTOP10TOLLTRANSACTIONS);
                        if (result == null)
                            return null;
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM element

                        NodeList nl = doc.getElementsByTagName("ns1:listOfTrxns");

                        for (int i = 0; i < nl.getLength(); i++) {

                            Element e = (Element) nl.item(i);

                            String transactionDate = parser.getValue(e, "ns1:transactionDate");
                            String licensePlate = parser.getValue(e, "ns1:licensePlate");
                            String lane = parser.getValue(e, "ns1:lane");
                            String dir = parser.getValue(e, "ns1:dir");
                            String amount = parser.getValue(e, "ns1:amount");
                            String month = "", day = "", time = "";
                            String laneString = "I-85";
                            Log.e("peachpass", amount + "---amount");

                            try {
                                String timeDetails[] = Utils.parseDate(transactionDate, Utils.DATE_PATTERN_SOAP, Utils.DATE_PATTERN_MY_PEACH_PASS).split("\\,");
                                month = timeDetails[0];
                                day = timeDetails[1];
                                time = timeDetails[2];

                            } catch (Exception e2) {
                                // TODO: handle exception
                            }

                            if (lane.contains("85A")) {
                        /*    data = new MyPeachPassData(month, day,
                                    "I-85", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass--1", data.toString());*/

                                laneString = "I-85";
                            }
                            if (lane.contains("85B")) {
                        /*    data = new MyPeachPassData(month, day,
                                    "I-85", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass--1", data.toString());*/

                                laneString = "I-85B";
                            }
                            if (lane.contains("75A")) {
                       /*      data = new MyPeachPassData(month, day,
                                    "I-75", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass---2", data.toString());*/
                                laneString = "I-75";
                            }
                            if (lane.contains("75B")) {
                       /*      data = new MyPeachPassData(month, day,
                                    "I-75", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass---2", data.toString());*/
                                laneString = "NWC";
                            }

                            MyPeachPassData data = new MyPeachPassData(month, day,
                                    laneString, dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass", data.toString());
                            myPeachPassdataList.add(data);
                            // adapter.addItem(new MyPeachPassData(month, day,
                            // "I-85", dir, licensePlate, lane, time, amount));
                        }
                        //Cache.getInstance().setMyPeachPassdataList(myPeachPassdataList);
                        return myPeachPassdataList;

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(ArrayList<MyPeachPassData> result) {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        Cache.fromBack = false;
                        if (result != null) {

                            if (result.size() > 0) {
                                handleVisibility(new int[]{View.VISIBLE, View.GONE, View.GONE});
                                adapter.clearItems();
                                adapter.insertList(result);
                                adapter.notifyDataSetChanged();

                            } else {
                                handleVisibility(new int[]{View.GONE, View.GONE, View.VISIBLE});
                                //ScreenUtils.raiseToast(mActivity, WebserviceUtils.NO_DATA_DISPLAY);
                            }

                        } else {
                            handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
                            //ScreenUtils.raiseToast(mActivity, WebserviceUtils.SOMETHING_WENT_WRONG);
                        }


                    } catch (Exception e) {
                        // TODO: handle exception
                        handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
                        //ScreenUtils.raiseToast(mActivity, WebserviceUtils.SOMETHING_WENT_WRONG);
                    }

                    super.onPostExecute(result);
                }
            }.execute();

        } else {
            handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
            ScreenUtils.raiseToast(mActivity,
                    WebserviceUtils.NO_INTERNET_CONNECTION);
        }

    }

//	private void fillComments(MyPeachPassData[] comments, LinearLayout ll) {
//		View comment;
//		TextView txt_mpp_month;
//		TextView txt_mpp_date;
//		TextView txt_mpp_line1, txt_mpp_line2, txt_mpp_currency;
//		LayoutInflater inflater = getActivity().getLayoutInflater();
//
//		for (MyPeachPassData s : comments) {
//			comment = inflater.inflate(R.layout.list_item_mypeachpass, null);
//
//			comment.setLayoutParams(new LinearLayout.LayoutParams(
//					LayoutParams.MATCH_PARENT, (int) getResources()
//							.getDimension(R.dimen.ll_mypeachpass_row_height)));
//
//			txt_mpp_month = (TextView) comment.findViewById(R.id.txt_mpp_month);
//			txt_mpp_date = (TextView) comment.findViewById(R.id.txt_mpp_date);
//			txt_mpp_line1 = (TextView) comment.findViewById(R.id.txt_mpp_line1);
//			txt_mpp_line2 = (TextView) comment.findViewById(R.id.txt_mpp_line2);
//			txt_mpp_currency = (TextView) comment
//					.findViewById(R.id.txt_mpp_currency);
//
//			txt_mpp_date.setText(s.month);
//			txt_mpp_month.setText(s.day);
//			txt_mpp_line1.setText(s.line1);
//			txt_mpp_line2.setText(s.line2);
//			txt_mpp_currency.setText(s.currency);
//
//			ll.addView(comment);
//		}
//	}

    public void addItem(MyPeachPassData item) {
        if (adapter != null)
            adapter.addItem(item);
    }

    public void clearList() {
        if (adapter != null)
            adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", 1);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

        setRetainInstance(true);
    }

    /**
     * List, No internet and No data
     */
    void handleVisibility(int[] values) {
        listView_mypeachpass.setVisibility(values[0]);
        ll_no_internet.setVisibility(values[1]);
        ll_no_data.setVisibility(values[2]);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.MYPEACHPASS_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}
