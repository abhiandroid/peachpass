package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peachpass.utils.ScreenUtils;
import com.srta.PeachPass.R;

public class ContactUsFragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";

    int imageSource;
    Context context;

    @SuppressLint("ValidFragment")
    public ContactUsFragment(int imageSource) {
        this.imageSource = imageSource;

    }

    public ContactUsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null)
                && savedInstanceState.containsKey(KEY_CONTENT)) {
            imageSource = savedInstanceState.getInt(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.contact_us_fragment, null);
        LinearLayout ll_contact_us = (LinearLayout) root.findViewById(R.id.ll_contact_us);
        ll_contact_us.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });
        Typeface font = ScreenUtils.returnTypeFace(getActivity());
        TextView txt_create_account_next3 = (TextView) root.findViewById(R.id.txt_create_account_next3);
        txt_create_account_next3.setTypeface(font);

        LinearLayout ll_cross = (LinearLayout) root.findViewById(R.id.ll_cross);
        ll_cross.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });


        setRetainInstance(true);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CONTENT, imageSource);
    }

}
