package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FaqFragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";

    int imageSource;
    Activity mActivity;
    Context context;

    @SuppressLint("ValidFragment")
    public FaqFragment(int imageSource) {
        this.imageSource = imageSource;

    }

    public FaqFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if ((savedInstanceState != null)
                && savedInstanceState.containsKey(KEY_CONTENT)) {
            imageSource = savedInstanceState.getInt(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(
                R.layout.faq_fragment_layout, null);

        //View layout_faq1 = root.findViewById(R.id.layout_faq1);
        View layout_faq2 = root.findViewById(R.id.layout_faq2);
        View layout_faq4 = root.findViewById(R.id.layout_faq4);
        //TextView txt_faq1 = (TextView) layout_faq1.findViewById(R.id.txt_faq1);
        TextView txt_faq2 = (TextView) layout_faq2.findViewById(R.id.txt_faq2);
        WebView webview = (WebView) layout_faq4.findViewById(R.id.txt_faq4);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadDataWithBaseURL(null, "<html>You can contact our Customer Service Center at <u><a href=\"tel:18557247277\">1-855-PCH-PASS(724-7277)</a></u> or email us at <a href=\"mailto:customerservice@georgiatolls.com\">customerservice@georgiatolls.com</a> .</html>", "text/html", "utf-8", null);


        //setUrlToTextView(txt_faq1, 0);
        //setUrlToTextView(txt_faq2, 0);

        //setUrlToTextView(txt_faq4, 1);
        txt_faq2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Log.e("peachpass", "$$urlToOpen--");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:1-855-PCH-PASS(724-7277)"));
                onClickList(intent, 1);
            }
        });
        //txt_faq2.setText(Html.fromHtml(mActivity.getResources().getString(R.string.q7)));
        setRetainInstance(true);

        Utils.setTracker(Utils.FAQ_VIEW);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CONTENT, imageSource);
    }

    private void setUrlToTextView(TextView textView, int type) {

        textView.setMovementMethod(LinkMovementMethod.getInstance());
        String content = textView.getText().toString();
        List<String> links = new ArrayList<String>();

        Pattern p = Patterns.WEB_URL;
        Pattern ph = Patterns.PHONE;
        Matcher m = p.matcher(content);

        while (m.find()) {
            String urlStr = m.group();
            links.add(urlStr);
        }

        if (type == 1) {
            m = ph.matcher(content);
            while (m.find()) {
                String urlStr = m.group();
                links.add(urlStr);
            }
        }

        SpannableString f = new SpannableString(content);

        for (int i = 0; i < links.size(); i++) {
            final String url = links.get(i);

            f.setSpan(
                    new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            int type = 0;
                            // TODO Auto-generated method stub
                            // Context ctx = v.getContext();
                            String urlToOpen = url;
//							if (!urlToOpen.startsWith("http://")
//									|| !urlToOpen.startsWith("https://"))
//								if(!Utils.isNumeric(urlToOpen))
//								urlToOpen = "http://" + urlToOpen;
                            // openURLInBrowser(urlToOpen, ctx);
                            Log.e("peachpass", "urlToOpen--" + urlToOpen);
                            if (urlToOpen.contains("7277") || urlToOpen.contains("855")) {
                                urlToOpen = "tel:" + "18557247277";
                                type = 1;
                            }
                            String intentType = "";
                            if (urlToOpen.contains("7277") || urlToOpen.contains("855")) {
                                intentType = Intent.ACTION_DIAL;
                                Intent browserIntent = new Intent(
                                        intentType,
                                        Uri.parse(urlToOpen));
                                onClickList(browserIntent, type);
                            } else {

                                email(mActivity, "customerservice@georgiatolls.com", "", "");
//								Uri uri = Uri.parse("mailto:" + "customerservice@georgiatolls.com")
//									    .buildUpon()
//									    .appendQueryParameter("subject", "")
//									    .appendQueryParameter("body", "")
//									    .appendQueryParameter("address", "customerservice@georgiatolls.com")
//									    .build();
//								Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
//								startActivity(Intent.createChooser(emailIntent, ""));
                            }


                        }
                    }, content.indexOf(url),
                    content.indexOf(url) + url.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setText(f);
    }


    private void onClickList(final Intent intent, int type) {
        String message = "";
        if (type == 0) {
            message = "Would you like to launch the browser app?";
        }
        if (type == 1) {
            message = "Would you like to launch the phone app?";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                startActivity(intent);
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    void email(Context context, String to, String subject, String body) {
        StringBuilder builder = new StringBuilder("mailto:" + Uri.encode(to));
        if (subject != null) {
            builder.append("?subject=" + Uri.encode(Uri.encode(subject)));
            if (body != null) {
                builder.append("&body=" + Uri.encode(Uri.encode(body)));
            }
        }
        String uri = builder.toString();
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
        context.startActivity(intent);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.FAQ_VIEW);

    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }
}
