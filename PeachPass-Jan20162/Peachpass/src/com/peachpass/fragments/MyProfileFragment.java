package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.AddCreditCardsActivity;
import com.peachpass.activity.AddVehicleActivity;
import com.peachpass.activity.EditProfileActivity;
import com.peachpass.activity.EditSettingsActivity;
import com.peachpass.activity.EditVehicleActivity;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.activity.UpdateCreditCardsActivity;
import com.peachpass.data.PrimaryBillingData;
import com.peachpass.data.VehicleData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class MyProfileFragment extends Fragment {

    private int mColorRes = -1;
    Context context;
    TextView txt_account_id, txt_balance;
    TextView txt_fullname, txt_address, txt_citystatezip, txt_phoneno,
            txt_emailid;
    LinearLayout linear_vehicledata, linear_billingdata;
    Activity mActivity;

    public static int VEHICLE_EDIT_SUCCESS = 111;
    public static int VEHICLE_ADD_SUCCESS = 222;
    public static int BILLING_EDIT_SUCCESS = 333;
    public static int BILLING_ADD_SUCCESS = 444;

    Typeface font = null;

    ArrayList<String> arrayofLicWithTags = new ArrayList<String>();

    LinearLayout ll_actual_screen, ll_no_internet;
    View view_no_internet;

    public MyProfileFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public MyProfileFragment(int colorRes) {
        mColorRes = colorRes;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");

        font = ScreenUtils.returnTypeFace(mActivity);
        View view = inflater.inflate(R.layout.myprofile, container, false);

        linear_vehicledata = (LinearLayout) view
                .findViewById(R.id.linear_vehicledata);

        linear_billingdata = (LinearLayout) view
                .findViewById(R.id.linear_billingdata);

        TextView txt_fa_scnd = (TextView) view.findViewById(R.id.txt_fa_scnd);
        txt_fa_scnd.setTypeface(font);

        txt_account_id = (TextView) view.findViewById(R.id.txt_account_id);
        txt_account_id.setText("Account ID: " + Utils.getAccountId(mActivity));

        txt_balance = (TextView) view.findViewById(R.id.txt_balance);
        txt_balance.setText("$" + Utils.getAccountBalance(mActivity));

        txt_fullname = (TextView) view.findViewById(R.id.txt_fullname);
        txt_address = (TextView) view.findViewById(R.id.txt_address);
        txt_citystatezip = (TextView) view.findViewById(R.id.txt_citystatezip);
        txt_phoneno = (TextView) view.findViewById(R.id.txt_phoneno);
        txt_emailid = (TextView) view.findViewById(R.id.txt_emailid);

        ll_actual_screen = (LinearLayout) view
                .findViewById(R.id.ll_actual_screen);
        ll_no_internet = (LinearLayout) view.findViewById(R.id.ll_no_internet);
        view_no_internet = (View) view.findViewById(R.id.view_no_internet);
        Button refresh = (Button) view_no_internet.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                handleVisibility(new int[]{View.VISIBLE, View.GONE});
                getVehicleTagInfoList();
                getPrimaryBillingMethod();
                getSecondaryBillingMethod();
            }
        });
        LinearLayout ll_statement = (LinearLayout) view
                .findViewById(R.id.ll_statement);
        LinearLayout ll_notification = (LinearLayout) view
                .findViewById(R.id.ll_notification);
        LinearLayout ll_alert = (LinearLayout) view.findViewById(R.id.ll_alert);

        setPersonalInfo();

        ll_statement.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new StatementFragment(R.color.red),
                                "STATEMENTS", null, true, false);
                    }
            }
        });
        ll_notification.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new MyPeachPassFragment(R.color.red),
                                "NOTIFICATIONS", null, false, false);
                    }
            }
        });

        TextView txt_fa_thrd = (TextView) view.findViewById(R.id.txt_fa_thrd);
        txt_fa_thrd.setTypeface(font);

        ll_alert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new AlertsFragment(R.color.red),
                                "ALERTS", null, true, false);
                    }
            }
        });

        TextView textObjEdit = (TextView) view
                .findViewById(R.id.txt_fa_myprofile_edit);
        textObjEdit.setTypeface(font);

        LinearLayout ll_edit_profile_fragment = (LinearLayout) view
                .findViewById(R.id.ll_edit_profile_fragment);

        ll_edit_profile_fragment.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivityForResult(new Intent(mActivity,
                        EditProfileActivity.class), 11);// change
            }
        });

        LinearLayout ll_add_vehicle = (LinearLayout) view
                .findViewById(R.id.ll_add_vehicle);
        ll_add_vehicle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivityForResult(new Intent(mActivity,
                        AddVehicleActivity.class), VEHICLE_ADD_SUCCESS);
            }
        });

        LinearLayout ll_add_credit = (LinearLayout) view
                .findViewById(R.id.ll_add_credit);
        ll_add_credit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivityForResult(new Intent(mActivity,
                        AddCreditCardsActivity.class), BILLING_ADD_SUCCESS);
            }
        });

        LinearLayout ll_editsettings = (LinearLayout) view
                .findViewById(R.id.ll_editsettings);
        ll_editsettings.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivityForResult(new Intent(mActivity,
                        EditSettingsActivity.class), 0);
            }
        });

        TextView textObj = (TextView) view.findViewById(R.id.txt_mpp_bell1);
        textObj.setTypeface(font);

        TextView textObj_1 = (TextView) view.findViewById(R.id.txt_mpp_bell2);
        textObj_1.setTypeface(font);

        TextView textObj_2 = (TextView) view.findViewById(R.id.txt_mpp_bell3);
        textObj_2.setTypeface(font);

        TextView textObj2 = (TextView) view
                .findViewById(R.id.txt_fa_cmyprofilecar_2);
        textObj2.setTypeface(font);
        TextView textObj3 = (TextView) view
                .findViewById(R.id.txt_fa_cmyprofilecar_3);
        textObj3.setTypeface(font);

		/*
         * TextView txt_mp_bi_star = (TextView) view
		 * .findViewById(R.id.txt_mp_bi_star); txt_mp_bi_star.setTypeface(font);
		 * 
		 * TextView txt_mp_bi_pencil = (TextView) view
		 * .findViewById(R.id.txt_mp_bi_pencil);
		 * txt_mp_bi_pencil.setTypeface(font);
		 */

        getVehicleTagInfoList();
        getPrimaryBillingMethod();
        getSecondaryBillingMethod();

        Utils.setTracker(Utils.MYPROFILE_VIEW);
        return view;
    }

    private void setPersonalInfo() {
        // TODO Auto-generated method stub
        txt_fullname.setText(Utils.firstCapitalName(SharedPreferenceHelper
                .getPreference(mActivity, SharedPreferenceHelper.FIRST_NAME))
                + " "
                + Utils.firstCapitalName(SharedPreferenceHelper.getPreference(
                mActivity, SharedPreferenceHelper.LAST_NAME)));
        txt_address.setText(SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.ADDRESS));
        txt_citystatezip.setText(SharedPreferenceHelper.getPreference(
                mActivity, SharedPreferenceHelper.CITY)
                + ", "
                + SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.STATE)
                + " "
                + SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.ZIP));
        txt_phoneno.setText(SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.PHONE));
        txt_emailid.setText(SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.EMAIL));
    }

    private void getVehicleTagInfoList() {
        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(mActivity))
            new AsyncTask<String, String, ArrayList<VehicleData>>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        mActivity,
                        WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                        false);

                String status = "";

                @Override
                protected void onPreExecute() {
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }

                ;

                @Override
                protected ArrayList<VehicleData> doInBackground(
                        String... params) {

                    arrayofLicWithTags = returnVehicleLicWithTags();
                    ArrayList<VehicleData> vechileArray = new ArrayList<VehicleData>();
                    try {
                        String requestBody = WebserviceUtils.readFromAsset(
                                "getAccountVehicleTollMode", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody + "$$$$$$$$");
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_GET_ACCOUNT_VEHICLE_TOLL_MODE);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        nl = doc.getElementsByTagName("ns1:vehicleTollModeList");

                        for (int i = 0; i < nl.getLength(); i++) {

                            e = (Element) nl.item(i);

                            String vehicleId_ = parser.getValue(e,
                                    "ns1:vehicleId");
                            String licPlate = parser
                                    .getValue(e, "ns1:licPlate");
                            String make = parser.getValue(e, "ns1:make");
                            String model = parser.getValue(e, "ns1:model");
                            String color = parser.getValue(e, "ns1:color");
                            String year = parser.getValue(e, "ns1:year");
                            String tollMode = parser
                                    .getValue(e, "ns1:tollMode");
                            String travelEndDate = parser.getValue(e,
                                    "ns1:travelEndDate");

                            VehicleData vehicleData = new VehicleData(
                                    vehicleId_, licPlate, make, model, color,
                                    year, tollMode, travelEndDate);
                            vechileArray.add(vehicleData);

//							if (i == 0) {
//								SharedPreferenceHelper
//										.savePreferences(
//												SharedPreferenceHelper.DUMMY_DRIVER_LIC,
//												"(GA)" + licPlate, mActivity);
//							}

                            // Log.e("peachpass", vehicleData.toString());
                        }


                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    return vechileArray;
                }

                private ArrayList<String> returnVehicleLicWithTags() {

                    ArrayList<String> vechileArray = new ArrayList<String>();
                    try {
                        String requestBody = WebserviceUtils.readFromAsset(
                                "getVehicleTagInfoList", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody + "$$$$$$$$");
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_GETVEHICLETAGINFOLIST);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        nl = doc.getElementsByTagName("ns1:vehicleTagInfoList");

                        for (int i = 0; i < nl.getLength(); i++) {

                            e = (Element) nl.item(i);
                            String licPlate = parser.getValue(e,
                                    "ns1:licPlate");

                            vechileArray.add(licPlate);
                            if (i == 0) {
                                SharedPreferenceHelper
                                        .savePreferences(
                                                SharedPreferenceHelper.DUMMY_DRIVER_LIC,
                                                "(GA)" + licPlate, mActivity);
                            }
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    return vechileArray;

                }

                @Override
                protected void onPostExecute(ArrayList<VehicleData> result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);

                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    if (result != null) {

                        try {
                            linear_vehicledata.removeAllViews();

                            View vehicleLayout = null;
                            LayoutInflater inflater = mActivity
                                    .getLayoutInflater();

                            for (int i = 0; i < result.size(); i++) {
                                vehicleLayout = inflater.inflate(
                                        R.layout.vehicle_row_my_profile,
                                        linear_vehicledata, false);
                                final VehicleData data = result.get(i);
                                final RelativeLayout linear_vehicleChild = (RelativeLayout) vehicleLayout
                                        .findViewById(R.id.linear_vehicleChild);
                                linear_vehicleChild
                                        .setLayoutParams(new LinearLayout.LayoutParams(
                                                LayoutParams.MATCH_PARENT,
                                                (int) getResources()
                                                        .getDimension(
                                                                R.dimen.ll_50dp)));
                                vehicleLayout.setTag(data);
                                TextView txt_veh_year_make_lic = (TextView) vehicleLayout
                                        .findViewById(R.id.txt_veh_year_make_lic);
                                txt_veh_year_make_lic.setText(data
                                        .getYear_new()
                                        + " "
                                        + data.getModel_new()
                                        + " "
                                        + data.getLicPlate_new());

                                TextView txt_mp_vehicle_1 = (TextView) vehicleLayout
                                        .findViewById(R.id.txt_mp_vehicle_1);
                                txt_mp_vehicle_1.setTypeface(font);
                                if (arrayofLicWithTags.contains(data.getLicPlate_new())) {
                                    txt_veh_year_make_lic.setTextColor(Color.parseColor("#000000"));
                                    txt_veh_year_make_lic.setTypeface(null, Typeface.BOLD);
                                } else {

                                    txt_veh_year_make_lic.setTextColor(Color.parseColor("#808080"));
                                    txt_veh_year_make_lic.setTypeface(null, Typeface.NORMAL);

                                }

                                linear_vehicledata.addView(linear_vehicleChild);

                                linear_vehicleChild
                                        .setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                // TODO Auto-generated method
                                                // stub

                                                if (mActivity != null)
                                                    if (mActivity instanceof FragmentChangeActivity) {

                                                        Intent intent = new Intent(
                                                                mActivity,
                                                                EditVehicleActivity.class);
                                                        intent.putExtra(
                                                                "vehdata", data);
                                                        startActivityForResult(
                                                                intent,
                                                                VEHICLE_EDIT_SUCCESS);
                                                    }

                                            }
                                        });
                            }

                            if (result.size() == 0) {
                                ScreenUtils.raiseToast(mActivity, "No vehicles to display.");
                            } else if (status != null && !status.equals("0")) {
                                String errorStatus = Utils
                                        .getStringResourceByName("status_"
                                                + status, mActivity);
                                ScreenUtils.raiseToast(mActivity, errorStatus);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                        if (!status.equals("0")) {

                            String errorStatus = Utils.getStringResourceByName(
                                    "status_" + status, mActivity);
                            ScreenUtils.raiseToast(mActivity, errorStatus);

                        }

                    } else {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }
                }

            }.execute();
        else {

            handleVisibility(new int[]{View.GONE, View.VISIBLE});

        }
    }

    private void getPrimaryBillingMethod() {
        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(mActivity))
            new AsyncTask<String, String, PrimaryBillingData>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        mActivity,
                        WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                        false);

                String status = "";

                @Override
                protected void onPreExecute() {
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }

                ;

                @Override
                protected PrimaryBillingData doInBackground(String... params) {
                    // TODO Auto-generated method stub
                    try {

                        // ArrayList<PrimaryBillingData> vechileArray = new
                        // ArrayList<PrimaryBillingData>();

                        String requestBody = WebserviceUtils.readFromAsset(
                                "getPrimaryBillingMethod", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody
                                + "-----------BillingData");
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_GETPRIMARYBILLINGMETHOD);
                        Log.e("peachpass", result
                                + "-----------resultBillingData");

                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        nl = doc.getElementsByTagName("ns1:primaryBillingMethod");// ??
                        String tagId = parser.getValue(e, "ns1:tagId");
                        String accountId = parser.getValue(e, "ns1:accountId");
                        String billingMethodId = parser.getValue(e,
                                "ns1:billingMethodId");
                        String ccLastFour = parser
                                .getValue(e, "ns1:ccLastFour");
                        String expDate = parser.getValue(e, "ns1:expDate");
                        String ccType = parser.getValue(e, "ns1:ccType");
                        String isPrimary = parser.getValue(e, "ns1:isPrimary");

                        String cardTypeName = Utils.getCardNamefromId(ccType);
                        // String isAchType = parser.getValue(e,
                        // "ns1:isAchType");

						/*
                         * Log.e("MyProfileFragment", "PrimaryBillingData{" +
						 * "accountId='" + accountId + '\'' +
						 * ", billingMethodId='" + billingMethodId + '\'' +
						 * ", ccLastFour='" + ccLastFour + '\'' + ", expDate='"
						 * + expDate + '\'' + ", ccType='" + ccType + '\'' +
						 * ", isPrimary='" + isPrimary + '\'' + ", tagID='" +
						 * tagId + '\'' + '}');
						 */

                        PrimaryBillingData pbd = new PrimaryBillingData(
                                accountId, billingMethodId, ccLastFour,
                                expDate, cardTypeName, isPrimary);

                        Log.e("PrimaryBillingMethod", pbd.toString() + "++++");
                        return pbd;

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(PrimaryBillingData result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);

                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    if (result == null) {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.NO_DATA_DISPLAY);
                    } else if (status != null && !status.equals("0")) {
                        String errorStatus = Utils.getStringResourceByName(
                                "status_" + status, mActivity);
                        ScreenUtils.raiseToast(mActivity, errorStatus);
                    }
                    if (result != null && status != null && status.equals("0")) {

                        try {
                            // linear_billingdata.removeAllViews();

                            View billingLayout = null;
                            LayoutInflater inflater = mActivity
                                    .getLayoutInflater();

                            // for (int i = 0; i < result.size(); i++) {
                            billingLayout = inflater.inflate(
                                    R.layout.layout_billing_detail,
                                    linear_billingdata, false);

                            final PrimaryBillingData data = result;

                            final RelativeLayout relative_billingChild = (RelativeLayout) billingLayout
                                    .findViewById(R.id.relative_billingChild);
                            relative_billingChild
                                    .setLayoutParams(new LinearLayout.LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            (int) getResources().getDimension(
                                                    R.dimen.ll_50dp)));

                            billingLayout.setTag(data);

                            TextView txt_mp_bi_star = (TextView) billingLayout
                                    .findViewById(R.id.txt_mp_bi_star);
                            txt_mp_bi_star.setTypeface(font);
                            txt_mp_bi_star.setText(R.string.fa_star);

                            TextView txt_mp_bi_detail = (TextView) billingLayout
                                    .findViewById(R.id.txt_mp_bi_detail);
                            txt_mp_bi_detail.setText(result.getCcType()
                                    + " - **** **** **** **** "
                                    + result.getCcLastFour() + "- EXP "
                                    + result.getExpDate());

                            TextView txt_mp_bi_pencil = (TextView) billingLayout
                                    .findViewById(R.id.txt_mp_bi_pencil);
                            txt_mp_bi_pencil.setTypeface(font);
                            // txt_mp_bi_pencil.setText(R.string.fa_pencil);

                            linear_billingdata.addView(relative_billingChild);

                            relative_billingChild
                                    .setOnClickListener(new OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method
                                            // stub

                                            if (mActivity != null)
                                                if (mActivity instanceof FragmentChangeActivity) {

                                                    Intent intent = new Intent(
                                                            mActivity,
                                                            UpdateCreditCardsActivity.class);
                                                    intent.putExtra("billdata",
                                                            data);
                                                    startActivityForResult(
                                                            intent,
                                                            BILLING_EDIT_SUCCESS);
                                                }

                                        }
                                    });
                            // }

                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                        if (!status.equals("0")) {

                            String errorStatus = Utils.getStringResourceByName(
                                    "status_" + status, mActivity);
                            ScreenUtils.raiseToast(mActivity, errorStatus);

                        }

                    } else {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }
                }

            }.execute();
        else {

            handleVisibility(new int[]{View.GONE, View.VISIBLE});

        }
    }

    private void getSecondaryBillingMethod() {
        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(mActivity))
            new AsyncTask<String, String, ArrayList<PrimaryBillingData>>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        mActivity,
                        WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                        false);

                String status = "";

                @Override
                protected void onPreExecute() {
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }

                ;

                @Override
                protected ArrayList<PrimaryBillingData> doInBackground(
                        String... params) {
                    // TODO Auto-generated method stub
                    try {

                        ArrayList<PrimaryBillingData> primaryArray = new ArrayList<PrimaryBillingData>();

                        String requestBody = WebserviceUtils.readFromAsset(
                                "getSecondaryBillingMethod", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody
                                + "-----------requestSecondaryBillingData");
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_GETSECONDARYBILLINGMETHOD);
                        Log.e("peachpass", result
                                + "-----------resultSecondaryBillingData");

                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        nl = doc.getElementsByTagName("ns1:responses");
                        for (int i = 0; i < nl.getLength(); i++) {

                            e = (Element) nl.item(i);

                            String tagId = parser.getValue(e, "ns1:tagId");
                            String accountId = parser.getValue(e,
                                    "ns1:accountId");
                            String billingMethodId = parser.getValue(e,
                                    "ns1:billingMethodId");
                            String ccLastFour = parser.getValue(e,
                                    "ns1:ccLastFour");
                            String expDate = parser.getValue(e, "ns1:expDate");
                            String ccType = parser.getValue(e, "ns1:ccType");
                            String isPrimary = parser.getValue(e,
                                    "ns1:isPrimary");

                            String cardTypeName = Utils
                                    .getCardNamefromId(ccType);

                            PrimaryBillingData pbd = new PrimaryBillingData(
                                    accountId, billingMethodId, ccLastFour,
                                    expDate, cardTypeName, isPrimary);
                            primaryArray.add(pbd);

                            Log.e("peachpass", pbd.toString());
                        }

                        return primaryArray;

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(
                        ArrayList<PrimaryBillingData> result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);

                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    if (result == null) {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.NO_DATA_DISPLAY);
                    } else if (status != null && !status.equals("0")) {
                        String errorStatus = Utils.getStringResourceByName(
                                "status_" + status, mActivity);
                        ScreenUtils.raiseToast(mActivity, errorStatus);
                    }
                    if (result != null && status != null && status.equals("0")) {

                        try {
                            // linear_billingdata.removeAllViews();

                            View billingLayout = null;
                            LayoutInflater inflater = mActivity
                                    .getLayoutInflater();

                            for (int i = 0; i < result.size(); i++) {
                                billingLayout = inflater.inflate(
                                        R.layout.layout_billing_detail,
                                        linear_billingdata, false);

                                final PrimaryBillingData data = result.get(i);

                                final RelativeLayout relative_billingChild = (RelativeLayout) billingLayout
                                        .findViewById(R.id.relative_billingChild);
                                relative_billingChild
                                        .setLayoutParams(new LinearLayout.LayoutParams(
                                                LayoutParams.MATCH_PARENT,
                                                (int) getResources()
                                                        .getDimension(
                                                                R.dimen.ll_50dp)));

                                billingLayout.setTag(data);

                                TextView txt_mp_bi_star = (TextView) billingLayout
                                        .findViewById(R.id.txt_mp_bi_star);
                                txt_mp_bi_star.setTypeface(font);
                                txt_mp_bi_star.setText(R.string.fa_star);
                                txt_mp_bi_star.setVisibility(View.INVISIBLE);

                                TextView txt_mp_bi_detail = (TextView) billingLayout
                                        .findViewById(R.id.txt_mp_bi_detail);
                                txt_mp_bi_detail.setText(data.getCcType()
                                        + " - **** **** **** **** "
                                        + data.getCcLastFour() + "- EXP "
                                        + data.getExpDate());

                                TextView txt_mp_bi_pencil = (TextView) billingLayout
                                        .findViewById(R.id.txt_mp_bi_pencil);
                                txt_mp_bi_pencil.setTypeface(font);
                                // txt_mp_bi_pencil.setText(R.string.fa_pencil);

                                linear_billingdata
                                        .addView(relative_billingChild);

                                relative_billingChild
                                        .setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                // TODO Auto-generated method
                                                // stub

                                                if (mActivity != null)
                                                    if (mActivity instanceof FragmentChangeActivity) {

                                                        Intent intent = new Intent(
                                                                mActivity,
                                                                UpdateCreditCardsActivity.class);
                                                        intent.putExtra(
                                                                "billdata",
                                                                data);
                                                        startActivityForResult(
                                                                intent,
                                                                BILLING_EDIT_SUCCESS);
                                                    }

                                            }
                                        });
                            }

                            if (result.size() == 0) {
                                ScreenUtils.raiseToast(mActivity, "No secondary billing information to display.");
                            } else if (status != null && !status.equals("0")) {
                                String errorStatus = Utils
                                        .getStringResourceByName("status_"
                                                + status, mActivity);
                                ScreenUtils.raiseToast(mActivity, errorStatus);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                        if (!status.equals("0")) {

                            String errorStatus = Utils.getStringResourceByName(
                                    "status_" + status, mActivity);
                            ScreenUtils.raiseToast(mActivity, errorStatus);

                        }

                    } else {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }
                }

            }.execute();
        else {

            handleVisibility(new int[]{View.GONE, View.VISIBLE});

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("peachpass", "onActivityResult called " + requestCode + " "
                + resultCode);
        if (requestCode == VEHICLE_EDIT_SUCCESS
                && resultCode == Activity.RESULT_OK) {

            getVehicleTagInfoList();
        } else if (requestCode == VEHICLE_ADD_SUCCESS
                && resultCode == Activity.RESULT_OK) {

            getVehicleTagInfoList();
        } else if (requestCode == BILLING_EDIT_SUCCESS
                && resultCode == Activity.RESULT_OK) {

            getPrimaryBillingMethod();
            getSecondaryBillingMethod();
        } else if (requestCode == BILLING_ADD_SUCCESS
                && resultCode == Activity.RESULT_OK) {

            getPrimaryBillingMethod();
            getSecondaryBillingMethod();
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    void handleVisibility(int[] values) {
        ll_actual_screen.setVisibility(values[0]);
        ll_no_internet.setVisibility(values[1]);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.MYPROFILE_VIEW);
    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}
