package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.adapter.ItemListAdapter;
import com.peachpass.data.FragmentTitle;
import com.peachpass.data.MyPeachPassData;
import com.peachpass.utils.Cache;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collections;

@SuppressLint("ValidFragment")
public class TransactionsFragment extends Fragment {

    ItemListAdapter adapter;
    private int mColorRes = -1;
    Activity mActivity;
    ListView listView_mypeachpass;
    LinearLayout ll_no_internet, ll_no_data;
    View view_no_internet;
    Context context;

    public TransactionsFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public TransactionsFragment(int colorRes) {
        mColorRes = colorRes;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");
        // int color = getResources().getColor(mColorRes);
        adapter = new ItemListAdapter(mActivity);

        Log.e("peachpass", "onCreateView transaction");

        // MyPeachPassData items[] = {
        // new MyPeachPassData("JULY", "30", "I-85 North - BEF0922",
        // "85A - JC02 - 21 | 8:23 am", " - $2.00"),
        // new MyPeachPassData("JULY", "28", "I-85 North - PLATE#",
        // "85A - CP08 - 21 | 9:33 am", " - $2.00"),
        // new MyPeachPassData("JULY", "24", "I-85 North - BEF0922",
        // "85A - JC02 - 21 | 10:43 am", " - $2.00"),
        // new MyPeachPassData("JULY", "23", "I-85 North - BEF0922",
        // "85A - CP08 - 21 | 11:53 am", " - $2.00"),
        // new MyPeachPassData("JULY", "22", "I-85 North - BEF0922",
        // "85A - JC02 - 21 | 12:23 pm", " - $2.00"),
        // new MyPeachPassData("JULY", "21", "I-85 North - BEF0922",
        // "85A - CP08 - 21 | 13:23 90", " - $2.00"),
        // new MyPeachPassData("JULY", "22", "I-85 North - BEF0922",
        // "85A - JC02 - 21 | 12:23 pm", " - $2.00"),
        // new MyPeachPassData("JULY", "21", "I-85 North - BEF0922",
        // "85A - CP08 - 21 | 13:23 90", " - $2.00"),
        // new MyPeachPassData("JULY", "20", "I-85 North - BEF0922",
        // "85A - JC02 - 21 | 14:23 pm", " - $2.00"),
        // new MyPeachPassData("JULY", "19", "I-85 North - BEF0922",
        // "85A - CP08 - 21 | 15:23 90", " - $2.00") };
        // for (MyPeachPassData temp : items)
        // adapter.addItem(temp);

        View view = inflater.inflate(R.layout.transactions, container, false);
        Typeface font = ScreenUtils.returnTypeFace(mActivity);

        TextView txt_fa_tran_crd_card = (TextView) view
                .findViewById(R.id.txt_fa_tran_crd_card);
        txt_fa_tran_crd_card.setTypeface(font);

        TextView txt_fa_tran_statements = (TextView) view
                .findViewById(R.id.txt_fa_tran_statements);
        txt_fa_tran_statements.setTypeface(font);

        listView_mypeachpass = (ListView) view
                .findViewById(R.id.listView_mypeachpass);
        listView_mypeachpass.setAdapter(adapter);

        ll_no_internet = (LinearLayout) view.findViewById(R.id.ll_no_internet);
        ll_no_data = (LinearLayout) view.findViewById(R.id.ll_no_data);

        LinearLayout ll_trn_alert = (LinearLayout) view
                .findViewById(R.id.ll_trn_alert);
        LinearLayout ll_trn_stmnt = (LinearLayout) view
                .findViewById(R.id.ll_trn_stmnt);

        view_no_internet = (View) view
                .findViewById(R.id.view_no_internet);
        Button refresh = (Button) view_no_internet.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadData();
            }
        });

        ll_trn_alert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new AlertsFragment(R.color.red),
                                "ALERTS", null, true, false);
                        FragmentTitle fragTitle = new FragmentTitle(
                                "TRANSACTIONS", false);
                        Cache.getInstance().getFragmentTitle().add(fragTitle);
                    }
            }
        });

        ll_trn_stmnt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mActivity != null)
                    if (mActivity instanceof FragmentChangeActivity) {
                        FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
                        fca.switchContent(new StatementFragment(R.color.red),
                                "STATEMENTS", null, true, false);
                    }
            }
        });

        loadData();
        Utils.setTracker(Utils.TRANSACTIONS_VIEW);
        return view;
    }

    @SuppressLint("StaticFieldLeak")
    private void loadData() {
        // TODO Auto-generated method stub
        new AsyncTask<String, String, ArrayList<MyPeachPassData>>() {
            ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(mActivity,
                    WebserviceUtils.PROGRESS_MESSAGE_FETCHING_TRANSACTIONS,
                    false);

            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                try {
                    pdialog.show();
                } catch (Exception e) {
                    // TODO: handle exception
                }
                super.onPreExecute();
            }

            @Override
            protected ArrayList<MyPeachPassData> doInBackground(
                    String... params) {
                // TODO Auto-generated method stub
                // Log.e("peachpass", "Cache.fromBack-"+Cache.fromBack);
                // Log.e("peachpass",
                // "Cache.getInstance().getMyPeachPassdataList()-"+Cache.getInstance().getMyPeachPassdataList());
                try {
                    ArrayList<MyPeachPassData> myPeachPassdataList = new ArrayList<MyPeachPassData>();
                    // if (Cache.fromBack) {
                    // if (Cache.getInstance().getMyPeachPassdataList() != null
                    // && Cache.getInstance().getMyPeachPassdataList()
                    // .size() > 0)
                    // myPeachPassdataList = Cache.getInstance()
                    // .getMyPeachPassdataList();
                    // // Log.e("peachpass", "return data here");
                    // return myPeachPassdataList;
                    // } else {
                    // myPeachPassdataList = new ArrayList<MyPeachPassData>();
                    // // Log.e("peachpass", "inside else");
                    // }

                    String requestBody = WebserviceUtils.readFromAsset(
                            "getTollTransactions", mActivity);
                    requestBody = requestBody.replaceAll("_accountId_",
                            Utils.getAccountId(mActivity));
                    requestBody = requestBody.replaceAll("_userName_",
                            Utils.getUsername(mActivity));
                    requestBody = requestBody.replaceAll("_sessionId_",
                            Utils.getSessionId(mActivity));

                    requestBody = requestBody.replaceAll("_osType_",
                            WebserviceUtils.OS_TYPE);
                    requestBody = requestBody.replaceAll("_osVersion_",
                            Utils.getOsVersion());
                    requestBody = requestBody.replaceAll("_ipAddress_",
                            WebserviceUtils.getExternalIP());

                    requestBody = requestBody.replaceAll("_id_",
                            WebserviceUtils.ID);

                    Log.e("peachpass", "requestBody---" + requestBody);
                    String result = WebserviceUtils.sendSoapRequest(mActivity,
                            requestBody,
                            WebserviceUtils.SOAP_ACTION_GETTOLLTRANSACTIONS);
                    if (result == null)
                        return null;
                    XMLParser parser = new XMLParser();
                    String xml = result;
                    Document doc = parser.getDomElement(xml); // getting DOM
                    // element


                    NodeList nl = doc.getElementsByTagName("ns1:txnDetails");

//
//					Element e = (Element) nl.item(0);
//					String status = parser.getValue(e, "ns1:status");
                    /*
                     * <ns1:location>Pleasant Hill 02/85A-PH02-21</ns1:location>
					 */
                    for (int i = 0; i < nl.getLength(); i++) {

                        Element e = (Element) nl.item(i);
                        String transactionDate = parser.getValue(e,
                                "ns1:txnDate");
                        String licensePlate = parser
                                .getValue(e, "ns1:licPlate");
                        String lane = parser.getValue(e, "ns1:lane");
                        String dir = parser.getValue(e, "ns1:direction");
                        String amount = parser.getValue(e, "ns1:amount");
                        String month = "", day = "", time = "";
                        String laneString = "";
                        try {
                            String timeDetails[] = Utils.parseDate(
                                    transactionDate, Utils.DATE_PATTERN_SOAP,
                                    Utils.DATE_PATTERN_MY_PEACH_PASS).split(
                                    "\\,");
                            month = timeDetails[0];
                            day = timeDetails[1];
                            time = timeDetails[2];

                            String dateInfo = month + "-" + day + "-" + time;
                            Log.e("newlog", "dateInfo---" + dateInfo);
                            //01-04 19:06:36.292: E/newlog(10465): dateInfo---Mar-13-05:54 AM


                        } catch (Exception e2) {
                            // TODO: handle exception
                        }

                        Log.e("peachpass", "lane---" + lane);

                        if (lane.contains("85A")) {
                        /*    data = new MyPeachPassData(month, day,
                                    "I-85", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass--1", data.toString());*/

                            laneString = "I-85";
                        }
                        if (lane.contains("85B")) {
                        /*    data = new MyPeachPassData(month, day,
                                    "I-85", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass--1", data.toString());*/

                            laneString = "I-85B";
                        }
                        if (lane.contains("75A")) {
                       /*      data = new MyPeachPassData(month, day,
                                    "I-75", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass---2", data.toString());*/
                            laneString = "I-75";
                        }
                        if (lane.contains("75B")) {
                       /*      data = new MyPeachPassData(month, day,
                                    "I-75", dir, licensePlate, lane, time, amount, transactionDate);
                            Log.e("peachpass---2", data.toString());*/
                            laneString = "NWC";
                        }

                        MyPeachPassData data = new MyPeachPassData(month, day,
                                laneString, dir, licensePlate, lane, time, amount, transactionDate);
                        Log.e("peachpass", data.toString());
                        myPeachPassdataList.add(data);
                        // adapter.addItem(new MyPeachPassData(month, day,
                        // "I-85", dir, licensePlate, lane, time, amount));
                    }
                    // Cache.getInstance().setMyPeachPassdataList(
                    // myPeachPassdataList);
                    return myPeachPassdataList;

                } catch (Exception e) {
                    // TODO: handle exception

                }
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<MyPeachPassData> result) {
                // TODO Auto-generated method stub
                try {
                    pdialog.dismiss();
                } catch (Exception e) {
                    // TODO: handle exception
                }
                try {
                    Cache.fromBack = false;
                    if (result != null) {

                        if (result.size() > 0) {
                            handleVisibility(new int[]{View.VISIBLE, View.GONE, View.GONE});
                            adapter.clearItems();

                            Collections.sort(result);
                            Collections.reverse(result);
//							try {
//								Collections.sort(result, new Comparator<MyPeachPassData>() {
//									  public int compare(MyPeachPassData o1, MyPeachPassData o2) {
//											if (o1.getTransacDate() == null || o2.getTransacDate() == null)
//											      return 0;
//											try {
//												SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_PATTERN_SOAP,Locale.ENGLISH);
//												 Date date1 = formatter.parse(o1.getTransacDate());
//												 Date date2 = formatter.parse(o2.getTransacDate());
//												    return date1.compareTo(date2);	
//											} catch (Exception e) {
//												// TODO: handle exception
//												e.printStackTrace();
//												return 0;
//											}
//									  }
//									});
//							} catch (Exception e) {
//								// TODO: handle exception
//								e.printStackTrace();
//							}


                            adapter.insertList(result);
                            adapter.notifyDataSetChanged();

                        } else {
                            handleVisibility(new int[]{View.GONE, View.GONE, View.VISIBLE});
//							ScreenUtils.raiseToast(mActivity,
//									WebserviceUtils.NO_DATA_DISPLAY);
                        }

                    } else {
                        handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
//						ScreenUtils.raiseToast(mActivity,
//								WebserviceUtils.SOMETHING_WENT_WRONG);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
//					ScreenUtils.raiseToast(mActivity,
//							WebserviceUtils.SOMETHING_WENT_WRONG);

                }

                super.onPostExecute(result);
            }
        }.execute();
    }

    public void addItem(MyPeachPassData item) {
        if (adapter != null)
            adapter.addItem(item);
    }

    public void clearList() {
        if (adapter != null)
            adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

        setRetainInstance(true);
    }

    /**
     * List, No internet and No data
     */
    void handleVisibility(int[] values) {
        listView_mypeachpass.setVisibility(values[0]);
        ll_no_internet.setVisibility(values[1]);
        ll_no_data.setVisibility(values[2]);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.TRANSACTIONS_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }
}
