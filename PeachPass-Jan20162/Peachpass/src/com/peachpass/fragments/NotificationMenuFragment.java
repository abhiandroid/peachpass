package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.adapter.NotifiactionListAdapter;
import com.peachpass.data.NotificationData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@SuppressLint("ValidFragment")
public class NotificationMenuFragment extends Fragment {

    NotifiactionListAdapter adapter;
    private int mColorRes = -1;
    Activity mActivity;
    LinearLayout ll_notifications;
    Context context;

    public static ArrayList<NotificationData> notificationArray = new ArrayList<NotificationData>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");
        // int color = getResources().getColor(mColorRes);
        adapter = new NotifiactionListAdapter(mActivity);


        View view = inflater
                .inflate(R.layout.rightmenulayout, container, false);

        ll_notifications = (LinearLayout) view
                .findViewById(R.id.ll_notifications);

        if (Utils.isNetworkAvailable(mActivity))
            downloadNotifications(ll_notifications);

        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mMessageReceiver,
                new IntentFilter("addVehicleNotification"));
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mMessageReceiver,
                new IntentFilter("refreshNotificationScreen"));

        Utils.setTracker(Utils.NOTIFICATIONS_VIEW);

        return view;
    }


    private void downloadNotifications(final LinearLayout ll_notifications) {
        new AsyncTask<String, String, ArrayList<NotificationData>>() {

            @Override
            protected ArrayList<NotificationData> doInBackground(
                    String... params) {
                // TODO Auto-generated method stub
                try {

                    notificationArray = new ArrayList<NotificationData>();

                    String requestBody = WebserviceUtils.readFromAsset(
                            "getAccountNotifications", mActivity);
                    requestBody = requestBody.replaceAll("_accountId_",
                            Utils.getAccountId(mActivity));
                    requestBody = requestBody.replaceAll("_userName_",
                            Utils.getUsername(mActivity));
                    requestBody = requestBody.replaceAll("_sessionId_",
                            Utils.getSessionId(mActivity));

                    requestBody = requestBody.replaceAll("_osType_",
                            WebserviceUtils.OS_TYPE);
                    requestBody = requestBody.replaceAll("_osVersion_",
                            Utils.getOsVersion());
                    requestBody = requestBody.replaceAll("_ipAddress_",
                            WebserviceUtils.getExternalIP());

                    requestBody = requestBody.replaceAll("_id_",
                            WebserviceUtils.ID);

                    //Log.e("peachpass", requestBody + "");
                    String result = WebserviceUtils
                            .sendSoapRequest(
                                    mActivity,
                                    requestBody,
                                    WebserviceUtils.SOAP_ACTION_GETACCOUNTNOTIFICATIONS);
                    XMLParser parser = new XMLParser();
                    String xml = result;
                    Document doc = parser.getDomElement(xml); // getting DOM
                    // element

                    NodeList nl = doc.getElementsByTagName("ns1:notifications");

                    //Set<String> notificationSet = SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.NOTIFICATIONS, mActivity);
                    for (int i = 0; i < nl.getLength(); i++) {

                        Element e = (Element) nl.item(i);

                        String compType = parser.getValue(e, "ns1:compType");
                        String status = parser.getValue(e, "ns1:status");
                        String description = parser.getValue(e,
                                "ns1:description");
                        String notificationDate = Utils.parseDate(parser.getValue(e,
                                "ns1:notificationDate"), Utils.DATE_PATTERN_SOAP, Utils.DATE_PATTERN_NOTIFICATION);

                        NotificationData notData = new NotificationData(
                                description, notificationDate, true, compType,
                                status);
                        notificationArray.add(notData);
                    }
                    return notificationArray;

                } catch (Exception e) {
                    // TODO: handle exception

                }
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<NotificationData> result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);

                if (result != null)
                    try {
                        Set<String> dummySet = new HashSet<String>();
                        Set<String> notificationSet = SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.NOTIFICATIONS, mActivity);
                        int count = 0;
                        for (NotificationData obj : result) {
                            if (isPresentInSet(notificationSet, obj)) {
                                //ignore
                                obj.setRead(true);
                            } else {
                                dummySet.add(obj.getDate() + "@" + 0);
                                obj.setRead(false);
                                count++;
                            }
                        }
                        notificationSet.addAll(dummySet);
                        SharedPreferenceHelper.savePreferencesSet(SharedPreferenceHelper.NOTIFICATIONS, notificationSet, mActivity);
                        SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADNOTIFICATIONS, count + "", mActivity);

                        sendBroadcastToLeftMenu();
                        sendBroadcastBaseActivity();
                        sendBroadcastFragActivity();

                        fillComments(
                                result.toArray(new NotificationData[result.size()]),
                                ll_notifications);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
            }


            boolean isPresentInSet(Set<String> notificationSet, NotificationData obj) {
                for (String prefSet : notificationSet) {
                    String sliptArray[] = prefSet.split("\\@");
                    String date = sliptArray[0];
                    if (obj.getDate().equals(date)) {
                        return true;
                    }
                }
                return false;
            }
        }.execute(new String[]{});
    }

    private void sendBroadcastToLeftMenu() {
        // TODO Auto-generated method stub

        Intent intent = new Intent("custom-event-name");
        intent.putExtra("message", "updatenoti");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void sendBroadcastBaseActivity() {
        // TODO Auto-generated method stub

        Intent intent = new Intent("baseactivity");
        intent.putExtra("message", "updatebubble");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void sendBroadcastFragActivity() {
        // TODO Auto-generated method stub

        Intent intent = new Intent("fragchangectivity");
        intent.putExtra("message", "updatebubble");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    /**
     * add items to the list adapter
     */
    public void addItem(NotificationData item) {
        if (adapter != null)
            adapter.addItem(item);
    }

    /**
     * clear items from list adapter
     */
    public void clearList() {
        if (adapter != null)
            adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    private void fillComments(NotificationData[] comments, LinearLayout ll) throws Exception {
        View comment;
        TextView txt_notf_text;
        TextView txt_notf_date;
        TextView txt_not_chaticon;

        LayoutInflater inflater = mActivity.getLayoutInflater();
        ll.removeAllViews();
        for (NotificationData s : comments) {
            comment = inflater.inflate(R.layout.list_item_notifications, null);

            comment.setLayoutParams(new LayoutParams(
                    LayoutParams.MATCH_PARENT, (int) mActivity.getResources()
                    .getDimension(R.dimen.ll_notification_row_height)));

            txt_notf_text = (TextView) comment.findViewById(R.id.txt_notf_text);
            txt_notf_date = (TextView) comment.findViewById(R.id.txt_notf_date);

            txt_notf_text.setText(s.getText() + "");
            txt_notf_date.setText(s.getDate() + "");

            txt_not_chaticon = (TextView) comment
                    .findViewById(R.id.txt_not_chaticon);
            txt_not_chaticon.setTypeface(ScreenUtils
                    .returnTypeFace(mActivity));

            if (s.isRead()) {
                txt_not_chaticon.setTextColor(mActivity.getResources()
                        .getColor(R.color.grey));
                txt_notf_text.setTextColor(mActivity.getResources()
                        .getColor(R.color.grey_text));
            } else {
                txt_not_chaticon.setTextColor(mActivity.getResources()
                        .getColor(R.color.orange));
                txt_notf_text.setTextColor(mActivity.getResources()
                        .getColor(R.color.white));
            }

            ll.addView(comment);
            ll.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (mActivity != null)
                        if (mActivity instanceof FragmentChangeActivity) {
//							FragmentChangeActivity fca = (FragmentChangeActivity) mActivity;
//							fca.switchContent(new TransactionsFragment(
//									R.color.red), "ALERTS", null, true);
                        }
                }
            });

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            if (message != null && message.equals("refreshlist")) {

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // TODO Auto-generated method stub
                        try {
                            Set<String> dummyNotSet = new HashSet<String>();
                            Set<String> notSet = SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.NOTIFICATIONS, mActivity);
                            for (String sets : notSet) {
                                String sliptArray[] = sets.split("\\@");
                                String date = sliptArray[0];
                                String status = "1";
                                dummyNotSet.add(date + "@" + status);
                            }
                            SharedPreferenceHelper.savePreferencesSet(SharedPreferenceHelper.NOTIFICATIONS, dummyNotSet, mActivity);
                            for (NotificationData obj : notificationArray) {
                                obj.setRead(true);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {

                        try {
                            SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.UNREADNOTIFICATIONS, 0 + "", mActivity);

                            sendBroadcastToLeftMenu();
                            sendBroadcastBaseActivity();
                            sendBroadcastFragActivity();
                            fillComments(
                                    notificationArray.toArray(new NotificationData[notificationArray.size()]),
                                    ll_notifications);
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    }

                    ;

                }.execute();


            } else {
                if (Utils.isNetworkAvailable(mActivity))
                    if (ll_notifications != null)
                        downloadNotifications(ll_notifications);
            }

        }
    };


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.NOTIFICATIONS_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}
