package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.data.NotificationData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

@SuppressLint("ValidFragment")
public class ResourceFragment extends Fragment {

    // NotifiactionListAdapter adapter;
    private int mColorRes = -1;
    Activity mActivity;
    Context context;
    String message[] = {"Peach Pass Mounting Instruction Video", "FAQ page", "Twitter page", "Facebook page", "TollTalk blog", "Newsletter"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");
        // int color = getResources().getColor(mColorRes);
        // adapter = new NotifiactionListAdapter(getActivity());

        /** Dummy values added here */
        NotificationData items[] = {
                new NotificationData("Peach Pass Mounting Instructions",
                        "Need help installing your Peach Pass? Watch here",
                        true, getActivity().getString(R.string.fa_youtube)),
                new NotificationData("Peach Pass FAQs", "Download our FAQs",
                        true, getActivity().getString(
                        R.string.fa_question_circle)),
                new NotificationData("Twitter", "Follow us on Twitter", false,
                        getActivity().getString(R.string.fa_twitter)),
                new NotificationData("Facebook", "Join us on Facebook", false,
                        getActivity().getString(R.string.fa_facebook)),
                new NotificationData("Newsletter",
                        "Sign up for the Peach Pass Press", true, getActivity()
                        .getString(R.string.fa_envelope_o))};

        // for (NotificationData temp : items)
        // adapter.addItem(temp);

        View view = inflater
                .inflate(R.layout.resource_layout, container, false);

        LinearLayout ll_notifications = (LinearLayout) view
                .findViewById(R.id.ll_notifications);

        fillComments(items, ll_notifications);

        return view;
    }

    /**
     * add items to the list adapter
     */
    public void addItem(NotificationData item) {
        // if (adapter != null)
        // adapter.addItem(item);
    }

    /**
     * clear items from list adapter
     */
    public void clearList() {
        // if (adapter != null)
        // adapter.clearItems();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    private void fillComments(NotificationData[] comments, LinearLayout ll) {
        View comment;
        LinearLayout ll_resource;
        TextView txt_notf_date;
        TextView txt_res_icon;

        LayoutInflater inflater = getActivity().getLayoutInflater();

        for (NotificationData s : comments) {
            comment = inflater.inflate(R.layout.list_item_resource, null);

            comment.setLayoutParams(new LayoutParams(
                    LayoutParams.MATCH_PARENT, (int) getResources()
                    .getDimension(R.dimen.ll_notification_row_height)));

            ll_resource = (LinearLayout) comment.findViewById(R.id.ll_resource);
            final TextView txt_notf_text = (TextView) comment
                    .findViewById(R.id.txt_notf_text);
            txt_notf_date = (TextView) comment.findViewById(R.id.txt_notf_date);
            txt_res_icon = (TextView) comment.findViewById(R.id.txt_res_icon);

            txt_notf_text.setText(s.getText() + "");
            txt_notf_date.setText(s.getDate() + "");
            txt_res_icon.setText(s.getFontAwsome());
            txt_res_icon.setTypeface(ScreenUtils.returnTypeFace(getActivity()));

            ll_resource.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (txt_notf_text.getText().toString().contains("Mounting")) {
                        Intent browserIntent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://www.youtube.com/watch?v=cb2-GA5ql8o"));
                        //startActivity(browserIntent);
                        onClickList(browserIntent, 0);
                    } else if (txt_notf_text.getText().toString().contains("FAQ")) {
                        Intent browserIntent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://www.peachpass.com/faq/"));
                        //startActivity(browserIntent);
                        onClickList(browserIntent, 1);
                    } else if (txt_notf_text.getText().toString()
                            .contains("Twitter")) {

                        try {
                            onClickList(getOpenTwitterIntent((getActivity())), 2);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                    } else if (txt_notf_text.getText().toString()
                            .contains("Facebook")) {

                        onClickList(getOpenFacebookIntent(getActivity()), 3);

//                    } else if (txt_notf_text.getText().toString()
//                            .contains("Blog")) {
//
//                        try {
//                            Intent browserIntent = new Intent(
//                                    Intent.ACTION_VIEW,
//                                    Uri.parse("http://www.peachpass.com/blog"));
//                            onClickList(browserIntent, 4);
//                        } catch (Exception e) {
//                            // TODO: handle exception
//                        }

                    } else if (txt_notf_text.getText().toString()
                            .contains("Newsletter")) {
                        try {
                            Intent browserIntent = new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://www.peachpass.com/news-and-alerts/ "));
                            onClickList(browserIntent, 5);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                    }

					/* twitter://user?screen_name=SRTA_PeachPass */

                }
            });

            ll.addView(comment);

        }
    }

    private Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://page/144581358949104"));

            //Uri.parse("fb://page/144581358949104"));

        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/PeachPass"));

        }
    }

    private Intent getOpenTwitterIntent(Context context) throws Exception {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.twitter.android", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/PeachPassGA"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/PeachPassGA"));

        }

    }


    public void onClickList(final Intent intent, int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage("This will open your device’s browser application to view our " + returnDialogMessage(type)).setTitle("Confirm Action?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                startActivity(intent);
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private String returnDialogMessage(int type) {

        return message[type];
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.RESOURCES_VIEW);

    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}