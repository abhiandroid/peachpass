package com.peachpass.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.FragmentChangeActivity;
import com.peachpass.utils.Cache;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class LeftMenuFragment extends Fragment {

    Activity mActivity;
    Context context;
    TextView txt_leftmenu_alert, txt_leftmenu_notifications;

    TextView txt_username;

    LinearLayout ll_blue_not_border, ll_red_not_border;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // return inflater.inflate(R.layout.list, null);

        View view = inflater.inflate(R.layout.leftmenulayout, container, false);
        Typeface font = ScreenUtils.returnTypeFace(getActivity());

        context = getActivity();

        final ScrollView scroll_view = (ScrollView) view
                .findViewById(R.id.scroll_view);

        int idsTxt[] = {R.id.txt_leftmenu_0, R.id.txt_leftmenu_1,
                R.id.txt_leftmenu_2, R.id.txt_leftmenu_3, R.id.txt_leftmenu_4,
                R.id.txt_leftmenu_5, R.id.txt_leftmenu_6, R.id.txt_leftmenu_7,
                R.id.txt_leftmenu_8, R.id.txt_leftmenu_9, R.id.txt_leftmenu_10};
        int idsLnr[] = {R.id.ll_leftmenu_0, R.id.ll_leftmenu_1,
                R.id.ll_leftmenu_2, R.id.ll_leftmenu_3, R.id.ll_leftmenu_4,
                R.id.ll_leftmenu_5, R.id.ll_leftmenu_6, R.id.ll_leftmenu_7,
                R.id.ll_leftmenu_8, R.id.ll_leftmenu_9, R.id.ll_leftmenu_10};

        /** Added title values here */
        final String titleArray[] = {"MY PEACH PASS", "CHANGE TOLL MODE",
                "TRANSACTIONS", "MY PROFILE", "ALERTS", "NOTIFICATIONS",
                "STATEMENTS", "FAQs", "RESOURCES", "CONTACT US", "LOGOUT"};

        for (int id : idsTxt) {
            TextView textObj = (TextView) view.findViewById(id);
            textObj.setTypeface(font);
        }

        String firstName = SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.FIRST_NAME);
        String lastName = SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.LAST_NAME);
        txt_username = (TextView) view.findViewById(R.id.txt_username);
        txt_username.setText(Utils.firstCapitalName(firstName) + " "
                + Utils.firstCapitalName(lastName));

        txt_leftmenu_alert = (TextView) view
                .findViewById(R.id.txt_leftmenu_alert);
        txt_leftmenu_notifications = (TextView) view
                .findViewById(R.id.txt_leftmenu_notifications);
        ll_blue_not_border = (LinearLayout) view
                .findViewById(R.id.ll_blue_not_border);
        ll_red_not_border = (LinearLayout) view
                .findViewById(R.id.ll_red_not_border);

        setNumbersToNitifications();
        setNumbersToAlerts();
        int count = 0;
        for (int id : idsLnr) {

            final int tempCount = count;
            LinearLayout llObj = (LinearLayout) view.findViewById(id);
            llObj.setOnClickListener(new OnClickListener() {
                /**
                 * All the click events from the Sliding Menu to other Fragments
                 * added here
                 */
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Cache.fromBack = false;
                    switch (tempCount) {
                        case 0:
                            /** access to My Peach Pass screen */
                            switchFragment(new MyPeachPassFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 1:
                            /** access to Change Toll Mode screen */
                            switchFragment(new ChangeTollModeFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 2:
                            /** access to Transaction screen */
                            switchFragment(new TransactionsFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 3:
                            /** access to My Profile screen */
                            switchFragment(new MyProfileFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 4:
                            /** access to Alerts screen */
                            switchFragment(new AlertsFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 5:
                            /** access to Notifications screen */
                            switchFragment(new MyPeachPassFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, false);
                            break;
                        case 6:
                            /** access to Statement screen */
                            switchFragment(new StatementFragment(R.color.red),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 7:
                            /** access to My Peach Pass screen */
//                            switchFragment(new FaqFragment(R.color.red),
//                                    titleArray[tempCount], scroll_view, true);
//                            break;
                            Utils.openLinkInBrowser(WebserviceUtils.FAQ_LINK,
                                    getActivity());
                            break;
                        case 8:
                            /** access to My Peach Pass screen */
                            switchFragment(new ResourceFragment(),
                                    titleArray[tempCount], scroll_view, true);
                            break;
                        case 9:
                            /** access to My Peach Pass screen */
                            // switchFragment(new ContactUsFragment(R.color.red),
                            // titleArray[tempCount], scroll_view, true);
                            // getActivity().startActivity(
                            // new Intent(getActivity(),
                            // ContactUsActivity.class));
                            Utils.openLinkInBrowser(WebserviceUtils.CONTACT_US,
                                    getActivity());
                            break;
                        case 10:
                            /**
                             * switchFragment(new MyPeachPassFragment(R.color.red),
                             * titleArray[tempCount], scroll_view, true);
                             */
                            // getActivity().startActivity(new Intent(getActivity(),
                            // LoginActivity.class));
                            // getActivity().finish();
                            logoutUser();
                            break;

                        default:
                            break;
                    }

                }
            });
            count++;
        }

        LocalBroadcastManager.getInstance(mActivity).registerReceiver(
                mMessageReceiver, new IntentFilter("custom-event-name"));

        return view;
    }

    protected void logoutUser() {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(getActivity()))
            new AsyncTask<String, String, String>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        mActivity, WebserviceUtils.LOGGING_OUT, false);

                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... params) {
                    // TODO Auto-generated method stub

                    try {

                        String requestBody = WebserviceUtils.readFromAsset(
                                "userLogout", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody + "");
                        String result = WebserviceUtils.sendSoapRequest(
                                mActivity, requestBody,
                                WebserviceUtils.SOAP_ACTION_USERLOGOUT);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element

                        NodeList nl = doc.getElementsByTagName("result");

                        for (int i = 0; i < nl.getLength(); ) {

                            Element e = (Element) nl.item(i);

                            String originalId = parser.getValue(e,
                                    "ns1:originalId");
                            String status = parser.getValue(e, "ns1:status");

                            return status;
                        }

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String result) {
                    // TODO Auto-generated method stub
                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        if (result != null) {

                            if (result.equals("0")) {

                                // SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.ACCOUNTBALANCE,
                                // result.getAccountBalance(), mActivity);
                                mActivity.finish();

                            } else {

                                ScreenUtils.raiseToast(mActivity,
                                        WebserviceUtils.SESSION_EXPIRED);
                                mActivity.finish();
                            }

                        } else {

                            ScreenUtils.raiseToast(mActivity,
                                    WebserviceUtils.SOMETHING_WENT_WRONG);
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.SOMETHING_WENT_WRONG);
                    }

                    super.onPostExecute(result);
                }
            }.execute();

        else {
            ScreenUtils.raiseToast(mActivity,
                    WebserviceUtils.NO_INTERNET_CONNECTION);
        }

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub

        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    // the code of switching the above fragment
    private void switchFragment(Fragment fragment, String title,
                                ScrollView scroll_view, boolean flag) {
        if (getActivity() == null)
            return;

        if (getActivity() instanceof FragmentChangeActivity) {
            FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
            fca.switchContent(fragment, title, scroll_view, flag, true);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(
                mMessageReceiver);
        super.onDestroy();
    }

    // class MyReceiver extends BroadcastReceiver {
    //
    // @Override
    // public void onReceive(Context context, Intent intent) {
    // txt_leftmenu_alert.setText(Math.random()*10+"");
    // }
    //
    // }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            txt_leftmenu_alert.setText((int) Math.random() * 10 + "");
            if (message != null && message.equals("name")) {
                try {
                    String firstName = SharedPreferenceHelper.getPreference(
                            mActivity, SharedPreferenceHelper.FIRST_NAME);
                    String lastName = SharedPreferenceHelper.getPreference(
                            mActivity, SharedPreferenceHelper.LAST_NAME);
                    txt_username.setText(Utils.firstCapitalName(firstName)
                            + " " + Utils.firstCapitalName(lastName));
                } catch (Exception e) {
                    // TODO: handle exception
                }

            } else if (message != null && message.equals("updatenoti")) {

                try {
                    setNumbersToNitifications();

                } catch (Exception e) {
                    // TODO: handle exception
                }
            } else if (message != null && message.equals("updatealert")) {

                try {
                    setNumbersToAlerts();

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    };

    protected void setNumbersToNitifications() {
        // TODO Auto-generated method stub
        String getUnreadCount = SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.UNREADNOTIFICATIONS);
        Log.e("peachpass", getUnreadCount + "------getUnreadCount");
        int count = 0;
        try {
            count = Integer.parseInt(getUnreadCount);
        } catch (Exception e) {
            // TODO: handle exception
            count = 0;
        }
        if (count == 0) {
            Log.e("peachpass", count + "------count1");
            ll_blue_not_border.setVisibility(View.GONE);
        } else {
            Log.e("peachpass", count + "------count2");
            ll_blue_not_border.setVisibility(View.VISIBLE);
        }

        if (count > 10) {
            Log.e("peachpass", count + "------count3");
            txt_leftmenu_notifications.setText("10+");
        } else {
            Log.e("peachpass", count + "------count4");
            txt_leftmenu_notifications.setText(count + "");
        }
    }


    protected void setNumbersToAlerts() {
        // TODO Auto-generated method stub
        String getUnreadCount = SharedPreferenceHelper.getPreference(mActivity,
                SharedPreferenceHelper.UNREADALERTS);
        Log.e("peachpass", getUnreadCount + "------getUnreadCount");
        int count = 0;
        try {
            count = Integer.parseInt(getUnreadCount);
        } catch (Exception e) {
            // TODO: handle exception
            count = 0;
        }
        if (count == 0) {
            Log.e("peachpass", count + "alert------count1");
            ll_red_not_border.setVisibility(View.GONE);
        } else {
            Log.e("peachpass", count + "alert------count2");
            ll_red_not_border.setVisibility(View.VISIBLE);
        }

        if (count > 10) {
            Log.e("peachpass", count + "alert------count3");
            txt_leftmenu_alert.setText("10+");
        } else {
            Log.e("peachpass", count + "alert------count4");
            txt_leftmenu_alert.setText(count + "");
        }
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.LEFT_MENU);

    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}
