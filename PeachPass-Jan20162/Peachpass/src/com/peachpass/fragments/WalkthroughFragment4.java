package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.CreateAccountInfoActivity;
import com.peachpass.activity.LoginScreenActivity;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

public class WalkthroughFragment4 extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";

    int imageSource;
    Context context;

    @SuppressLint("ValidFragment")
    public WalkthroughFragment4(int imageSource) {
        this.imageSource = imageSource;

    }

    public WalkthroughFragment4() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null)
                && savedInstanceState.containsKey(KEY_CONTENT)) {
            imageSource = savedInstanceState.getInt(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.sample_page4,
                null);
        LinearLayout image = (LinearLayout) root.findViewById(R.id.txt_x_cross_4);
        image.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i2 = new Intent(getActivity(), LoginScreenActivity.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i2);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        LinearLayout ll_bototm_tab_1 = (LinearLayout) root.findViewById(R.id.ll_bototm_tab_1);
        ll_bototm_tab_1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /**Cross button click event*/
                Intent i2 = new Intent(getActivity(), LoginScreenActivity.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i2);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        LinearLayout ll_bototm_tab_2 = (LinearLayout) root.findViewById(R.id.ll_bototm_tab_2);
        ll_bototm_tab_2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /**Cross button click event*/
                Intent i2 = new Intent(getActivity(), CreateAccountInfoActivity.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i2);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CONTENT, imageSource);
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(getActivity());

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.WALKTHROUGH4);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(getActivity());

        FlurryAgent.onEndSession(context);
    }
}
