package com.peachpass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.peachpass.activity.AddVehicleActivity;
import com.peachpass.customtime.ArrayWheelAdapter;
import com.peachpass.customtime.DayWheelAdapter;
import com.peachpass.customtime.NumericWheelAdapter;
import com.peachpass.customtime.WheelView;
import com.peachpass.data.TollModeData;
import com.peachpass.data.VehicleData;
import com.peachpass.utils.ScreenUtils;
import com.peachpass.utils.Utils;
import com.peachpass.utils.WebserviceUtils;
import com.peachpass.utils.XMLParser;
import com.sharedpreference.SharedPreferenceHelper;
import com.srta.PeachPass.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@SuppressLint("ValidFragment")
public class ChangeTollModeFragment extends Fragment {

    private int mColorRes = -1;
    String WHITE_COLOR = "#ECEDED";
    String BLACK_COLOR = "#8A898A";
    String GREEN_COLOR = "#75BE43";

    HorizontalScrollView hrzntl_scrolview;
    LinearLayout linear_vehicledata;
    TextView txt_car_year_and_name, txt_car_lic_plate;
    private static final String TAG = "NonTollMode";

    Activity mActivity;
    Typeface font;
    Context context;

    LinearLayout ll_ctm_tg_1;
    LinearLayout ll_ctm_tg_2;

    TextView txt_changetoll_play_1;
    TextView txt_changetoll_play_2;
    TextView txt_changetoll_calendr_1;
    TextView txt_changetoll_calendr_2;
    TextView txt_changetoll_play;
    TextView txt_changetoll_calendr, tv_toll_mode_change_to_text,
            tv_toll_mode_change_to_change, txt_ctm_mode, txt_ctm_next,
            txt_chngetollmode_1;

    LinearLayout ll_change_to_free, ll_sub_text, ll_toll_mode_change_to_text;

    LinearBoolean flag = new LinearBoolean(0);
    View view;
    boolean isScheduleLaterDateSet = false;

    boolean flag_duration = false;

    String licPlateNumber = "";
    public String vehicleId = "";
    String durationId = "", effectiveTime = "", effectiveData = "", effectiveData1 = "";

    LinearLayout ll_actual_screen, ll_no_internet, ll_no_data;
    View view_no_internet;

    @SuppressLint("ValidFragment")
    public ChangeTollModeFragment() {
        this(R.color.white);
    }

    @SuppressLint("ValidFragment")
    public ChangeTollModeFragment(int colorRes) {
        mColorRes = colorRes;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        if (savedInstanceState != null)
            mColorRes = savedInstanceState.getInt("mColorRes");

        font = ScreenUtils.returnTypeFace(mActivity);
        view = inflater.inflate(R.layout.changetollmode, container, false);

        hrzntl_scrolview = (HorizontalScrollView) view
                .findViewById(R.id.hrzntl_scrolview);

        linear_vehicledata = (LinearLayout) view
                .findViewById(R.id.linear_vehicledata);

        ll_change_to_free = (LinearLayout) view
                .findViewById(R.id.ll_change_to_free);
        ll_sub_text = (LinearLayout) view.findViewById(R.id.ll_sub_text);
        ll_toll_mode_change_to_text = (LinearLayout) view
                .findViewById(R.id.ll_toll_mode_change_to_text);
        tv_toll_mode_change_to_text = (TextView) view
                .findViewById(R.id.tv_toll_mode_change_to_text);
        tv_toll_mode_change_to_change = (TextView) view
                .findViewById(R.id.tv_toll_mode_change_to_change);
        txt_ctm_mode = (TextView) view.findViewById(R.id.txt_ctm_mode);
        txt_ctm_next = (TextView) view.findViewById(R.id.txt_ctm_next);
        txt_chngetollmode_1 = (TextView) view
                .findViewById(R.id.txt_chngetollmode_1);

        ll_actual_screen = (LinearLayout) view
                .findViewById(R.id.ll_actual_screen);


        ll_no_internet = (LinearLayout) view.findViewById(R.id.ll_no_internet);
        ll_no_data = (LinearLayout) view.findViewById(R.id.ll_no_data);

        view_no_internet = (View) view
                .findViewById(R.id.view_no_internet);
        Button refresh = (Button) view_no_internet.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getVehicleTagInfoList();
            }
        });

        TextView txt_ctm_date = (TextView) view.findViewById(R.id.txt_ctm_date);
        txt_ctm_date.setTypeface(font);

        TextView txt_changetoll_arrow = (TextView) view
                .findViewById(R.id.txt_changetoll_arrow);
        txt_changetoll_arrow.setTypeface(font);

        TextView txt_ctm_arrow = (TextView) view
                .findViewById(R.id.txt_ctm_arrow);
        txt_ctm_arrow.setTypeface(font);

        ImageView txt_fa_ctm_scroll = (ImageView) view
                .findViewById(R.id.txt_fa_ctm_scroll);

        txt_car_year_and_name = (TextView) view
                .findViewById(R.id.txt_car_year_and_name);
        txt_car_lic_plate = (TextView) view
                .findViewById(R.id.txt_car_lic_plate);

        txt_fa_ctm_scroll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hrzntl_scrolview.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // hsv.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                        hrzntl_scrolview.smoothScrollBy(300, 0);
                    }
                }, 100);
            }
        });

        initToggleButtons();

        /** toggle logic for Start Now and Schedule Later */
        ll_ctm_tg_1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flag.setSet(0);
                ll_toll_mode_change_to_text.setVisibility(View.GONE);
                isScheduleLaterDateSet = false;
                clickToggle1();

            }
        });
        ll_ctm_tg_2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flag.setSet(1);

                clickToggle2(true);
            }
        });

        initStatusOfToggleButtons();

        final TextView txt_ctm_duration = (TextView) view
                .findViewById(R.id.txt_ctm_duration);
        txt_ctm_duration.setText("4 HOURS (Duration)");

        RelativeLayout rel_chtm_dur = (RelativeLayout) view
                .findViewById(R.id.rel_chtm_dur);

        durationId = 1 + "";
        flag_duration = true;
        ll_change_to_free.setVisibility(View.VISIBLE);
        ll_sub_text.setVisibility(View.VISIBLE);
        clickToggle2(false);
        clickToggle1();

        ll_ctm_tg_1.setEnabled(true);
        ll_ctm_tg_2.setEnabled(true);

        rel_chtm_dur.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /** Added dummy values here */
                final CharSequence[] items = {"4 HOURS (Duration)", "1 DAY",
                        "WEEKDAYS", "INDEFINITE"};
                // ScreenUtils.showMultipleChoiceDlg(items, mActivity,
                // txt_ctm_duration, "duration", mActivity, null);

                ScreenUtils.hideKeyboard(txt_ctm_duration, mActivity);

                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Choose a " + "duration");
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        txt_ctm_duration.setText(items[which]);
                        // if (which == 0) {
                        // flag_duration = false;
                        // ll_change_to_free.setVisibility(View.GONE);
                        // ll_sub_text.setVisibility(View.GONE);
                        // clickToggle2(false);
                        // clickToggle1();
                        //
                        // ll_ctm_tg_1.setEnabled(false);
                        // ll_ctm_tg_2.setEnabled(false);
                        // durationId = "0";
                        // } else
                        {
                            // durationId = which + "";
                            durationId = which + 1 + "";
                            flag_duration = true;
                            ll_change_to_free.setVisibility(View.VISIBLE);
                            ll_sub_text.setVisibility(View.VISIBLE);
                            clickToggle2(false);
                            clickToggle1();

                            ll_ctm_tg_1.setEnabled(true);
                            ll_ctm_tg_2.setEnabled(true);
                        }

                    }
                });

                builder.show();
            }
        });

        getVehicleTagInfoList();


        tv_toll_mode_change_to_change.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    SetTime(mActivity);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        ll_change_to_free.setOnClickListener(new OnClickListener() {

            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View v) {
                Log.e("TimeZone", TimeZone.getDefault().getDisplayName());
                if ("Eastern Standard Time".equals(TimeZone.getDefault().getDisplayName())) {
                    // TODO Auto-generated method stub
                    if (Utils.isNetworkAvailable(mActivity)) {
                        new AsyncTask<TollModeData, String, String>() {

                            String apiName = "";
                            String webCall = "";
                            String getMode = txt_ctm_next.getText().toString();

                            ProgressDialog pdialog = ScreenUtils
                                    .returnProgDialogObj(
                                            mActivity,
                                            WebserviceUtils.PROGRESS_MESSAGE_UPLOADING_DETAILS,
                                            false);

                            String status = "";

                            @Override
                            protected void onPreExecute() {
                                try {
                                    pdialog.show();
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }
                            }

                            ;

                            @Override
                            protected String doInBackground(TollModeData... params) {
                                // TODO Auto-generated method stub
                                try {

                                    if (getMode.startsWith("Non-Toll")) {
                                        apiName = "saveVehicleNonTollMode";
                                        webCall = WebserviceUtils.SOAP_ACTION_SAVE_VEHICLE_NON_TOLL_MODE;
                                    } else {
                                        apiName = "removeVehicleNonTollMode";
                                        webCall = WebserviceUtils.SOAP_ACTION_REMOVEVEHICLENONTOLLMODE;
                                    }

                                    Log.e("peachpass", params[0].toString());

                                    // getVehicleId();

                                    String requestBody = WebserviceUtils
                                            .readFromAsset(apiName, mActivity);

                                    if (vehicleId == null
                                            || vehicleId.length() == 0)
                                        return "no_vehcile_id";
                                    requestBody = requestBody.replaceAll(
                                            "_vehicleId_", vehicleId);
                                    requestBody = requestBody.replaceAll(
                                            "_durationId_",
                                            params[0].getDurationId());
                                    requestBody = requestBody.replaceAll(
                                            "_effectiveTime_",
                                            params[0].getEffectiveTime());

                                    requestBody = requestBody.replaceAll(
                                            "_accountId_",
                                            Utils.getAccountId(mActivity));
                                    requestBody = requestBody.replaceAll(
                                            "_userName_",
                                            Utils.getUsername(mActivity));
                                    requestBody = requestBody.replaceAll(
                                            "_sessionId_",
                                            Utils.getSessionId(mActivity));

                                    requestBody = requestBody.replaceAll(
                                            "_osType_", WebserviceUtils.OS_TYPE);
                                    requestBody = requestBody.replaceAll(
                                            "_osVersion_", Utils.getOsVersion());
                                    requestBody = requestBody.replaceAll(
                                            "_ipAddress_",
                                            WebserviceUtils.getExternalIP());

                                    requestBody = requestBody.replaceAll("_id_",
                                            WebserviceUtils.ID);

                                    Log.e("peachpass", requestBody + "");
                                    String result = WebserviceUtils
                                            .sendSoapRequest(mActivity,
                                                    requestBody, webCall);
                                    XMLParser parser = new XMLParser();
                                    String xml = result;
                                    Document doc = parser.getDomElement(xml); // getting
                                    // DOM
                                    // element
                                    NodeList nl = doc
                                            .getElementsByTagName("result");
                                    Element e = (Element) nl.item(0);
                                    status = parser.getValue(e, "ns1:status");
                                    Log.e("peachpass", status + "****status");
                                    return status;

                                } catch (Exception e) {
                                    // TODO: handle exception

                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String result) {
                                // TODO Auto-generated method stub
                                super.onPostExecute(result);

                                // result ="0";
                                Log.e("peachpass", result + "<-----result");
                                try {
                                    pdialog.dismiss();
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }
                                if (result != null) {

                                    if (result.equals("0")) {
                                        //
                                        int status = getTimeStatus();
                                        if (status == 0) {
                                            ScreenUtils.raiseToast(mActivity, "Toll Mode changed successfully.");
                                            getVehicleTagInfoList();

                                        } else {
                                            ScreenUtils.raiseToast(mActivity,
                                                    "Toll mode has been scheduled successfully. The change should take effect according to your scheduled date and time.");
                                            Log.e("AfterChanging ", effectiveData);

                                            getVehicleTagInfoList();
                                        }


                                    } else if (result.equals("no_vehcile_id")) {
                                        ScreenUtils.raiseToast(mActivity,
                                                "Invalid vehicle id !");
                                    } else if (result.equals("1") || result.equals("101")) {
                                        ScreenUtils.raiseToast(mActivity,
                                                "A toll mode change for this vehicle is still pending. You may have to wait up to 15 minutes before making changes.");
                                        Log.e("EffectiveTime 1 status ", effectiveData);
                                        getVehicleTagInfoList();
                                    } else {
                                        String errorStatus = Utils
                                                .getStringResourceByName("status_"
                                                        + result, mActivity);
                                        ScreenUtils.raiseToast(mActivity,
                                                errorStatus);
                                        getVehicleTagInfoList();
                                    }

                                } else {
                                    ScreenUtils.raiseToast(mActivity,
                                            WebserviceUtils.SOMETHING_WENT_WRONG);
                                }
                            }

                        }.execute(new TollModeData[]{new TollModeData(vehicleId,
                                durationId, effectiveTime)});
                    } else {
                        ScreenUtils.raiseToast(mActivity,
                                WebserviceUtils.NO_INTERNET_CONNECTION);
                    }
                } else {
                    Toast.makeText(context, "You may only change your toll mode if your device is set to Eastern time", Toast.LENGTH_LONG).show();
                }
            }
        });

        Utils.setTracker(Utils.TOLLMODE_VIEW);

        return view;
    }

    private String getVehicleId() {
        try {
            String requestBody = WebserviceUtils.readFromAsset("getVehicleId",
                    mActivity);

            Log.e("peachpass", "licPlateNumber---" + licPlateNumber);

            requestBody = requestBody.replaceAll("_licPlateNumber_",
                    licPlateNumber);
            requestBody = requestBody.replaceAll("_accountId_",
                    Utils.getAccountId(mActivity));

            requestBody = requestBody.replaceAll("_userName_",
                    Utils.getUsername(mActivity));
            requestBody = requestBody.replaceAll("_sessionId_",
                    Utils.getSessionId(mActivity));

            requestBody = requestBody.replaceAll("_osType_",
                    WebserviceUtils.OS_TYPE);
            requestBody = requestBody.replaceAll("_osVersion_",
                    Utils.getOsVersion());
            requestBody = requestBody.replaceAll("_ipAddress_",
                    WebserviceUtils.getExternalIP());

            requestBody = requestBody.replaceAll("_id_", WebserviceUtils.ID);
            Log.e("peachpass", "requestBody--" + requestBody);
            String result = WebserviceUtils.sendSoapRequest(mActivity,
                    requestBody, WebserviceUtils.SOAP_ACTION_GET_VEHICLE_ID);
            Log.e("peachpass", "result---" + result);
            XMLParser parser = new XMLParser();
            String xml = result;
            Document doc = parser.getDomElement(xml); // getting DOM
            // element
            NodeList nl = doc.getElementsByTagName("result");
            Element e = (Element) nl.item(0);
            String status = parser.getValue(e, "ns1:status");
            String vehicleId = parser.getValue(e, "ns1:vehicleId");
            Log.e("peachpass", "status---" + status);
            if (status != null && status.equals("0"))
                Log.e("peachpass", "vehicleId---" + vehicleId);
            this.vehicleId = vehicleId;
            return vehicleId;
        } catch (Exception e) {
            // TODO: handle exception
            this.vehicleId = null;
            return null;
        }

    }

    protected void clickToggle1() {
        // TODO Auto-generated method stub
        String bgColor = BLACK_COLOR;
        if (flag_duration)
            bgColor = GREEN_COLOR;
        if (returnTag(ll_ctm_tg_1) == 0) {
            setBgColor(ll_ctm_tg_1, bgColor);
            ll_ctm_tg_1.setTag(1);
            setTextColor(txt_changetoll_play, WHITE_COLOR);
            setTextColor(txt_changetoll_play_1, WHITE_COLOR);
            setTextColor(txt_changetoll_play_2, WHITE_COLOR);
            ll_toll_mode_change_to_text.setVisibility(View.GONE);
            txt_chngetollmode_1.setText("Change to ");

            effectiveTime = Utils.addMinutesToDate(5, "MM/dd/yyyy HH:mm:ss");
            Log.e("peachpass", effectiveTime + "---effectiveTime");
            // Utils.getDateInThisFormat("MM/dd/yyyy HH:mm:ss");
        } else {
            setBgColor(ll_ctm_tg_1, WHITE_COLOR);
            ll_ctm_tg_1.setTag(0);
            setTextColor(txt_changetoll_play, BLACK_COLOR);
            setTextColor(txt_changetoll_play_1, BLACK_COLOR);
            setTextColor(txt_changetoll_play_2, BLACK_COLOR);
        }

        if (returnTag(ll_ctm_tg_2) == 0) {
            setBgColor(ll_ctm_tg_2, bgColor);
            ll_ctm_tg_2.setTag(1);
            setTextColor(txt_changetoll_calendr, WHITE_COLOR);
            setTextColor(txt_changetoll_calendr_1, WHITE_COLOR);
            setTextColor(txt_changetoll_calendr_2, WHITE_COLOR);

        } else {
            setBgColor(ll_ctm_tg_2, WHITE_COLOR);
            ll_ctm_tg_2.setTag(0);
            setTextColor(txt_changetoll_calendr, BLACK_COLOR);
            setTextColor(txt_changetoll_calendr_1, BLACK_COLOR);
            setTextColor(txt_changetoll_calendr_2, BLACK_COLOR);
        }
    }

    protected void clickToggle2(boolean showDialog) {
        // TODO Auto-generated method stub
        String bgColor = BLACK_COLOR;
        if (flag_duration)
            bgColor = GREEN_COLOR;

        if (returnTag(ll_ctm_tg_1) == 0) {
            setBgColor(ll_ctm_tg_1, bgColor);
            ll_ctm_tg_1.setTag(1);
            setTextColor(txt_changetoll_play, WHITE_COLOR);
            setTextColor(txt_changetoll_play_1, WHITE_COLOR);
            setTextColor(txt_changetoll_play_2, WHITE_COLOR);
        } else {
            setBgColor(ll_ctm_tg_1, WHITE_COLOR);
            ll_ctm_tg_1.setTag(0);
            setTextColor(txt_changetoll_play, BLACK_COLOR);
            setTextColor(txt_changetoll_play_1, BLACK_COLOR);
            setTextColor(txt_changetoll_play_2, BLACK_COLOR);
        }

        if (returnTag(ll_ctm_tg_2) == 0) {
            setBgColor(ll_ctm_tg_2, bgColor);
            ll_ctm_tg_2.setTag(1);
            setTextColor(txt_changetoll_calendr, WHITE_COLOR);
            setTextColor(txt_changetoll_calendr_1, WHITE_COLOR);
            setTextColor(txt_changetoll_calendr_2, WHITE_COLOR);
            if (showDialog)
                alertDialogToSelectDate();
        } else {
            setBgColor(ll_ctm_tg_2, WHITE_COLOR);
            ll_ctm_tg_2.setTag(0);
            setTextColor(txt_changetoll_calendr, BLACK_COLOR);
            setTextColor(txt_changetoll_calendr_1, BLACK_COLOR);
            setTextColor(txt_changetoll_calendr_2, BLACK_COLOR);
        }
    }

    private void alertDialogToSelectDate() {
        try {
            SetTime(mActivity);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("peachpass", e.toString());
            e.printStackTrace();
        }
    }

    private void initToggleButtons() {
        // TODO Auto-generated method stub
        // 0 - white 1 - black
        ll_ctm_tg_1 = (LinearLayout) view.findViewById(R.id.ll_ctm_tg_1);
        ll_ctm_tg_1.setTag(0);
        ll_ctm_tg_2 = (LinearLayout) view.findViewById(R.id.ll_ctm_tg_2);
        ll_ctm_tg_2.setTag(1);

        txt_changetoll_play_1 = (TextView) view
                .findViewById(R.id.txt_changetoll_play_1);
        txt_changetoll_play_2 = (TextView) view
                .findViewById(R.id.txt_changetoll_play_2);
        txt_changetoll_calendr_1 = (TextView) view
                .findViewById(R.id.txt_changetoll_calendr_1);
        txt_changetoll_calendr_2 = (TextView) view
                .findViewById(R.id.txt_changetoll_calendr_2);
        txt_changetoll_play = (TextView) view
                .findViewById(R.id.txt_changetoll_play);
        txt_changetoll_play.setTypeface(font);
        txt_changetoll_calendr = (TextView) view
                .findViewById(R.id.txt_changetoll_calendr);
        txt_changetoll_calendr.setTypeface(font);

        flag = new LinearBoolean(0);

        ll_ctm_tg_1.setEnabled(false);
        ll_ctm_tg_2.setEnabled(false);
    }

    private void initStatusOfToggleButtons() {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub

        flag.setSet(0);

        // if (flag.getSet() != 0)
        {

            if (returnTag(ll_ctm_tg_1) == 0) {
                setBgColor(ll_ctm_tg_1, BLACK_COLOR);
                ll_ctm_tg_1.setTag(1);
                setTextColor(txt_changetoll_play, WHITE_COLOR);
                setTextColor(txt_changetoll_play_1, WHITE_COLOR);
                setTextColor(txt_changetoll_play_2, WHITE_COLOR);
            } else {
                setBgColor(ll_ctm_tg_1, WHITE_COLOR);
                ll_ctm_tg_1.setTag(0);
                setTextColor(txt_changetoll_play, BLACK_COLOR);
                setTextColor(txt_changetoll_play_1, BLACK_COLOR);
                setTextColor(txt_changetoll_play_2, BLACK_COLOR);
            }

            if (returnTag(ll_ctm_tg_2) == 0) {
                setBgColor(ll_ctm_tg_2, BLACK_COLOR);
                ll_ctm_tg_2.setTag(1);
                setTextColor(txt_changetoll_calendr, WHITE_COLOR);
                setTextColor(txt_changetoll_calendr_1, WHITE_COLOR);
                setTextColor(txt_changetoll_calendr_2, WHITE_COLOR);
            } else {
                setBgColor(ll_ctm_tg_2, WHITE_COLOR);
                ll_ctm_tg_2.setTag(0);
                setTextColor(txt_changetoll_calendr, BLACK_COLOR);
                setTextColor(txt_changetoll_calendr_1, BLACK_COLOR);
                setTextColor(txt_changetoll_calendr_2, BLACK_COLOR);
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void getVehicleTagInfoList() {
        // TODO Auto-generated method stub
        if (Utils.isNetworkAvailable(mActivity))
            new AsyncTask<String, String, ArrayList<VehicleData>>() {
                ProgressDialog pdialog = ScreenUtils.returnProgDialogObj(
                        mActivity,
                        WebserviceUtils.PROGRESS_MESSAGE_FETCHING_DETAILS,
                        false);

                String status = "";

                @Override
                protected void onPreExecute() {
                    try {
                        pdialog.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }

                ;

                @Override
                protected ArrayList<VehicleData> doInBackground(
                        String... params) {
                    // TODO Auto-generated method stub
                    try {

                        ArrayList<VehicleData> vechileArrayTaginfo = getTagInfoList();
                        ArrayList<String> vechileArrayOnlyTaginfo = returnVehicleLicWithTags();
                        ArrayList<VehicleData> getNonTollList = getNonTollModeList();


                        ArrayList<VehicleData> vechileNewArray = new ArrayList<VehicleData>();

                        for (VehicleData tempData : vechileArrayTaginfo) {

                            String licPlate_new = tempData.getLicPlate_new();
                            if (vechileArrayOnlyTaginfo.contains(licPlate_new))
                                vechileNewArray.add(tempData);

                        }

                        vechileArrayTaginfo = vechileNewArray;


                        // ArrayList<VehicleData> vechileArrayNonToll =
                        // getNonTollModeList();
                        // ArrayList<VehicleData> vechileArrayToll = new
                        // ArrayList<VehicleData>();

                        // ArrayList<Integer> arrayPosArray = new
                        // ArrayList<Integer>();
                        // for (VehicleData vehTag : vechileArrayTaginfo) {
                        // Log.e("peachpass", "1");
                        // String licTag = vehTag.getLicPlate();
                        // Log.e("peachpass", "licTag----"+licTag);
                        // for (int i = 0; i < vechileArrayNonToll.size(); i++)
                        // {
                        // Log.e("peachpass", "i----"+i);
                        // VehicleData vehNonToll = vechileArrayNonToll
                        // .get(i);
                        // String licNonToll = vehNonToll.getLicPlate();
                        //
                        // Log.e("peachpass",
                        // "licNonToll----licTag"+licNonToll+"---"+licTag);
                        // if (licNonToll != null && licTag != null
                        // && licNonToll.equals(licTag)) {
                        //
                        // arrayPosArray.add(i);
                        // }
                        // }
                        // }
                        // for (Integer reqPosn : arrayPosArray) {
                        // Log.e("peachpass", "reqPosn---"+reqPosn);
                        // vechileArrayTaginfo.remove(reqPosn.intValue());
                        // }

                        // for (VehicleData vehToll : vechileArrayTaginfo) {
                        // Log.e("peachpass", "vehToll---"+vehToll);
                        // VehicleData data = new VehicleData(
                        // vehToll.getTagId(), vehToll.getTagStatus(),
                        // vehToll.getTagType(),
                        // vehToll.getLicPlate(), vehToll.getYear(),
                        // vehToll.getModel(),
                        // vehToll.getTravelStartDate(),
                        // vehToll.getTravelEndDate(), "Paid");
                        // vechileArrayToll.add(data);
                        // }

                        // vechileArrayToll.addAll(vechileArrayNonToll);

                        // return vechileArray;
                        // return vechileArrayToll;
                        return vechileArrayTaginfo;

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(ArrayList<VehicleData> result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);

                    try {
                        pdialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        linear_vehicledata.removeAllViews();

                        if (result != null && result.size() > 0) {

                            handleVisibility(new int[]{View.VISIBLE, View.GONE, View.GONE});

                            View vehicleLayout = null;
                            LayoutInflater inflater = mActivity
                                    .getLayoutInflater();

                            for (int i = 0; i < result.size(); i++) {
                                vehicleLayout = inflater.inflate(
                                        R.layout.vehicle_layout,
                                        linear_vehicledata, false);
                                final VehicleData data = result.get(i);
                                final LinearLayout linear_vehicleChild = (LinearLayout) vehicleLayout
                                        .findViewById(R.id.linear_vehicleChild);
                                linear_vehicleChild
                                        .setLayoutParams(new LinearLayout.LayoutParams(
                                                (int) getResources()
                                                        .getDimension(
                                                                R.dimen.ll_vechile_circle),
                                                (int) getResources()
                                                        .getDimension(
                                                                R.dimen.ll_vechile_circle)));
                                Log.e("peachpass", "vehicleId---" + vehicleId);
                                if (i == 0 && (vehicleId == null || vehicleId.length() == 0)) {

                                    vehicleId = data.getVehicleId_new();
                                    linear_vehicleChild
                                            .setBackgroundResource(R.drawable.shape_circle_green_color);

                                    txt_car_year_and_name.setText(data
                                            .getYear_new()
                                            + " "
                                            + data.getModel_new());
                                    txt_car_lic_plate.setText("Plate: GA - "
                                            + data.getLicPlate_new());

                                    Log.e(TAG, "Car Selected 1--" + data.getVehicleId_new());
                                    Log.e(TAG, "Car Selected 1--" + data.getTollMode_new());
                                    Log.e(TAG, "Car Selected 1--" + data.getLicPlate_new());
                                    Log.e(TAG, "Car Selected 1--" + data.getModel_new());

                                    String tollState = data.getTollMode_new() + "";
                                    //Change of terminology. AGAIN! "Paid" should say "Toll" and "Free" should say "Non-Toll"
                                    if (tollState.startsWith("Toll")) {
                                        txt_ctm_next.setText("Non-Toll ");
                                        txt_ctm_mode.setText("Toll");

//										txt_ctm_next.setText("Free");
//										txt_ctm_mode.setText("Paid");
                                    } else {
                                        txt_ctm_next.setText("Toll ");
                                        txt_ctm_mode.setText("Non-Toll");
                                    }
                                }
                                if (vehicleId != null && vehicleId.equals(data.getVehicleId_new())) {
                                    linear_vehicleChild
                                            .setBackgroundResource(R.drawable.shape_circle_green_color);

                                    txt_car_year_and_name.setText(data
                                            .getYear_new()
                                            + " "
                                            + data.getModel_new());
                                    txt_car_lic_plate.setText("Plate: GA - "
                                            + data.getLicPlate_new());

                                    Log.e(TAG, "Car Selected--" + data.getVehicleId_new());
                                    Log.e(TAG, "Car Selected--" + data.getMake_new());
                                    Log.e(TAG, "Car Selected--" + data.getTollMode_new());
                                    Log.e(TAG, "Car Selected--" + data.getLicPlate_new());
                                    Log.e(TAG, "Car Selected--" + data.getModel_new());

                                    String tollState = data.getTollMode_new() + "";
                                    //Change of terminology. AGAIN! "Paid" should say "Toll" and "Free" should say "Non-Toll"
                                    if (tollState.startsWith("Toll")) {
                                        txt_ctm_next.setText("Non-Toll ");
                                        txt_ctm_mode.setText("Toll");


                                        //								txt_ctm_next.setText("Free");
                                        //								txt_ctm_mode.setText("Paid");
                                    } else {
                                        txt_ctm_next.setText("Toll ");
                                        txt_ctm_mode.setText("Non-Toll");

                                    }

                                } else {
                                    linear_vehicleChild
                                            .setBackgroundResource(R.drawable.shape_circle_grey);
                                }
                                vehicleLayout.setTag(data);
                                TextView txt_vehicle_year = (TextView) vehicleLayout
                                        .findViewById(R.id.txt_vehicle_year);
                                TextView txt_vehicle_name = (TextView) vehicleLayout
                                        .findViewById(R.id.txt_vehicle_name);
                                TextView txt_fa_plus = (TextView) vehicleLayout
                                        .findViewById(R.id.txt_fa_plus);
                                // txt_fa_plus.setTypeface(font);
                                txt_fa_plus.setVisibility(View.GONE);
                                ImageView img_vehicle = (ImageView) vehicleLayout
                                        .findViewById(R.id.img_vehicle);
                                img_vehicle.setVisibility(View.VISIBLE);
                                txt_vehicle_year.setText(data.getYear_new());
                                txt_vehicle_name.setText(data.getModel_new());
                                linear_vehicledata.addView(linear_vehicleChild);

                                View emptyView = inflater.inflate(
                                        R.layout.blank_view,
                                        linear_vehicledata, false);
                                linear_vehicledata.addView(emptyView);

                                licPlateNumber = "(GA)"
                                        + data.getLicPlate_new();

                                Log.e(TAG, "Car Selected--" + data.getVehicleId_new());
                                Log.e(TAG, "Car Selected--" + data.getTollMode_new());
                                Log.e(TAG, "Car Selected--" + data.getLicPlate_new());
                                Log.e(TAG, "Car Selected--" + data.getModel_new());

                                linear_vehicleChild
                                        .setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                // TODO Auto-generated method
                                                // stub

                                                int childCount = linear_vehicledata
                                                        .getChildCount();
                                                for (int i = 0; i < childCount; i++) {

                                                    LinearLayout childLayout = (LinearLayout) linear_vehicledata
                                                            .getChildAt(i);
                                                    if (i != childCount - 1)
                                                        childLayout
                                                                .setBackgroundResource(R.drawable.shape_circle_grey);
                                                }
                                                linear_vehicleChild
                                                        .setBackgroundResource(R.drawable.shape_circle_green_color);

                                                vehicleId = data
                                                        .getVehicleId_new();
                                                txt_car_year_and_name.setText(data
                                                        .getYear_new()
                                                        + " "
                                                        + data.getModel_new());
                                                txt_car_lic_plate.setText("Plate: GA - "
                                                        + data.getLicPlate_new());

                                                Log.e(TAG, "Car Selected--" + vehicleId);
                                                Log.e(TAG, "Car Selected--" + data.getMake_new());
                                                Log.e(TAG, "Car Selected--" + data.getTollMode_new());
                                                Log.e(TAG, "Car Selected--" + data.getLicPlate_new());
                                                Log.e(TAG, "Car Selected--" + data.getModel_new());
                                                String tollState = data
                                                        .getTollMode_new() + "";
                                                if (tollState.startsWith("Toll")) {
                                                    txt_ctm_next.setText("Non-Toll ");
                                                    txt_ctm_mode.setText("Toll");
                                                } else {
                                                    txt_ctm_next.setText("Toll ");
                                                    txt_ctm_mode.setText("Non-Toll");
                                                }

                                            }
                                        });
                            }

                            addPlusVehicle(inflater);

                            if (result.size() == 0) {

                                handleVisibility(new int[]{View.GONE, View.GONE, View.VISIBLE});
                                ScreenUtils.raiseToast(mActivity,
                                        WebserviceUtils.NO_DATA_DISPLAY);
                            }
                        } else {

                            handleVisibility(new int[]{View.GONE, View.GONE, View.VISIBLE});
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        try {
                            handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
                        } catch (Exception e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }

                }

                private ArrayList<VehicleData> getTagInfoList()
                        throws Exception {

                    ArrayList<VehicleData> vechileArray = new ArrayList<VehicleData>();
                    String requestBody = WebserviceUtils.readFromAsset(
                            "getAccountVehicleTollMode", mActivity);
                    requestBody = requestBody.replaceAll("_accountId_",
                            Utils.getAccountId(mActivity));
                    requestBody = requestBody.replaceAll("_userName_",
                            Utils.getUsername(mActivity));
                    requestBody = requestBody.replaceAll("_sessionId_",
                            Utils.getSessionId(mActivity));

                    requestBody = requestBody.replaceAll("_osType_",
                            WebserviceUtils.OS_TYPE);
                    requestBody = requestBody.replaceAll("_osVersion_",
                            Utils.getOsVersion());
                    requestBody = requestBody.replaceAll("_ipAddress_",
                            WebserviceUtils.getExternalIP());

                    requestBody = requestBody.replaceAll("_id_",
                            WebserviceUtils.ID);

                    Log.e("peachpass", requestBody + "$$$$$$$$ ");
                    String result = WebserviceUtils
                            .sendSoapRequest(
                                    mActivity,
                                    requestBody,
                                    WebserviceUtils.SOAP_ACTION_GET_ACCOUNT_VEHICLE_TOLL_MODE);
                    XMLParser parser = new XMLParser();
                    String xml = result;
                    Document doc = parser.getDomElement(xml); // getting DOM
                    // element
                    NodeList nl = doc.getElementsByTagName("result");
                    Element e = (Element) nl.item(0);
                    status = parser.getValue(e, "ns1:status");
                    nl = doc.getElementsByTagName("ns1:vehicleTollModeList");

                    for (int i = 0; i < nl.getLength(); i++) {

                        e = (Element) nl.item(i);

                        String vehicleId_ = parser.getValue(e, "ns1:vehicleId");
                        String licPlate = parser.getValue(e, "ns1:licPlate");
                        String make = parser.getValue(e, "ns1:make");
                        String model = parser.getValue(e, "ns1:model");
                        String color = parser.getValue(e, "ns1:color");
                        String year = parser.getValue(e, "ns1:year");
                        String tollMode = parser.getValue(e, "ns1:tollMode");
                        String travelEndDate = parser.getValue(e,
                                "ns1:travelEndDate");
                        //durationId=parser.getValue(e,"ns1:durationId");


                        // String tagId = parser.getValue(e, "ns1:tagId");
                        // String tagStatus = parser.getValue(e,
                        // "ns1:tagStatus");
                        // String tagType = parser.getValue(e, "ns1:tagType");
                        // String licPlate = parser.getValue(e, "ns1:licPlate");
                        // String year = parser.getValue(e, "ns1:year");
                        // String model = parser.getValue(e, "ns1:model");
                        // String travelStartDate = parser.getValue(e,
                        // "ns1:travelStartDate");
                        // String travelEndDate = parser.getValue(e,
                        // "ns1:travelEndDate");
                        //
                        VehicleData vehicleData = new VehicleData(vehicleId_,
                                licPlate, make, model, color, year, tollMode,
                                travelEndDate);
                        vechileArray.add(vehicleData);

                        if (i == 0) {
                            //vehicleId = vehicleId_;
                            SharedPreferenceHelper.savePreferences(
                                    SharedPreferenceHelper.DUMMY_DRIVER_LIC,
                                    "(GA)" + licPlate, mActivity);
                        }

                        // Log.e("peachpass", vehicleData.toString());
                    }
                    return vechileArray;
                }


                private ArrayList<String> returnVehicleLicWithTags() {

                    ArrayList<String> vechileArray = new ArrayList<String>();
                    try {
                        String requestBody = WebserviceUtils.readFromAsset(
                                "getVehicleTagInfoList", mActivity);
                        requestBody = requestBody.replaceAll("_accountId_",
                                Utils.getAccountId(mActivity));
                        requestBody = requestBody.replaceAll("_userName_",
                                Utils.getUsername(mActivity));
                        requestBody = requestBody.replaceAll("_sessionId_",
                                Utils.getSessionId(mActivity));

                        requestBody = requestBody.replaceAll("_osType_",
                                WebserviceUtils.OS_TYPE);
                        requestBody = requestBody.replaceAll("_osVersion_",
                                Utils.getOsVersion());
                        requestBody = requestBody.replaceAll("_ipAddress_",
                                WebserviceUtils.getExternalIP());

                        requestBody = requestBody.replaceAll("_id_",
                                WebserviceUtils.ID);

                        Log.e("peachpass", requestBody + "$$$$$$$$");
                        String result = WebserviceUtils
                                .sendSoapRequest(
                                        mActivity,
                                        requestBody,
                                        WebserviceUtils.SOAP_ACTION_GETVEHICLETAGINFOLIST);
                        XMLParser parser = new XMLParser();
                        String xml = result;
                        Document doc = parser.getDomElement(xml); // getting DOM
                        // element
                        NodeList nl = doc.getElementsByTagName("result");
                        Element e = (Element) nl.item(0);
                        status = parser.getValue(e, "ns1:status");
                        nl = doc.getElementsByTagName("ns1:vehicleTagInfoList");

                        for (int i = 0; i < nl.getLength(); i++) {

                            e = (Element) nl.item(i);
//							String tagId = parser.getValue(e,
//									"ns1:tagId");
                            String licPlate = parser.getValue(e,
                                    "ns1:licPlate");

                            vechileArray.add(licPlate);
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    return vechileArray;

                }

                private ArrayList<VehicleData> getNonTollModeList()
                        throws Exception {

                    ArrayList<VehicleData> vechileArray = new
                            ArrayList<VehicleData>();
                    String requestBody = WebserviceUtils.readFromAsset(
                            "getVehicleNonTollModeList", mActivity);
                    requestBody = requestBody.replaceAll("_accountId_",
                            Utils.getAccountId(mActivity));
                    requestBody = requestBody.replaceAll("_userName_",
                            Utils.getUsername(mActivity));
                    requestBody = requestBody.replaceAll("_sessionId_",
                            Utils.getSessionId(mActivity));

                    requestBody = requestBody.replaceAll("_osType_",
                            WebserviceUtils.OS_TYPE);
                    requestBody = requestBody.replaceAll("_osVersion_",
                            Utils.getOsVersion());
                    requestBody = requestBody.replaceAll("_ipAddress_",
                            WebserviceUtils.getExternalIP());

                    requestBody = requestBody.replaceAll("_id_",
                            WebserviceUtils.ID);

                    Log.e("peachpassNonToll", requestBody + "");
                    String result = WebserviceUtils
                            .sendSoapRequest(
                                    mActivity,
                                    requestBody,
                                    WebserviceUtils.SOAP_ACTION_GETVEHICLENONTOLLMODELIST);
                    XMLParser parser = new XMLParser();
                    String xml = result;
                    Document doc = parser.getDomElement(xml); // getting DOM
                    // element
                    NodeList nl = doc.getElementsByTagName("result");
                    Element e = (Element) nl.item(0);
                    status = parser.getValue(e, "ns1:status");
                    nl = doc.getElementsByTagName("ns1:details");

                    Log.e(TAG, "Response Body-- " + result);


                    for (int i = 0; i < nl.getLength(); i++) {

                        e = (Element) nl.item(i);

                        Log.e("Values$$$", "*****");

                        /*
                         * <ns1:vehicleColor>RED</ns1:vehicleColor>
                         * <ns1:vehicleMake>A4</ns1:vehicleMake>
                         * <ns1:tollMode>Non Toll (09/16/2015
                         * 07:40:14)</ns1:tollMode>
                         * <ns1:effectiveDate>09/16/2015
                         * 07:40:14</ns1:effectiveDate>
                         * <ns1:durationId>2</ns1:durationId>
                         * <ns1:expiredDate>09/17/2015
                         * 07:40:14</ns1:expiredDate>
                         * <ns1:vehicleId>4698077</ns1:vehicleId>
                         * <ns1:vehicleModel>ACCORD</ns1:vehicleModel>
                         * <ns1:licPlateTypeCode>PAS</ns1:licPlateTypeCode>
                         */

                        String tagId = "";
                        String tagStatus = "";
                        String tagType = "";
                        String licPlate = parser.getValue(e,
                                "ns1:licPlateNumber");
                        String year = parser.getValue(e, "ns1:vehicleYear");
                        String model = parser.getValue(e, "ns1:vehicleModel");
                        String travelStartDate = "";
                        String travelEndDate = "";
                        vehicleId = parser.getValue(e, "ns1:vehicleId");

                        // effectiveTime="";
                        effectiveTime = parser.getValue(e, "ns1:effectiveDate");
                        durationId = "";
                        durationId = parser.getValue(e, "ns1:durationId");
                        effectiveData1 = parser.getValue(e, "ns1:effectiveDate");

                        VehicleData vehicleData = new VehicleData(tagId,
                                tagStatus, tagType, licPlate, year, model,
                                travelStartDate, travelEndDate);
                        vechileArray.add(vehicleData);

                        Log.e(TAG, "Nontollist effectiveDate-- " + effectiveTime);
                        Log.e(TAG, "Nontollist licPlate-- " + licPlate);
                        Log.e(TAG, "Nontollist model-- " + model);
                        Log.e(TAG, "Nontollist vehicleId-- " + vehicleId);
                        Log.e(TAG, "Nontollist DurationId-- " + durationId);

                    }
                    return vechileArray;
                }

                private void addPlusVehicle(LayoutInflater inflater) {
                    // TODO Auto-generated method stub
                    View vehicleLayout = inflater.inflate(
                            R.layout.vehicle_layout, linear_vehicledata, false);
                    LinearLayout linear_vehicleChild = (LinearLayout) vehicleLayout
                            .findViewById(R.id.linear_vehicleChild);
                    linear_vehicleChild
                            .setLayoutParams(new LinearLayout.LayoutParams(
                                    (int) getResources().getDimension(
                                            R.dimen.ll_vechile_circle),
                                    (int) getResources().getDimension(
                                            R.dimen.ll_vechile_circle)));
                    linear_vehicleChild
                            .setBackgroundResource(R.drawable.shape_circle_add_vehicle);
                    TextView txt_vehicle_year = (TextView) vehicleLayout
                            .findViewById(R.id.txt_vehicle_year);
                    TextView txt_vehicle_name = (TextView) vehicleLayout
                            .findViewById(R.id.txt_vehicle_name);
                    TextView txt_fa_plus = (TextView) vehicleLayout
                            .findViewById(R.id.txt_fa_plus);
                    txt_fa_plus.setTypeface(font);
                    txt_fa_plus.setVisibility(View.VISIBLE);
                    ImageView img_vehicle = (ImageView) vehicleLayout
                            .findViewById(R.id.img_vehicle);
                    img_vehicle.setVisibility(View.GONE);
                    txt_vehicle_year.setText("Add");
                    txt_vehicle_name.setText("Vehicle");
                    txt_vehicle_year.setTextColor(mActivity.getResources()
                            .getColor(R.color.black));
                    txt_vehicle_name.setTextColor(mActivity.getResources()
                            .getColor(R.color.black));
                    linear_vehicledata.addView(linear_vehicleChild);

                    linear_vehicleChild
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    startActivityForResult(
                                            new Intent(mActivity,
                                                    AddVehicleActivity.class),
                                            MyProfileFragment.VEHICLE_ADD_SUCCESS);
                                }
                            });

                }
            }.execute();
        else {

            handleVisibility(new int[]{View.GONE, View.VISIBLE, View.GONE});
        }
    }


    int returnTag(View v) {
        return (Integer) v.getTag();
    }

    void setBgColor(LinearLayout ll, String color) {
        ll.setBackgroundColor(Color.parseColor(color));
    }

    void setTextColor(TextView ll, String color) {
        ll.setTextColor(Color.parseColor(color));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", mColorRes);
    }

    class LinearBoolean {

        int set;

        public int getSet() {
            return set;
        }

        public void setSet(int set) {
            this.set = set;
        }

        public LinearBoolean(int set) {
            super();
            this.set = set;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

        setRetainInstance(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MyProfileFragment.VEHICLE_ADD_SUCCESS
                && resultCode == Activity.RESULT_OK) {

            getVehicleTagInfoList();
        }
    }

    private String getDate(Date dateObj, String format) {

        Date date3 = dateObj;
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.ENGLISH);

        return df.format(date3);
        // System.out.println(df.format(date3)+" at "+hh+":"+mm+ " "+amPm);
    }

    private void SetTime(Activity activity) {
        String[] ampmArray = new String[]{"am", "pm"};

        // With a custom method I get the next following 30 days from now
        @SuppressWarnings("unchecked")
        ArrayList<Date> days = getNextNumberOfDays(new Date(), 30);

        // Create a custom dialog with the dialog_date.xml file
        final Dialog dialogSearchByDate = new Dialog(mActivity);
        dialogSearchByDate.setContentView(R.layout.date_time_dialog);
        dialogSearchByDate.setTitle("Select a Date");

        // Configure Days Column
        final WheelView day = (WheelView) dialogSearchByDate
                .findViewById(R.id.day);
        final DayWheelAdapter dayAdapter = new DayWheelAdapter(activity, days);
        day.setViewAdapter(dayAdapter);
        day.getCurrentItem();

        // Configure Hours Column
        final WheelView hour = (WheelView) dialogSearchByDate
                .findViewById(R.id.hour);
        final NumericWheelAdapter hourAdapter = new NumericWheelAdapter(
                activity, 1, 12, "%02d");
        hourAdapter.setItemResource(R.layout.wheel_item_time);
        hourAdapter.setItemTextResource(R.id.time_item);
        hour.setViewAdapter(hourAdapter);

        // Configure Minutes Column
        final WheelView min = (WheelView) dialogSearchByDate
                .findViewById(R.id.minute);
        final NumericWheelAdapter minAdapter = new NumericWheelAdapter(
                activity, 0, 59, "%02d");
        minAdapter.setItemResource(R.layout.wheel_item_time);
        minAdapter.setItemTextResource(R.id.time_item);
        min.setViewAdapter(minAdapter);

        // Configure am/pm Marker Column
        final WheelView ampm = (WheelView) dialogSearchByDate
                .findViewById(R.id.ampm);
        final ArrayWheelAdapter<String> ampmAdapter = new ArrayWheelAdapter<String>(
                mActivity, ampmArray);
        ampmAdapter.setItemResource(R.layout.wheel_item_time);
        ampmAdapter.setItemTextResource(R.id.time_item);
        ampm.setViewAdapter(ampmAdapter);
        // dialogSearchByDate.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Button set = (Button) dialogSearchByDate.findViewById(R.id.set);
        Button cancel = (Button) dialogSearchByDate.findViewById(R.id.cancel);
        set.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String hh = hourAdapter.getItemText(hour.getCurrentItem()) + "";
                String mm = minAdapter.getItemText(min.getCurrentItem()) + "";
                String amPm = ampmAdapter.getItemText(ampm.getCurrentItem()) + "";

                int dayPosition = day.getCurrentItem();
                Log.e("peachpass", "dayPosition---" + dayPosition);
                if (dayPosition == 0) {
                    String currentTime = Utils.getDateInThisFormat("hh:mm:a");
                    String timeSplit[] = currentTime.split("\\:");
                    String curHour = timeSplit[0];
                    String curMin = timeSplit[1];
                    String curAmPm = timeSplit[2];

                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:a", Locale.ENGLISH);
                        Date currentDate = sdf.parse(curHour + ":" + curMin + ":" + curAmPm);
                        Date userDate = sdf.parse(hh + ":" + mm + ":" + amPm);

                        if (currentDate.compareTo(userDate) > 0) {
                            System.out.println("currentDate is greater than userDate");
                            ScreenUtils.raiseToast(mActivity, "Selected time must be greater than current time");
                        } else if (currentDate.compareTo(userDate) < 0) {
                            System.out.println("currentDate is less than userDate");
                            dialogSearchByDate.dismiss();
                        } else if (currentDate.compareTo(userDate) == 0) {
                            System.out.println("currentDate is equal to userDate");
                            dialogSearchByDate.dismiss();
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }

                } else {

                    dialogSearchByDate.dismiss();
                }


                // Log.e("peachpass",
                // dayAdapter.getItemAtIndex(day.getCurrentItem())+"--"+hour.getCurrentItem()+"---"+min.getCurrentItem()+"---"+ampm.getCurrentItem());
                Log.e("peachpass",
                        dayAdapter.getItemAtIndex(day.getCurrentItem()) + "--"
                                + hh + "---" + mm + "---" + amPm);
                // MM dd yyyy
                Date date3 = dayAdapter.getItemAtIndex(day.getCurrentItem());
                // SimpleDateFormat df = new
                // SimpleDateFormat("MMM dd",Locale.ENGLISH);
                String outputs = getDate(date3, "MMM dd");
                System.out.println(outputs + " at " + hh + ":" + mm + " "
                        + amPm);

                String getMode = txt_ctm_next.getText().toString();
                if (getMode.startsWith("Non-Toll"))
                    tv_toll_mode_change_to_text.setText("Toll mode change to Non-Toll "
                            + outputs + " at " + hh + ":" + mm + " " + amPm);
                else
                    tv_toll_mode_change_to_text.setText("Non-Toll mode change to Toll "
                            + outputs + " at " + hh + ":" + mm + " " + amPm);

                Log.e("peachpass", Utils.parseDate(getDate(date3, "MM/dd/yyyy")
                                + " " + hh + ":" + mm + " " + amPm,
                        "MM/dd/yyyy hh:mm aa", "MM/dd/yyyy HH:mm:ss"));

                effectiveTime = Utils.parseDate(getDate(date3, "MM/dd/yyyy")
                                + " " + hh + ":" + mm + " " + amPm,
                        "MM/dd/yyyy hh:mm aa", "MM/dd/yyyy HH:mm:ss");
                ll_toll_mode_change_to_text.setVisibility(View.VISIBLE);
                txt_chngetollmode_1.setText("Schedule change to ");
                // String inputType = "yyyy/MM/dd";
                // String outputType = "MMM dd";
                // String output = Utils.parseDate(inputDate, inputType,
                // outputType);
            }
        });
        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialogSearchByDate.dismiss();

                /*reset to startnow*/
                flag.setSet(0);
                ll_toll_mode_change_to_text.setVisibility(View.GONE);
                isScheduleLaterDateSet = false;
                clickToggle1();
            }
        });
        dialogSearchByDate.show();
    }

    ArrayList getNextNumberOfDays(Date originalDate, int days) {
        ArrayList dates = new ArrayList();
        long offset;
        for (int i = 0; i <= days; i++) {
            offset = 86400 * 1000L * i;
            Date date = new Date(originalDate.getTime() + offset);
            dates.add(date);
        }
        return dates;
    }

    private ArrayWheelAdapter<String> returnArrayAdapter(String[] array) {

        return new ArrayWheelAdapter<String>(mActivity, array);
    }

//	private void showDateTimeDialog() throws Exception {
//		// Create the dialog
//		final Dialog mDateTimeDialog = new Dialog(mActivity);
//		// Inflate the root layout
//		final LinearLayout mDateTimeDialogView = (LinearLayout) mActivity
//				.getLayoutInflater().inflate(R.layout.date_time_dialog, null);
//		// // Grab widget instance
//		// final DateTimePicker mDateTimePicker = (DateTimePicker)
//		// mDateTimeDialogView
//		// .findViewById(R.id.DateTimePicker);
//		// // Check is system is set to use 24h time (this doesn't seem to work
//		// as
//		// // expected though)
//		// final String timeS = android.provider.Settings.System.getString(
//		// mActivity.getContentResolver(),
//		// android.provider.Settings.System.TIME_12_24);
//		// final boolean is24h = !(timeS == null || timeS.equals("12"));
//		//
//		// // Update demo TextViews when the "OK" button is clicked
//		// ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime))
//		// .setOnClickListener(new OnClickListener() {
//		//
//		// public void onClick(View v) {
//		// mDateTimePicker.clearFocus();
//		// // TODO Auto-generated method stub
//		// // ((TextView) findViewById(R.id.Date)).setText();
//		// Log.e("peachpass",
//		// mDateTimePicker.get(Calendar.YEAR)
//		// + "/"
//		// + (mDateTimePicker.get(Calendar.MONTH) + 1)
//		// + "/"
//		// + mDateTimePicker
//		// .get(Calendar.DAY_OF_MONTH));
//		//
//		// if (mDateTimePicker.is24HourView()) {
//		// // ((TextView) findViewById(R.id.Time)).setText();
//		// Log.e("peachpass",
//		// mDateTimePicker.get(Calendar.HOUR_OF_DAY)
//		// + ":"
//		// + mDateTimePicker
//		// .get(Calendar.MINUTE));
//		// } else {
//		// // ((TextView) findViewById(R.id.Time)).setText();
//		// Log.e("peachpass",
//		// mDateTimePicker.get(Calendar.HOUR)
//		// + ":"
//		// + mDateTimePicker
//		// .get(Calendar.MINUTE)
//		// + " "
//		// + (mDateTimePicker
//		// .get(Calendar.AM_PM) == Calendar.AM ? "AM"
//		// : "PM"));
//		// }
//		//
//		// ll_toll_mode_change_to_text.setVisibility(View.VISIBLE);
//		// String inputDate = mDateTimePicker.get(Calendar.YEAR)
//		// + "/"
//		// + (mDateTimePicker.get(Calendar.MONTH) + 1)
//		// + "/"
//		// + mDateTimePicker.get(Calendar.DAY_OF_MONTH);
//		// String inputType = "yyyy/MM/dd";
//		// String outputType = "MMM dd";
//		// String output = Utils.parseDate(inputDate, inputType,
//		// outputType);
//		//
//		// try {
//		// if (Utils.ifDatePassed(inputDate)) {
//		//
//		// ScreenUtils.raiseToast(mActivity,
//		// "Already passed date selected !");
//		// } else {
//		// String time = mDateTimePicker
//		// .get(Calendar.HOUR)
//		// + ":"
//		// + mDateTimePicker.get(Calendar.MINUTE)
//		// + " "
//		// + (mDateTimePicker.get(Calendar.AM_PM) == Calendar.AM ? "AM"
//		// : "PM");
//		// tv_toll_mode_change_to_text
//		// .setText("Toll mode change to Free "
//		// + output + " at " + time);
//		//
//		// output = Utils.parseDate(
//		// inputDate + " " + time,
//		// "yyyy/MM/dd hh:mm a",
//		// "MM/dd/yyyy HH:mm:ss");
//		// Log.e("peachpass",
//		// inputDate
//		// + " "
//		// + time
//		// + "---yyyy/MM/dd hh:mm a to MM/dd/yyyy HH:mm:ss---output--"
//		// + output);
//		// effectiveTime = output;
//		// }
//		// } catch (Exception e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//		//
//		// mDateTimeDialog.dismiss();
//		// }
//		// });
//		//
//		// // Cancel the dialog when the "Cancel" button is clicked
//		// ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog))
//		// .setOnClickListener(new OnClickListener() {
//		//
//		// public void onClick(View v) {
//		// // TODO Auto-generated method stub
//		// mDateTimeDialog.cancel();
//		// }
//		// });
//		//
//		// // Reset Date and Time pickers when the "Reset" button is clicked
//		// ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime))
//		// .setOnClickListener(new OnClickListener() {
//		//
//		// public void onClick(View v) {
//		// // TODO Auto-generated method stub
//		// mDateTimePicker.reset();
//		// }
//		// });
//		//
//		// // Setup TimePicker
//		// mDateTimePicker.setIs24HourView(is24h);
//		// No title on the dialog window
//		mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		// Set the dialog content view
//		mDateTimeDialog.setContentView(mDateTimeDialogView);
//		// Display the dialog
//		mDateTimeDialog.show();
//	}

    /**
     * List, No internet and No data
     */
    void handleVisibility(int[] values) {
        ll_actual_screen.setVisibility(values[0]);
        ll_no_internet.setVisibility(values[1]);
        ll_no_data.setVisibility(values[2]);
    }

    int getTimeStatus() {
        if (ll_toll_mode_change_to_text.getVisibility() == View.VISIBLE) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(context).reportActivityStart(mActivity);

        FlurryAgent.onStartSession(context);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.logEvent(Utils.TOLLMODE_VIEW);

    }


    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GoogleAnalytics.getInstance(context).reportActivityStop(mActivity);

        FlurryAgent.onEndSession(context);
    }

}
