package com.peachpass.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.peachpass.data.AccountDetailData;
import com.peachpass.utils.Utils;
import com.srta.PeachPass.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class StatementListAdapter extends BaseAdapter {
	List<AccountDetailData> itemList = new ArrayList<AccountDetailData>();
	Context context;

	public StatementListAdapter(Context c) {
		context = c;
	}

	@Override
	public int getCount() {
		return itemList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View arg1, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.list_item_transaction,
				parent, false);
		TextView txt_mpp_month = (TextView) itemView
				.findViewById(R.id.txt_mpp_month);
		TextView txt_mpp_date = (TextView) itemView
				.findViewById(R.id.txt_mpp_date);
		TextView txt_mpp_line1 = (TextView) itemView
				.findViewById(R.id.txt_mpp_line1);
		TextView txt_mpp_line2 = (TextView) itemView
				.findViewById(R.id.txt_mpp_line2);
		TextView txt_mpp_currency = (TextView) itemView
				.findViewById(R.id.txt_mpp_currency);

		try {
			Calendar transactionDate = Utils.getDate(itemList.get(position).getTransactionDate());
			if(transactionDate!=null){
				Log.e("peachpass", "---"+transactionDate.toString());
				String month = transactionDate.getDisplayName(Calendar.MONTH,Calendar.SHORT, Locale.ENGLISH);
				String date =  transactionDate.getDisplayName(Calendar.DAY_OF_MONTH,Calendar.LONG, Locale.ENGLISH);
				txt_mpp_month.setText(month);
				txt_mpp_date.setText(transactionDate.get(Calendar.DAY_OF_MONTH)+"");
				txt_mpp_line1.setText(itemList.get(position).getLocation());
				txt_mpp_line2.setText(itemList.get(position).getTransTypeDescr());
				txt_mpp_currency.setText("- $"+itemList.get(position).getAmount());	
			}else{
				txt_mpp_line1.setText(itemList.get(position).getLocation());
				txt_mpp_line2.setText(itemList.get(position).getTransTypeDescr());
				txt_mpp_currency.setText("- $"+itemList.get(position).getAmount());	
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("peachpass", e.toString());
		}
		
		return itemView;
	}

	public void addItem(AccountDetailData item) {
		itemList.add(item);
		notifyDataSetChanged();
	}

	public void clearItems() {
		itemList.clear();
		notifyDataSetChanged();
	}
}
