package com.peachpass.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.peachpass.data.MyPeachPassData;
import com.srta.PeachPass.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ItemListAdapter extends BaseAdapter {
    ArrayList<MyPeachPassData> itemList = new ArrayList<MyPeachPassData>();
    Context context;

    public ItemListAdapter(Context c) {
        context = c;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    public void insertList(List<MyPeachPassData> itemList) {
        this.itemList.addAll(itemList);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View arg1, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.list_item_transaction,
                parent, false);
        TextView txt_mpp_month = (TextView) itemView
                .findViewById(R.id.txt_mpp_month);
        TextView txt_mpp_date = (TextView) itemView
                .findViewById(R.id.txt_mpp_date);
        TextView txt_mpp_line1 = (TextView) itemView
                .findViewById(R.id.txt_mpp_line1);
        TextView txt_mpp_line2 = (TextView) itemView
                .findViewById(R.id.txt_mpp_line2);
        TextView txt_mpp_currency = (TextView) itemView
                .findViewById(R.id.txt_mpp_currency);
        try {
            if (itemList.get(position).getMonth() != null && itemList.get(position).getMonth().length()>0) {
                txt_mpp_month.setText(itemList.get(position).getMonth());
            }else{
                txt_mpp_month.setText("");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        txt_mpp_month.setText(itemList.get(position).getMonth());
        txt_mpp_date.setText(itemList.get(position).getDay());
        txt_mpp_line1.setText(itemList.get(position).getI85() + " " + itemList.get(position).getDirection() + " - " + itemList.get(position).getLicPlateNo());
        txt_mpp_line2.setText(itemList.get(position).getLane() + " | " + itemList.get(position).getTime());

        try {
            boolean isPositive = false;
            String currency = itemList.get(position).getCurrency();
            if (currency.contains("-"))
                isPositive = false;
            else isPositive = true;
            currency = currency.replaceAll("\\-", "");
            currency = currency.replaceAll("\\$", "");
            if (currency.contains(".")) {

            } else {
                currency = currency + ".";
            }
            currency = currency + "0000";
            Log.e("peachpass", currency + "----");
            DecimalFormat df = new DecimalFormat("#0.00");
            currency = "$" + df.format(Double.parseDouble(currency));
            //currency = "$"+Double.valueOf(df.format(Double.parseDouble(currency)));

            //Double newVal = new BigDecimal(currency).setScale(2,RoundingMode.HALF_UP).doubleValue();
            //currency = "$"+newVal;
            if (!isPositive)
                currency = " - " + currency;
            // - $2.00
            Log.e("peachpass", currency + "!!!!!");
//			if(currency.contains("\\$\\."))
//				currency = currency.replaceAll("\\$\\.", "\\$0\\.");
            txt_mpp_currency.setText(currency);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


        return itemView;
    }

    public void addItem(MyPeachPassData item) {
        itemList.add(item);
        notifyDataSetChanged();
    }

    public void clearItems() {
        itemList.clear();
        notifyDataSetChanged();
    }
}
