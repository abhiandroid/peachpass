package com.peachpass.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.peachpass.fragments.WalkthroughFragment;
import com.peachpass.fragments.WalkthroughFragment2;
import com.peachpass.fragments.WalkthroughFragment3;
import com.peachpass.fragments.WalkthroughFragment4;
import com.peachpass.fragments.WalkthroughFragment5;
import com.srta.PeachPass.R;

public class TestFragmentAdapter extends FragmentPagerAdapter {
	private int[] offerImages = { R.drawable.temp_coming_soon };

	// private int mCount = offerImages.length;
	private int mCount = 5;

	public TestFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		
		switch (position) {
		case 0:
			return new WalkthroughFragment(offerImages[0]);
		case 1:
			return new WalkthroughFragment2(offerImages[0]);
		case 2:
			return new WalkthroughFragment3(offerImages[0]);
		case 3:
			return new WalkthroughFragment4(offerImages[0]);
		case 4:
			return new WalkthroughFragment5(offerImages[0]);

		default:
			return new WalkthroughFragment(offerImages[0]);
		}

	}

	@Override
	public int getCount() {
		return mCount;
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}
