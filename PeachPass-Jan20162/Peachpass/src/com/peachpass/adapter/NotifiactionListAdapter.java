package com.peachpass.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.peachpass.data.NotificationData;
import com.peachpass.utils.ScreenUtils;
import com.srta.PeachPass.R;

import java.util.ArrayList;
import java.util.List;

public class NotifiactionListAdapter extends BaseAdapter {
	List<NotificationData> itemList = new ArrayList<NotificationData>();
	Context context;

	public NotifiactionListAdapter(Context c) {
		context = c;
	}

	@Override
	public int getCount() {
		return itemList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View arg1, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.list_item_notifications,
				parent, false);
		TextView txt_not_chaticon = (TextView) itemView
				.findViewById(R.id.txt_not_chaticon);

		txt_not_chaticon.setTypeface(ScreenUtils.returnTypeFace(context));
		TextView txt_notf_text = (TextView) itemView
				.findViewById(R.id.txt_notf_text);
		TextView txt_notf_date = (TextView) itemView
				.findViewById(R.id.txt_notf_date);

		txt_notf_text.setText(itemList.get(position).getText());
		txt_notf_date.setText(itemList.get(position).getDate());

		if (itemList.get(position).isRead()) {
			txt_not_chaticon.setTextColor(context.getResources().getColor(
					R.color.orange));
			txt_notf_text.setTextColor(context.getResources().getColor(
					R.color.white));
		} else {
			txt_notf_text.setTextColor(context.getResources().getColor(
					R.color.grey));
			txt_not_chaticon.setTextColor(context.getResources().getColor(
					R.color.grey_text));
		}

		return itemView;
	}

	public void addItem(NotificationData item) {
		itemList.add(item);
		notifyDataSetChanged();
	}

	public void clearItems() {
		itemList.clear();
		notifyDataSetChanged();
	}
}
