package com.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

public class SharedPreferenceHelper {

    public static final String SESSION = "session";


    public static final String SESSION_ID = "sessionId";
    public static final String ACTIVATION_DATE_TIME = "activationDateTime";
    public static final String ACCOUNT_ID = "accountId";
    public static final String USERNAME = "userName";
    public static final String ACCOUNTBALANCE = "accountBalance";
    public static final String PRIMARY_EMAIL_ID = "primaryEmailAddress";
    public static final String TAG_ID = "tagId";

    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String COUNTY = "county";
    public static final String PHONE = "phone";
    public static final String DRIVER_LISENCE = "driver_licence";
    public static final String DRIVER_LISENCE_STATE = "driver_licence_state";
    public static final String DUMMY_DRIVER_LIC = "dummy_driver_lic";

    public static final String EDIT_NOTIFICATION_EMAIL = "edt_notifications_email";
    public static final String EDIT_NOTIFICATION_PUSH = "edt_notifications_push";

    public static final String NOTIFICATIONS = "notifications";
    public static final String UNREADNOTIFICATIONS = "unreadnotifications";

    public static final String ALERTS = "alerts";
    public static final String UNREADALERTS = "unreadalerts";
    public static final String COUNTYVALUE = "";


    public static final String PASSWORD = "password";
    public static final String REMEMBERME = "rememberme";
    public static final String CountyValue = "";


    public static final boolean MONKEYSURVEY = true; // true if suvery is online otherwise false
    public static final String MONKEYSURVEYPOPUP = "monkeysurveypopup"; // for user action on pop up

    public static int MONKEYSURVEYVALUE = 1; //save user action on pop up
    public static final String APPOPENINGSTATUS = "appopeningstatus"; // App status for second time pop up

    public static final String MONKEYSURVEYFILL = "false";

    public static void savePreferences(String key, String value, Context ctx) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreference(Context ctx, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        return sharedPreferences.getString(key, "");
    }

    public static void savePreferencesSet(String key, Set value, Context ctx) {
        // Log.e("pavestone", SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.NOTIFICATIONS, ctx).toString()+"--before");

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();

        Set<String> set = new HashSet<String>();
        set.addAll(value);
        editor.putStringSet(key, set);
        editor.commit();
        // Log.e("pavestone", SharedPreferenceHelper.getPreferenceSet(SharedPreferenceHelper.NOTIFICATIONS, ctx).toString() + "--after");
    }

    public static Set getPreferenceSet(String key, Context ctx) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);

        return sharedPreferences.getStringSet(key, new HashSet<String>());
    }

    public static void saveIntPreferences(String key, int value, Context ctx) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getIntPreference(Context ctx, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        return sharedPreferences.getInt(key, 0);
    }
}
